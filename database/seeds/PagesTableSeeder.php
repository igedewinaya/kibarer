<?php

use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        \App\Page::create([
            'user_id' => 1,
            'route' => 'home',
            'status' => 1
        ]);

        \App\Page::create([
            'user_id' => 1,
            'route' => 'about',
            'status' => 1
        ]);

        \App\Page::create([
            'user_id' => 1,
            'route' => 'contact',
            'status' => 1
        ]);
        
    }
}
