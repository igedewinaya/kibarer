<?php

use Illuminate\Database\Seeder;

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        \App\Customer::create([

            'username' => 'gusman',
            'email' => 'gusman@kesato.com',
            'password' => '$2y$10$PMXrmUKV/4znb9rTJXGpSuHnzagpRggTWab/zvogSGHMAMRKPq9.e',
            'remember_token' => str_random(10),
            'firstname' => 'gusman',
            'lastname' => 'widodo',
            'address' => 'jalan plawa 8',
            'phone' => '0361',
            'city' => 'denpasar',
            'active' => 1

        ]);
    }
}
