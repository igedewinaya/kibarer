<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        \App\Category::create([
            'name' => 'Villas for Sale',
            'slug' => 'villas-for-sale',
            'route' => 'villa',
            'type' => 'property',
            'order' => 0,
            'parent_id' => 0
        ]);

        \App\Category::create([
            'name' => 'Villas for Rent',
            'slug' => 'villas-for-rent',
            'route' => 'villa_rental',
            'type' => 'property',
            'order' => 0,
            'parent_id' => 0
        ]);

        \App\Category::create([
            'name' => 'land',
            'slug' => 'land',
            'route' => 'land',
            'type' => 'property',
            'order' => 1,
            'parent_id' => 0
        ]);

        \App\Category::create([
            'name' => 'uncategorized',
            'slug' => 'uncategorized',
            'route' => 'uncategorized',
            'type' => 'post',
            'order' => 0,
            'parent_id' => 0
        ]);

        \App\Category::create([
            'name' => 'investissement villa Bali',
            'slug' => 'investissement-villa-bali',
            'route' => 'investissement_villa_bali',
            'type' => 'post',
            'order' => 0,
            'parent_id' => 0
        ]);

        \App\Category::create([
            'name' => 'Tourist Destination',
            'slug' => 'tourist-destination',
            'route' => 'tourist_destination',
            'type' => 'post',
            'order' => 0,
            'parent_id' => 0
        ]);
    }
}
