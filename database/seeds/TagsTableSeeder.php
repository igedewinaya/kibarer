<?php

use Illuminate\Database\Seeder;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        \App\Tag::create([
            'name' => 'beachfront',
            'slug' => 'beachfront',
            'route' => 'beachfront_property',
            'type' => 'property',
            'order' => 0,
            'parent_id' => 0
        ]);

        \App\Tag::create([
            'name' => 'home and retirement',
            'slug' => 'home-and-retirement',
            'route' => 'home_and_retirement',
            'type' => 'property',
            'order' => 0,
            'parent_id' => 0
        ]);

        \App\Tag::create([
            'name' => '< $500.000',
            'slug' => 'less-than-500k',
            'route' => 'less_than_500k',
            'type' => 'property',
            'parent_id' => 0
        ]);

        \App\Tag::create([
            'name' => '> $500.000',
            'slug' => 'more-than-500k',
            'route' => 'less_than_500k',
            'type' => 'property',
            'order' => 0,
            'parent_id' => 0
        ]);

    }
}
