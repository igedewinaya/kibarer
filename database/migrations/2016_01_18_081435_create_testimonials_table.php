<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestimonialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('testimonials', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('property_id')->default(0);
            $table->integer('user_id');
            $table->string('customer_name');
            $table->string('title');
            $table->text('content');
            $table->string('slug');
            $table->boolean('status')->default(0);
            $table->timeStamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('testimonials');
    }
}
