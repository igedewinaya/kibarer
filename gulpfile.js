var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.less('admin.less', 'public/assets/css/admin.css')

        .less('website/main.less', 'public/assets/css/main.css')

        .less('website/responsive.less', 'public/assets/css/responsive.css')

        // .less('website/form.less', 'public/assets/css/form.css')

        .scripts('website/form/select2.js', 'public/assets/js/form/select2.js')

        .scripts(
            'website/form/additional-methods.min.js', 'public/assets/js/form/additional-methods.min.js')

        .scripts(
            'website/form/jquery-ui-1.10.4.custom.min.js', 'public/assets/js/form/jquery-ui-1.10.4.custom.min.js')

        .scripts(
            'website/form/jquery-ui-monthpicker.min.js', 'public/assets/js/form/jquery-ui-monthpicker.min.js')

        .scripts(
            'website/form/jquery-ui-timepicker.min.js', 'public/assets/js/form/jquery-ui-timepicker.min.js')

        .scripts(
            'website/form/jquery-ui-touch-punch.min.js', 'public/assets/js/form/jquery-ui-touch-punch.min.js')

        .scripts(
            'website/form/jquery.form.min.js', 'public/assets/js/form/jquery.form.min.js')

        .scripts(
            'website/form/jquery.maskedinput.js', 'public/assets/js/form/jquery.maskedinput.js')

        .scripts(
            'website/form/jquery.placeholder.min.js', 'public/assets/js/form/jquery.placeholder.min.js')

        .scripts(
            'website/form/jquery.spectrum.min.js', 'public/assets/js/form/jquery.spectrum.min.js')

        .scripts(
            'website/form/jquery.stepper.min.js', 'public/assets/js/form/jquery.stepper.min.js')

        .scripts(
            'website/form/jquery.validate.min.js', 'public/assets/js/form/jquery.validate.min.js')

        .scripts(
            'website/kibarer.js', 'public/assets/js/scripts.js')


        .scripts(
            'website/hammer.min.js', 'public/assets/js/hammer.min.js')

        .scripts(
            'website/wNumb.js', 'public/assets/js/wNumb.js')      

        .scripts(
            'admin/dropzone.js', 'public/assets/js/dropzone.js')

        // .scripts('website/jquery.js', 'public/assets/js/jquery.js')
        // .scripts('website/jquery.event.move.js', 'public/assets/js/jquery.event.move.js')
        // .scripts('website/bootstrap.js', 'public/assets/js/bootstrap.js')
        // .scripts('website/theme.js', 'public/assets/js/theme.js')        
        // .scripts('website/responsive-slider.js', 'public/assets/js/responsive-slider.js')
        // .scripts('website/bootstrap-slider.js', 'public/assets/js/bootstrap-slider.js')
        // .scripts([
        //     // 'website/markerwithlabel.js',
        //     'website/jquery.jscroll.js',
        //     // 'website/select2.js',
        //     'website/scripts.js'], 'public/assets/js/scripts.js')

        .scripts(
            'website/map.js', 'public/assets/js/map.js')

        .scripts(
            'admin/jquery.dataTables.js', 'public/assets/js/admin/jquery.dataTables.js')

        .scripts([
        'admin/scripts.js',
        'admin/ajax.js',
        'admin/monolog.js',
        'admin/ui.js',
        'admin/redactor.js'
    ], 'public/assets/js/admin/scripts.js');
});
