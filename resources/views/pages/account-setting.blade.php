@extends('index')
@section('content')
<!-- breadcrumb -->
<section class="breadcrumb" style="background-image:url('assets/img/testimonials-bg.jpg');">
    <div class="breadcrumb-holder">
        <div class="page-title ta-center">
            <h3 class="white">
                Account Setting
            </h3>
            <p class="white fw-300">
                Find your property in the different area of Bali
            </p>
        </div>
    </div>
</section>

<!-- MAIN CONTAINER -->
<div id="content"> 

    <!-- BLOG & TESTIMONIALS -->
    <section class="bottom-section flexbox flexbox-wrap justify-between">
        
        @include('fragments.account-sidebar')

        <section class="w66">

            <div class="account-right-content">
                <div class="profile-stting">
                   <h1 class="title">Profile Setting</h1>
                   <div class="title-border-bottom"></div>
                   <div class="kesato-form">

                        {!! Form::open(['url' => route('api.customer.update', ['id' => $customer->id, 'type' => 'profile', 'sess' => 'front']), 'id' => 'validation-form', 'class' => 'form-profile']) !!}
                        {!! method_field('put') !!}

                           <div class="form-body">
                               <div class="frm-row">
                                   <div class="section colm colm6">
                                       <label for="reg_firstname" class="field prepend-icon">
                                           <input value="{{ $customer->firstname }}" type="text" name="firstname" id="reg_firstname" class="pl-15 gui-input" placeholder="First name">
                                       </label>
                                   </div>
                                   <div class="section colm colm6">
                                       <label for="reg_lastname" class="field prepend-icon">
                                           <input value="{{ $customer->lastname }}" type="text" name="lastname" id="reg_lastname" class="gui-input" placeholder="Last name">
                                       </label>
                                   </div>
                               </div>
                       <!--         <div class="section">
                                   <label for="reg_email" class="field prepend-icon">
                                       <input value="{{ $customer->email }}" type="email" name="email" id="reg_email" class="gui-input" placeholder="Email address">
                                   </label>
                               </div> -->
                               <div class="section">
                                   <label for="reg_phone" class="field prepend-icon">
                                       <input value="{{ $customer->phone }}" type="tel" name="phone" id="reg_phone" class="gui-input phone-group" placeholder="Phone number">
                                   </label>
                               </div>
                               
                               <div class="section">
                                   <label for="reg_address" class="field prepend-icon">
                                       <textarea class="gui-textarea" id="reg_address" name="address" placeholder="Address">{{ $customer->address }}</textarea>
                                   </label>
                               </div>
                               <div class="section">
                                   <label for="reg_phone" class="field prepend-icon">
                                       <input value="{{ $customer->city }}" type="tel" name="location" id="reg_phone" class="gui-input phone-group" placeholder="Location">
                                   </label>
                               </div>

                           </div><!-- end .form-body section -->

                           <div class="form-footer">
                               <button id="save-profile" type="submit" class="button btn-primary">{{ trans('word.save') }}</button>
                           </div>
                       {!! Form::close() !!}

                   </div><!-- END KESATO FORM -->
               </div>
               
                <div class="account-setting">
                    <h1 class="title">Account Setting</h1>
                    <div class="title-border-bottom"></div>
                    <div class="kesato-form">

                        {!! Form::open(['url' => route('api.customer.update', ['id' => $customer->id, 'type' => 'account', 'sess' => 'front']), 'id' => 'validation-form', 'class' => 'form-account']) !!}
                        {!! method_field('put') !!}

                            <div class="form-body">
                                <div class="section">
                                    <label for="reg_email" class="field prepend-icon">
                                        <input value="{{ $customer->email }}" type="email" name="email" id="reg_email" class="gui-input" placeholder="Email address" readonly>
                                    </label>
                                </div>
                                
                                <div class="section">
                                    <label for="reg_password" class="field prepend-icon">
                                        <input type="password" name="password" id="reg_password" class="gui-input" placeholder="Password">
                                    </label>
                                </div>
                                <div class="section">
                                    <label for="reg_repeatPassword" class="field prepend-icon">
                                        <input type="password" name="password_confirmation" id="reg_repeatPassword" class="gui-input" placeholder="Confirmation password">
                                    </label>
                                </div>             
                            </div><!-- end .form-body section -->

                            <div class="form-footer">
                                <button id="save-account" type="submit" class="button btn-primary">{{ trans('word.save') }}</button>
                            </div>
                        {!! Form::close() !!}

                    </div><!-- END KESATO FORM -->
                </div>
                
            </div>
        </section>
    </section>
</div>

@endsection

@section('scripts')
<script>
  
$(document).on('submit', '.form-profile', function(event) {
  event.preventDefault();
  /* Act on the event */

  $('.form-profile #notif-success').remove();

  var btn = $('#save-profile'),
  btn_word = btn.html(),
  frm = $(this),
  data = frm.serialize();

  btn.html("{{ trans('word.sending') }}");

  $.post(frm.attr('action'), data, function(data, textStatus, xhr) {
    /*optional stuff to do after success */
    if (data.status == 200) {

      var notif = '<em id="notif-success" class="state-success" style="color: green;">Data has been saved successfully.</em>';

      frm.append(notif);

    } else {

    }

    btn.html(btn_word);

  });

});

$(document).on('submit', '.form-account', function(event) {
  event.preventDefault();
  /* Act on the event */

  $('.form-account #notif-success').remove();

  var btn = $('#save-account'),
  btn_word = btn.html(),
  frm = $(this),
  data = frm.serialize();

  btn.html("{{ trans('word.sending') }}");

  $.post(frm.attr('action'), data, function(data, textStatus, xhr) {
    /*optional stuff to do after success */
    if (data.status == 200) {

      var notif = '<em id="notif-success" class="state-success" style="color: green;">Data has been saved successfully.</em>';

      $('input[type=password]').val('');

      frm.append(notif);

    } else {

    }

    btn.html(btn_word);

  });

});

</script>

@endsection