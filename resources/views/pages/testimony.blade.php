@extends('index')

@section('description', '')

@section('content')

<!-- BREADCRUMB -->
<section class="breadcrumb" style="background-image:url('assets/img/testimonials-bg.jpg');">
    <div class="breadcrumb-holder">
        <div class="page-title ta-center">
            <h3 class="white">
                {{ trans('word.breadcrumb_testimonials') }}
            </h3>
            <p class="white fw-300">
                Find your property in the different area of Bali
            </p>
        </div>
    </div>
</section>



<section id="content"> 
    <div class="home-page">
    
        <!-- BLOG & TESTIMONIALS -->
        <section class="bottom-section flexbox flexbox-wrap justify-between">
            <section class="full-width">
                                
                <div class="testimonials flexbox flexbox-wrap justify-between">

                    <?php $i=0; ?>
                    @foreach($testimonials as $testimony)

                    <?php $thumb = count($testimony->attachments) > 0 ? $testimony->attachments[0]->thumb : '/assets/img/no-image.png'; ?>
                    <?php $i++; ?>
                    <a href="{{ route('testimonials', ['testimonials' => trans('url.testimonials'), 'term' => $testimony->slug]) }}" class="flexbox flexbox-wrap justify-between" style="background-image:url({{ $thumb }});">
                        <div class="desc">
                            <h4>{{ $testimony->title }}</h4>
                            {{ $testimony->customer_name }}
                            <div class="md-button md-button-outline">{{ trans('word.read_more') }}</div>
                        </div>
                    </a>
                    @endforeach

                </div>
            </section>
    
        </section>
        
    </div>
</section>
    
    
<!-- PAGINATION -->        
@include('fragments.pagination', ['paginator' => $testimonials])

@stop

@section('scripts')
<script type="text/javascript">

var lastPage = {{ $testimonials->lastPage() }} - 1;

$(document).ready(function() {
    
    // $('.jscroll').jscroll({
    //     debug: false,
    //     autoTrigger: true,
    //     autoTriggerUntil: lastPage,
    //     loadingHtml: '<img src="assets/img/loading_bar.gif" alt="Loading" />',
    //     padding: 300,
    //     nextSelector: 'a.jscroll-next:last',
    //     pagingSelector: 'a.jscroll-next:last',
    //     callback: function () {
    //         console.log('loaded');
    //     }
    // });

    $('.testimony-save').click(function(event) {

        console.log('save clicked');

        var frm = $('#testimony-form'),
        url = frm.attr('action'),
        data = frm.serialize();;

        $.post(url, data, function(data, textStatus, xhr) {

            if(data.status == 200) {

                $('#add-testimony').modal('hide');

                $('input').val('');
                $('textarea').val('');
            }
        });

        event.preventDefault();
    });

});

</script>
@stop