@extends('index')
@section('content')
<section class="breadcrumb" style="background-image:url('assets/img/testimonials-bg.jpg');">
    <div class="breadcrumb-holder">
        <div class="page-title ta-center">
            <h3 class="white">
                {{ trans('word.breadcrumb_resource') }}
            </h3>
            <p class="white fw-300">
                Lorem Ipsum is simply dummy text of the printing
            </p>
        </div>
    </div>
</section>

<div id="content" class="resource">
    
    <div class="row">
        
        <section class="description">
            <p>Buying a property in Bali at the moment is definitely a good decision, as many people have realized over the past few years.</p>

            <p>There is a great choice of real estate in a very small area and if you look at the Seminyak market today, property prices are getting close to European or Australian price levels. In fact in 2012 alone luxury property prices in the area have gone up more than anywhere else in the world.</p>

            <p>You may wonder if this is due to speculation, or if there is a property bubble in Bali. Do you need to be scared to lose your investment in Bali in 2014?</p>

            <h5>We have a simple answer with a simple explanation :</h5>
            
            <ul class="explanation">
                <li>You WON’T lose any money in Bali because whereas most Western countries have suffered through the financial crisis, properties get paid cash in Bali and the property market isn’t reliant on credit. This means that in case of a crisis, people won’t be in the rush to sell their property because they cannot meet their mortgage payments and banks won’t be repossessing properties.</li>
                <li>While Monaco is still the world’s most expensive place for residential real estate, if the trends continues in Indonesia and Bali this could soon change. The region’s middle class is growing exponentially, so is the number of rich people. Indeed there are now more millionaires in Asia than anywhere else in the world!<</li>
                <li>It is important to note also that villas prices in Bali are going up, but so is the quality of the buildings.</li>
                <li>Many investors wonder if it is better to buy a freehold or a leasehold property in Bali?</li>
                <li>We are of the opinion that it depends on the opportunity. For a first investment in Bali, we always suggest to foreign investors to start with buying a leasehold property with a good rental history. It is then easy to achieve 8% to 12% of return per year. You will also avoid surprises and you can buy under you own name.</li>
            </ul>


            <h5>Buying a property in Bali can be beneficial more many reasons :</h5>

            <ul class="reasons">
                <li>you can expect good profits</li>
                <li>you pay less tax</li>
                <li>you have the opportunity to increase the value of your assets</li>
                <li>last but not least, isn’t it great to own a property in the paradise that is Bali?</li>
                <li>Greetings from Kibarer property</li>
            </ul>
        </section>


        <section class="link">
            <h1 class="caption"> Links to Bali Accommodation Sites</h1>
                        
            <div class="flexbox two-col">
                <div class="list">
                    <div class="flexbox two-col-inner">
                        <div class="item image-box">
                            <a href="#"><img src="assets/img/resource-page/logo/1.jpg" alt="1"></a>
                        </div>
                        <div class="item text">
                            <a href="http://www.Hanginggardens.com">Hanginggardens.com</a>
                            <p>Hanging Gardens Ubud offers supreme luxury and ultimate privacy combined with five star service, set in the abundant and breathtaking Ubud countryside</p>
                        </div>
                    </div>

                    <div class="flexbox two-col-inner" >
                        <div class="item image-box">
                            <a href="#"><img src="assets/img/resource-page/logo/2.jpg" alt="1"></a>
                        </div>
                        <div class="item text">
                            <a href="http://www.VillaKubu.com">VillaKubu.com</a>
                            <p>Villa Kubu Boutique Villa Hotel and Spa – Seminyak, Bali</p>
                        </div>
                    </div>


                    <div class="flexbox two-col-inner">
                        <div class="item image-box">
                            <a href="#"><img src="assets/img/resource-page/logo/3.jpg" alt="1"></a>
                        </div>
                        <div class="item text">
                            <a href="http://www.beachvilla-bali.com/">Villa Insulinde Lovina</a>
                            <p>Our private vacation villa on Bali consists of two separate holiday homes, Villa Insulinde and Villa Lumbung,</p>
                        </div>
                    </div>

                    <div class="flexbox two-col-inner">
                        <div class="item image-box">
                            &nbsp;
                        </div>
                        <div class="item text">
                            <a href="http://www.Theapartmentsumalas.com">Theapartmentsumalas.com</a>
                        </div>
                    </div>
                </div>
                <div class="list image-wrapper">
                    <a href="#"><img src="assets/img/resource-page/Links-to-Bali-Accommodation-Sites1.jpg" alt=""></a>
                </div>
            </div>

            
        </section>

        <h1 class="caption">Links to Bali Activity Sites</h1>

        <section class="link">     
            <h1 class="sub">Bali Tours</h1>
            
            <div class="flexbox two-col">
                <div class="list">

                    <div class="flexbox two-col-inner">
                        <div class="item image-box">
                            <a href="#"><img src="assets/img/resource-page/logo/4.jpg" alt="1"></a>
                        </div>
                        <div class="item text">
                            <a href="http://www.romanticbalitours.com/">Bali Tour</a>
                            <p>is the most popular package which is looked for by the local or the foreign tourists</p>
                        </div>
                    </div>

                    <div class="flexbox two-col-inner">
                        <div class="item image-box">
                            <a href="#"><img src="assets/img/resource-page/logo/5.jpg" alt="1"></a>
                        </div>
                        <div class="item text">
                            <a href="#">BALI TOURS – Your Bali travel partner</a>
                            <p>Bali Tours.co now providing some package of your leisure of Bali Holiday tours</p>
                        </div>
                    </div>


                    <div class="flexbox two-col-inner">
                        <div class="item image-box">
                            <a href="#"><img src="assets/img/resource-page/logo/6.jpg" alt="1"></a>
                        </div>
                        <div class="item text">
                            <a href="http://www.beachvilla-bali.com/">Bali Tour</a>
                            <p>Is this your first time to Bali and you are wondering what to see</p>
                        </div>
                    </div>

                </div>
                <div class="list image-wrapper">
                     <a href="#"><img src="assets/img/resource-page/Bali-Adventure1.jpg" alt=""></a>
                </div>
            </div>
            
        </section>

        <section class="link">     
            <h1>Bali Property</h1>
            
            <div class="flexbox two-col">
                <div class="list">
                    <div class="flexbox two-col-inner">
                        <div class="item image-box">
                            <a href="#"><img src="http://villabalisale.com/wp-content/uploads/2013/04/balinana-300x54.png" alt="1"></a>
                        </div>
                        <div class="item text">
                            <a href="http://www.romanticbalitours.com/">Property and real estate in Bali</a>
                            <p>Bali property provide property villa house land balinana</p>
                        </div>
                    </div>

                    <div class="flexbox two-col-inner">
                        <div class="item image-box">
                            &nbsp;
                        </div>
                        <div class="item text">
                            <a href="BALI TOURS – Your Bali travel partner">Dotlahpis Bali Property</a>
                            <p>Dotlahpis Bali Property merupakan sarana yang efektif bagi para Agen/Broker dan Owner Property  </p>
                        </div>
                    </div>

                    <div class="flexbox two-col-inner">
                        <div class="item image-box">
                            &nbsp;
                        </div>
                        <div class="item text">
                            <a href="#">BALIVILLASALES.COM</a>
                            <p>Bali Villas for Sale freehold OR leasehold – Buy Property in Bali</p>
                        </div>
                    </div>
                </div>
                <div class="list image-wrapper">
                    <a href="#"><img src="assets/img/resource-page/Bali-Other-sites2.jpg" alt=""></a>
                </div>
            </div>
            
        </section>

        <section class="link">     
            <h1>Bali Adventure</h1>
            
            <div class="flexbox two-col">
                <div class="list">
                    <div class="flexbox two-col-inner">
                        <div class="item image-box">
                            <a href="#"><img src="assets/img/resource-page/logo/7.jpg" alt="1"></a>
                        </div>
                        <div class="item text">
                            <a href="http://www.romanticbalitours.com/">Bali Dirt Bike Adventure</a>
                            <p>Bali Dirt Bike Adventure is limited adventure activity in Bali. Our trail to cross dirt road, Lava Field, Rice Paddy, Sacred River and Hidden Villages with Forest base camp.</p>
                        </div>
                    </div>

                    <div class="flexbox two-col-inner">
                        <div class="item image-box">
                            <a href="#"><img src="assets/img/resource-page/logo/10.jpg" alt="1"></a>
                        </div>
                        <div class="item text">
                            <a href="#">Scuba Diving Trips & PADI Dive Courses in Timor Leste</a>
                            <p>Dive Timor Lorosae is Timor Leste’s premier PADI 5  divetimor</p>
                        </div>
                    </div>


                    <div class="flexbox two-col-inner">
                        <div class="item image-box">
                            <a href="#"><img src="http://villabalisale.com/wp-content/uploads/2013/04/silatour.png" alt="1"></a>
                        </div>
                        <div class="item text">
                            <a href="#">Trip in Bali Paragliding Introductory</a>
                            <p>Welcome to bali adventure   paragiling</p>
                        </div>
                    </div>

                    <div class="flexbox two-col-inner">
                        <div class="item image-box">
                            &nbsp;
                        </div>
                        <div class="item text">
                            <a href="#">Bali Water Sports</a>
                            <p>Welcome to Silatours.com. We are the service provider of the most comprehensive Bali water sports.</p>
                        </div>
                    </div>
                </div>
                <div class="list image-wrapper">
                     <a href="#"><img src="assets/img/resource-page/Links-to-Bali-Activity-Sites1.jpg" alt=""></a>
                </div>
            </div>
            
        </section>

        <section class="link">     
            <h1>Bali Other sites!</h1>
            
            <div class="flexbox two-col">
                <div class="list">
                    <div class="flexbox two-col-inner">
                        <div class="item image-box">
                            <a href="#"><img src="http://villabalisale.com/wp-content/uploads/2013/04/msbalicarrental-300x63.png" alt="1"></a>
                        </div>
                        <div class="item text">
                            <a href="http://www.romanticbalitours.com/">Bali Car Rental msbalicarrental.com</a>
                            <p>We serve the rental car shuttle at the airport, hotels, villas and housesp.</p>
                        </div>
                    </div>

                    <div class="flexbox two-col-inner">
                        <div class="item image-box">
                            <a href="#"><img src="http://villabalisale.com/wp-content/uploads/2013/04/queendoor.png" alt="1"></a>
                        </div>
                        <div class="item text">
                            <a href="#">Indian restaurant in Bali</a>
                            <p>Restaurant in bali</p>
                        </div>
                    </div>


                    <div class="flexbox two-col-inner">
                        <div class="item image-box">
                            <a href="#"><img src="http://villabalisale.com/wp-content/uploads/2013/04/marinasrikandilogo.png" alt="1"></a>
                        </div>
                        <div class="item text">
                            &nbsp;
                        </div>
                    </div>

                </div>
                <div class="list image-wrapper">
                     &nbsp;
                </div>
            </div>
            
        </section>

        <section class="link">     
            <h1>Others</h1>
            
            <div class="flexbox two-col">
                <div class="list">
                    <div class="flexbox two-col-inner">
                        <div class="item text" style="padding-left: 0px">
                            <a href="http://www.romanticbalitours.com/">Directory World</a>
                        </div>
                    </div>

                    <div class="flexbox two-col-inner">
                        <div class="item text" style="padding-left: 0px">
                            <a href="#">Travel Agent Directory</a>
                        </div>
                    </div>


                    <div class="flexbox two-col-inner">
                        <div class="item text" style="padding-left: 0px">
                            <a href="#">Resourcelinks Business Directory</a>
                        </div>
                    </div>

                    <div class="flexbox two-col-inner">
                        <div class="item text" style="padding-left: 0px">
                            <a href="#">AzListed</a>
                            <p>Human edited directory that offers webmasters a choice of a free or paid website submission.</p>
                        </div>
                    </div>

                </div>
                <div class="list image-wrapper">
                     &nbsp;
                </div>
            </div>
            
        </section>

   
    </div> <!-- row -->


</div> <!-- #content -->

<!--
<div id="content">
	<article class="custom-page">
		<div class="resource-desc">
			<p>Buying a property in Bali at the moment is definitely a good decision, as many people have realized over the past few years. There is a great choice of real estate in a very small area and if you look at the Seminyak market today, property prices are getting close to European or Australian price levels. In fact in 2012 alone luxury property prices in the area have gone up more than anywhere else in the world. You may wonder if this is due to speculation, or if there is a property bubble in Bali. Do you need to be scared to lose your investment in Bali in 2014? We have a simple answer with a simple explanation :</p>

	        <p>You WON’T lose any money in Bali because whereas most Western countries have suffered through the financial crisis, properties get paid cash in Bali and the property market isn’t reliant on credit. This means that in case of a crisis, people won’t be in the rush to sell their property because they cannot meet their mortgage payments and banks won’t be repossessing properties. While Monaco is still the world’s most expensive place for residential real estate, if the trends continues in Indonesia and Bali this could soon change. The region’s middle class is growing exponentially, so is the number of rich people. Indeed there are now more millionaires in Asia than anywhere else in the world!</p>

	        <p>It is important to note also that villas prices in Bali are going up, but so is the quality of the buildings. Many investors wonder if it is better to buy a freehold or a leasehold property in Bali? We are of the opinion that it depends on the opportunity. For a first investment in Bali, we always suggest to foreign investors to start with buying a leasehold property with a good rental history. It is then easy to achieve 8% to 12% of return per year. You will also avoid surprises and you can buy under you own name.</p>

	        <p>Buying a property in Bali can be beneficial more many reasons :</p>

	        <ul class="resource-ul">
	            <li>You can expect good profits</li>
	            <li>You pay less tax</li>
	            <li>You have the opportunity to increase the value of your assets</li>
	            <li>Last but not least, isn’t it great to own a property in the paradise that is Bali?</li>
	            <li>Greetings from Kibarer property</li>
	        </ul>
        </div>

        <div class="line"></div>

        <h2 class="title-resource">Links to Bali Accommodation Sites</h2>

        <div class="resource-holder">
            <div class="logo-resource">
                <img src="assets/img/resource-page/logo/1.jpg">
                <img src="assets/img/resource-page/logo/3.jpg">
                <img src="assets/img/resource-page/logo/2.jpg">
            </div>
            <div class="content-resource">
                <h3>Hanginggardens.com</h3>
                <p>Hanging Gardens Ubud offers supreme luxury and ultimate privacy combined with five star service, set in the abundant and breathtaking Ubud countryside</p>
                <br>
                <h3>VillaKubu.com</h3>
                <p>Villa Kubu Boutique Villa Hotel and Spa – Seminyak, Bali</p>
                <br>
                <h3>Villa Insulinde Lovina</h3>
                <p>Our private vacation villa on Bali consists of two separate holiday homes, Villa Insulinde and Villa Lumbung</p>
                <br>   
                <h3>Theapartmentsumalas.com</h3>
                <br>  
            </div>
            <div class="img-resource">
                <div class="img-wrapper">
                    <img src="assets/img/resource-page/Links-to-Bali-Accommodation-Sites1.jpg" alt="" class="img-responsive">
                </div>
            </div>
        </div>

        <div class="line"></div>


        <h2 class="title-resource">Links to Bali Activity Sites</h2>

        <div class="resource-holder">
        <div class="logo-resource">
                <img src="assets/img/resource-page/logo/4.jpg">
                <img src="assets/img/resource-page/logo/5.jpg">
                <img src="assets/img/resource-page/logo/6.jpg">
                <img src="assets/img/resource-page/logo/7.jpg">
                <img src="assets/img/resource-page/logo/8.jpg">
                <img src="assets/img/resource-page/logo/9.jpg">
                <img src="assets/img/resource-page/logo/10.jpg">
                <img src="assets/img/resource-page/logo/11.jpg">
                <img src="assets/img/resource-page/logo/12.jpg">
                <img src="assets/img/resource-page/logo/12.jpg">
            </div>
            <div class="content-resource">
                <h3>Bali Tour</h3>
                <p>is the most popular package which is looked for by the local or the foreign tourists</p>
                <br>
                <h3>BALI TOURS – Your Bali travel partner</h3>
                <p>Bali Tours.co now providing some package of your leisure of Bali Holiday tours</p>
                <br>
                <h3>Bali Tour</h3>
                <p>Is this your first time to Bali and you are wondering what to see</p>
                <br>
                <h3>Property and real estate in Bali</h3>
                <p>Bali property provide property villa house land</p>
                <br>
                <h3>Dotlahpis Bali Property</h3>
                <p>Dotlahpis Bali Property merupakan sarana yang efektif bagi para Agen/Broker dan Owner Property</p>
                <br>
                <h3>Balivillasales.com</h3> 
                <p>Bali Villas for Sale freehold OR leasehold – Buy Property in Bali</p>
                <br>
                <h3>Bali Dirt Bike Adventure</h3>
                <p>Bali Dirt Bike Adventure is limited adventure activity in Bali. Our trail to cross dirt road, Lava Field, Rice Paddy, Sacred River and Hidden Villages with Forest base camp.</p>
                <br>
                <h3>Scuba Diving Trips & PADI Dive Courses in Timor Leste</h3>
                <p>Dive Timor Lorosae is Timor Leste’s premier PADI 5</p>
                <br>
                <h3>Trip in Bali Paragliding Introductory</h3>
                <p>Welcome to bali adventure</p>
                <br>
                <h3>Bali Water Sports</h3>
                <p>Welcome to Silatours.com. We are the service provider of the most comprehensive Bali water sports.</p>
                <br>
                <h3>Bali Car Rental msbalicarrental.com</h3>
                <p>We serve the rental car shuttle at the airport, hotels, villas and houses</p>
                <br>
                <h3>Indian restaurant in Bali</h3>
                <p>Restaurant in bali</p>

                <br>

            </div>
            <div class="img-resource">
                <div class="img-wrapper">
                    <img src="assets/img/resource-page/Bali-Adventure1.jpg" alt="" class="img-responsive">
                </div>
                <div class="img-wrapper">
                    <img src="assets/img/resource-page/Bali-Other-sites2.jpg" alt="" class="img-responsive">
                </div>
                <div class="img-wrapper">
                    <img src="assets/img/resource-page/Links-to-Bali-Activity-Sites1.jpg" alt="" class="img-responsive">
                </div>
            </div>
        </div>

	</article>
</div> --> <!-- END MAIN -->

@endsection
