@extends('index')



@section('title')
    {{checkEmptyTwoString(FirstReplace($category),FirstReplace($area),'Search Results','Search Results')}}
@endsection

@section('description')
    {{$properties->total()}} ++ Best {{FirstReplace($category)}} in {{ucwords($area)}}
@endsection


@section('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/8.3.0/nouislider.min.css">
    <style>

        .noUi-connect {
            background: #ee5b2c;
        }

        #fountainTextG {
            width: 234px;
            margin: auto;
        }

        .fountainTextG {
            color: rgb(0, 0, 0);
            font-family: Arial;
            font-size: 24px;
            text-decoration: none;
            font-weight: normal;
            font-style: normal;
            float: left;
            animation-name: bounce_fountainTextG;
            -o-animation-name: bounce_fountainTextG;
            -ms-animation-name: bounce_fountainTextG;
            -webkit-animation-name: bounce_fountainTextG;
            -moz-animation-name: bounce_fountainTextG;
            animation-duration: 2.09s;
            -o-animation-duration: 2.09s;
            -ms-animation-duration: 2.09s;
            -webkit-animation-duration: 2.09s;
            -moz-animation-duration: 2.09s;
            animation-iteration-count: infinite;
            -o-animation-iteration-count: infinite;
            -ms-animation-iteration-count: infinite;
            -webkit-animation-iteration-count: infinite;
            -moz-animation-iteration-count: infinite;
            animation-direction: normal;
            -o-animation-direction: normal;
            -ms-animation-direction: normal;
            -webkit-animation-direction: normal;
            -moz-animation-direction: normal;
            transform: scale(.5);
            -o-transform: scale(.5);
            -ms-transform: scale(.5);
            -webkit-transform: scale(.5);
            -moz-transform: scale(.5);
        }

        #fountainTextG_1 {
            animation-delay: 0.75s;
            -o-animation-delay: 0.75s;
            -ms-animation-delay: 0.75s;
            -webkit-animation-delay: 0.75s;
            -moz-animation-delay: 0.75s;
        }

        #fountainTextG_2 {
            animation-delay: 0.9s;
            -o-animation-delay: 0.9s;
            -ms-animation-delay: 0.9s;
            -webkit-animation-delay: 0.9s;
            -moz-animation-delay: 0.9s;
        }

        #fountainTextG_3 {
            animation-delay: 1.05s;
            -o-animation-delay: 1.05s;
            -ms-animation-delay: 1.05s;
            -webkit-animation-delay: 1.05s;
            -moz-animation-delay: 1.05s;
        }

        #fountainTextG_4 {
            animation-delay: 1.2s;
            -o-animation-delay: 1.2s;
            -ms-animation-delay: 1.2s;
            -webkit-animation-delay: 1.2s;
            -moz-animation-delay: 1.2s;
        }

        #fountainTextG_5 {
            animation-delay: 1.35s;
            -o-animation-delay: 1.35s;
            -ms-animation-delay: 1.35s;
            -webkit-animation-delay: 1.35s;
            -moz-animation-delay: 1.35s;
        }

        #fountainTextG_6 {
            animation-delay: 1.5s;
            -o-animation-delay: 1.5s;
            -ms-animation-delay: 1.5s;
            -webkit-animation-delay: 1.5s;
            -moz-animation-delay: 1.5s;
        }

        #fountainTextG_7 {
            animation-delay: 1.64s;
            -o-animation-delay: 1.64s;
            -ms-animation-delay: 1.64s;
            -webkit-animation-delay: 1.64s;
            -moz-animation-delay: 1.64s;
        }

        @keyframes bounce_fountainTextG {
            0% {
                transform: scale(1);
                color: rgb(0, 0, 0);
            }

            100% {
                transform: scale(.5);
                color: rgb(255, 255, 255);
            }
        }

        @-o-keyframes bounce_fountainTextG {
            0% {
                -o-transform: scale(1);
                color: rgb(0, 0, 0);
            }

            100% {
                -o-transform: scale(.5);
                color: rgb(255, 255, 255);
            }
        }

        @-ms-keyframes bounce_fountainTextG {

        0
        %
        {
            -ms-transform: scale(1)
        ;
            color: rgb(0, 0, 0)
        ;
        }

        100
        %
        {
            -ms-transform: scale(.5)
        ;
            color: rgb(255, 255, 255)
        ;
        }
        }

        @-webkit-keyframes bounce_fountainTextG {
            0% {
                -webkit-transform: scale(1);
                color: rgb(0, 0, 0);
            }

            100% {
                -webkit-transform: scale(.5);
                color: rgb(255, 255, 255);
            }
        }

        @-moz-keyframes bounce_fountainTextG {
            0% {
                -moz-transform: scale(1);
                color: rgb(0, 0, 0);
            }

            100% {
                -moz-transform: scale(.5);
                color: rgb(255, 255, 255);
            }
        }
    </style>
    @endsection

    @section('content')

            <!-- BREADCRUMB -->
    <section class="breadcrumb" style="background-image:url('assets/img/testimonials-bg.jpg');">
        <div class="breadcrumb-holder">
            <div class="page-title ta-center">
                <h1 class="white">
                    {{checkEmptyTwoString(FirstReplace($category),FirstReplace($area),'','')}}
                </h1>
                <p class="white fw-300">
                    {{$properties->total()}} Best {{FirstReplace($category)}} in {{ucwords($area)}}
                </p>
            </div>
        </div>
    </section>


    <section class="properties">

        <div class="modal-filter hiddenBox">
            <div class="spinner"></div>
        </div>

        <div class="property-list">

            <div class="property-filter">
                <div class="first-filter">
                    <div class="kesato-form">
                        <form method="post" action="/" id="formKesato">
                        {{csrf_field()}}
                        @include("fragments.search.$category")


                    </div>
                </div><!-- END FIRST FILTER -->

                <div id="second-filter" class="second-filter" style="display: none;">
                    <div class="kesato-form">

                        @include("fragments.search.more-$category")

                        <div class="apply-filter">
                            <div class="apply">
                                <button type="button" class="button btn-primary btn-second-filter"
                                        onclick="close_more_filter()">Close
                                </button>
                                {{--<button type="submit" class="button btn-primary btn-second-filter">Search Now--}}
                                {{--</button>--}}
                            </div>
                        </div><!-- APPLY FILTER -->


                    </div><!-- END KESATO FORM -->
                </div><!-- END SECOND FILTER -->

            </div>
            <!h1-- END PROPERTY FILTER -->

            <input type="hidden" id="nwLat" name="nwLat">
            <input type="hidden" id="nwLng" name="nwLng">
            <input type="hidden" id="seLat" name="seLat">
            <input type="hidden" id="seLng" name="seLng">


            <div id="box">


                @include('pages.ajax-property')

            </div><!-- END BOX -->

        </div><!-- END PROPERTY LIST -->
        </form>

        <div class="property-map">
            <!--    <input id="pac-input" class="controls" type="text" placeholder="Enter a location"
                      style="z-index: 0; position: absolute; left: 107px; top: 0px; width: 50%; argin-top: 10px; padding: 10px 15px; background: white; color: #666; border: none; border-bottom-left-radius: 2px; border-top-left-radius: 2px; webkit-background-clip: padding-box; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; background-clip: padding-box; font-family: Roboto, Arial, sans-serif; -webkit-user-select: none; font-size: 12px; margin: 30px; font-weight: 400;">
                -->
            <div id="map" style="width:100%; height: 100%;"></div>
        </div><!-- END PROPERTY MAP -->

    </section><!-- END PROPERTIES -->


    <!-- IDENTITY PAGE
    <div class="data hidden">
        <input type="hidden" id="CURRENTPAGE" value="property"/>
    </div>
    -->
    <?php $search_page = true; ?>
@endsection

@section('scripts')

    <script src="http://maps.googleapis.com/maps/api/js?libraries=places"></script>
    <script src="assets/js/form/jquery-ui-1.10.4.custom.min.js"></script>
    <script src="assets/js/form/jquery-ui-touch-punch.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/8.3.0/nouislider.min.js"></script>
    <script src="assets/js/wNumb.js"></script>

    <script type="text/javascript">
        var firstload = 0;
        var category = '{!! $category !!}';
        var areas = '{!! $area !!}';
        var type = '{!! $type !!}';
        var maxPriceProperty = {!!  $maxPrice  !!};
        $(document).ready(function () {
            initMap();
        });
    </script>

    <script>
        var currency = "{!! Session::get('currency') !!}";
        currency = currency.toUpperCase();
        var slider = document.getElementById('slider-range');

        var min = 0;
        var max = maxPriceProperty;

        noUiSlider.create(slider, {
            start: [{!!  replaceComma(convertCurrency(0, 'usd', Session::get('currency')))  !!}, maxPriceProperty],
            connect: true,
            step: {!!  replaceComma(convertCurrency(5, 'usd', Session::get('currency')))  !!},
            range: {
                'min': 0,
                'max': maxPriceProperty
            },
            format: wNumb({
                decimals: 0,
                thousand: ','
            })
        });


    </script>

    <script type="text/javascript">


        function initMap() {
            var boxSearch = $('#box');
            var allMarker = [];
            var myLatLng = {lat: -8.4420734, lng: 114.9356164};
            var pin = 'assets/img/marker.png';


            var map = new google.maps.Map(document.getElementById('map'), {
                mapTypeControl: false,
                center: myLatLng,
                scrollwheel: true,
                zoom: 10,
                icon: pin,
                maxZoom: 15,
                minZoom: 8,
                draggable: true,
                zoomControl: true,
                disableDoubleClickZoom: false,
                streetViewControl: false
            });

            var infowindow = new google.maps.InfoWindow();
            var input2 = document.getElementById('location');
            var searchBox2 = new google.maps.places.SearchBox(input2);

            searchBox2.addListener('places_changed', function() {
                var places = searchBox2.getPlaces();

                if (places.length == 0) {
                    return;
                }



                var bounds = new google.maps.LatLngBounds();
                places.forEach(function(place) {

                    if (place.geometry.viewport) {
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }
                });
                map.fitBounds(bounds);
                allInOne();
            });

            google.maps.event.addListener(map, 'idle', function () {
                allInOne();
            });

            //            $(document).on('mouseover','.box',function(){
            //
            //                for (var i = 0; i < allMarker.length; i++) {
            //                    allMarker[i].setMap(null);
            //                }
            //
            //                id = $(this).attr('rel');
            //                newlat = $('#lat'+id).val();
            //                newlng = $('#long'+id).val();
            //
            //                map.panTo({
            //                    lat: parseFloat(newlat), lng: parseFloat(newlng)
            //                });
            //                firstload = 0;
            //
            //                var marker = new google.maps.Circle({
            //                    strokeColor: '#ee5b2c',
            //                    strokeOpacity: 0.8,
            //                    strokeWeight: 0,
            //                    fillColor: '#ee5b2c',
            //                    fillOpacity: 0.35,
            //                    map: map,
            //                    center: {lat: parseFloat(newlat), lng: parseFloat(newlng)},
            //                    radius: 10 * 100
            //                });
            //
            //                allMarker.push(marker);
            //
            //            });
            @if ($area)
            @if ($category == 'villas-for-sale')
                    setCircle();
            @else
              $.ajax({
                dataType: 'json',
                url: 'http://maps.google.com/maps/api/geocode/json?address={!! $area !!}}&sensor=false',
                success: function (data) {

                    newLat = data.results[0].geometry.location.lat;
                    newLong = data.results[0].geometry.location.lng;
                    map.panTo({
                        lat: newLat, lng: newLong
                    });
                    map.setZoom(14);


                }
            });
            @endif
            @endif

            function setCircle(){
                $.ajax({
                    dataType: 'json',
                    url: 'http://maps.google.com/maps/api/geocode/json?address={!! $area !!}}&sensor=false',
                    success: function (data) {

                        newLat = data.results[0].geometry.location.lat;
                        newLong = data.results[0].geometry.location.lng;
                        var marker = new google.maps.Circle({
                            strokeColor: '#ee5b2c',
                            strokeOpacity: 0.8,
                            strokeWeight: 0,
                            fillColor: '#ee5b2c',
                            fillOpacity: 0.35,
                            map: map,
                            center: {lat: newLat, lng: newLong},
                            radius: 11 * 100
                        });

                        map.panTo({
                            lat: newLat, lng: newLong
                        });
                        map.setZoom(12);


                    }
                });
            }

            $('.selectAjax').on('change',function(){
                getAjaxbyHrefThenShow();
            });

            $('.ajaxSearch').on('click', function () {
                getAjaxbyHrefThenShow();
            });

            function showLoading(){
                $('.modal-filter').removeClass('hiddenBox');
                $('.property-list').addClass('opacity5');
            }

            function hideLoading(){
                $('.modal-filter').addClass('hiddenBox');
                $('.property-list').removeClass('opacity5');
            }

            slider.noUiSlider.on('update', function (values) {
                $('#amounts').val(currency + ' ' + values[0] + ' - ' + currency + ' ' + values[1]);
            });

            slider.noUiSlider.on('change', function (values) {
                allInOne();
            });


                    @if ($category == 'villas-for-sale' OR $category == 'land')
            var sliderLand = document.getElementById('slider-land');
            noUiSlider.create(sliderLand, {
                start: [0, 1000],
                connect: true,
                step: 1,
                range: {
                    'min': 0,
                    'max': 1000
                },
                format: wNumb({
                    decimals: 0,
                    thousand: ',',
                })
            });

            sliderLand.noUiSlider.on('update', function (values) {
                $('#land-size').val(values[0] + ' m2 - ' + values[1] + ' m2');
            });

            sliderLand.noUiSlider.on('change', function (values) {
                allInOne();
            });

            @endif

            $(document).ready(function () {
                setNewMarker();
            });

            function getNewLatLong() {
                $('#nwLng').val(map.getBounds().j.R);
                $('#nwLat').val(map.getBounds().R.R);
                $('#seLng').val(map.getBounds().j.j);
                $('#seLat').val(map.getBounds().R.j);
            }

            function allInOne() {

                if (firstload != 0) {
                    for (var i = 0; i < allMarker.length; i++) {
                        allMarker[i].setMap(null);
                    }
                }
                pushHistory();
                getAjaxbyHrefThenShow();
                setNewMarker();


            }

            function getAjaxbyHrefThenShow() {


                if (firstload != 0) {
                    newUrl = '{!! url('search') !!}/';

                    if (category) {
                        newUrl += category;
                        if (areas) {
                            newUrl += '/' + type;
                            if (type) {
                                newUrl += '/' + areas;
                            }
                        }
                    }
                    console.log(newUrl);
                    showLoading();
                    $.ajax({
                        url: newUrl,
                        method: 'POST',
                        data: $('#formKesato').serialize(),
                        success: function (data) {
                            hideLoading();
                            boxSearch.html(data);

                        }
                    })
                }
                firstload++;

            }


            function setNewMarker() {
                // for (var i = 0; i < allMarker.length; i++) {
                //     allMarker[i].setMap(null);
                // }
                if (document.getElementById('newLatLong') != null)
                {
                    var hiddenLatLong = document.getElementById('newLatLong').value;
                }else {
                    var hiddenLatLong = null;
                }
                if (hiddenLatLong) {
                    var listLatLong = JSON.parse(hiddenLatLong);
                    $.each(listLatLong, function (key, val) {
                        if (category != 'villas-for-sale') {
                            addMarker(parseFloat(val[0]), parseFloat(val[1]), val[2]);
                        }

                        // $.each(val, function (key2, val2) {
                        //     addMarker(parseFloat(key2), parseFloat(val2));
                        // })
                    })
                }
            }


            function addMarker(newlat, newlng, id) {
                var marker = new google.maps.Marker({
                    position: {lat: newlat, lng: newlng},
                    icon: pin,
                    map: map
                });


                var image = $('#currentThumb' + id).attr('src');
                var title = $('#title' + id).html();
                var url = $('#url' + id).attr('href');
                var description = $('#description' + id).html();
                var objectfirst = $('#objectfirst' + id).html();
                var objectsecond = $('#objectsecond' + id).html();
                objectsecond = objectsecond.replace('<img ', '<img style="width:15px" ');
                var contentString = '<div id="content-form" style="10px 0px 30px 0px;min-width: 200px;">' +
                        '<a href="' + url + '"><img src="' + image + '" style="width:50px; height:50px; float:left; margin: 0px 10px 0px 0px;"></a>' +
                        '<a href="' + url + '" target="_blank"><p id="title" style="font-size: 1em;color: #ee5b2c;font-weight: 600; text-transform: uppercase; margin-bottom:10px;">' + title + '</p></a><div class="clearfix"></div>' +
                        '<div id="bodyContent" style="max-width: 300px;overflow: scroll;">' +
                        '<p>' + description + '</p>' +
                        '<p>' + objectfirst + '&nbsp;&nbsp;' + objectsecond + '</p>' +
                        '</div>' +
                        '</div>';


                google.maps.event.addListener(marker, 'click', function () {
                    firstload = 0;
                    infowindow.close();
                    infowindow.setContent(contentString);
                    infowindow.open(map, marker);

                });

                allMarker.push(marker);
            }


            function pushHistory() {
                getNewLatLong();
            }

            // map.addListener('bounds_changed', function() {
            //   searchBox.setBounds(map.getBounds());
            // });


            // searchBox.addListener('places_changed', function() {
            //   var places = searchBox.getPlaces();

            //   if (places.length == 0) {
            //     return;
            //   }

            //   // For each place, get the icon, name and location.
            //   var bounds = new google.maps.LatLngBounds();
            //   places.forEach(function(place) {

            //     if (place.geometry.viewport) {
            //       // Only geocodes have viewport.
            //       bounds.union(place.geometry.viewport);
            //     } else {
            //       bounds.extend(place.geometry.location);
            //     }

            //   });
            //   map.fitBounds(bounds);

            // });
//
//            searchBox2.addListener('places_changed', function () {
//                var places = searchBox2.getPlaces();
//
//                if (places.length == 0) {
//                    return;
//                }
//
//                // For each place, get the icon, name and location.
//                var bounds = new google.maps.LatLngBounds();
//                places.forEach(function (place) {
//
//                    if (place.geometry.viewport) {
//                        // Only geocodes have viewport.
//                        bounds.union(place.geometry.viewport);
//                    } else {
//                        bounds.extend(place.geometry.location);
//                    }
//
//                });
//                map.fitBounds(bounds);
//
//            });


        }


        $(document).on('click', '.changeThumbnail', function (e) {
                    e.preventDefault();

                    var curIDThumb = $(this).attr('rel');
                    var curIDProp = $(this).attr('name');
                    var totalThumb = $('#totalThumb' + curIDProp).val();
                    if ($(this).attr('about') == 'next') {
                        if (curIDThumb == totalThumb) {
                            curIDThumb = 0;
                        } else {
                            curIDThumb = parseInt(curIDThumb) + 1;
                        }

                    } else {
                        if (curIDThumb == 0) {
                            curIDThumb = totalThumb;
                        } else {
                            curIDThumb = parseInt(curIDThumb) - 1;
                        }
                    }
                    var combineID = curIDProp + curIDThumb;
                    console.log(combineID);
                    if ($('#thumb' + combineID).length != 'undefined') {
                        var newBG = $('#thumb' + combineID).val();
                        console.log(newBG);
                        $('#currentThumb' + curIDProp).attr('src', newBG);
                        $(this).attr('rel', curIDThumb);
                        $(this).closest('.changeThumbnail').attr('rel', curIDThumb);
                    }


                }
        );

        $(document).on('click', '.box-link-detail', function () {
            showItem();
            hideDetail();
            var detailID = $(this).attr('rel');
            $('#boxitem' + detailID).fadeOut('fast');
            $('#boxdetail' + detailID).fadeIn('fast');

        });


        hideDetail();

        function hideDetail() {
            $('.box-detailz').fadeOut('fast');
        }

        function showItem() {
            $('.box-item').fadeIn('fast');
        }




    </script>
@endsection
