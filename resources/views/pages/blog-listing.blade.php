
@extends('index')

@section('content')

<div id="content">
    
    <div class="blog">
        <div class="blog-content">
            <?php $i = 0; ?>
            @foreach($posts as $post)
            <?php $i++; ?>
            <article class="blog-list">
                <div class="thumbnail"><img src="http://lorempixel.com/800/300/business?{{ time() + $i }}"></div>
                <div class="title"><a href="{{ route('blog', ['blog' => trans('url.blog'), 'term' => $post->lang()->slug]) }}">{{ $post->lang()->title }}</a></div>
                <div class="sub">Category : {{ $post->categories[0]->name }} | Posted on 28 March 2016</div>
                <div class="desc">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. 
                </div>
            </article>
            @endforeach

            @include('fragments.pagination', ['paginator' => $posts])
        </div>

        @include('fragments.blog-sidebar')
    </div><!-- END BLOG --> 
</div><!-- END MAIN -->

@stop
