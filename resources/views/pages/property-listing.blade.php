@extends('index')
@section('content')

<section class="breadcrumb" style="background-image:url('assets/img/testimonials-bg.jpg');">
    <div class="breadcrumb-holder">
        <div class="page-title ta-center">
            <h3 class="white">
                {{ trans('word.breadcrumb_search_icon') }}
            </h3>
            <p class="white fw-300">
                Find your property in the different area of Bali
            </p>
        </div>
    </div>
</section>

<!-- MAIN CONTAINER -->
<section id="content"> 
    <div class="home-page">
    
        <!-- LOCATIONS TITLE -->
        <div class="page-title ta-center">
            <h3 class="orange">
                {{ trans('word.bali_villa') }}
            </h3>
            <p>
                {{ trans('word.bali_villa_sub') }}
            </p>
        </div>
    
        <!-- LOCATION BOXES -->
        <section class="locations-search-icon flexbox flexbox-wrap justify-between">
            <div class="single" style="background-image: url('assets/img/canggu.jpg');">
                <h2 class="location-name">{{ trans('word.villa_sell') }}</h2>
                <div class="desc flexbox flexbox-wrap justify-center">
                    <div>
                        <h4><i class="material-icons">home</i>{{ trans('word.villa_sell') }}</h4>
                        <p>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'villas-for-sale',
                'type' => 'less-than-500k']) }}">{{ trans('word.less_than_500k') }}</a>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'villas-for-sale',
                'type' => 'more-than-500k']) }}">{{ trans('word.more_than_500k') }}</a>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'villas-for-sale',
                'tag' => 'home-and-retirement']) }}">{{ trans('word.home_and_retirement') }}</a>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'villas-for-sale',
                'tag' => 'beachfront-property']) }}">{{ trans('word.beachfront_property') }}</a>
                            
                        </p>    
                    </div>
                </div>
            </div>
            <div class="single" style="background-image: url('assets/img/ubud.jpg');">
                <h2 class="location-name">{{ trans('word.villa_rental') }}</h2>
                <div class="desc flexbox flexbox-wrap justify-center">
                    <div>
                        <h4><i class="material-icons">home</i>{{ trans('word.villa_rental') }}</h4>
                        <p>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'villas-for-rent',
                'type' => 'short-term']) }}">{{ trans('word.short_term') }}</a>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'villas-for-rent',
                'type' => 'long-term']) }}">{{ trans('word.long_term') }}</a>
                            

                        </p>    
                    </div>
                </div>
            </div>
            <div class="single" style="background-image: url('assets/img/jimbaran.jpg');">
                <h2 class="location-name">{{ trans('word.land') }}</h2>
                <div class="desc flexbox flexbox-wrap justify-center">
                    <div>
                        <h4><i class="material-icons">pin_drop</i>{{ trans('word.land') }}</h4>
                        <p>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'land',
                'type' => 'less-than-500k']) }}">{{ trans('word.less_than_500k') }}</a>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'land',
                'type' => 'more-than-500k']) }}">{{ trans('word.more_than_500k') }}</a>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'land',
                'tag' => 'beachfront-property']) }}">{{ trans('word.beachfront_property') }}</a>
                        </p> 
                    </div>
                </div>
            </div>
        </section>

        <div class="page-title ta-center">
            <h3 class="orange">
                {{ trans('word.browse_title') }}
            </h3>
            <p>
                 {{ trans('word.browse_sub_title') }}
            </p>
        </div>

        <section class="suggest flexbox flexbox-wrap justify-between">
            <a href="facebook.com">
            <div class="suggest-item flexbox flexbox-wrap">
                <div class="suggest-pict"><img src="assets/img/ubud.jpg"></div>
                <div class="suggest-title">
                <span>{{ trans('word.amed') }} <br>
                    {{ trans('word.lets_go') }} <i class="fa fa-arrow-right"></i> </span>
                </div>
            </div>
            </a>

            <a href="facebook.com">
            <div class="suggest-item flexbox flexbox-wrap">
                <div class="suggest-pict"><img src="assets/img/ubud.jpg"></div>
                <div class="suggest-title">
                <span>{{ trans('word.amed') }} <br>
                    {{ trans('word.lets_go') }} <i class="fa fa-arrow-right"></i> </span>
                </div>
            </div>
            </a>

            <a href="facebook.com">
            <div class="suggest-item flexbox flexbox-wrap">
                <div class="suggest-pict"><img src="assets/img/ubud.jpg"></div>
                <div class="suggest-title">
                <span>{{ trans('word.amed') }} <br>
                    {{ trans('word.lets_go') }} <i class="fa fa-arrow-right"></i> </span>
                </div>
            </div>
            </a>

            <a href="facebook.com">
            <div class="suggest-item flexbox flexbox-wrap">
                <div class="suggest-pict"><img src="assets/img/ubud.jpg"></div>
                <div class="suggest-title">
                <span>{{ trans('word.amed') }} <br>
                    {{ trans('word.lets_go') }} <i class="fa fa-arrow-right"></i> </span>
                </div>
            </div>
            </a>

            <a href="facebook.com">
            <div class="suggest-item flexbox flexbox-wrap">
                <div class="suggest-pict"><img src="assets/img/ubud.jpg"></div>
                <div class="suggest-title">
                <span>{{ trans('word.amed') }} <br>
                    {{ trans('word.lets_go') }} <i class="fa fa-arrow-right"></i> </span>
                </div>
            </div>
            </a>

            <a href="facebook.com">
            <div class="suggest-item flexbox flexbox-wrap">
                <div class="suggest-pict"><img src="assets/img/ubud.jpg"></div>
                <div class="suggest-title">
                <span>{{ trans('word.amed') }} <br>
                    {{ trans('word.lets_go') }} <i class="fa fa-arrow-right"></i> </span>
                </div>
            </div>
            </a>

            <a href="facebook.com">
            <div class="suggest-item flexbox flexbox-wrap">
                <div class="suggest-pict"><img src="assets/img/ubud.jpg"></div>
                <div class="suggest-title">
                <span>{{ trans('word.amed') }} <br>
                    {{ trans('word.lets_go') }} <i class="fa fa-arrow-right"></i> </span>
                </div>
            </div>
            </a>

            <a href="facebook.com">
            <div class="suggest-item flexbox flexbox-wrap">
                <div class="suggest-pict"><img src="assets/img/ubud.jpg"></div>
                <div class="suggest-title">
                <span>{{ trans('word.amed') }} <br>
                    {{ trans('word.lets_go') }} <i class="fa fa-arrow-right"></i> </span>
                </div>
            </div>
            </a>

            <a href="facebook.com">
            <div class="suggest-item flexbox flexbox-wrap">
                <div class="suggest-pict"><img src="assets/img/ubud.jpg"></div>
                <div class="suggest-title">
                <span>{{ trans('word.amed') }} <br>
                    {{ trans('word.lets_go') }} <i class="fa fa-arrow-right"></i> </span>
                </div>
            </div>
            </a>

            <a href="facebook.com">
            <div class="suggest-item flexbox flexbox-wrap">
                <div class="suggest-pict"><img src="assets/img/ubud.jpg"></div>
                <div class="suggest-title">
                <span>{{ trans('word.amed') }} <br>
                    {{ trans('word.lets_go') }} <i class="fa fa-arrow-right"></i> </span>
                </div>
            </div>
            </a>

            <a href="facebook.com">
            <div class="suggest-item flexbox flexbox-wrap">
                <div class="suggest-pict"><img src="assets/img/ubud.jpg"></div>
                <div class="suggest-title">
                <span>{{ trans('word.amed') }} <br>
                    {{ trans('word.lets_go') }} <i class="fa fa-arrow-right"></i> </span>
                </div>
            </div>
            </a>

            <a href="facebook.com">
            <div class="suggest-item flexbox flexbox-wrap">
                <div class="suggest-pict"><img src="assets/img/ubud.jpg"></div>
                <div class="suggest-title">
                <span>{{ trans('word.amed') }} <br>
                    {{ trans('word.lets_go') }} <i class="fa fa-arrow-right"></i> </span>
                </div>
            </div>
            </a>

            <a href="facebook.com">
            <div class="suggest-item flexbox flexbox-wrap">
                <div class="suggest-pict"><img src="assets/img/ubud.jpg"></div>
                <div class="suggest-title">
                <span>{{ trans('word.amed') }} <br>
                    {{ trans('word.lets_go') }} <i class="fa fa-arrow-right"></i> </span>
                </div>
            </div>
            </a>

            <a href="facebook.com">
            <div class="suggest-item flexbox flexbox-wrap">
                <div class="suggest-pict"><img src="assets/img/ubud.jpg"></div>
                <div class="suggest-title">
                <span>{{ trans('word.amed') }} <br>
                    {{ trans('word.lets_go') }} <i class="fa fa-arrow-right"></i> </span>
                </div>
            </div>
            </a>

            <a href="facebook.com">
            <div class="suggest-item flexbox flexbox-wrap">
                <div class="suggest-pict"><img src="assets/img/ubud.jpg"></div>
                <div class="suggest-title">
                <span>{{ trans('word.amed') }} <br>
                    {{ trans('word.lets_go') }} <i class="fa fa-arrow-right"></i> </span>
                </div>
            </div>
            </a>

            <a href="facebook.com">
            <div class="suggest-item flexbox flexbox-wrap">
                <div class="suggest-pict"><img src="assets/img/ubud.jpg"></div>
                <div class="suggest-title">
                <span>{{ trans('word.amed') }} <br>
                    {{ trans('word.lets_go') }} <i class="fa fa-arrow-right"></i> </span>
                </div>
            </div>
            </a>

        </section>
        
    </div>
</section>

@stop

@section('scripts')
@stop
