@extends('index')
@section('content')

<!-- breadcrumb -->
<section class="breadcrumb" style="background-image:url('assets/img/testimonials-bg.jpg');">
    <div class="breadcrumb-holder">
        <div class="page-title ta-center">
            <h3 class="white">
                Account Dashboard
            </h3>
            <p class="white fw-300">
                Find your property in the different area of Bali
            </p>
        </div>
    </div>
</section>

<!-- MAIN CONTAINER -->
<div id="content"> 
    
    <section class="bottom-section flexbox flexbox-wrap justify-between">
    <!-- BLOG & TESTIMONIALS -->
        @include('fragments.account-sidebar')

        <section class="w66">

            <div class="account-right-content">
                <div class="dashboard-holder">
                    <div class="overview-holder flexbox flexbox-wrap">
                        <div class="wishlist-list-holder">
                            <a href="{{ route('account.wishlist', ['account' => trans('url.account'), 'wishlist' => trans('url.wishlist')]) }}">
                                <div class="wishlist-main">
                                    <i class="fa fa-heart"></i> <br> <span class="numeric">{{ count($customer->wishlists) }}</span> <br><br> <span class="title">Wishlist</span>
                                </div>
                            </a>
                        </div>
                        <div class="villa-list-holder">
                            <a href="{{ route('account.wishlist', ['account' => trans('url.account'), 'wishlist' => trans('url.wishlist'), 'category' => 'villa']) }}">
                            <div class="villa-main">
                                <i class="fa fa-home"></i> <br> <span class="numeric">{{ count($customer->wishlists) }}</span> <br><br> <span class="title">Villa</span>
                            </div>
                            </a>
                        </div>
                        <div class="land-list-holder">
                            <a href="{{ route('account.wishlist', ['account' => trans('url.account'), 'wishlist' => trans('url.wishlist'), 'category' => 'land']) }}">
                            <div class="land-main">
                                <i class="fa fa-map-marker"></i> <br> <span class="numeric">{{ count($customer->wishlists) }}</span> <br><br> <span class="title">Land</span>
                            </div>
                            </a>
                        </div>
                    </div>

                    <div class="map-holder">
                        <div id="map-contact" style="width:100%; height: 400px;"></div>
                    </div>
                </div>
            </div>

        </section>
    </section>
</div>

@endsection

@section('scripts')
<script>
    Kibarer.initContact();
</script>
@endsection
