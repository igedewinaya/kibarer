@extends('index')

@section('styles')
<style type="text/css">
.kesato-form .state-error {
    display: block!important;
    margin-top: 6px;
    padding: 0 3px;
    font-family: Arial, Helvetica, sans-serif;
    font-style: normal;
    line-height: normal;
    font-size: 0.85em;
    color: #DE888A;
}
</style>
@endsection

@section('content')
<div id="content">
  <div class="account">
    
    <div class="register-box" style="padding-left: 0;">
      <img src="assets/img/canggu.jpg" style="width:100%;">
    </div>

    <div class="register-box">
      <h1>{{ trans('word.register') }}</h1>
        <div class="kesato-form" style="padding-bottom: 50px; margin-bottom: 50px; border-bottom: 1px solid #DDD;">

                {!! Form::open(array('url' => route('register.store', trans('url.register')), 'id' => 'validation-form')) !!}
                <div class="form-body">
                      <div class="frm-row">
                          <div class="section colm colm6">
                              <label for="reg_firstname" class="field prepend-icon">
                                  <input type="text" name="firstname" id="reg_firstname" class="pl-15 gui-input" placeholder="First name">
                              </label>
                        </div>
                          <div class="section colm colm6">
                              <label for="reg_lastname" class="field prepend-icon">
                                  <input type="text" name="lastname" id="reg_lastname" class="gui-input" placeholder="Last name">
                              </label>
                        </div>
                      </div>
                    <div class="section">
                            <label for="reg_phone" class="field prepend-icon">
                                <input type="tel" name="phone" id="reg_phone" class="gui-input phone-group" placeholder="Phone number">
                            </label>
                      </div>
                    <div class="section">
                        <label for="reg_email" class="field prepend-icon">
                            <input type="email" name="email" id="reg_email" class="gui-input" placeholder="Email address">
                          </label>
                      </div>
                    <div class="section">
                        <label for="reg_password" class="field prepend-icon">
                            <input type="password" name="password" id="reg_password" class="gui-input" placeholder="Password">
                          </label>
                      </div>
                    <div class="section">
                        <label for="reg_repeatPassword" class="field prepend-icon">
                            <input type="password" name="password_confirmation" id="reg_repeatPassword" class="gui-input" placeholder="Confirmation password">
                          </label>
                      </div>  

                    <div class="section">
                            <label for="reg_phone" class="field prepend-icon">
                                <input style="padding-left: 30px" type="text" name="location" id="pac-input-filter" class="gui-input phone-group" placeholder="Location">
                                <label for="firstname" class="field-icon"><i
                                  class="fa fa-map-marker"></i></label>
                               </label>
                            </label>
                      </div>
                  </div><!-- end .form-body section -->

                  <div class="section">
                      <label for="" class="field prepend-icon">
                          {!! Recaptcha::render() !!}
                      </label>
                  </div>
                  
                  <div class="form-footer">
                    <button type="submit" class="button btn-primary">{{ trans('word.register') }}</button>
                    <!--
                      <button type="reset" class="button"> Cancel </button>
                      -->
                  </div>
              
              {!! Form::close() !!}
          </div><!-- END KESATO FORM -->
          
      <h1>Already have an account ?</h1>
      <p style="line-height: 20px;margin-top:0;">
        By creating an account with our store, you will be able to move through the checkout process faster, store multiple shipping addresses, view and track your orders in your account and more.
      </p>
      <br>
      <a href="{{ route('login', trans('url.login')) }}" class="primary-link" style="margin-top:12px;float:left;">Login now</a>
          
    </div><!-- ENG REGISTER BOX -->
    
  </div><!-- END ACCOUNT -->
</div><!-- END MAIN -->
@endsection

@section('scripts')
<script src='https://www.google.com/recaptcha/api.js'></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBdeZlfIdUJZR6TA1TSCQS6v5HcLdlbZR8&callback=initMap&libraries=places"></script>
<script type="text/javascript">

   function initMap() {   
     var input = document.getElementById('pac-input-filter'),
     searchBox = new google.maps.places.SearchBox(input);
   }



  $(".select-city").select2({
    placeholder: "Select a city",
    allowClear: true
  });

  $(document).ready(function() {
    
    $('#validation-form').submit(function(event) {
      /* Act on the event */
      event.preventDefault();

      $('.state-error').remove();

      $('button[type=submit]').html('Sending...');

      $('button[type=submit]').prop('disabled', true);

      var url = $(this).attr('action');
      var data = $(this).serialize();

      $.post(url, data, function(data, textStatus, xhr) {
        /*optional stuff to do after success */

        if (data.status == 200) {

          window.location.href = "{{ route('register_success') }}";

        }

        if (data.status == 500) {

          if (data.monolog.message.email)
            $('input[name=email]').closest('.section').append('<em for="reg_email" class="state-error">'+ data.monolog.message.email +'</em>');

          if (data.monolog.message.password)
            $('input[name=password]').closest('.section').append('<em for="reg_password" class="state-error">'+ data.monolog.message.password +'</em>');

          if (data.monolog.message.firstname)
            $('input[name=firstname]').closest('.section').append('<em for="reg_firstname" class="state-error">'+ data.monolog.message.firstname +'</em>');

          if (data.monolog.message.location)
            $('input[name=location]').closest('.section').append('<em for="reg_location" class="state-error">'+ data.monolog.message.location +'</em>');

          if (data.monolog.message['g-recaptcha-response']) {
                if(data.monolog.message['g-recaptcha-response'].length > 1){
                    var notif = '<em for="reg_recaptcha" class="state-error">' + data.monolog.message['g-recaptcha-response'][1] + '</em>';
                }
                else{
                    var notif = '<em for="reg_recaptcha" class="state-error">' + data.monolog.message['g-recaptcha-response'][0] + '</em>';
                }
                $('.g-recaptcha').closest('.section').append(notif);
            }

        }

        $('button[type=submit]').html('Register');

        $('button[type=submit]').prop('disabled', false);

      });

    });

  });

</script>

@endsection
