@extends('index')

@section('content')
<!-- breadcrumb -->
<section class="breadcrumb" style="background-image:url('assets/img/testimonials-bg.jpg');">
    <div class="breadcrumb-holder">
        <div class="page-title ta-center">
            <h3 class="white">
                {{ trans('word.breadcrumb_lawyer') }}
            </h3>
            <p class="white fw-300">
                Find your property in the different area of Bali
            </p>
        </div>
    </div>
</section>

<!-- MAIN CONTAINER -->
<div id="content" class="lawyer-and-notary"> 

    <!-- BLOG & TESTIMONIALS -->
    <section class="bottom-section flexbox flexbox-wrap justify-between">
        <section class="w33 sidebar">
            <div class="lawyer-notary-categories flexbox flexbox-wrap justify-between">
                <aside class="blog-sidebar">
                        <ul class="box" id="box">
                            <li>
                                <div>
                                    <h6 class="title">Visa Categories</h6>
                                    <span class="title-border-bottom"></span> 
                                </div>
                                <ul class="list-link">
                                    <li>
                                     <a href="#social-culture-visa">SOCIAL & CULTURE VISA</a>
                                     <div class="toggle">&nbsp;</div>
                                    </li>
                                    <li>
                                     <a href="#single-entry-business-visa">SINGLE ENTRY BUSINESS VISA</a>
                                     <div class="toggle">&nbsp;</div>
                                    </li>
                                    <li>
                                     <a href="#family-kitas">FAMILY KITAS</a>
                                     <div class="toggle">&nbsp;</div>
                                    </li>
                                    <li>
                                     <a href="#epo">EPO (EXIT PERMIT ONLY)</a>
                                     <div class="toggle">&nbsp;</div>
                                    </li>
                                    <li>
                                     <a href="#local-company">LOCAL COMPANY (PT)</a>
                                     <div class="toggle">&nbsp;</div>
                                    </li>
                                    <li>
                                     <a href="#retirement-visa">RETIREMENT VISA</a>
                                     <div class="toggle">&nbsp;</div>
                                    </li>
                                    <li>
                                     <a href="#multiple-entry-business-visa">MULTIPLE ENTRY BUSINESS VISA</a>
                                     <div class="toggle">&nbsp;</div>
                                    </li>
                                    <li>
                                     <a href="#working-visa-kitas">WORKING VISA KITAS</a>
                                     <div class="toggle">&nbsp;</div>
                                    </li>
                                    <li>
                                     <a href="#pma">PMA (FOREIGN INVESTMENT COMPANY)</a>
                                     <div class="toggle">&nbsp;</div>
                                    </li>
                                    <li>
                                     <a href="#imb">IMB (BUILDING PERMIT)</a>
                                     <div class="toggle">&nbsp;</div>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <div>
                                    <h6 class="title">Services of Notary</h6>
                                    <span class="title-border-bottom"></span> 
                                </div>
                                <ul class="list-link">
                                    <li>
                                     <a href="#notary-consultation">NOTARY CONSULTATION</a>
                                     <div class="toggle">&nbsp;</div>
                                    </li>
                                    <li>
                                     <a href="#amendements-of-company-deed">AMENDEMENTS OF COMPANY DEED</a>
                                     <div class="toggle">&nbsp;</div>
                                    </li>
                                    <li>
                                     <a href="#purchased-leasehold-property">PURCHASED LEASEHOLD PROPERTY</a>
                                     <div class="toggle">&nbsp;</div>
                                    </li>
                                    <li>
                                     <a href="#deed-of-granting-mortgage">DEED OF GRANTING MORTGAGE</a>
                                     <div class="toggle">&nbsp;</div>
                                    </li>
                                    <li>
                                     <a href="#power-of-attorney">POWER OF ATTORNEY</a>
                                     <div class="toggle">&nbsp;</div>
                                    </li>
                                    <li>
                                     <a href="#legalization-document">LEGALIZATION DOCUMENT</a>
                                     <div class="toggle">&nbsp;</div>
                                    </li>
                                    <li>
                                     <a href="#company-deed">COMPANY DEED</a>
                                     <div class="toggle">&nbsp;</div>
                                    </li>
                                    <li>
                                     <a href="#due-diligence">DUE DILIGENCE</a>
                                     <div class="toggle">&nbsp;</div>
                                     </li>
                                    <li>
                                     <a href="#purchased-freehold-property">PURCHASED FREEHOLD PROPERTY</a>
                                     <div class="toggle">&nbsp;</div>
                                    </li>
                                    <li>
                                     <a href="#roya">ROYA</a>
                                     <div class="toggle">&nbsp;</div>
                                    </li>
                                    <li>
                                     <a href="#legalize-inventory">LEGALIZE INVENTORY</a>
                                     <div class="toggle">&nbsp;</div>
                                    </li>
                                    <li>
                                     <a href="#watermark-document">WATERMARK DOCUMENT</a>
                                     <div class="toggle">&nbsp;</div>
                                    </li>
                                </ul>
                            </li>
                            <li>
                               <div>
                                    <h6 class="title">Services of Lawyer</h6>
                                    <span class="title-border-bottom"></span> 
                               </div>                         
                               <ul class="list-link">
                                <li>
                                 <a href="#litigation-non-litigation">LITIGATION & NON LITIGATION</a>
                                 <div class="toggle">&nbsp;</div>
                                </li>
                               </ul>
                            </li>
                        </ul>    

                </aside>

            </div>
        </section>
   
        <section class="w66 main">
           
            <div class="lawyer-notary-box-content flexbox flexbox-wrap justify-between" id="social-culture-visa">
               <div class="content">
                   <h6 class="title"> SOCIAL & CULTURE VISA <span>|</span> VALID FOR 6 MONTH</h6>
                   <p>Untitle The purpose of this visa is to visit friends / relationships, social - cultural or educational exchange. With this visa you are not allowed to do business activities in Indonesia or to take up employment. Sponsorship is required by an Indonesian citizen that have Bali ID card. You need to supply the following documents: The social & culture visa ( Visa Index 211) is a single entry visa that cannot be used for working but for all activities which have connection with government, tourism, social and culture, it is given 60 (sixty) days, then extendable 4 times every 30 days. This visa can be sponsored individually. <br> <br>
                      
                      <strong>This visa is suitable for :</strong>
                          <ul>
                           <li>Tourism</li>
                           <li>Social and family</li>
                           <li>Between educational foundation</li>
                           <li>Journalist that already has permit from the authority</li>
                           <li>Conducting business conversation such as sale and purchase of goods and services as well as production or goods quality supervision</li>
                           <li>Attending international exhibition</li>
                           <li>Joining a meeting that is organized by the head office or its representative office in Indonesia</li>
                       </ul>
                   
                   
                    <strong>Your Indonesian sponsor needs to supply : </strong>
                       <ul>
                           <li>Copy of sponsor identity card whose residence is in Bali</li>
                       </ul>
                       <strong>You need to supply :</strong>
                       <ul>
                           <li>Copy of your passport</li>
                           <li>2 (two) passport sized photographs (4 x 6) red background Extension: After the first 60 days you need to extend your visa every 30 days. For 1st and 2nd extension will took up to 7 working days process and for 3rd and 4th extension will took up to 15 working days process. For each extension you may requested to come to immigration office to take picture.</li>
                       </ul>
                    
                    <strong>Cost :</strong>
                    
                    <table>
                        <tr>
                            <td>Prepare Sponsor Letter and arrange with agent in Singapore</td>                            
                            <td>USD 50.00</td>
                        </tr>
                        <tr>
                            <td>Sponsor to be provided by the client</td> 
                            <td>USD 150.00</td>
                        </tr>
                        <tr>
                            <td>Sponsor by Kibarer Property</td> 
                            <td>USD 100.00 per extension</td>
                        </tr>
                        <tr>
                            <td>Extension 1st / 2nd</td> 
                            <td>USD 120.00 per extension</td>
                        </tr>
                        <tr>
                            <td>Extension 3rd / 4th</td> 
                            <td>USD 400.00 excludes sponsor</td>
                        </tr>
                        <tr>
                            <td>Full Package</td> 
                            <td>USD 500.00 Includes sponsor</td>
                        </tr>
                    </table>
                   </p>
               </div>
            </div>
            
            <div class="lawyer-notary-box-content flexbox flexbox-wrap justify-between disable" id="single-entry-business-visa">
                <div class="content">
                    <h6 class="title"> SINGLE ENTRY BUSINESS VISA <span>|</span> VALID FOR 6 MONTH</h6>
                    <p>The purpose of this visa is doing business activities in Indonesia which do not involve taking up employment. Sponsorship is required by an Indonesian company. <strong>You need provide the following documents : </strong> <br> <br>

                        <strong>Company sponsor data :</strong>
                    <ul>
                        <li>Copy of the company’s deed</li>
                        <li>Copy of Certificate of Ministry of Justice</li>
                        <li>Copy of the company’s business license letter (SIUP & TDP)</li>
                        <li>Copy of Office License and Hinder Ordonnantie (SITU & HO)</li>
                        <li>Copy of the company’s locality letter (SKTU)</li>
                        <li>Copy of the company’s tax number (NPWP)</li>
                        <li>Copy of the director’s identity card</li>
                        <li>5 company letters head, signed by the one of the manager and sealed with the company stamp</li>
                    </ul>
                    </p>

                    <strong>Foreigner data : </strong>
                    <ul>
                        <li>Copy of the foreigner’s passport</li>
                        <li>2 (two) passport sized photograph (4 X 6)</li>
                    </ul>
                    <strong>The procedure :</strong> <br> <br>
                    <p>
                    When we have received your documents we will start to process. The above document needs to be sending to Jakarta for approval. A fee is payable in local currencies for the visa application and duration of stay is depending on the embassies/ consulate where the visa is applied but we highly recommend you to go to Singapore as you can use an agent for one day service. Extension: After the first 60 days you need to extend your visa every 30 days. For 1st and 2nd extension will took up to 7 working days process and for 3rd and 4th extension will took up to 15 working days process. For each extension you may requested to come to immigration office to take picture. <br> <br>
                    </p>
                    <strong>Cost :</strong>

                    <table>
                        <tr>
                            <td>Prepare Sponsor Letter and arrange with agent in Singapore</td>                            
                            <td>USD 50.00</td>
                        </tr>
                        <tr>
                            <td>Sponsor to be provided by the client</td> 
                            <td>USD 150.00</td>
                        </tr>
                        <tr>
                            <td>Sponsor by Kibarer Property</td> 
                            <td>USD 100.00 per extension</td>
                        </tr>
                        <tr>
                            <td>Extension 1st / 2nd</td> 
                            <td>USD 120.00 per extension</td>
                        </tr>
                        <tr>
                            <td>Extension 3rd / 4th</td> 
                            <td>USD 400.00 excludes sponsor</td>
                        </tr>
                        <tr>
                            <td>Full Package</td> 
                            <td>USD 500.00 Includes sponsor</td>
                        </tr>
                    </table>
                    
                </div>
            </div>
            
            <div class="lawyer-notary-box-content flexbox flexbox-wrap justify-between disable" id="family-kitas">
                <div class="content">
                    <h6 class="title"> FAMILY KITAS <span>|</span> VALID FOR 1 (ONE) YEAR</h6>
                    <p>KITAS for spouse and/or member of your family. Valid for 1 year and extendable. <br> <br>

                        <strong>Documents required :</strong>
                    <ul>
                        <li>Copy of the wife’s and children’s passport</li>
                        <li>Copy of the marriage certificate</li>
                        <li>Copy of the children’s birth certificate</li>
                        <li>Passport sized photograph (4 X 6 = 8 pieces, 3 X 4 = 6 pieces, 2 X 3 = 6 pieces)</li>
                        <li>Copy of the family register</li>
                    </ul>
                    </p>

                <strong>The procedure :</strong> <br> <br>
                <p>
                    When we received your documents we will start to process. The documents have to be send to Jakarta for approval. If your application is approved you will receive a telex visa, this process can take up to 30 days before a decisions is made and the visa is granted. When telex visa is issued you can apply for the visa at an Indonesian embassy or consulate overseas near to you. A fee is payable in local currencies for the visa application and duration of stay – processing depends on the embassy / consulate where the visa is requested. When you get your visa, you must return to Indonesia straightaway and report to immigration within a maximum of 7 days from the arrival date. <br> <br>
                </p>

                <strong>The process can take up to 30 days in immigration and then you get the following documents : </strong>
                <ul>
                    <li>KITAS, limited stay permit card</li>
                    <li>MERP, Multiple Exit Re-Entry Permit</li>
                    <li>STM, report certificate from the police</li>
                </ul>

                <p>
                    The above documents are valid for one ( 1 ) year. <strong>What if I need to go overseas? </strong> You must apply exit re-entry permit in Immigration, which we will put this in to the package in the working visa. Multiple exit re-entry permit valid for 11 months will took about 3 working days to process.
                    <br> <br>
                </p>

                <strong>Cost :</strong>

                <table>
                    <tr>
                        <td>Starts from </td>                            
                        <td>USD 890.00/ Person </td>
                    </tr>
                </table>
                <p>The package cost includes multiple exit re-entry permits.</p>
                </div>
            </div>
            
            <div class="lawyer-notary-box-content flexbox flexbox-wrap justify-between disable" id="epo">
                <div class="content">
                    <h6 class="title"> EPO (EXIT PERMIT ONLY) </h6>
                        <p>EPO is needed if you don’t want to extend your Working or Retirement Kitas or you want to change the sponsor. <br> <br>

                            <strong>The requirement :</strong>
                            <ul>
                                <li>Original passport</li>
                                <li>Original blue book</li>
                                <li>Original KITAS</li>
                                <li>Original DPKK slip</li>
                                <li>Copy of ticket</li>
                                <li>10 sheets of empty letterhead which already signed and stamped by director or sponsor</li>
                            </ul>
                        </p>

                    <strong>Copy of company’s document : </strong>
                    <ul>
                        <li>TDP</li>
                        <li>NPWP</li>
                        <li>SIUP</li>
                        <li>Certificate from notary</li>
                        <li>Company certificate from notary if there’s any change</li>
                        <li>Certificate of business domicile (SKTU)</li>
                    </ul>

                    <strong>Cost :</strong>

                    <table>
                        <tr>
                            <td>EPO (EXIT PERMIT ONLY) Cost</td>                            
                            <td>USD 120.00 </td>
                        </tr>
                    </table>
                </div>
            </div>
            
            <div class="lawyer-notary-box-content flexbox flexbox-wrap justify-between disable" id="local-company">
                <div class="content">
                    <h6 class="title"> LOCAL COMPANY (PT) </h6>
                    <p>The company can sponsored foreign employees for their working permit and also can sponsored your foreign client and or visitor for business Visa. <br> <br>

                    <strong>Requirements for PT business license in Bali :</strong>
                    <ul>
                        <li>Name or PT Enterprises, along Address Phone Number Office, Field conducted the total capital of the Company, the division of Stock Composition and arrangement of the Board of Directors</li>
                        <li>Photograph Copy of ID and Tax Number (NPWP) of minimum 2 shareholders</li>
                        <li>Lease Agreement and IMB of the building / office</li>
                    </ul>
                    </p>

                    <strong>Documents received for governance and business license PT in Bali : </strong>
                    <ul>
                        <li>Company Deed from Public Notary</li>
                        <li>Statement from the State (Berita Negara)</li>
                        <li>Certificate of Ministry of Justice</li>
                        <li>Domicile of Company (SKTU)</li>
                        <li>Company Tax Number (NPWP)</li>
                        <li>SITU and HO</li>
                        <li>Company Listed Certification (TDP & SIUP)</li>
                        <li>UKL/UPL – depends on the business nature of the company</li>
                    </ul>

                    <strong>Cost :</strong>

                    <table>
                        <tr>
                            <td>Start from</td>                            
                            <td>USD 1,800.00 </td>
                        </tr>
                    </table>
                </div>
            </div>
            
            <div class="lawyer-notary-box-content flexbox flexbox-wrap justify-between disable" id="retirement-visa">
                <div class="content">
                    <h6 class="title"> RETIREMENT VISA <span>|</span> VALID FOR 1 (ONE) YEAR </h6>
                    <p>The retirement visa (Visa Index 311) is a temporary stay permit for retirement purposes. The process time will take up to 2 months. Retirement Visa for Bali Indonesia If you are over 55 years and are able to support yourself fully you may be allowed to retire in Indonesia especially in Bali on an extended temporary basis. <br> <br>

                        <strong>To be eligible for a visa for retirement you must :</strong>
                        <ul>
                            <li>Be 55 years or older</li>
                            <li>Have no intention to work in Indonesia</li>
                            <li>Be in good health and character</li>
                        </ul>
                        </p>

                    <strong>The requirement ( The following document must be supplied before your visa application can be processed ) : </strong>
                    <ul>
                        <li>Copy of your passport which are valid for at least one year</li>
                        <li>Proof of holding a pension or deposit – current month of Bank statement</li>
                        <li>Health and life Insurance</li>
                        <li>Photographs with Red Background (soft copy)</li>
                        <li>Have accommodation in Bali with rental agreement</li>
                        <li>Any applicant whose spouse wishes to apply for a Retirement Visa must provide a copy of their marriage certificate</li>
                        <li>Employment Letter. You must employ at least one Indonesian citizen for assistances kind of work, such as nursing or housekeeping during your stay in Indonesia. In that regard, you must submit a letter stating that you are currently employing or intend to employ an Indonesian assistant. ID of the employee.</li>
                    </ul>
                    <p>
                        <strong>How long may I stay?</strong> If your application is approved, you will be allowed to come to Indonesia initially for one year and your visa can be extended every year to a total of 5 years. When finished you will receive the following documents :
                        <ul>
                            <li>KITAS, limited stay permit card</li>
                            <li>MERP, Multiple Exit Re-Entry Permit</li>
                            <li>STM, report certificate from the police</li>
                        </ul>
                    </p>
                    <p>
                        <strong>What if I need to go overseas?</strong> You should check that the availability of your KITAS (Limited Stay Permit Card) is still valid for applying the following Exit Re-entry Permits. Multiple Exit re-entry permit that is valid for 11 months. What if I do not extend the visa? After you finished your visa in one year and do not want to extend, you must apply for an Exit Permit Only (EPO) <br><br>
                   </p>
                   
                    <strong>Cost :</strong>

                    <table>
                        <tr>
                            <td>Retirement Visa</td>                            
                            <td>USD 1,100.00 </td>
                        </tr>
                    </table>
                </div>
            </div>
            
            <div class="lawyer-notary-box-content flexbox flexbox-wrap justify-between disable" id="multiple-entry-business-visa">
                <div class="content">
                    <h6 class="title"> MULTIPLE ENTRY BUSINESS VISA <span>|</span> VALID FOR 12 MONTH </h6>
                    <p>The purpose of this visa is doing business activities in Indonesia which do not involve taking up employment. Sponsorship is required by an Indonesian company for instance in Bali. You need provide the following documents. <br> <br>

                        <strong>Company sponsor data :</strong>
                        <ul>
                            <li>Copy of the company’s deed</li>
                            <li>Copy of Certificate of Ministry of Justice</li>
                            <li>Copy of the company’s business license letter (SIUP &TDP)</li>
                            <li>Copy of Office License and Hinder Ordonnantie (SITU & HO)</li>
                            <li>Copy of the company’s locality letter (SKTU)</li>
                            <li>Copy of the company’s tax number (NPWP)</li>
                            <li>Copy of the company's bank account statements</li>
                            <li>Copy of the director’s identity card</li>
                            <li>Two company letters head, signed by the director and sealed with the company stamp</li>
                        </ul>
                        </p>

                        <strong>Foreigner data :</strong>
                        <ul>
                            <li>Copy of the foreigner’s passport</li>
                            <li>2 (two) passport sized photograph (4 X 6)</li>
                        </ul>
                        <p>
                            <strong>The procedure : </strong> When we have received your documents we will start to process. The above document needs to be sending to Jakarta for approval. If you application is approved you will receive telex visa, this process can take up 15 working days before a decision is made and the visa granted. A fee is payable in local currencies for the visa application and duration of stay is depending on the embassies/ consulate where the visa is applied but we highly recommend you to go to Singapore as you can use an agent for one day service. BUT you only can stay in Bali maximum 60 days.
                        </p>

                    <strong>Cost :</strong>

                    <table>
                        <tr>
                            <td>The service is include prepare and arrange with the related departments in Jakarta to have your telex and arrange with agent in Singapore</td>                            
                            <td>USD 500.00 </td>
                        </tr>
                    </table>
                </div>
            </div>
            
<div class="lawyer-notary-box-content flexbox flexbox-wrap justify-between disable" id="working-visa-kitas">
                <div class="content">
                    <h6 class="title"> WORKING VISA KITAS <span>|</span> VALID FOR 1 (ONE) YEAR </h6>
                    <p>The working visa or kitas is a temporary stay visa used for working purposes in Indonesia and is valid for a period of 1 (one) year. The process will take up to 2 months. <br> <br>

                        <strong>his visa is suitable for :</strong>
                        <ul>
                            <li>Working as an expert with temporary stay permit</li>
                            <li>Individual cooperation with Indonesian Government</li>
                            <li>Private Organization Cooperation with Indonesian Government</li>
                            <li>Private Foreign Investment with Indonesian Government</li>
                            <li>oining of working on the boat or floating that operated under the territory of Indonesian Water, territorial sea or continent landing installation as well as Indonesian Exclusive Economy Zone with Temporary Stay Permit (KITAS)</li>
                            <li>Conducting activities that have connection with profession as to receiving payment, sport, artist, entertainment, consultant, trading and other profession that already has permit from the authority</li>
                            <li>Giving guidance and counseling as well as training in application and Industrial Technology Innovation to improve the quality of industrial product design and to have overseas marketing cooperation for Indonesia</li>
                            <li>Conducting the activity in term of commercial movie production that already has permit from the authority</li>
                        </ul>
                        </p>
                        
                        <p>
                            <strong>Working permit visa / KITAS</strong> For foreigners who want to stay and work in Indonesia. You will need a sponsorship from an Indonesian based company.
                        </p>

                    <strong>The company sponsored you needs to supply the following documents to process KITAS :</strong>
                    <ul>
                        <li>Copy of company’s Deed</li>
                        <li>Copy of Certificate of Ministry of Justice</li>
                        <li>Copy of domicile of company (SKTU)</li>
                        <li>Copy of the company’s tax number (NPWP)</li>
                        <li>Copy of Office License and Hinder Ordonnantie (SITU & HO)</li>
                        <li>Copy of the company listed certification (SIUP &TDP)</li>
                        <li>Copy of the director’s identity card</li>
                        <li>Copy of the KTP of an Indonesian work colleague</li>
                        <li>20 company letter heads, signed by the company director and sealed with the company stamp</li>
                        <li>Organization Structure</li>
                    </ul>
                    
                    <strong>Foreign applicants papers needed to process KITAS :</strong>
                    <ul>
                        <li>Copy of the foreigner’s passport</li>
                        <li>Curriculum Vitae/ Resume</li>
                        <li>Copy of Certificate from University</li>
                        <li>Passport photographs with red backgrounds (soft copy)</li>
                    </ul>
                    <p>
                        <strong>Procedure : </strong> The above documents need to be sent to Jakarta for approval. If your application is approved you will receiving a telex visa, this process can take up 40 working days before a decision is made and the visa granted. When the telex visa is issued you can apply for the real visa at the Indonesia embassy or consulate overseas, nearest to you. We highly suggest you to go to Singapore as it is hassle free and will not have complication with the Indonesian embassy there. When you get your visa, you must return to Indonesia and then report to Immigration maximum 7 days within the arrival date. We can do all this for you and guide you in processing. The process can take up within 14 days in Immigration offices <br><br>
                    </p>
                    
                    <strong>When finished you will receive the following documents</strong>
                    <ul>
                        <li>KITAS, limited stay permit card</li>
                        <li>MERP, Multiple Exit Re-Entry Permit</li>
                        <li>STM, report certificate from the police</li>
                        <li>IMTA, working permit</li>
                    </ul>
                    
                    <p>
                        <strong>What if I need to go overseas? </strong> You must apply exit re-entry permit in Immigration, which we will put this in to the package in the working visa. Multiple exit re-entry permit valid for 11 months will took about 3 working days to process. <br><br>
                    </p>
                    
                    <strong>Cost :</strong>

                    <table>
                        <tr>
                            <td>Working Visa KITAS</td>                            
                            <td>USD 890.00 </td>
                        </tr>
                    </table>
                    <p>
                    The package cost includes multiple exit re-entry permit but excludes the tax to the government USD 1200 to the government for your 1 whole year stay in Indonesia.</p>
                </div>
            </div>
            
            <div class="lawyer-notary-box-content flexbox flexbox-wrap justify-between disable" id="pma">
                <div class="content">
                    <h6 class="title"> PMA (FOREIGN INVESTMENT COMPANY) </h6>
                    <p>The difference from Limited Liability Company and PMA (Foreign Investment) is that the PMA should be through Investment Coordinating Board to obtain consent. <br> <br>

                        <strong>Requirements for PMA business license in Bali :</strong>
                        <ul>
                            <li>Photocopy Passport of Foreign shareholder and overseas address</li>
                            <li>Photo Copy of Article of Association (Articles of Incorporation) Foreign Enterprises including address [if shareholder Foreign Legal Entity]</li>
                            <li>Photograph Copy of ID and Tax Number (NPWP) of WNI shareholders</li>
                            <li>The photo Copy Identity (ID / Passport) members of the President and Board of Commissioners</li>
                            <li>Lease Agreement and IMB of the building / office</li>
                            <li>Domicile Certificate owners of office buildings stand</li>
                            <li>Name or PT Enterprises, along Address Phone Number Office, Field conducted the total capital of the Company, the division of Stock Composition and arrangement of the Board of Directors</li>
                        </ul>
                        </p>

                    <strong>Documents received for governance and business license PMA in Bali :</strong>
                    <ul>
                        <li>Registration of Investment (BKPM)</li>
                        <li>Company Deed from Public Notary</li>
                        <li>Domicile of Company</li>
                        <li>Company Tax Number (NPWP)</li>
                        <li>Verification from Minister of Justice</li>
                        <li>Company Listed Certification (Tanda Daftar Perusahaan)</li>
                        <li>Statement from the State (Berita Negara)</li>
                        <li>IUT (Ijin Usaha Tetap) – It’s a certification from the state that you will have after one (1) year operation. This IUT is not included in the package.</li>
                    </ul>

                    <strong>Cost :</strong>

                    <table>
                        <tr>
                            <td>Start from</td>                            
                            <td>USD 3,600.00 </td>
                        </tr>
                    </table>
                </div>
            </div>
            
            <div class="lawyer-notary-box-content flexbox flexbox-wrap justify-between disable" id="imb">
                <div class="content">
                    <h6 class="title"> IMB (BUILDING PERMIT) </h6>
                    <p>IMB is needed either for your residence or business residence. The process will take around 2-3 months and the cost will depends on location and the size of the building. <br> <br>

                        <strong>Requirements for IMB in Bali :</strong>
                        <ul>
                            <li>Complete drawing of the building</li>
                            <li>Building address</li>
                            <li>The propose of the building</li>
                        </ul>
                        </p>
                </div>
            </div>
            
            <div class="lawyer-notary-box-content flexbox flexbox-wrap justify-between disable" id="notary-consultation">
                <div class="content">
                    <h6 class="title"> NOTARY CONSULTATION </h6>
                    <p>You can have consultation with our partner, public notary on creating a business or investment and every aspect of state legal documents. <br> <br>

                        <strong>Cost :</strong>

                        <table>
                            <tr>
                                <td>Start from</td>                            
                                <td>USD 70.00 </td>
                            </tr>
                        </table>
                        </p>
                </div>
            </div>
            
            <div class="lawyer-notary-box-content flexbox flexbox-wrap justify-between disable" id="amendements-of-company-deed">
                <div class="content">
                    <h6 class="title"> AMENDEMENTS OF COMPANY DEED </h6>
                    <p>Made by our partner, public notary. This Amendment is to be made if you want to make changes in your company deed. <br> <br>

                            <strong>Cost :</strong>

                        <table>
                            <tr>
                                <td>Start from</td>                            
                                <td>USD 1.700.00 </td>
                            </tr>
                        </table>
                        </p>
                </div>
            </div>
            
            <div class="lawyer-notary-box-content flexbox flexbox-wrap justify-between disable" id="purchased-leasehold-property">
                <div class="content">
                    <h6 class="title"> PURCHASED LEASEHOLD PROPERTY </h6>
                    <p>This is a package of notary service includes due diligence of the property and purchased deed. <br> <br>

                        <strong>Cost :</strong>

                        <table>
                            <tr>
                                <td>Start from</td>                            
                                <td>USD 300.00 </td>
                            </tr>
                        </table>
                        </p>
                </div>
            </div>
            
            <div class="lawyer-notary-box-content flexbox flexbox-wrap justify-between disable" id="deed-of-granting-mortgage">
                <div class="content">
                    <h6 class="title"> DEED OF GRANTING MORTGAGE </h6>
                    <p>Put in mortgage on title deed of a land. <br> <br>

                        <strong>Cost :</strong>

                        <table>
                            <tr>
                                <td>Start from</td>                            
                                <td>1% of the mortgage value </td>
                            </tr>
                        </table>
                        </p>
                </div>
            </div>
            
            <div class="lawyer-notary-box-content flexbox flexbox-wrap justify-between disable" id="power-of-attorney">
                <div class="content">
                    <h6 class="title"> POWER OF ATTORNEY </h6>
                    <p></p>
                        <strong>Cost :</strong>

                        <table>
                            <tr>
                                <td>Start from</td>                            
                                <td>USD 50.00 </td>
                            </tr>
                        </table>
                        </p>
                </div>
            </div>
            
            <div class="lawyer-notary-box-content flexbox flexbox-wrap justify-between disable" id="legalization-document">
                <div class="content">
                    <h6 class="title"> LEGALIZATION DOCUMENT </h6>
                    <p></p>
                    <strong>Cost :</strong>

                        <table>
                            <tr>
                                <td>Start from</td>                            
                                <td>USD 150.00</td>
                            </tr>
                        </table>
                        </p>
                </div>
            </div>
            
            <div class="lawyer-notary-box-content flexbox flexbox-wrap justify-between disable" id="company-deed">
                <div class="content">
                    <h6 class="title"> COMPANY DEED </h6>
                    <p></p>
                    <strong>Cost :</strong>
                    <p>Made by our partner, public notary. This deed is one of the requirements to starts a new business/ company.</p>
                        <table>
                            <tr>
                                <td>Start from</td>                            
                                <td>USD 1.800.00</td>
                            </tr>
                        </table>
                        </p>
                </div>
            </div>
            
            <div class="lawyer-notary-box-content flexbox flexbox-wrap justify-between disable" id="due-diligence">
                <div class="content">
                    <h6 class="title"> DUE DILIGENCE </h6>
                    <p></p>
                        <strong>Cost :</strong>
                        <p>Made by our partner, public notary. This deed is one of the requirements to starts a new business/ company.</p>
                        <table>
                            <tr>
                                <td>Start from</td>                            
                                <td>USD 1.800.00</td>
                            </tr>
                        </table>
                        </p>
                </div>
            </div>
            
            <div class="lawyer-notary-box-content flexbox flexbox-wrap justify-between disable" id="purchased-freehold-property">
                <div class="content">
                    <h6 class="title"> PURCHASED FREEHOLD PROPERTY </h6>
                        <p></p>
                        <strong>Cost :</strong>
                        <p>This is a package of notary service includes due diligence of the property and purchased deed.</p>
                        <table>
                            <tr>
                                <td>Start from</td>                            
                                <td>1% of purchased price</td>
                            </tr>
                        </table>
                        </p>
                </div>
            </div>
            
            <div class="lawyer-notary-box-content flexbox flexbox-wrap justify-between disable" id="roya">
                <div class="content">
                    <h6 class="title"> ROYA</h6>
                        <p>Write off mortgage.</p>
                        <strong>Cost :</strong>
                        <table>
                            <tr>
                                <td>Start from</td>                            
                                <td>USD 150.00</td>
                            </tr>
                        </table>
                        </p>
                </div>
            </div>
            
            <div class="lawyer-notary-box-content flexbox flexbox-wrap justify-between disable" id="legalize-inventory">
                <div class="content">
                    <h6 class="title"> LEGALIZE INVENTORY</h6>
                    <p></p>

                        <strong>Cost :</strong>
                        <table>
                            <tr>
                                <td>Start from</td>                            
                                <td>USD 250.00</td>
                            </tr>
                        </table>
                        </p>
                </div>
            </div>
            
            <div class="lawyer-notary-box-content flexbox flexbox-wrap justify-between disable" id="watermark-document">
                <div class="content">
                    <h6 class="title"> WATERMARK DOCUMENT</h6>
                    <p></p>
                        <strong>Cost :</strong>
                        <table>
                            <tr>
                                <td>Start from</td>                            
                                <td>USD 250.00</td>
                            </tr>
                        </table>
                        </p>
                </div>
            </div>
            
            <div class="lawyer-notary-box-content flexbox flexbox-wrap justify-between disable" id="litigation-non-litigation">
                <div class="content">
                    <h6 class="title"> LITIGATION</h6>
                    <p>
                        It is a court handling procedures, either criminal or civil. For criminal, it starts from reporting the opposing party to the police station until the verdict will have been legally binding (could be until Supreme Court). For civil case, it starts from filing the lawsuit to the first instance court until the verdict will have been legally binding (could be until Supreme Court).
                    </p>
                    
                    <ul>
                        <li>Divorce : USD 3.000</li>
                        <li>Non Divorce : USD 4.200</li>
                        <li>If the case is about property, the fee is subject to success fee amount of minimum 10% of the net recovery (negotiable)</li>
                    </ul>
                        
                    <h6 class="title"> NON LITIGATION</h6>
                    <p>
                        It is out off-court handling, such as Legal consultancy, negotiation and mediation (as long as out of court handling) for civil court, and out of police process for criminal case. The non litigation process is undertaken until the settlement reached.
                    </p>
                    
                    <ul>
                        <li>Legal Consultation: USD 90.00 per hour</li>
                        <li>If the case must be handled through non litigation process, the fee will be USD 1.800 including the consultation fee as set forth above</li>
                    </ul>
                    
                    <strong>The fees set forth above are :</strong>
                    <ul>
                        <li>If the client has paid consultation fee, the fee will be deducted by the consultation fee</li>
                        <li>For minimum basis in Badung and Denpasar area</li>
                        <li>For normal legal process</li>
                        <li>For the first instance court process only; we will charge the client another 50% for appealing process and another 50% for supreme court process</li>
                    </ul>
                </div>
            </div>
            
        </section>
    </section>
</div>
@endsection

@section('scripts')

<script>

 //LAWYER & NOTARY
function hide_show() {
var w = $(window).width();
if (w > 640) {

$('.lawyer-and-notary .main').show();
$( '.list-link a').on('click', function(e){
    e.preventDefault();

    $('.list-link a').removeClass('active');

    $(this).addClass('active');

    var x = $(this).attr('href');
    //console.log(x);
    $('.lawyer-notary-box-content').hide();
    $(x).fadeIn();
    return false;
});

 }
}


function accordion_mobile() {


var w = $(window).width();
if (w < 640) {

$("#box > li > div").css('cursor','pointer');

$("#box > li > div").click(function(){
 
    if(false == $(this).next().is(':visible')) {
        $('#box ul').slideUp(300);
    }
    $(this).next().slideToggle(300);
});
 
$('#box ul:eq(0)').show();

$('.list-link .toggle').hide();
$('.lawyer-and-notary .main').hide();

$(".list-link > li > a").click(function(e){
    
    e.preventDefault();

    var same_id = $(this).attr("href");

    //console.log(same_id);
    if(false == $(this).next().is(':visible')) {
        $('.list-link .toggle').slideUp(300);
    }

    //$( ".hello" ).clone().appendTo( ".goodbye" );
    var content = $('.main div' + same_id).clone();

    $(this).siblings().append(content);
    $(this).siblings().children().css('display','block');
    $(this).siblings().addClass('lawyer-notary-box-content flexbox flexbox-wrap justify-between');
    $(this).siblings().css('position','relative');
    $(this).next().slideToggle(300);
});

}

}

$( window ).resize(function() {
  hide_show();
  accordion_mobile();
});

$( window ).load(function() {
  hide_show();
  //accordion_mobile();
});

$( document ).ready(function() {
  hide_show();
  accordion_mobile();
  
});


</script>
@endsection