@extends('index')
@section('content')

<div id="content">
    <div class="account">
        
        <div class="register-box" style="padding-left: 0;">
            <img src="assets/img/canggu.jpg" style="width:100%;">
        </div>

        <div class="login-box">
                
          @if(Session::has('confirm-success'))
              <div class="flash-message success">
                  <p class="alert">{{ Session::get('confirm-success') }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>

              </div> <!-- end .flash-message -->
          @endif


            <h1>Login</h1>
            <div class="kesato-form" style="padding-bottom: 50px; margin-bottom: 50px; border-bottom: 1px solid #DDD;">

                {!! Form::open(['url' => route('login.attempt', trans('url.login')), 'id' => 'validation-form']) !!}
                    <div class="form-body">
                        <div class="section">
                            <label for="log_email" class="field prepend-icon">
                                <input type="email" name="email" id="log_email" class="gui-input" placeholder="Email address">
                            </label>
                        </div>
                        <div class="section">
                            <label for="log_password" class="field prepend-icon">
                                <input type="password" name="password" id="log_password" class="gui-input" placeholder="Password">
                            </label>
                        </div>
                        <p>{{ trans('word.forgot_password') }} ? <a href="{{ route('forgot_password') }}" style="color:#ee5b2c;">Click here</a></p>
                    </div>


                    <div class="form-footer">
                        <button type="submit" class="button btn-primary">Submit</button>
                        <button type="button" class="button btn-primary btn-facebook" style="background-color: #3A5795;" onclick="window.location='{{ url('/login/fb') }}'; return false;"><i class="fa fa-facebook" style="margin-right: 5px;"></i> | <span style="margin-left: 3px; font-weight: bold;">Login with facebook</span></button>
                    </div>
                {!! Form::close() !!}
            </div><!-- END KESATO FORM -->
            
            <h1>Don't have account yet</h1>
            <div style="line-height: 20px;margin-top:0;">
                By creating an account with our store, you will be able to move through the checkout process faster, store multiple shipping addresses, view and track your orders in your account and more.
            </div>
            <a href="{{ route('register', trans('url.register')) }}" class="primary-link" style="float: left;margin-top:12px;">Create an account</a>
            
        </div><!-- END LOGIN BOX -->
        
    </div><!-- END ACCOUNT -->
</div><!-- END MAIN -->

@endsection

@section('scripts')

<!-- FORM  -->
<script src="assets/js/form/select2.js"></script>
<script src="assets/js/form/jquery.validate.min.js"></script>
<script src="assets/js/form/additional-methods.min.js"></script>

<script>
    
    $(document).on('click', '.flash-message .close', function(event) {
        event.preventDefault();
        
        $('.flash-message').hide();
    });

</script>
@endsection
