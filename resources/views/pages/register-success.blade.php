@extends('index')

@section('content')
<div id="content">
    <div class="notification">
        <div class="success">
            <div class="icon">
                <i class="fa fa-check-circle"></i>
            </div>
            <h1 class="title">Registered Success</h1>
            <div class="desc">
                {{ trans('notification.check_email') }}
            </div>
            <a href="{{ route('login', trans('url.login')) }}" class="login-link">{{ trans('word.login') }}</a>
        </div>
        
        
    </div><!-- END ACCOUNT -->
</div><!-- END MAIN -->

@endsection