@extends('index')

@section('content')


<!-- MAIN CONTAINER -->
<div id="content"> 

<!-- backup nya disnin -->

    <div class="testimonial-detail">
<!--         <h1 class="name">{{ $testimony->title }}</h1>
        <p class="code">on 03 December 2016 </p> -->

        <div class="left-side">
            
            <div class="testimonial-slide">
                <div class="testimonial_detail_slide" tabindex="1" autofocus="autofocus">
                  
                  @if(count($testimony->attachments) > 0)
                    @foreach($testimony->attachments as $attachment)
                    <figure>
                        <img src="{{ $attachment->file }}" alt=""/>
                        <!-- <figcaption>Lorem</figcaption> -->
                    </figure>
                    @endforeach
                  @else
                    <figure>
                        <img src="/assets/img/no-image.png" alt=""/>
                        <!-- <figcaption>Lorem</figcaption> -->
                    </figure>
                  @endif

                </div>              
            </div><!-- END testimonial SLIDE -->
        </div><!-- END testimonial DESCRIPTION -->
            
    </div><!-- END LEFT SIDE -->
        
    </div><!-- END PROPERTIES -->

</div><!-- END MAIN -->

<div class="testimonial-description">
    <div id="content"> 
      <div class="description">
          <div class="detail-page">
              <div class="page-view-section-body">
                  <div class="container">
                    
                      <div class="testimonial-user-holder flexbox flexbox-wrap">
                      <!-- 
                          <div class="avatar-holder">
                                    <div class="avatar">
                                        <img src="assets/img/customer.jpg" alt="" class="avatar-icon">
                                    </div>    
                                    <div class="account-desc">
                                        <h3 class="name">
                                            Andre Mahendra
                                        </h3>
                                        <p class="join">
                                            on 03 Desember 1994
                                        </p>
                                    </div>
                          </div>
                           -->
                          <div class="the-testimonial">
                                    <div class="title-the-testimonial">
                                        <h3>{{ $testimony->title }}</h3>
                                    </div>
                                    <div class="desc-the-testimonial">
                                        <p>
                                            {!! $testimony->content !!}
                                        </p>
                                    </div>
                          </div>
                      </div>
                            
                  </div>
              </div>
          </div>
      </div>
    </div>
    </div>
</div>

<!-- IDENTITY PAGE -->
<div class="data hidden">
    <input type="hidden" id="CURRENTPAGE" value="testimonialdetail" />
</div>

@endsection

@section('scripts')
  <script src="assets/js/hammer.min.js"></script>
@endsection
