@extends('index')

@section('title', 'Sold Villa')

@section('description', '')

@section('content')

<!-- BREADCRUMB -->
<section class="breadcrumb" style="background-image:url('assets/img/testimonials-bg.jpg');">
    <div class="breadcrumb-holder">
        <div class="page-title ta-center">
            <h3 class="white">
                {{ trans('word.breadcrumb_sold_villas') }}
            </h3>
            <p class="white fw-300">
                Find your property in the different area of Bali
            </p>
        </div>
    </div>
</section>



<section id="content"> 
    <div class="home-page">
    
        <!-- BLOG & TESTIMONIALS -->
        <section class="bottom-section flexbox flexbox-wrap justify-between">
            <section class="full-width">
                                
                <div class="testimonials flexbox flexbox-wrap justify-between">
                    <?php $i=0; ?>
                    @foreach($properties as $property)
                    <?php $i++; ?>

                    <a href="" class="flexbox flexbox-wrap justify-between" style="background-image:url('http://lorempixel.com/400/200/city?{{ time() + $i }}');">
                        <div class="sold-holder" style=" width: 100px; height: 100px; position: absolute; background: red; z-index: 999; right: 0;">
                        <div class="sold-img">
                            
                        </div>
                    </div>

                        <div class="desc">
                            <h4>{{ $property->lang()->title }}</h4>

                            <!-- <div class="md-button md-button-outline">{{ trans('word.read_more') }}</div> -->
                        </div>
                    </a>
                    @endforeach

                </div>
            </section>
    
        </section>
        
    </div>
</section>
    
    
<!-- PAGINATION -->        
@include('fragments.pagination', ['paginator' => $properties])

@stop

@section('scripts')
<script type="text/javascript">

var lastPage = {{ $properties->lastPage() }} - 1;

$(document).ready(function() {
    
    // $('.jscroll').jscroll({
    //     debug: false,
    //     autoTrigger: true,
    //     autoTriggerUntil: lastPage,
    //     loadingHtml: '<img src="assets/img/loading_bar.gif" alt="Loading" />',
    //     padding: 300,
    //     nextSelector: 'a.jscroll-next:last',
    //     pagingSelector: 'a.jscroll-next:last',
    //     callback: function () {
    //         console.log('loaded');
    //     }
    // });

    $('.testimony-save').click(function(event) {

        console.log('save clicked');

        var frm = $('#testimony-form'),
        url = frm.attr('action'),
        data = frm.serialize();;

        $.post(url, data, function(data, textStatus, xhr) {

            if(data.status == 200) {

                $('#add-testimony').modal('hide');

                $('input').val('');
                $('textarea').val('');
            }
        });

        event.preventDefault();
    });

});

</script>
@stop