@extends('index')

@section('description', '')

@section('content')

<!-- BREADCRUMB -->
<section class="breadcrumb" style="background-image:url('assets/img/testimonials-bg.jpg');">
    <div class="breadcrumb-holder">
        <div class="page-title ta-center">
            <h3 class="white">
                Sitemap
            </h3>
            <p class="white fw-300">
                Find your property in the different area of Bali
            </p>
        </div>
    </div>
</section>

<section class="sitemap">
    <div class="bg-white">
        <div class="row"> 
            <div class="list">
                <h1>Page</h1>
                <div class="full-width flexbox">
                    <div class="item">
                        <a href="#">Lorem ipsum dolor sit amet</a>
                        <a href="#">consectetur adipiscing elit</a>
                        <a href="#">Nulla posuere lectus vel tellus dignissim porttitor</a>
                        <a href="#">Morbi molestie</a>
                        <a href="#">tortor et auctor sollicitudin</a>
                        <a href="#">ante sapien lobortis tortor</a>
                        <a href="#">sed finibus ex lorem quis augue</a>
                        <a href="#">Aliquam a mauris in orci volutpat laoreet</a>
                    </div>
                    <div class="item">
                        &nbsp;
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="bg-grey">       
        <div class="row"> 
            <div class="list">
                <h1>Villas</h1>
                <h2>Balangan</h2>
                <div class="two-col flexbox">
                    <div class="item">
                        <a href="#">Lorem ipsum dolor sit amet</a>
                        <a href="#">consectetur adipiscing elit</a>
                        <a href="#">Nulla posuere lectus vel tellus dignissim porttitor</a>
                        <a href="#">Morbi molestie</a>
                        <a href="#">tortor et auctor sollicitudin</a>
                        <a href="#">ante sapien lobortis tortor</a>
                        <a href="#">sed finibus ex lorem quis augue</a>
                        <a href="#">Aliquam a mauris in orci volutpat laoreet</a>
                    </div>   
                    <div class="item">
                        <a href="#">Lorem ipsum dolor sit amet</a>
                        <a href="#">consectetur adipiscing elit</a>
                        <a href="#">Nulla posuere lectus vel tellus dignissim porttitor</a>
                        <a href="#">Morbi molestie</a>
                        <a href="#">tortor et auctor sollicitudin</a>
                        <a href="#">ante sapien lobortis tortor</a>
                        <a href="#">sed finibus ex lorem quis augue</a>
                        <a href="#">Aliquam a mauris in orci volutpat laoreet</a>
                    </div>  
                </div>
            </div>

            <div class="list">
                <h2>Balangan</h2>
                <div class="two-col flexbox">
                    <div class="item">
                        <a href="#">Lorem ipsum dolor sit amet</a>
                        <a href="#">consectetur adipiscing elit</a>
                        <a href="#">Nulla posuere lectus vel tellus dignissim porttitor</a>
                        <a href="#">Morbi molestie</a>
                        <a href="#">tortor et auctor sollicitudin</a>
                        <a href="#">ante sapien lobortis tortor</a>
                        <a href="#">sed finibus ex lorem quis augue</a>
                        <a href="#">Aliquam a mauris in orci volutpat laoreet</a>
                    </div>   
                    <div class="item">
                        <a href="#">Lorem ipsum dolor sit amet</a>
                        <a href="#">consectetur adipiscing elit</a>
                        <a href="#">Nulla posuere lectus vel tellus dignissim porttitor</a>
                        <a href="#">Morbi molestie</a>
                        <a href="#">tortor et auctor sollicitudin</a>
                        <a href="#">ante sapien lobortis tortor</a>
                        <a href="#">sed finibus ex lorem quis augue</a>
                        <a href="#">Aliquam a mauris in orci volutpat laoreet</a>
                    </div>  
                </div>
            </div>

            <div class="list">
                <h2>Balangan</h2>
                <div class="two-col flexbox">
                    <div class="item">
                        <a href="#">Lorem ipsum dolor sit amet</a>
                        <a href="#">consectetur adipiscing elit</a>
                        <a href="#">Nulla posuere lectus vel tellus dignissim porttitor</a>
                        <a href="#">Morbi molestie</a>
                        <a href="#">tortor et auctor sollicitudin</a>
                        <a href="#">ante sapien lobortis tortor</a>
                        <a href="#">sed finibus ex lorem quis augue</a>
                        <a href="#">Aliquam a mauris in orci volutpat laoreet</a>
                    </div>   
                    <div class="item">
                        <a href="#">Lorem ipsum dolor sit amet</a>
                        <a href="#">consectetur adipiscing elit</a>
                        <a href="#">Nulla posuere lectus vel tellus dignissim porttitor</a>
                        <a href="#">Morbi molestie</a>
                        <a href="#">tortor et auctor sollicitudin</a>
                        <a href="#">ante sapien lobortis tortor</a>
                        <a href="#">sed finibus ex lorem quis augue</a>
                        <a href="#">Aliquam a mauris in orci volutpat laoreet</a>
                    </div>  
                </div>
            </div>
        </div>
    </div>

    <div class="bg-white">
        <div  class="row"> 
            <div class="list">
                <h1>Land</h1>
                <h2>Balangan</h2>
                <div class="two-col flexbox">
                    <div class="item">
                        <a href="#">Lorem ipsum dolor sit amet</a>
                        <a href="#">consectetur adipiscing elit</a>
                        <a href="#">Nulla posuere lectus vel tellus dignissim porttitor</a>
                        <a href="#">Morbi molestie</a>
                        <a href="#">tortor et auctor sollicitudin</a>
                        <a href="#">ante sapien lobortis tortor</a>
                        <a href="#">sed finibus ex lorem quis augue</a>
                        <a href="#">Aliquam a mauris in orci volutpat laoreet</a>
                    </div>   
                    <div class="item">
                        <a href="#">Lorem ipsum dolor sit amet</a>
                        <a href="#">consectetur adipiscing elit</a>
                        <a href="#">Nulla posuere lectus vel tellus dignissim porttitor</a>
                        <a href="#">Morbi molestie</a>
                        <a href="#">tortor et auctor sollicitudin</a>
                        <a href="#">ante sapien lobortis tortor</a>
                        <a href="#">sed finibus ex lorem quis augue</a>
                        <a href="#">Aliquam a mauris in orci volutpat laoreet</a>
                    </div>  
                </div>
            </div>

            <div class="list">
                <h2>Balangan</h2>
                <div class="two-col flexbox">
                    <div class="item">
                        <a href="#">Lorem ipsum dolor sit amet</a>
                        <a href="#">consectetur adipiscing elit</a>
                        <a href="#">Nulla posuere lectus vel tellus dignissim porttitor</a>
                        <a href="#">Morbi molestie</a>
                        <a href="#">tortor et auctor sollicitudin</a>
                        <a href="#">ante sapien lobortis tortor</a>
                        <a href="#">sed finibus ex lorem quis augue</a>
                        <a href="#">Aliquam a mauris in orci volutpat laoreet</a>
                    </div>   
                    <div class="item">
                        <a href="#">Lorem ipsum dolor sit amet</a>
                        <a href="#">consectetur adipiscing elit</a>
                        <a href="#">Nulla posuere lectus vel tellus dignissim porttitor</a>
                        <a href="#">Morbi molestie</a>
                        <a href="#">tortor et auctor sollicitudin</a>
                        <a href="#">ante sapien lobortis tortor</a>
                        <a href="#">sed finibus ex lorem quis augue</a>
                        <a href="#">Aliquam a mauris in orci volutpat laoreet</a>
                    </div>  
                </div>
            </div>

            <div class="list">
                <h2>Balangan</h2>
                <div class="two-col flexbox">
                    <div class="item">
                        <a href="#">Lorem ipsum dolor sit amet</a>
                        <a href="#">consectetur adipiscing elit</a>
                        <a href="#">Nulla posuere lectus vel tellus dignissim porttitor</a>
                        <a href="#">Morbi molestie</a>
                        <a href="#">tortor et auctor sollicitudin</a>
                        <a href="#">ante sapien lobortis tortor</a>
                        <a href="#">sed finibus ex lorem quis augue</a>
                        <a href="#">Aliquam a mauris in orci volutpat laoreet</a>
                    </div>   
                    <div class="item">
                        <a href="#">Lorem ipsum dolor sit amet</a>
                        <a href="#">consectetur adipiscing elit</a>
                        <a href="#">Nulla posuere lectus vel tellus dignissim porttitor</a>
                        <a href="#">Morbi molestie</a>
                        <a href="#">tortor et auctor sollicitudin</a>
                        <a href="#">ante sapien lobortis tortor</a>
                        <a href="#">sed finibus ex lorem quis augue</a>
                        <a href="#">Aliquam a mauris in orci volutpat laoreet</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

    
<!-- PAGINATION -->        


@stop

@section('scripts')
<script type="text/javascript">



</script>
@stop