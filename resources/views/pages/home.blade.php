@extends('index')  

@section('content') 
 <!-- JUMBO VIDEO -->
<section id="jumbotron">

    <!-- VIDEO -->
    <div id="video-container">
        <video id="video" autoplay="autoplay" loop="loop" muted="muted">
            <source src="assets/video/Alban48s.m4v" type="video/mp4">
            <source src="assets/video/Alban48s.webm" type="video/webm">
        </video>
    </div>

    <div class="wrapper">
        <h2>Your dreams come to live</h2>
        <a href="{{ route('search', ['search' => trans('url.search'), 'term' => 'villa']) }}" class="category-btn" data-target="#villa"><i class="material-icons">home</i>Search Villa</a>
        <a href="{{ route('search', ['search' => trans('url.search'), 'term' => 'land']) }}" class="category-btn" data-target="#land"><i class="material-icons">pin_drop</i>Search Land</a>
    </div>

    <a class="pause-btn play" href>
        <i class="show material-icons">pause_circle_outline</i>
        <i class="hide material-icons">play_circle_outline</i>
    </a>

    <!-- CATERGORIES -->  
    <div class="categories">

        <div class="wrapper flexbox flexbox-wrap justify-between" id="villa">
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'villas-for-sale', 
                'type' => 'less-than-500k']) }}">
                <div class="text-wrapper">
                    <h3>Investment <br> <span>under $500.000</span></h3>
                    <h4>First investment <br><span>8% - 12% NET R.O.I.</span></h4>
                </div>
            </a>
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'villas-for-sale', 
                'type' => 'more-than-500k']) }}">
                <div class="text-wrapper">
                    <h3>Investment <br><span>over $500.000</span></h3>
                    <h4>Luxurious Villas <br><span>8% - 12% NET R.O.I. Capital gain</span></h4>
                </div>
            </a>
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'villas-for-sale', 
                'tag' => 'home-and-retirement']) }}">
                <div class="text-wrapper">
                    <h3>Home &amp; <br><span>Retirement</span></h3>
                    <h4>Find here more than 1000 real estate for sale</h4>
                </div>
            </a>
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'villas-for-sale', 
                'tag' => 'beachfront-property']) }}">
                <div class="text-wrapper">
                    <h3>Beachfront <br><span>Properties</span></h3>
                    <h4>Your tropical Dolce Vita</h4>
                </div>
            </a>
        </div>

        <div class="wrapper flexbox flexbox-wrap justify-between" id="land">
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'land', 
                'type' => 'less-than-500k']) }}">
                <div class="text-wrapper">
                    <h3>Investment <br> <span>under $500.000</span></h3>
                    <h4>First investment <br><span>8% - 12% NET R.O.I.</span></h4>
                </div>
            </a>
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'land', 
                'type' => 'more-than-500k']) }}">
                <div class="text-wrapper">
                    <h3>Investment <br><span>over $500.000</span></h3>
                    <h4>Luxurious Villas <br><span>8% - 12% NET R.O.I. Capital gain</span></h4>
                </div>
            </a>
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'land', 
                'tag' => 'beachfront-property']) }}">
                <div class="text-wrapper">
                    <h3>Beachfront <br><span>Properties</span></h3>
                    <h4>Your tropical Dolce Vita</h4>
                </div>
            </a>
        </div>

    </div>

</section>

<section class="about-company-holder">
    <div class="desc">
            <div class="page-title ta-center">
            <h3 class="orange">
                Real Estate for Sale in Bali
            </h3>
            <p>
                Kibarer Property is THE ONLY realtor in Bali that also provides Lawyers and Notary under the same roof. Our, more than1000 villa listings covers all types of villas ranging from luxury beach front villas to home and retirement villas. We provide property and houses for sale in Bali with access to the largest villa listings, the Best investment 2015 in Bali with ROI guarantee. You will have the option of using the legal advices from our Notary and Lawyer for your safety, please see below for more info.
            </p>
        </div>
    </div>
</section>

<!-- MAIN CONTAINER -->
<section id="content"> 
    <div class="home-page">

        <!-- LOCATIONS TITLE -->
        
    
        <!-- LOCATIONS TITLE -->
        <div class="page-title ta-center">
            <h3 class="orange">
                Locations
            </h3>
            <p>
                Find your property in the different area of Bali
            </p>
        </div>
    
        <!-- LOCATION BOXES -->
        <section class="locations flexbox flexbox-wrap justify-between">
            <div class="double" style="background-image: url('assets/img/seminyak.jpg');">
                <h2 class="location-name">Seminyak</h2>
                <div class="desc flexbox flexbox-wrap justify-center">
                    <div>
                        <h4><i class="material-icons">home</i>Villa</h4>
                        <p>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'villas-for-sale', 
                'type' => 'less-than-500k',
                'area' => 'seminyak']) }}">Investment &lt; $500.000</a>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'villas-for-sale', 
                'type' => 'more-than-500k',
                'area' => 'seminyak']) }}">Investment &gt; $500.000</a>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'villas-for-sale', 
                'tag' => 'home-and-retirement',
                'area' => 'seminyak']) }}">Home &amp; Retirement</a>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'villas-for-sale', 
                'tag' => 'beachfront-property',
                'area' => 'seminyak']) }}">Beach front properties</a>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'villas-for-rent', 
                'tag' => 'beachfront-property',
                'area' => 'seminyak']) }}">Villa for rent</a>
                        </p>    
                    </div>
                    <div>
                        <h4><i class="material-icons">pin_drop</i>Land</h4>
                        <p>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'land', 
                'type' => 'less-than-500k',
                'area' => 'seminyak']) }}">Investment &lt; $500.000</a>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'land', 
                'type' => 'more-than-500k',
                'area' => 'seminyak']) }}">Investment &gt; $500.000</a>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'land', 
                'tag' => 'beachfront-property',
                'area' => 'seminyak']) }}">Beach front</a>
                        </p> 
                    </div>
                </div>
            </div>
            <div class="single" style="background-image: url('assets/img/sanur.jpg');">
                <h2 class="location-name">Sanur</h2>
                <div class="desc flexbox flexbox-wrap justify-center">
                    <div>
                        <h4><i class="material-icons">home</i>Villa</h4>
                        <p>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'villas-for-sale', 
                'type' => 'less-than-500k',
                'area' => 'sanur']) }}">Investment &lt; $500.000</a>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'villas-for-sale', 
                'type' => 'more-than-500k',
                'area' => 'sanur']) }}">Investment &gt; $500.000</a>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'villas-for-sale', 
                'tag' => 'home-and-retirement',
                'area' => 'sanur']) }}">Home &amp; Retirement</a>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'villas-for-sale', 
                'tag' => 'beachfront-property',
                'area' => 'sanur']) }}">Beach front properties</a>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'villas-for-rent', 
                'tag' => 'beachfront-property',
                'area' => 'sanur']) }}">Villa for rent</a>
                        </p>    
                    </div>
                    <div>
                        <h4><i class="material-icons">pin_drop</i>Land</h4>
                        <p>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'land', 
                'type' => 'less-than-500k',
                'area' => 'sanur']) }}">Investment &lt; $500.000</a>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'land', 
                'type' => 'more-than-500k',
                'area' => 'sanur']) }}">Investment &gt; $500.000</a>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'land', 
                'tag' => 'beachfront-property',
                'area' => 'sanur']) }}">Beach front</a>
                        </p> 
                    </div>
                </div>
            </div>
            <div class="single" style="background-image: url('assets/img/canggu.jpg');">
                <h2 class="location-name">Canggu</h2>
                <div class="desc flexbox flexbox-wrap justify-center">
                    <div>
                        <h4><i class="material-icons">home</i>Villa</h4>
                        <p>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'villas-for-sale', 
                'type' => 'less-than-500k',
                'area' => 'canggu']) }}">Investment &lt; $500.000</a>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'villas-for-sale', 
                'type' => 'more-than-500k',
                'area' => 'canggu']) }}">Investment &gt; $500.000</a>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'villas-for-sale', 
                'tag' => 'home-and-retirement',
                'area' => 'canggu']) }}">Home &amp; Retirement</a>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'villas-for-sale', 
                'tag' => 'beachfront-property',
                'area' => 'canggu']) }}">Beach front properties</a>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'villas-for-rent', 
                'tag' => 'beachfront-property',
                'area' => 'canggu']) }}">Villa for rent</a>
                        </p>    
                    </div>
                    <div>
                        <h4><i class="material-icons">pin_drop</i>Land</h4>
                        <p>
                            <a href="">Investment &lt; $500.000</a>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'land', 
                'type' => 'more-than-500k',
                'area' => 'canggu']) }}">Investment &gt; $500.000</a>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'land', 
                'tag' => 'beachfront-property',
                'area' => 'canggu']) }}">Beach front</a>
                        </p> 
                    </div>
                </div>
            </div>
            <div class="single" style="background-image: url('assets/img/ubud.jpg');">
                <h2 class="location-name">Ubud</h2>
                <div class="desc flexbox flexbox-wrap justify-center">
                    <div>
                        <h4><i class="material-icons">home</i>Villa</h4>
                        <p>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'villas-for-sale', 
                'type' => 'less-than-500k',
                'area' => 'ubud']) }}">Investment &lt; $500.000</a>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'villas-for-sale', 
                'type' => 'more-than-500k',
                'area' => 'ubud']) }}">Investment &gt; $500.000</a>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'villas-for-sale', 
                'tag' => 'home-and-retirement',
                'area' => 'ubud']) }}">Home &amp; Retirement</a>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'villas-for-sale', 
                'tag' => 'beachfront-property',
                'area' => 'ubud']) }}">Beach front properties</a>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'villas-for-rent', 
                'tag' => 'beachfront-property',
                'area' => 'ubud']) }}">Villa for rent</a>
                        </p>    
                    </div>
                    <div>
                        <h4><i class="material-icons">pin_drop</i>Land</h4>
                        <p>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'land', 
                'type' => 'less-than-500k',
                'area' => 'ubud']) }}">Investment &lt; $500.000</a>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'land', 
                'type' => 'more-than-500k',
                'area' => 'ubud']) }}">Investment &gt; $500.000</a>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'land', 
                'tag' => 'beachfront-property',
                'area' => 'ubud']) }}">Beach front</a>
                        </p> 
                    </div>
                </div>
            </div>
            <div class="single" style="background-image: url('assets/img/jimbaran.jpg');">
                <h2 class="location-name">Jimbaran</h2>
                <div class="desc flexbox flexbox-wrap justify-center">
                    <div>
                        <h4><i class="material-icons">home</i>Villa</h4>
                        <p>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'villas-for-sale', 
                'type' => 'less-than-500k',
                'area' => 'jimbaran']) }}">Investment &lt; $500.000</a>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'villas-for-sale', 
                'type' => 'more-than-500k',
                'area' => 'jimbaran']) }}">Investment &gt; $500.000</a>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'villas-for-sale', 
                'tag' => 'home-and-retirement',
                'area' => 'jimbaran']) }}">Home &amp; Retirement</a>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'villas-for-sale', 
                'tag' => 'beachfront-property',
                'area' => 'jimbaran']) }}">Beach front properties</a>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'villas-for-rent', 
                'tag' => 'beachfront-property',
                'area' => 'jimbaran']) }}">Villa for rent</a>
                        </p>    
                    </div>
                    <div>
                        <h4><i class="material-icons">pin_drop</i>Land</h4>
                        <p>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'land', 
                'type' => 'less-than-500k',
                'area' => 'jimbaran']) }}">Investment &lt; $500.000</a>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'land', 
                'type' => 'more-than-500k',
                'area' => 'jimbaran']) }}">Investment &gt; $500.000</a>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'land', 
                'tag' => 'beachfront-property',
                'area' => 'jimbaran']) }}">Beach front</a>
                        </p> 
                    </div>
                </div>
            </div>
            <div class="single" style="background-image: url('assets/img/lovina.jpg');">
                <h2 class="location-name">Lovina</h2>
                <div class="desc flexbox flexbox-wrap justify-center">
                    <div>
                        <h4><i class="material-icons">home</i>Villa</h4>
                        <p>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'villas-for-sale', 
                'type' => 'less-than-500k',
                'area' => 'lovina']) }}">Investment &lt; $500.000</a>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'villas-for-sale', 
                'type' => 'more-than-500k',
                'area' => 'lovina']) }}">Investment &gt; $500.000</a>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'villas-for-sale', 
                'tag' => 'home-and-retirement',
                'area' => 'lovina']) }}">Home &amp; Retirement</a>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'villas-for-sale', 
                'tag' => 'beachfront-property',
                'area' => 'lovina']) }}">Beach front properties</a>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'villas-for-rent', 
                'tag' => 'beachfront-property',
                'area' => 'lovina']) }}">Villa for rent</a>
                        </p>    
                    </div>
                    <div>
                        <h4><i class="material-icons">pin_drop</i>Land</h4>
                        <p>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'land', 
                'type' => 'less-than-500k',
                'area' => 'lovina']) }}">Investment &lt; $500.000</a>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'land', 
                'type' => 'more-than-500k',
                'area' => 'lovina']) }}">Investment &gt; $500.000</a>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'land', 
                'tag' => 'beachfront-property',
                'area' => 'lovina']) }}">Beach front</a>
                        </p> 
                    </div>
                </div>
            </div>
            <div class="double" style="background-image: url('assets/img/tanahlot.jpg');">    
                <h2 class="location-name">Tanah Lot</h2>
                <div class="desc flexbox flexbox-wrap justify-center">
                    <div>
                        <h4><i class="material-icons">home</i>Villa</h4>
                        <p>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'villas-for-sale', 
                'type' => 'less-than-500k',
                'area' => 'beraban']) }}">Investment &lt; $500.000</a>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'villas-for-sale', 
                'type' => 'more-than-500k',
                'area' => 'beraban']) }}">Investment &gt; $500.000</a>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'villas-for-sale', 
                'tag' => 'home-and-retirement',
                'area' => 'beraban']) }}">Home &amp; Retirement</a>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'villas-for-sale', 
                'tag' => 'beachfront-property',
                'area' => 'beraban']) }}">Beach front properties</a>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'villas-for-rent', 
                'tag' => 'beachfront-property',
                'area' => 'beraban']) }}">Villa for rent</a>
                        </p>    
                    </div>
                    <div>
                        <h4><i class="material-icons">pin_drop</i>Land</h4>
                        <p>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'land', 
                'type' => 'less-than-500k',
                'area' => 'beraban']) }}">Investment &lt; $500.000</a>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'land', 
                'type' => 'more-than-500k',
                'area' => 'beraban']) }}">Investment &gt; $500.000</a>
                            
            <a href="{{ route('search', [
                'search' => trans('url.search'), 
                'term' => 'land', 
                'tag' => 'beachfront-property',
                'area' => 'beraban']) }}">Beach front</a>
                        </p> 
                    </div>
                </div>
            </div>
        </section>
        
        <!-- BLOG & TESTIMONIALS -->
        <section class="bottom-section flexbox flexbox-wrap justify-between">
            <section class="w66">
                
                <div class="page-title ta-left">
                    <h3 class="orange">
                        {{ trans('word.testimonial') }}
                    </h3>
                    <p>
                        Find your property in the different area of Bali
                    </p>
                </div>
                                
                <div class="testimonials flexbox flexbox-wrap justify-between">
                    <?php $i = 0 ?>
                    @foreach($testimonials as $testimony)
                    <?php $i++ ?>
                    <a href="{{ route('testimonials', ['testimonials' => trans('url.testimonials'), 'term' => $testimony->slug]) }}" class="flexbox flexbox-wrap justify-between" style="background-image:url('assets/img/testimonial{{$i}}.jpg');">
                        <div class="desc">
                            <h4>{{ $testimony->title }}</h4>
                            {{ $testimony->customer_name }}
                            <div class="md-button md-button-outline">{{ trans('word.read_more') }}</div>
                        </div>
                    </a>
                    @endforeach

                </div>
            </section>
    
            <section class="w33">
                
                <div class="page-title ta-left">
                    <h3 class="orange">
                        {{ trans('word.blog')}}
                    </h3>
                    <p>
                        Find your property in the different area of Bali
                    </p>
                </div>
                
                <div class="blog flexbox flexbox-wrap justify-between">
                    <?php $i = 0 ?>
                    @foreach($posts as $post)
                    <?php $i++ ?>
                    <a href="{{ route('blog', ['blog' => 'blog', 'term' => $post->lang()->slug]) }}" style="background-image: url('assets/img/blog{{ ($i < 3) ? $i : '1' }}.jpg');">
                        <div class="desc">
                            <h3>{{ $post->lang()->title }}</h3>
                            <div class="date">{{ $post->created_at->format('d M Y') }}</div>
    
                            <p>{{ str_limit($post->content, 100) }}</p>            
                            <div class="md-button md-button-outline">Read more</div>
                        </div>
                    </a>
                    @endforeach

                </div>
            </section>
        </section>
        
    </div>
</section>


<!-- IDENTITY PAGE -->
<div class="data hidden">
    <input type="hidden" id="CURRENTPAGE" value="home" />
</div>
@endsection