@extends('index')
@section('content')

<!-- breadcrumb -->
<section class="breadcrumb" style="background-image:url('assets/img/testimonials-bg.jpg');">
    <div class="breadcrumb-holder">
        <div class="page-title ta-center">
            <h3 class="white">
                Wishlist Property
            </h3>
            <p class="white fw-300">
                Find your property in the different area of Bali
            </p>
        </div>
    </div>
</section>

<!-- MAIN CONTAINER -->
<div id="content"> 

    <section class="bottom-section flexbox flexbox-wrap justify-between">
        @include('fragments.account-sidebar')

        <section class="w66">

            @foreach($wishlists as $wishlist)

            <div class="account-right-content flexbox flexbox-wrap">
                <div class="wishlist-setting flexbox flexbox-wrap">
                    <div class="img-holder">
                    @if(count($wishlist->property->thumb) > 0)
                        <img src="{{ $wishlist->property->thumb[0] }}" alt="" class="img-responsive">
                    @else
                        <img src="assets/img/no-image.png" alt="" class="img-responsive">
                    @endif

                    </div>

                    <div class="content-holder">
                    <!--  
                        kalau villa <i class="fa fa-home"></i>
                        kalau land <i class="fa fa-map-marker"></i>
                    -->
                        <div class="wishlist-date">
                            <i class="fa fa-home"></i> {{ $wishlist->property->categories[0]->name }},  added on {{ $wishlist->property->created_at->format('M d, Y') }}
                            <div class="wishlist-date-bottom"></div>
                        </div>
                        <div class="property-title">
                            <h3>{{ $wishlist->property->lang()->title }}</h3>
                            <p><i class="fa fa-map-marker"></i> {{ $wishlist->property->city }}</p>
                        </div>
                        <div class="property-desc">
                            <p>
                                {!! $wishlist->property->lang()->content !!}
                            </p>
                        </div>
                        <div class="property-facilities-holder flexbox flexbox-wrap">
                            <div class="icon"><i class="fa fa-bed"></i> <span>x {{ $wishlist->property->bedroom }}</span></div>
                            <div class="icon"><i class="fa fa-arrows"></i> <span>x {{ $wishlist->property->land_size }}</span></div>
                            <div class="icon"><i class="fa fa-map-marker"></i> <span>x {{ $wishlist->property->building_size }}</span></div>
                        </div>
                    </div>

                    <div class="price-holder">

                        <h3 class="currency">{{ Session::get('currency') }}</h3>
                        <h3 class="price">{{ convertCurrency($wishlist->property->price, $wishlist->property->currency, Session::get('currency')) }}</h3>
                        <div class="button-holder">
                            <div class="kesato-form">
                                <form method="post" action="" id="validation-form">
                                    <div class="form-footer">
                                    <?php $property_term = property_term($wishlist->property); ?>
                                        <a href="{{ route('property', ['property' => trans('url.property'), 'term' => $property_term] ).'/'. $wishlist->property->lang()->slug }}" class="button btn-primary">View Detail</a> 
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="remove-property">
                    <div class="icon">
                        <a href=""><i class="fa fa-close"></i></a>
                    </div>
                    
                </div>
            </div>

            @endforeach

            <div>
                {{ $wishlists->render() }}
            </div>

        </section>
    </section>
</div>

@stop

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        
        $('.delete-wishlist').click(function(event) {

            var question = 'Are you sure delete this item?';

            if(confirm(question)){

                var url = $(this).attr('data-url');
                var customerId = "{{ $customer->id }}";
                var token = "{{ csrf_token() }}";
                var method = 'delete';

                $.post(url, {id: id, customer_id: customerId, _token: token, _method: method}, function(data, textStatus, xhr) {
                    
                    console.log(data);
                    if (data == 'deleted') location.reload();
                });

            }

            event.preventDefault();
        });
    });     
</script>
@stop