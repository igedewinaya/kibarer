<?php $i = 0;$newLatLong = []; ?>
@if(count($properties) == 0)
    <div class="empty-property">
        <p>
            Kami tidak dapat menemukan hasil yang sesuai dengan kriteria Anda, namun sedikit merubah kriteria pencarian
            Anda mungkin akan bisa membantu. Cobalah beberapa ide berikut :
        <ul>
            <li>Matikan beberapa filter.</li>
            <li>Perluas daerah pencarian Anda.</li>
            <li>Cari kota, alamat, atau bangunan penting.</li>
        </ul>
        </p>
    </div>
@endif
@foreach($properties as $property)


    <?php $i++ ?>

    <?php $property_term = property_term($property); ?>

    <?php array_push($newLatLong, [$property->map_latitude, $property->map_longitude, $property->id]); ?>

    @if ($property->thumbs)
        <?php $thumbs = explode(',', $property->thumbs);?>
    @else
        <?php $thumbs = ['assets/img/no-image.png'];?>
    @endif
    <div class="box">
        <div id="boxitem{{$property->id}}" class="box-item">

            <div class="img">

                <a id="url{{$property->id}}"
                   href="{{ route('property', ['property' => trans('url.property'), 'term' => $property_term] ) }}">
                    <div class="price-holder">{{ Session::get('currency')}}  {{ convertCurrency($property->price, $property->currency, Session::get('currency')) }}</div>

                </a>

                <img id="currentThumb{{$property->id}}" src="{{url($thumbs[0])}}">


                <div class="arrows">
                    <a href="#" class="changeThumbnail" about="prev" rel="0" name="{{$property->id}}"><i
                                class="fa fa-angle-left"></i></a>
                    <a href="#" class="changeThumbnail" about="next" rel="0" name="{{$property->id}}"><i
                                class="fa fa-angle-right"></i></a>
                </div>

            </div>

            <a href="{{ route('property', ['property' => trans('url.property'), 'term' => $property_term] ) }}">
                <div class="name" id="title{{$property->id}}">{{ $property->title }}</div>
                <div class="desc">
                    <p id="description{{$property->id}}">{!! strip_tags(str_limit($property->code))  !!}</p>
                </div>
            </a>
            <!-- <a href="{{ route('property', ['property' => trans('url.property'), 'term' => $property_term] ) }}"> -->
            <div class="box-links">
                <div class="box-link-location"><i class="fa fa-map-marker"></i>&nbsp;
                    &nbsp; {{ucfirst($property->city)}}, {{ucfirst($property->province)}}</div>
                @if ($category == 'villas-for-sale' OR $category == 'villas-for-rental')
                    <div class="box-link-bed" id="objectfirst{{$property->id}}"><i class="fa fa-bed"></i>&nbsp;
                        &nbsp; {{$property->bedroom}}</div>
                    <div class="box-link-bath" id="objectsecond{{$property->id}}"><img src="assets/img/bathub.png">&nbsp;
                        &nbsp;{{$property->bathroom}}</div>
                    <div class="box-link-detail" rel="{{$property->id}}"><i class="fa fa-plus"></i> Detail</div>
                @else
                    <div class="box-link-bed" id="objectfirst{{$property->id}}" style="width:66.66%;"><i
                                class="fa fa-arrows-alt"></i>&nbsp;
                        &nbsp; {{$property->land_size}} m2
                    </div>
                    <div id="objectsecond{{$property->id}}"></div>
                    <div class="box-link-detail" rel="{{$property->id}}"><i class="fa fa-plus"></i> Detail</div>
                @endif

            </div>
            <!-- </a> -->


            <input type="hidden" id="totalThumb{{$property->id}}" value="{{count($thumbs)}}">
            @if(is_array($thumbs))
                @foreach ($thumbs as $key => $val)
                    <input type="hidden" id="thumb{{$property->id}}{{$key}}" value="{{url($val)}}">
                @endforeach
            @endif

            @if ($category == 'villas-for-sale')
                <input type="hidden" id="lat{{$property->id}}" value="{{$property->map_latitude}}">
                <input type="hidden" id="long{{$property->id}}" value="{{$property->map_longitude}}">
            @endif

        </div>

        <div class="box-detailz hiddenBox" id="boxdetail{{$property->id}}">
            <div class="detail-price-holder">
                <div class="the-price-holder"> 
                    <div class="currency">{{ Session::get('currency')}}</div>
                    <div class="nominal">
                        {{ convertCurrency($property->price, $property->currency, Session::get('currency')) }}
                    </div>
                </div>
                <div class="close-button box-link-detail">
                    <i class="fa fa-close"> </i>
                </div>
            </div>

            <div class="box-detail-desc"> {{ $property->title }} </div>

            <div class="main-detail">
                <ul>
                    <li>
                        <div class="title"><i class="fa fa-barcode"></i>Code</div>
                        <div class="values"> {{ $property->code }} </div>
                    </li>
                    <li>
                        <div class="title"><i class="fa fa-map-marker"></i>Location</div>
                        <div class="values"> {{ucfirst($property->city)}}, {{ucfirst($property->province)}} </div>
                    </li>
                    <li>
                        <div class="title"><i class="fa fa-clock-o"></i>Status</div>
                        <div class="values"> @if ($property->status == 0) {{trans('word.sold')}} @else {{trans('word.available')}} @endif </div>
                    </li>
                    @if ($category == 'villas-for-sale' OR $category == 'villas-for-rental')
                        <li>
                            <div class="title"><i class="fa fa-home"></i>Building Size</div>
                            <div class="values"> {{ $property->building_size }} </div>
                        </li>
                        <li>
                            <div class="title"><i class="fa fa-arrows-alt"></i>Land Size</div>
                            <div class="values"> {{ $property->land_size }} m2</div>
                        </li>
                        <li>
                            <div class="title"><i class="fa fa-bed"></i>Bedrooms</div>
                            <div class="values"> {{ $property->bedroom }} </div>
                        </li>
                    @else
                        <li>
                            <div class="title"><i class="fa fa-arrows-alt"></i>Land Size</div>
                            <div class="values"> {{ $property->land_size }} m2</div>
                        </li>
                    @endif
                </ul>
            </div>


            <!-- <a href="{{ route('property', ['property' => trans('url.property'), 'term' => $property_term] ) }}"> -->
            <div class="box-links-detail">

                <div class="box-link-enquiry" onclick="open_modal_enquiry()" id="objectsecond{{$property->id}}"><i
                            class="fa fa-envelope-o"></i>Enquiry
                </div>
                <div class="box-link-favorite"><i class="fa fa-star-o"></i> Favorite</div>

                <div class="box-view-more" onclick="location.href='{{ route('property', ['property' => trans('url.property'), 'term' => $property_term] ) }}' " >  <i class="fa fa-info-circle"></i> View More </div>



            </div>
            <!-- </a> -->


        </div>
    </div>



@endforeach

<div class="modal">
    <div class="modal-dialog hide">
        <div class="modal-header">
            <div class="title"><i class="fa fa-envelope-o"></i> Enquiry Form</div>
            <div class="close-button">
                <i class="fa fa-close" onclick="close_modal_enquiry()"> </i>
            </div>
        </div>
        <div class="modal-body">

            <div class="kesato-form">
                <form method="post" action="" id="validation-form">
                    <div class="form-body">
                        <div class="section">
                            <label for="property_detail_name" class="field prepend-icon">
                                <input type="text"  id="property_detail_name"
                                       class="pl-15 gui-input" placeholder="Full name">
                            </label>
                        </div>
                        <div class="section">
                            <label for="property_detail_email" class="field prepend-icon">
                                <input type="email" id="property_detail_email"
                                       class="gui-input" placeholder="Email address">
                            </label>
                        </div>
                        <div class="section">
                            <label for="property_detail_phone" class="field prepend-icon">
                                <input type="tel" id="property_detail_phone"
                                       class="gui-input phone-group" placeholder="Phone number">
                            </label>
                        </div>
                        <div class="section">
                            <label for="property_detail_message" class="field prepend-icon">
                                    <textarea class="gui-textarea" id="property_detail_message"
                                              placeholder="Your Message"></textarea>
                            </label>
                        </div>
                    </div>
                    <div class="form-footer">
                        <div class="display-flex">
                            <button type="submit" class="submit button btn-primary">Submit</button>
                        </div>
                    </div>
                </form>
            </div><!-- END KESATO FORM -->

        </div>
    </div>
</div>
<input type="hidden" id="newLatLong" value="{{json_encode($newLatLong)}}">
<input type="hidden" id="category" value="{{$category}}">
<input type="hidden" id="area" value="{{$area}}">
<input type="hidden" id="type" value="{{$type}}">
<div id="pagination">
    {!! $properties->render() !!}
</div>

<script>
 $(document).ready(function() {
    $(".property-list .box").each(function(e){ $(this).children('.box-detailz').css("height",$(this).children('.box-item').css("height"));})
 });
</script>
<!-- 
<div class="smartforms-px">
                        <a href="#" data-smart-modal="#contact-modal-form" class="smartforms-modal-trigger">Show Modal Form</a>
                    </div> -->


<!-- <div id="contact-modal-form" class="smartforms-modal" role="alert">
            <div class="smartforms-modal-container">
            
                <div class="smartforms-modal-header">
                    <h3>Contact Form</h3>
                    <a href="#" class="smartforms-modal-close">&times;</a>            
                </div><.smartforms-modal-header -->
                
                <!-- <div class="smartforms-modal-body">
                    <div class="smart-wrap">
                        <div class="smart-forms smart-container wrap-full">
                            ock
                        </div>end .smart-forms section
                    </div>< end .smart-wrap section            
                </div>< smartforms-modal-body
            </div> smartforms-modal-container
            
        </div> smartforms-modal --> 



