@extends('index')
@section('content')

<?php
$custLog = Auth::customer()->get();
?>
<div id="content" class="image-box">
    <div class="property-detail" data-id="{{ $property->id }}" data-customerId="{{ $custLog ? $custLog->id : 0 }}">
        <h1 class="name">{{ $property->lang()->title }}</h1>
        <p class="code">CODE : {{ $property->code }}</p>

        <div class="left-side">
            
            <div class="property-slide">
                <div class="property_detail_slide" tabindex="1" autofocus="autofocus">
                
                    @if(count($property->galleries) > 0)
                        @foreach($property->galleries as $gallery)
                        <figure>
                            <img src="{{ $gallery->file }}" alt=""/>
                            <!-- <figcaption>Lorem</figcaption> -->
                        </figure>
                        @endforeach
                    @else
                        <figure>
                            <img src="/assets/img/no-image.png" alt=""/>
                            <!-- <figcaption>Lorem</figcaption> -->
                        </figure>
                    @endif

                </div>              
            </div><!-- END PROPERTY SLIDE -->

            <div class="available">
                <div class="item">
                    <i class="material-icons icon" style="top:-2px;">place</i>
                    <strong style="text-transform: uppercase;">Denpasar</strong>
                    <p>Bali</p>
                </div>

                @if($property->categories[0]->id != 3)
                <div class="item">
                    <i class="material-icons icon" style="top:-2px;">hotel</i>
                    <strong>{{ $property->bedroom }} Bedrooms</strong>
                    <p>{{ $property->bathroom }} Bathrooms</p>
                </div>
                @endif

                <div class="item">
                    <i class="material-icons icon">zoom_out_map</i>
                    <strong>210 m<sup>2</sup></strong>
                    <p>Land 2 are</p>
                </div>
                <div class="item">
                    <i class="material-icons icon" style="top:-2px;">info_outline</i>
                    <strong style="text-transform: uppercase">{{ $property->type }}</strong>
                    <p>property</p>
                </div>
            </div>
            
            <!-- <div class="property-description">
                <div class="name">{{ $property->lang()->title }}, {{ $property->city }}</div>
                <div class="code">CODE : {{ $property->code }}</div>
                <div class="price" style="text-transform: uppercase">
                    {{ Session::get('currency') }} {{ convertCurrency($property->price, $property->currency, Session::get('currency')) }}
                </div>
                

                <div class="detail-page">

                    <div class="page-view-section-body">

                        <div class="container">
                            <div class="property-view-description-container">
                                <div class="property-description-row flexbox">
                                    <div class="property-description-column">
                                        <p>{{ trans('word.general_informations') }}</p>
                                    </div>
                                    <div class="property-description-column flexbox flexbox-wrap double">
                                        <p>Code: <strong>{{ $property->code }}</strong></p>
                                        <p>{{ trans('word.location') }}: <strong>{{ $property->city }}</strong></p>
                                        <p>{{ trans('word.land_size') }}: <strong>{{ $property->land_size }}</strong></p>

                                        <p>Status: <strong>{{ $property->type }}</strong></p>
                                        <p>{{ trans('word.year_built') }}: <strong>{{ $property->year }}</strong></p>
                                        <p>{{ trans('word.building_size') }}: <strong>{{ $property->building_size }}</strong></p>
                                    </div>
                                </div>

                                <div class="property-description-row flexbox">
                                    <div class="property-description-column">
                                        <p>{{ trans('word.facilities') }}</p>
                                    </div>
                                    <div class="property-description-column flexbox flexbox-wrap double">
                                        <?php $facilities = $property->facilities()->lists('name')->toArray() ?>
                                        @foreach(\Config::get('facility.sale') as $facility => $icon)
                                        <p class="property-facility{{ in_array($facility, $facilities) ? ' available' : '' }}">{!! in_array($facility, $facilities) ? '<i class="material-icons">' . $icon . '</i>' : '' !!} {{ $facility }}</p>
                                        @endforeach
                                    </div>
                                </div>

                                <div class="property-description-row flexbox" id="distance-row">
                                    <div class="property-description-column">
                                        <p>{{ trans('word.distance_to') }}</p>
                                    </div>
                                    <div class="property-description-column flexbox flexbox-wrap double">
                                        @foreach($property->distances as $distance)
                                        <p class="property-distance">{{ $distance->name }}: <strong>{{ $distance->value }}</strong></p>
                                        @endforeach
                                    </div>
                                </div>

                                <div class="property-description-row flexbox">
                                    <div class="property-description-column">
                                        <p>{{ trans('word.description') }}</p>
                                    </div>
                                    <div class="property-description-column double" id="property-description-container">
                                        {!! $property->lang()->content !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div><!-- END PROPERTY DESCRIPTION -->
            
        </div><!-- END LEFT SIDE -->
        
        
       <div class="right-side">

            <div class="price" style="text-transform: uppercase">
                <div class="currency">
                    <div>
                        <span>Currency</span>
                        <br>
                        {{ Session::get('currency') }}
                    </div>
                    <div> 
                        <i class="fa fa-dollar"></i> <i class="fa fa-chevron-down"></i>
                    </div>

                    <div class="other-currency">
                        <li><a href="{{ route('set_currency', 'usd') }}">USD</a></li>
                        <?php $altCurrencies = Config::get('currencies.alt_currencies'); ?>
                        @foreach($altCurrencies as $curr)
                        <li><a href="{{ route('set_currency', $curr) }}" style="text-transform: uppercase">{{ $curr }}</a></li>
                        @endforeach

                    </div>

                </div>
                <div class="regular-price">
                    {{ Session::get('currency') }} {{ convertCurrency($property->price, $property->currency, Session::get('currency')) }} 

                    @if($property->categories[0]->id == 2)
                    <sub>/ daily</sub>
                    @endif
                    
                </div>
            </div>
            <div class="enquiry">
                <div id="box-enquiry">
                    <a class="open-box-enquiry-form">
                        <div class="icon">
                           <i class="fa fa-envelope-o"></i>
                        </div>
                        <div class="content">
                            <div class="title">enquire this villa</div>
                            <div class="sub-title">simple form it take only 5 min</div>
                        </div>
                    </a> 
                    <div id="enquiry-popup" class="popup">
                        <div id="box-enquiry-form">
                            <div class="kesato-form form-inner">

                                {!! Form::open(['url' => route('api.enquiry.store'), 'id' => 'validation-form']) !!}

                                <input type="hidden" name="property_id" value="{{ $property->id }}">
                                <input type="hidden" name="price" value="{{ convertCurrency($property->price, $property->currency, Session::get('currency')) }} ">

                                <div class="form-body">
                                    <div class="section">
                                        <label for="property_detail_name" class="field prepend-icon">
                                            <input type="text" name="name" id="property_detail_name" class="pl-15 gui-input" placeholder="Full name">
                                        </label>
                                    </div>
                                    <div class="section">
                                        <label for="property_detail_phone" class="field prepend-icon">
                                            <input type="tel" name="phone" id="property_detail_phone" class="gui-input phone-group" placeholder="Phone number">
                                        </label>
                                    </div>
                                    <div class="section">
                                        <label for="property_detail_email" class="field prepend-icon">
                                            <input type="email" name="email" id="property_detail_email" class="gui-input" placeholder="Email address">
                                        </label>
                                    </div>
                                    <div class="section">
                                        <label for="property_detail_message" class="field prepend-icon">
                                            <textarea class="gui-textarea" id="property_detail_message" name="message" placeholder="Your Message"></textarea>
                                        </label>
                                    </div>
                                    
                                    <div class="section">
                                        <label for="" class="field prepend-icon">
                                            {!! Recaptcha::render() !!}
                                        </label>
                                    </div>
                                    
                                </div>
                                <div class="form-footer">
                                    <div class="display-flex">
                                        <button type="button" class="cancel" onclick="box_enquiry_hide()">Close</button>
                                        <button type="submit" class="submit">Submit</button>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div><!-- END KESATO FORM -->
                        </div><!-- END BOX ENQURY FORM -->
                    </div>
                </div><!-- END BOX ENQUIRY -->
                <div id="box-enquiry-form" style="display:none;">
                    <div class="kesato-form">

                    {!! Form::open(['url' => route('api.enquiry.store'), 'id' => 'validation-form']) !!}

                        <input type="hidden" name="property_id" value="{{ $property->id }}">
                        <input type="hidden" name="price" value="{{ convertCurrency($property->price, $property->currency, Session::get('currency')) }} ">

                        <div class="form-body">
                            <div class="section">
                                <label for="property_detail_name" class="field prepend-icon">
                                    <input type="text" name="name" id="property_detail_name" class="pl-15 gui-input" placeholder="Full name">
                                </label>
                            </div>
                            <div class="section">
                                <label for="property_detail_phone" class="field prepend-icon">
                                    <input type="tel" name="phone" id="property_detail_phone" class="gui-input phone-group" placeholder="Phone number">
                                </label>
                            </div>
                            <div class="section">
                                <label for="property_detail_email" class="field prepend-icon">
                                    <input type="email" name="email" id="property_detail_email" class="gui-input" placeholder="Email address">
                                </label>
                            </div>
                            <div class="section">
                                <label for="property_detail_message" class="field prepend-icon">
                                    <textarea class="gui-textarea" id="property_detail_message" name="message" placeholder="Your Message"></textarea>
                                </label>
                            </div>

                            <div class="section">
                              <label for="" class="field prepend-icon">
                                  <div class="g-recaptcha" data-sitekey="6Lf8DBsTAAAAAOwOKY1jXnTcC5fL5NVkaw3TkrKS" style=""></div>    
                              </label>
                            </div>

                        </div>
                        <div class="form-footer">
                            <div class="display-flex">
                                <button type="button" class="cancel" onclick="box_enquiry_hide()">Close</button>
                                <button type="submit" class="submit">Submit</button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                    </div><!-- END KESATO FORM -->
                </div><!-- END BOX ENQURY FORM -->
            </div><!-- END ENQUIRY -->
            
            <div class="share">
                <div class="share-holder display-flex">
                    <div class="print">
                        <a href="{{ request()->fullUrl() }}?print=pdf" target="_blank">Print</a>
                    </div>
                    <div class="wishlist">
                        <a href="" onclick="alert('Sorry,You must login first.');return false;">Add to Favorite</a>
                    </div>
                    <div class="email">
                        <a href="mailto:{{ $property->user->email }}?subject=Kibarer Property: {{ $property->code }}">Send E-mail</a>
                    </div>
                </div>
                <div class="sharing-buttons">
                    <div class="social-item">
                        <div class="facebook">
                            <a class="display-flex" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fwww.fourtonfish.com%2Fsimple-sharing-buttons-generator%2F" target="_blank">
                                <div class="facebook-icon">
                                    <i class="fa fa-facebook"></i>
                                </div>
                                <div class="facebook-label">
                                    <span class="small">SHARE ON</span> <br>
                                    <span class="big">FACEBOOK</span>
                                </div>
                            </a>
                        </div><!-- END FACEBOOK -->
                    </div><!-- END SOCIAL ITEM -->
                    <div class="social-item">
                        <div class="facebook">
                            <a class="display-flex" href="https://twitter.com/intent/tweet?url=http%3A%2F%2Fwww.fourtonfish.com%2Fsimple-sharing-buttons-generator%2F&text=Simple%20sharing%20buttons%20generator.&via=fourtonfish"target="_blank">
                                <div class="linkedin-icon">
                                    <i class="fa fa-linkedin"></i>
                                </div>
                                <div class="facebook-label">
                                    <span class="small">SHARE ON</span> <br>
                                    <span class="big">LINKEDIN</span>
                                </div>
                            </a>
                        </div><!-- END FACEBOOK -->
                    </div><!-- END SOCIAL ITEM -->
                    <div class="social-item">
                        <div class="google-plus">
                            <a class="display-flex" href="https://twitter.com/intent/tweet?url=http%3A%2F%2Fwww.fourtonfish.com%2Fsimple-sharing-buttons-generator%2F&text=Simple%20sharing%20buttons%20generator.&via=fourtonfish"target="_blank">
                                <div class="google-plus-icon">
                                    <i class="fa fa-google-plus"></i>
                                </div>
                                <div class="google-plus-label">
                                    <span class="small">SHARE ON</span> <br>
                                    <span class="big">GOOGLE +</span>
                                </div>
                            </a>
                        </div><!-- END FACEBOOK -->
                    </div><!-- END SOCIAL ITEM -->

                </div>
            </div><!-- END SHARE -->
            <div class="map-location">
                <!--
                <h6 class="side-title">Map Location : </h6>
                -->
                <div id="propertydetailMapLocation"></div>
            </div> 

        </div><!-- END RIGHT SIDE -->
        
    </div><!-- END PROPERTIES -->

</div><!-- END MAIN -->

<div class="property-description">
    <div id="content" class="height1">
        
    <div class="description">
        <div class="detail-page">
            <div class="page-view-section-body">
                <div class="container">
<!--                     <div class="kesato-form"> 
                        <div class="frm-row">
                            <div class="section colm colm6">
                             <div class="fieldset">
                                  <div class="field-custom">
                                      <div class="field-bedroom">
                                          <label class="field select">
                                              <select id="bedroom_villa_sell" name="bedroom_villa_sell">
                                                  <option value="">Land Size</option>
                                                  <option value="">are</option>
                                                  <option value="">sqm</option>
                                                  <option value="">sq ft</option> 
                                              </select>
                                              <i class="arrow"></i>
                                          </label>
                                      </div>
                                  </div>
                              </div>
                            </div>

                            <div class="section colm colm6">
                             <div class="fieldset">
                                  <div class="field-custom">
                                      <div class="field-bedroom">
                                          <label class="field select">
                                              <select id="bathroom_villa_sell" name="bathroom_villa_sell">
                                                  <option value="">Building Size</option>
                                                  <option value="">are</option>
                                                  <option value="">sqm</option>
                                                  <option value="">sq ft</option> 
                                              </select>
                                              <i class="arrow"></i>
                                          </label>
                                      </div>
                                  </div>
                              </div>
                            </div>
                            </div>
                    </div> -->
                    <div class="property-view-description-container">
                    

                        <div class="property-description-row flexbox"> 
                            <div class="property-description-column">
                                <p>{{ trans('word.general_informations') }}</p>
                            </div>
                            <div class="property-description-column flexbox flexbox-wrap double">
                                <p>Code: <strong>{{ $property->code }}</strong></p>
                                <p>{{ trans('word.location') }}: <strong>{{ $property->city }}</strong></p>
                                <p>{{ trans('word.land_size') }}: <strong>{{ $property->land_size }} 
                                
                                <span class="select-wrapper">
                                  <select class="select-converter">
                                    <option>are</option>
                                    <option>sqm</option>
                                    <option>sq ft</option>
                                  </select>
                                  <i class="material-icons">keyboard_arrow_down</i>
                                </span>

                                
                                </strong>  </p>  
                                <p>Status: <strong>{{ $property->type }}</strong></p>
                                <p>{{ trans('word.year_built') }}: <strong>{{ $property->year }}</strong></p>

                                @if($property->categories[0]->id != 3)
                                <p>{{ trans('word.building_size') }}: <strong>{{ $property->building_size }} 

                                <span class="select-wrapper">
                                  <select class="select-converter">
                                    <option>are</option>
                                    <option>sqm</option>
                                    <option>sq ft</option>
                                  </select>
                                  <i class="material-icons">keyboard_arrow_down</i>
                                </span>
                                
                     
                                </strong></p>
                                @endif
                                
                                <!-- <div class="the-converter">
                                    <ul>
                                        <li>are</li>
                                        <li>sqm</li>
                                        <li>sq ft</li>
                                    </ul>
                                </div> -->
                            </div>
                        </div>

                        @if($property->categories[0]->id == 2)
                        <div class="property-description-row flexbox">
                            <div class="property-description-column">
                                <p>{{ trans('word.facilities') }}</p>
                            </div>
                            <div class="property-description-column flexbox flexbox-wrap double">
                                <?php $facilities = $property->facilities()->lists('name')->toArray() ?>
                                @foreach(\Config::get('facility.sale') as $facility => $icon)
                                <p class="property-facility{{ in_array($facility, $facilities) ? ' available' : '' }}">{!! in_array($facility, $facilities) ? '<i class="material-icons">' . $icon . '</i>' : '' !!} {{ $facility }}</p>
                                @endforeach
                            </div>
                        </div>
                        @endif

                        <div class="property-description-row flexbox" id="distance-row">
                            <div class="property-description-column">
                                <p>{{ trans('word.distance_to') }}</p>
                            </div>
                            <div class="property-description-column flexbox flexbox-wrap double">
                                @foreach($property->distances as $distance)
                                <p class="property-distance">{{ $distance->name }}: <strong>{{ $distance->value }}</strong></p>
                                @endforeach
                            </div>
                        </div>

                        <div class="property-description-row flexbox">
                            <div class="property-description-column">
                                <p>{{ trans('word.description') }}</p>
                            </div>
                            <div class="property-description-column double" id="property-description-container">
                                {!! $property->lang()->content !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</div><!-- END PROPERTY DESCRIPTION -->


<!-- IDENTITY PAGE -->
<div class="data hidden">
    <input type="hidden" id="CURRENTPAGE" value="propertydetail" />
</div>



@endsection

@section('scripts')
<script src="http://maps.googleapis.com/maps/api/js?libraries=places"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script src="assets/js/hammer.min.js"></script>
<script type="text/javascript">

    $(document).on('click', '.converter', function(event) {
        event.preventDefault();
        
        open_converter();
    });

    $(document).on('click', '.open-box-enquiry-form', function(event) {
        event.preventDefault();

        //$(this).removeClass('open-box-enquiry-form').addClass('close-box-enquiry-form')
        
        //box_enquiry_show();
    });

    $(document).on('click', '.close-box-enquiry-form', function(event) {
        event.preventDefault();

        //$(this).removeClass('close-box-enquiry-form').addClass('open-box-enquiry-form')
        
        //box_enquiry_hide();
    });

    $(document).on('click', '#print-property', function(event) {
        event.preventDefault();

        window.print();
    });

    $(document).on('click', '#add-wishlist', function(event) {
        event.preventDefault();

        console.log('favorite clicked');

        var propertyId = $('.property-detail').attr('data-id');

        var customerId = $('.property-detail').attr('data-customerId');

        if (customerId != 0) {

            var url = "{{ route('api.wishlist.store') }}";
            var token = "{{ csrf_token() }}";

            $.post(url, {property_id: propertyId, customer_id: customerId, _token: token}, function(data, textStatus, xhr) {

                console.log(data);

                if (data.status == 200) {

                    alert('saved to wishlist');
                }

                if (data.status == 300) {

                    alert('saved to wishlist');
                }

            });

        } else {

            alert('Please login!');
        }

        event.preventDefault();
    });

    $('#validation-form').submit(function(event) {
        event.preventDefault();

        $('.state-error').remove();

        var btn = $('button[type=submit]'),
        btn_text = btn.html();

        btn.html("{{ trans('word.sending') }}");

        btn.prop('disabled', true);

        console.log('send enquiry');

        var url = $(this).attr('action');
        var data = $(this).serialize();

        $.post(url, data, function(data, textStatus, xhr) {

            console.log(data);

            if (data.status == 200) {

                $('input[name=name]').val('');
                $('input[name=email]').val('');
                $('input[name=phone]').val('');
                // $('input[name=subject]').val('');
                $('textarea').val('');

                $('form').append('<em for="property_detail_email" class="state-success">Thank You. Your enquiry has been sent successfully.</em>')

                console.log(data.monolog.message);
            }

            if (data.status == 500) {

                if (data.monolog.message.name) $('input[name=name]').closest('.section').append('<em for="property_detail_email" class="state-error">'+ data.monolog.message.name +'</em>');

                if (data.monolog.message.phone) $('input[name=phone]').closest('.section').append('<em for="property_detail_email" class="state-error">'+ data.monolog.message.phone +'</em>');

                if (data.monolog.message.email) $('input[name=email]').closest('.section').append('<em for="property_detail_email" class="state-error">'+ data.monolog.message.email +'</em>');

                if (data.monolog.message.message) $('textarea[name=message]').closest('.section').append('<em for="property_detail_email" class="state-error">'+ data.monolog.message.message +'</em>');
            }

            btn.html(btn_text);

            btn.prop('disabled', false);
        });
    });

</script>


@endsection