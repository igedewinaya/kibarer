@extends('index')
@section('styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/8.3.0/nouislider.min.css">
<style type="text/css">
   .kesato-form .state-error {
   display: block!important;
   margin-top: 6px;
   padding: 0 3px;
   font-family: Arial, Helvetica, sans-serif;
   font-style: normal;
   line-height: normal;
   font-size: 0.85em;
   color: #DE888A;
   }
   .noUi-connect {
   background: #ee5b2c;
   }
   #fountainTextG {
   width: 234px;
   margin: auto;
   }
   .fountainTextG {
   color: rgb(0, 0, 0);
   font-family: Arial;
   font-size: 24px;
   text-decoration: none;
   font-weight: normal;
   font-style: normal;
   float: left;
   animation-name: bounce_fountainTextG;
   -o-animation-name: bounce_fountainTextG;
   -ms-animation-name: bounce_fountainTextG;
   -webkit-animation-name: bounce_fountainTextG;
   -moz-animation-name: bounce_fountainTextG;
   animation-duration: 2.09s;
   -o-animation-duration: 2.09s;
   -ms-animation-duration: 2.09s;
   -webkit-animation-duration: 2.09s;
   -moz-animation-duration: 2.09s;
   animation-iteration-count: infinite;
   -o-animation-iteration-count: infinite;
   -ms-animation-iteration-count: infinite;
   -webkit-animation-iteration-count: infinite;
   -moz-animation-iteration-count: infinite;
   animation-direction: normal;
   -o-animation-direction: normal;
   -ms-animation-direction: normal;
   -webkit-animation-direction: normal;
   -moz-animation-direction: normal;
   transform: scale(.5);
   -o-transform: scale(.5);
   -ms-transform: scale(.5);
   -webkit-transform: scale(.5);
   -moz-transform: scale(.5);
   }
   #fountainTextG_1 {
   animation-delay: 0.75s;
   -o-animation-delay: 0.75s;
   -ms-animation-delay: 0.75s;
   -webkit-animation-delay: 0.75s;
   -moz-animation-delay: 0.75s;
   }
   #fountainTextG_2 {
   animation-delay: 0.9s;
   -o-animation-delay: 0.9s;
   -ms-animation-delay: 0.9s;
   -webkit-animation-delay: 0.9s;
   -moz-animation-delay: 0.9s;
   }
   #fountainTextG_3 {
   animation-delay: 1.05s;
   -o-animation-delay: 1.05s;
   -ms-animation-delay: 1.05s;
   -webkit-animation-delay: 1.05s;
   -moz-animation-delay: 1.05s;
   }
   #fountainTextG_4 {
   animation-delay: 1.2s;
   -o-animation-delay: 1.2s;
   -ms-animation-delay: 1.2s;
   -webkit-animation-delay: 1.2s;
   -moz-animation-delay: 1.2s;
   }
   #fountainTextG_5 {
   animation-delay: 1.35s;
   -o-animation-delay: 1.35s;
   -ms-animation-delay: 1.35s;
   -webkit-animation-delay: 1.35s;
   -moz-animation-delay: 1.35s;
   }
   #fountainTextG_6 {
   animation-delay: 1.5s;
   -o-animation-delay: 1.5s;
   -ms-animation-delay: 1.5s;
   -webkit-animation-delay: 1.5s;
   -moz-animation-delay: 1.5s;
   }
   #fountainTextG_7 {
   animation-delay: 1.64s;
   -o-animation-delay: 1.64s;
   -ms-animation-delay: 1.64s;
   -webkit-animation-delay: 1.64s;
   -moz-animation-delay: 1.64s;
   }
   @keyframes bounce_fountainTextG {
   0% {
   transform: scale(1);
   color: rgb(0, 0, 0);
   }
   100% {
   transform: scale(.5);
   color: rgb(255, 255, 255);
   }
   }
   @-o-keyframes bounce_fountainTextG {
   0% {
   -o-transform: scale(1);
   color: rgb(0, 0, 0);
   }
   100% {
   -o-transform: scale(.5);
   color: rgb(255, 255, 255);
   }
   }
   @-ms-keyframes bounce_fountainTextG {
   0
   %
   {
   -ms-transform: scale(1)
   ;
   color: rgb(0, 0, 0)
   ;
   }
   100
   %
   {
   -ms-transform: scale(.5)
   ;
   color: rgb(255, 255, 255)
   ;
   }
   }
   @-webkit-keyframes bounce_fountainTextG {
   0% {
   -webkit-transform: scale(1);
   color: rgb(0, 0, 0);
   }
   100% {
   -webkit-transform: scale(.5);
   color: rgb(255, 255, 255);
   }
   }
   @-moz-keyframes bounce_fountainTextG {
   0% {
   -moz-transform: scale(1);
   color: rgb(0, 0, 0);
   }
   100% {
   -moz-transform: scale(.5);
   color: rgb(255, 255, 255);
   }
   }
</style>
@endsection
@section('content')
<section class="breadcrumb" style="background-image:url('assets/img/testimonials-bg.jpg');">
    <div class="breadcrumb-holder">
        <div class="page-title ta-center">
            <h3 class="white">
                {{ trans('word.breadcrumb_sell_property') }}
            </h3>
            <p class="white fw-300">
                Find your property in the different area of Bali
            </p>
        </div>
    </div>
</section>
<div id="sell-poperty-main">
   <div class="sell-property-holder">
      <div class="input-propety-box">
         <div class="input-holder">
            <div class="sell-poperty-categories">
               <ul>
                  <a href="#villa-sell" data-category="1" class="active">
                     <li>Villa Sell</li>
                  </a>
                  <a href="#villa-rent" data-category="2">
                     <li>Villa Rent</li>
                  </a>
                  <a href="#land" data-category="3">
                     <li>Land</li>
                  </a>
               </ul>
            </div>
            {!! Form::open(['url' => route('sellproperty.store'), 'id' => 'validation-form', 'files'=> true]) !!}
            <div class="kesato-form">
               <input type="hidden" value="0" name="map_latitude" id="map_latitude">
               <input type="hidden" value="0" name="map_longitude" id="map_longitude">

               <input value="1" type="hidden" name="category" id="property-category">

               <div class="form-body">

                  <div class="frm-row">
                     <div class="section colm colm6">
                        <label for="sell_firstname" class="field prepend-icon">
                        <input type="text" name="owner_firstname" id="sell_firstname" class="pl-15 gui-input" placeholder="First name">
                        </label>
                     </div>
                     <div class="section colm colm6">
                        <label for="sell_lastname" class="field prepend-icon">
                        <input type="text" name="owner_lastname" id="sell_lastname" class="gui-input" placeholder="Last name">
                        </label>
                     </div>
                  </div>
                  <div class="section">
                     <label for="sell_email" class="field prepend-icon">
                     <input type="email" name="owner_email" id="sell_email" class="gui-input" placeholder="Email address">
                     </label>
                  </div>
                  <div class="section">
                     <label for="sell_phone" class="field prepend-icon">
                     <input type="tel" name="owner_phone" id="sell_phone" class="gui-input phone-group" placeholder="Phone number">
                     </label>
                  </div>

                  <!-- LOCATION -->
                  <div class="section ">
                     <label for="land_size_land" class="field prepend-icon">
                     <input id="pac-input-filter" type="text" name="location" class="pl-35 gui-input" placeholder="Enter your address">
                     <label for="firstname" class="field-icon"><i
                        class="fa fa-map-marker"></i></label>
                     </label>
                  </div>

                  <div class="frm-row">
                     <div class="section colm colm9">
                        <label for="price_villa_sell" class="field prepend-icon">
                        <input type="number" min="0" name="price" id="price_villa_sell" class="pl-15 gui-input" placeholder="Price">
                        </label>
                     </div>
                     <div class="section colm colm3">
                        <div class="fieldset">
                           <div class="field-custom">
                              <div class="field-bedroom">
                                 <label class="field select">
                                    <select id="currency_villa_sell" name="currency">
                                       <option value="">USD</option>
                                       <option value="">IDR</option>
                                       <option value="">EUR</option>
                                       <option value="">AUD</option>
                                       <option value="">SGD</option>
                                    </select>
                                    <i class="arrow"></i>
                                 </label>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>                  

                  <div class="frm-row property-type">
                     
                     <div class="section colm colm6">
                          <label for="leasehold" class="option block">
                              <input type="radio" name="type" id="leasehold" value="lease hold" checked="checked">
                              <span class="radio"></span> LEASE HOLD
                          </label>                                                
                     </div>
                     <div class="section colm colm6">
                          <label for="freehold" class="option block">
                              <input type="radio" name="type" id="freehold" value="free hold">
                              <span class="radio"></span> FREE HOLD
                          </label>                                                
                     </div>

                  </div>

                  <!-- CHANGEABLE -->
                  <div class="form-holder " id="villa-sell">
                     <div class="frm-row">
                        <div class="section colm colm6">
                           <label for="building_size" class="field prepend-icon">
                           <input type="number" min="0" name="building_size_villa_sell" id="building_size" class="pl-15 gui-input" placeholder="Building Size ( In meter square by default )">
                           </label>
                        </div>
                        <div class="section colm colm6">
                           <label for="land_size" class="field prepend-icon">
                           <input type="number" min="0" name="land_size_villa_sell" id="land_size" class="pl-15 gui-input" placeholder="Land Size ( In meter square by default ) ">
                           </label>
                        </div>
                     </div>
                     <div class="frm-row">
                        <div class="section colm colm6">
                           <div class="fieldset">
                              <div class="field-custom">
                                 <div class="field-bedroom">
                                    <label class="field select">
                                       <select id="bedroom_villa_sell" name="bedroom_villa_sell">
                                          <option value="">-- Bedroom --</option>
                                          <option value="">1</option>
                                          <option value="">2</option>
                                          <option value="">3</option>
                                          <option value="">4</option>
                                          <option value="">5</option>
                                          <option value="">6</option>
                                       </select>
                                       <i class="arrow"></i>
                                    </label>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="section colm colm6">
                           <div class="fieldset">
                              <div class="field-custom">
                                 <div class="field-bedroom">
                                    <label class="field select">
                                       <select id="bathroom_villa_sell" name="bathroom_villa_sell">
                                          <option value="">-- Bathroom --</option>
                                          <option value="">1</option>
                                          <option value="">2</option>
                                          <option value="">3</option>
                                          <option value="">4</option>
                                          <option value="">5</option>
                                          <option value="">6</option>
                                       </select>
                                       <i class="arrow"></i>
                                    </label>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>

                  <div class="form-holder disable" id="villa-rent">
                     <div class="frm-row">
                        <div class="section colm colm6">
                           <label for="building_size" class="field prepend-icon">
                           <input type="number" min="0" name="building_size_villa_rent" id="building_size" class="pl-15 gui-input" placeholder="Building Size ( In meter square by default )">
                           </label>
                        </div>
                        <div class="section colm colm6">
                           <label for="land_size" class="field prepend-icon">
                           <input type="number" min="0" name="land_size_villa_rent" id="land_size" class="pl-15 gui-input" placeholder="Land Size ( In meter square by default ) ">
                           </label>
                        </div>
                     </div>
                     <div class="frm-row">
                        <div class="section colm colm6">
                           <div class="fieldset">
                              <div class="field-custom">
                                 <div class="field-bedroom">
                                    <label class="field select">
                                       <select id="bedroom_villa_rent" name="bedroom_villa_rent">
                                          <option value="">-- Bedroom --</option>
                                          <option value="">1</option>
                                          <option value="">2</option>
                                          <option value="">3</option>
                                          <option value="">4</option>
                                          <option value="">5</option>
                                          <option value="">6</option>
                                       </select>
                                       <i class="arrow"></i>
                                    </label>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="section colm colm6">
                           <div class="fieldset">
                              <div class="field-custom">
                                 <div class="field-bedroom">
                                    <label class="field select">
                                       <select id="bathroom_villa_rent" name="bathroom_villa_rent">
                                          <option value="">-- Bathroom --</option>
                                          <option value="">1</option>
                                          <option value="">2</option>
                                          <option value="">3</option>
                                          <option value="">4</option>
                                          <option value="">5</option>
                                          <option value="">6</option>
                                       </select>
                                       <i class="arrow"></i>
                                    </label>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="section">
                        <div class="fieldset fieldset-checkbox">
                           <div class="label-custom">
                              Facilities
                           </div>
                           <div class="field-custom field-custom-checkbox">
                              @foreach(\Config::get('facility.sale') as $key => $val) 
                              <div class="field-amenities-item"> 
                                 <label class="option block"> 
                                 <input type="checkbox" name="facility_name[]" value="{{ $key }}"> 
                                 <span class="checkbox"></span> {{ ucwords($key) }} 
                                 </label> 
                              </div>
                              @endforeach
                           </div>
                        </div>
                        <!-- END FIELDSET -->
                     </div>
                  </div>

                 <div id="land" class="form-holder disable">
                     <div class="section colm colm6">
                        <label for="land_size_land" class="field prepend-icon">
                        <input type="number" min="0" name="land_size_land" id="land_size_land" class="pl-15 gui-input" placeholder="Land Size ( In meter square by default ) ">
                        </label>
                     </div>
                 </div>

                 <div class="section">
                    <div class="img-upload-holder">
                       <div class="form-group file-area">
                          <input type="file" name="images[]" class="images"/>
                          <div class="file-dummy">
                             <div class="success"><i class="fa fa-check"></i></div>
                             <div class="default"><i class="fa fa-picture-o"></i></div>
                             <div class="upload-title">Please select some picture</div>
                          </div>
                       </div>
                       <div class="form-group file-area">
                          <input type="file" name="images[]" class="images"/>
                          <div class="file-dummy">
                             <div class="success"><i class="fa fa-check"></i></div>
                             <div class="default"><i class="fa fa-picture-o"></i></div>
                             <div class="upload-title">Please select some picture</div>
                          </div>
                       </div>
                       <div class="form-group file-area">
                          <input type="file" name="images[]" class="images"/>
                          <div class="file-dummy">
                             <div class="success"><i class="fa fa-check"></i></div>
                             <div class="default"><i class="fa fa-picture-o"></i></div>
                             <div class="upload-title">Please select some picture</div>
                          </div>
                       </div>
                       <div class="form-group file-area">
                          <input type="file" name="images[]" class="images"/>
                          <div class="file-dummy">
                             <div class="success"><i class="fa fa-check"></i></div>
                             <div class="default"><i class="fa fa-picture-o"></i></div>
                             <div class="upload-title">Please select some picture</div>
                          </div>
                       </div>
                    </div>
                 </div>

                  <div class="section">
                     <label for="" class="field prepend-icon">
                        {!! Recaptcha::render() !!}
                     </label>
                  </div>
                  <div class="form-footer">
                     <button id="sell-submit" type="submit" class="button btn-primary">Submit</button>
                     <p><em id="notif_success" class="state-success" style="color:green"></em></p>
                  </div>

                  <!-- END VILLA -->
               </div>
               <!-- end .form-body section -->
            </div>
            <!-- END KESATO FORM -->

            {!! Form::close() !!}
         </div>
      </div>
      <!-- ENG REGISTER BOX -->
      <div class="map-property-box">
         <div class="notice">
            <div class="content">
               <div class="alert-sell">
                  <p> This is a clickable map. You can click the location of your villa on the map and the marker will appear. </p>
               </div>
               <div class="close-sell"><i class="fa fa-close close-holder"></i></div>
            </div>
         </div>
         <div class="map-tooltip show-tooltip">
            <div id="map" style=""></div>
         </div>
         <input id="pac-input" class="controls" type="text" placeholder="Enter a location" style=" display: none; z-index: 0; position: absolute; left: 107px; top: 0px; width: 50%; argin-top: 10px; padding: 10px 15px; background: white; color: #666; border: none; border-bottom-left-radius: 2px; border-top-left-radius: 2px; webkit-background-clip: padding-box; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; background-clip: padding-box; font-family: Roboto, Arial, sans-serif; -webkit-user-select: none; font-size: 12px; margin: 30px; font-weight: 400;">
      </div>
      <!-- END LOGIN BOX -->
   </div>
   <!-- END ACCOUNT -->
</div>
<!-- END MAIN -->
<!-- FOOTER -->

@endsection

@section('scripts')
<script src='https://www.google.com/recaptcha/api.js'></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBdeZlfIdUJZR6TA1TSCQS6v5HcLdlbZR8&callback=initMap&libraries=places"></script>
<script type="text/javascript">

$(document).on('click', '.close-sell', function(event) {
   event.preventDefault();
   $(this).closest('.notice').hide();
   $('.map-tooltip').removeClass('show-tooltip');
});

$(document).on('change', '.images', function(event) {
   event.preventDefault();

   var input = $(this);
   var reader = new FileReader();

   var preview = $(this).siblings('.file-dummy');

   reader.onload = function (e) {
     // get loaded data and render thumbnail.
     // document.getElementById("image").src = e.target.result;

     preview.html('');

     var el = '<img style="height: 100%" src="'+ e.target.result +'" alt="">';
     preview.append(el);
   };

   // read the image file as a data URL.
   reader.readAsDataURL(this.files[0]);

});


   function initMap() {
             var boxSearch = $('#box');
             var allMarker = [];
             var myLatLng = {lat: -8.4420734, lng: 114.9356164};
             var pin = 'assets/img/marker.png';

             var markers = [];
   
   
             var map = new google.maps.Map(document.getElementById('map'), {
                 mapTypeControl: false,
                 center: myLatLng,
                 scrollwheel: true,
                 zoom: 10,
                 icon: pin,
                 maxZoom: 13,
                 minZoom: 8,
                 draggable: true,
                 zoomControl: true,
                 disableDoubleClickZoom: false,
                 streetViewControl: false
             });

             // placeMarker(myLatLng, map);
   
             var input = document.getElementById('pac-input-filter'),
             searchBox = new google.maps.places.SearchBox(input);


            searchBox.addListener('places_changed', function() {
               removeMarkers();
                var places = searchBox.getPlaces();

                if (places.length == 0) {
                    return;
                }

                var bounds = new google.maps.LatLngBounds();
                places.forEach(function(place) {

                    if (place.geometry.viewport) {
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }
                });
                map.fitBounds(bounds);

                placeMarker(map.getCenter(), map);

                $('#map_latitude').val(map.getCenter().lat());
                $('#map_longitude').val(map.getCenter().lng());
            });

            google.maps.event.addListener(map, "click", function (e) {
                removeMarkers();
                var latLng = e.latLng,
                    strlatlng = latLng.toString(),
                    spllatlng = strlatlng.split(','),
                    lats = spllatlng[0].replace("(", ""), 
                    longs = spllatlng[1].replace(")", "");
                
                placeMarker(latLng, map);

                $("#map_latitude").val(lats);
                $("#map_longitude").val(longs);

            });

            function getCityName(location)
            {
                var geocoder;
                geocoder = new google.maps.Geocoder();
                var latlng = location;

                geocoder.geocode(
                    {'latLng': latlng}, 
                    function(results, status) {

                        if (status == google.maps.GeocoderStatus.OK) {

                                console.log(results);

                                var address = [];

                                if (results[0]) {

                                    for (var i=1; i<5; i++) {

                                        if (results[0].address_components[i] != null) {
                                            address.push(results[0].address_components[i].long_name);
                                        }
                                    }

                                    var location = address.join(", ");

                                    $('#pac-input-filter').val(location);

                                }

                        }

                    }
                );
            }

            function placeMarker(location, map) {
                var marker = new google.maps.Marker({
                    position: location, 
                    icon: pin,
                    map: map
                });

                markers.push(marker);

                getCityName(location);
            }

            function removeMarkers() {
                for(var i = 0; i < markers.length; i++) {
                    markers[i].setMap(null);
                }
            }  

   }
   
   $(document).on('submit', '#validation-form', function(event) {
     /* Act on the event */
     event.preventDefault();
   
     $('.state-error').remove();
   
     console.log('send property');
   
     $('button[type=submit]').html("{{ trans('word.please_wait') }}");

     $('button[type=submit]').prop('disabled', true);
   
     var url = $(this).attr('action'),
         fd = new FormData($('#validation-form')[0]);

     $.ajax({

         url: url,

         type: 'POST',

         data: fd,

         processData: false,  // tell jQuery not to process the data. IMPORTANT for file upload.

         contentType: false,   // tell jQuery not to set contentType. IMPORTANT for file upload.

         dataType: 'json',

         async: true,

         error: function(xhr, status, message){

         // consoleLog(xhr.responseText);

     }}).done(function(data) {

         if (data.status == 200) {
   
           $('#notif_success').html(data.monolog.message);

           var token = $('input[name=_token]').val();

           $('input').val('');

           $('input[name=_token]').val(token);

           var el = ''
           + '<div class="default"><i class="fa fa-picture-o"></i></div>'
           + '<div class="upload-title">Please select some picture</div>';

           $('.file-dummy').html(el);
   
         }
   
         if (data.status == 500) {
   
             if (data.monolog.message.owner_firstname)
               $('input[name=owner_firstname]').closest('.section').append('<em for="reg_firstname" class="state-error">'+ data.monolog.message.owner_firstname +'</em>');
   
             if (data.monolog.message.owner_phone)
               $('input[name=owner_phone]').closest('.section').append('<em for="reg_phone" class="state-error">'+ data.monolog.message.owner_phone +'</em>');
   
             if (data.monolog.message.location)
               $('input[name=location]').closest('.section').append('<em for="reg_phone" class="state-error">'+ data.monolog.message.location +'</em>');
   
   
         }
   
         $('button[type=submit]').html("{{ trans('word.send') }}");

         $('button[type=submit]').prop('disabled', false);

     });

   
     // $.post(url, data, function(data, textStatus, xhr) {
     //     /*optional stuff to do after success */
   
     //     if (data.status == 200) {
   
     //       $('#notif_success').html(data.monolog.message);
   
     //     }
   
     //     if (data.status == 500) {
   
     //         if (data.monolog.message.owner_firstname)
     //           $('input[name=owner_firstname]').closest('.section').append('<em for="reg_firstname" class="state-error">'+ data.monolog.message.owner_firstname +'</em>');
   
     //         if (data.monolog.message.owner_phone)
     //           $('input[name=owner_phone]').closest('.section').append('<em for="reg_phone" class="state-error">'+ data.monolog.message.owner_phone +'</em>');
   
   
     //     }
   
     //     $('button[type=submit]').html("{{ trans('word.send') }}");
     // });
   
   });
</script>
<script>
   $( '.sell-poperty-categories a').on('click', function(e){
       e.preventDefault();

       var category = $(this).attr('data-category');

       if (category == 2) {
         $('.property-type').hide();
       } else {
         $('.property-type').show();
       }

       $('#property-category').val(category);
   
       $('.sell-poperty-categories a').removeClass('active');
   
       $(this).addClass('active');
   
       var x = $(this).attr('href');
       //console.log(x);
       $('.form-holder').hide();
       $(x).fadeIn();
       return false;
   });
   
</script>
@endsection