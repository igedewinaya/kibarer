@extends('index')


@section('content')
<section class="breadcrumb" style="background-image:url('assets/img/testimonials-bg.jpg');">
    <div class="breadcrumb-holder">
        <div class="page-title ta-center">
            <h3 class="white">
                {{ trans('word.breadcrumb_contact') }}
            </h3>
            <p class="white fw-300">
                Find your property in the different area of Bali
            </p>
        </div>
    </div>
</section>

<div id="contact-main">
   
    <div class="map-property-box">
        <div id="map-contact" style="width:100%; height: 400px;"></div>   
    </div><!-- END LOGIN BOX -->
    
    <div id="content" class="contact-holder">
        <div class="input-propety-box">
            <div class="input-holder flexbox">
                
                <div class="contact-form">
                    <h1>Send us an email</h1>
                    <div class="kesato-form">
                            {!! Form::open(['url' => route('api.message.store'), 'id' => 'validation-form']) !!}
                            <div class="form-body">
                                <div class="frm-row">
                                    <div class="section colm colm6">
                                        <label for="sell_firstname" class="field prepend-icon">
                                            <input value="{{ old('firstname') }}" type="text" name="firstname" id="sell_firstname" class="pl-15 gui-input" placeholder="First name">
                                        </label>
                                    </div>
                                    <div class="section colm colm6">
                                        <label for="sell_lastname" class="field prepend-icon">
                                            <input value="{{ old('lastname') }}" type="text" name="lastname" id="sell_lastname" class="gui-input" placeholder="Last name">
                                        </label>
                                    </div>
                                </div>
                                <div class="section">
                                    <label for="sell_email" class="field prepend-icon">
                                        <input value="{{ old('email') }}" type="email" name="email" id="sell_email" class="gui-input" placeholder="Email address">
                                    </label>
                                </div>

                                <div class="section">
                                    <label for="sell_desc" class="field prepend-icon">
                                        <textarea class="gui-textarea" id="sell_desc" name="message" placeholder="Description">{{ old('message') }}</textarea>
                                    </label>
                                </div>

                                <div class="section">
                                  <label for="" class="field prepend-icon">
                                      <div class="g-recaptcha" data-sitekey="6Lf8DBsTAAAAAOwOKY1jXnTcC5fL5NVkaw3TkrKS" style=""></div>    
                                  </label>
                                </div>
                  
                            </div><!-- end .form-body section -->

                            <div class="form-footer">
                                <button type="submit" class="button btn-primary">{{ trans('word.send') }}</button>
                            </div>
                            {!! Form::close() !!}
                    </div><!-- END KESATO FORM -->
                </div>
                
                <div class="content">
                    <?php $company_contact = json_decode(\File::get(storage_path('json/general_info.json'))); ?>
                    <p class="desc">
                        {{ trans('notification.contact_description') }}
                    </p>
                    <div class="email">
                        <h6 class="title"> {{ trans('word.email') }} </h6>
                        <div class="title-border-bottom"></div>
                        <a href="mailto:{{ $company_contact->email }}">{{ $company_contact->email }}</a>
                    </div>
                    <div class="phone">
                        <h6 class="title"> {{ trans('word.phone') }} </h6>
                        <div class="title-border-bottom"></div>
                        <a href="tel:623614741212">{{ $company_contact->phone }}</a>
                    </div>
                    <div class="address">
                       <h6 class="title"> {{ trans('word.address') }} </h6>
                        <div class="title-border-bottom"></div>
                        <p>{{ $company_contact->address }}</p>
                    </div>
                </div>
                
            </div>
        </div><!-- ENG REGISTER BOX -->

    </div><!-- END ACCOUNT -->
</div><!-- END MAIN -->

@endsection


@section('scripts')
    <script src="http://maps.googleapis.com/maps/api/js?libraries=places"></script>
	<script src='https://www.google.com/recaptcha/api.js'></script>
	<script type="text/javascript">
    
      Kibarer.initContact();
      
      $(document).on('click', 'button[type=submit]', function(event) {
        event.preventDefault();
        /* Act on the event */

        $('.state-error').remove();

        var word = $(this).html();

        console.log('submit clicked');

        $(this).html("{{ trans('word.sending') }}");

        var frm = $('#validation-form'),
            url = frm.attr('action'),
            data = frm.serialize();

        $.post(url, data, function(data, textStatus, xhr) {

          if (data.status == 200) {
            
            console.log(data);

            $('input[name=firstname]').val('');
            $('input[name=lastname]').val('');
            $('input[name=email]').val('');
            $('textarea').val('');

          } else {

            console.log(data);

            if (data.monolog.message.firstname) {
              var notif = '<em for="sell_firstname" class="state-error" style="color: red;">' + data.monolog.message.firstname + '</em>';
              $('input[name=firstname]').closest('.section').append(notif);
            }
            if (data.monolog.message.lastname) {
              var notif = '<em for="sell_lastname" class="state-error" style="color: red;">' + data.monolog.message.lastname + '</em>';
              $('input[name=lastname]').closest('.section').append(notif);
            }
            if (data.monolog.message.email) {
              var notif = '<em for="sell_email" class="state-error" style="color: red;">' + data.monolog.message.email + '</em>';
              $('input[name=email]').closest('.section').append(notif);
            }
            if (data.monolog.message['g-recaptcha-response']) {
                if(data.monolog.message['g-recaptcha-response'].length > 1){
                    var notif = '<em for="sell_recaptcha" class="state-error" style="color: red;">' + data.monolog.message['g-recaptcha-response'][1] + '</em>';
                }
                else{
                    var notif = '<em for="sell_recaptcha" class="state-error" style="color: red;">' + data.monolog.message['g-recaptcha-response'][0] + '</em>';
                }
                
                $('.g-recaptcha').closest('.section').append(notif);
            }

          }

          $('button[type=submit]').html(word);

        });

      });


	</script>
@endsection
