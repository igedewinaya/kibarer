@extends('index')

@section('meta_description', '')
@section('meta_keyword', '')

@section('content')
    <div class="bc-bg">
        <ul class="breadcrumb container">
            <li><a href="{{ route('home') }}">{{ trans('url.home') }}</a></li>
            <li class="active">{{ trans('url.estimate') }}</li>
        </ul>
    </div>
    <div class="line-top">
        <h3>
            <small>{{ trans('url.estimate') }}</small>
        </h3>
    </div>
    <div class="container">
        <br>
        <div class="col-md-6">

            <div class="alert alert-danger">
                <ul id="ulError">

                </ul>
            </div>


            <form role="form" id="estimateForm">

                {{csrf_field()}}

                <div class="form-group">
                    <label for="cities_id">Select Cities:</label>
                    <select name="cities_id" class="form-control">
                        @foreach ($cities as $key => $val)
                            <option value="{{$key}}">{{$val}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="land_size">Land Size:</label>
                    <input type="land" class="form-control" name="land">
                </div>
                <div class="form-group">
                    <label for="building_size">Building Size:</label>
                    <input type="building" class="form-control" name="building">
                </div>
                <div class="form-group">
                    <label for="bedroom">Bedroom:</label>
                    <input type="bedroom" class="form-control" name="bedroom">
                </div>
                <div class="form-group">
                    <label for="bathroom">Bathroom:</label>
                    <input type="bathroom" class="form-control" name="bathroom">
                </div>

                <button type="submit" class="btn btn-default">Estimate Price</button>
            </form>
        </div>
        <div class="col-md-6">
            <div id="estimated"></div>
        </div>
    </div>




@stop

@section('scripts')

    <script>
        $('.alert-danger').hide();
        $('#estimateForm').on('submit', function (e) {
            $('.alert-danger').hide();
            e.preventDefault();
            $.ajax({
                url: '{!! route('estimate') !!}',
                method: 'POST',
                data: $(this).serialize(),
                dataType: 'json',
                success: function (data) {
                    $('#estimated').html('<h1>'+data.result+'</h1>');
                },
                statusCode: {
                    400: function(data){
                        errorJson = jQuery.parseJSON(data.responseText);
                        errList = [];
                        $.each(errorJson.errors,function(key,val){
                            errList += '<li><i class="fa fa-close"></i> '+val+'</li>';
                        })
                        $('.alert-danger').toggle('slow');
                        $('#ulError').html(errList);
                    }
                }

            });

        })

    </script>
@stop