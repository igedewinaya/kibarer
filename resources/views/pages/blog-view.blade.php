@extends('index')

@section('content')

<div id="content">
    
    <div class="blog">
        <div class="blog-content">
            
            <article class="blog-list">
                <!-- <div class="thumbnail"><img src="assets/img/blog-thumbnail.jpg"></div> -->
                <div class="title">{{ $post->lang()->title }}</div>
                <div class="sub">Category : Adventure | Posted on {{ $post->created_at->format('M d, Y') }}</div>
                <div class="desc">
                    <p>{!! $post->lang()->content !!}</p>
                </div>
            </article>

        </div>
    
        @include('fragments.blog-sidebar')
    </div><!-- END BLOG --> 
</div><!-- END MAIN -->
@endsection

@section('scripts')

@stop
