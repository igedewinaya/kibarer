@extends('index')
@section('title', '404 | Page Not Found')
@section('pageId', '404')
@section('content')

<!-- Main -->
<div id="content">
  <div class="notification">
        <div class="error-404">
      <div class="icon">
      </div>
      <h1 class="title">404</h1>
            <p>Page Not Found</p>
      <div class="desc">
                The page you were looking for does not exist. You might've been following an old link or misspelled something in the URL.
      </div>
      <a href="" class="login-link">Back to Home</a>
    </div>
    
    
  </div><!-- END ACCOUNT -->
</div><!-- END MAIN -->
@stop
