@if(empty($search_page))
    <footer id="kibarer-footer">
        <div class="row flexbox flexbox-wrap justify-between">
            <div class="footer-box">
                <div class="title-link">Links</div>
                <ul>
                    <li><a href="{{ route('resource', trans('url.resource')) }}">Resource</a></li>
                    <li><a href="{{ route('sitemap', trans('url.sitemap')) }}">Site Map</a></li>
                    <li><a href="{{ route('sold_villa', trans('url.sold_villa')) }}">Sold Villas</a></li>
                </ul>
            </div>
            <div class="footer-box">
                <div class="title-link">Accounts</div>
                <ul>
                    <li><a href="{{ route('account', trans('url.account')) }}">My Account</a></li>
                    <li><a href="{{ route('account.wishlist', ['account' => trans('url.account'), 'wishlist' => trans('url.wishlist')]) }}">Favorites</a></li>
                    <li><a href="{{ route('account.wishlist', ['account' => trans('url.account'), 'wishlist' => trans('url.wishlist')]) }}">Wishlist</a></li>
                </ul>
            </div>
            <div class="footer-box">
                <div class="title-link">Partners</div>
                <ul>
                    <li><a href="https://en.balijetaime.com/">Bali Je t’aime</a></li>
                    <li><a href="http://www.atlantis-bali-diving.com/">Atlantis International</a></li>
                </ul>
            </div>
            <div class="footer-social">
                <div class="flexbox flexbox-wrap justify-between">

                    <?php $socials = json_decode(\File::get(storage_path('json/social_accounts.json'))); ?>
                    <a href="{{ $socials->facebook }}">
                        <span class="fa fa-facebook"></span>
                    </a>
                    <a href="{{ $socials->google }}">
                        <span class="fa fa-google-plus"></span>
                    </a>
                    <a href="{{ $socials->twitter }}">
                        <span class="fa fa-twitter"></span>
                    </a>
                    <a href="{{ $socials->linkedin }}">
                        <span class="fa fa-linkedin"></span>
                    </a>
                    <a href="{{ $socials->youtube }}">
                        <span class="fa fa-youtube"></span>
                    </a>
                    <a href="{{ $socials->skype }}">
                        <span class="fa fa-skype"></span>
                    </a>
                </div>
                <div class="newsletter">

                    {!! Form::open(['url' => route('api.newsletter.store'), 'id' => 'form-newsletter']) !!}
                    <input name="newsletter_email" type="email" placeholder="Subscribe to our newsletter">
                    <div class="fab-button fab-button-small fab-button-transparent">
                        <a id="newsletter_submit" href="#">
                            <i class="material-icons">done</i>
                        </a>
                    </div>
                    {!! Form::close() !!}
                </div>
                <div class="copyright">&copy; Copyright 2016 - Kibarer Development</div>
            </div>
        </div>
    </footer>

@endif