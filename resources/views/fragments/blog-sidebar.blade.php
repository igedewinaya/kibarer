<aside class="blog-sidebar">
    <div class="box">
        <h6 class="title">Categories</h6>
        <span class="title-border-bottom"></span>
        <ul class="list-link">

            <?php $blog_categories = \App\Category::where('type', 'post')->orderBy('name', 'asc')->get() ?>
            @foreach($blog_categories as $category)
            <li><a href="{{ route('blog', ['blog' => trans('url.blog'), 'term' => null, 'category' => $category->slug]) }}">{{ $category->name }}</a></li>
            @endforeach
        </ul>
    </div>
    <div class="box popular=post">
        <h6 class="title">Popular Post</h6>
        <span class="title-border-bottom"></span>
        <ul class="list-link">
            <?php $blog_populars = \App\PostLocale::where('locale', \Lang::getLocale())->orderBy('view', 'desc')->take(20)->get() ?>
            @foreach($blog_populars as $popular)
            <li><a href="{{ route('blog', ['blog' => trans('url.blog'), 'term' => $popular->slug]) }}" class="lh-20">{{ $popular->title }}</a></li>
            @endforeach
        </ul>
    </div>
</aside>
