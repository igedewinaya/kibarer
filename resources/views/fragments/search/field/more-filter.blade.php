
<div class="more-filter-holder">
    <div class="more-filter">
        @if ($category != 'villas-for-rent')
        <a onclick="more_filter()" class="link"><i class="fa fa-filter"></i><span>More filter</span></a>
        @endif
        <a onclick="changesort()" class="link"><i class="fa fa-sort-amount-desc ajaxSearch"></i> <span>Price</span></a>
        <script>
            function changesort(){
                $('#sortby').val('pricedesc');
            }
        </script>
        <input type="hidden" id="sortby" value="default">
        
            <div class="select-search-holder"> 
            <span onclick="" class=""> Show
                <select id="showTotal" class="selectAjax select-search">
                    <option value="10">10</option>
                    <option value="25">25</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
            </div>


        </span>
        <a onclick="" class="villa-found">
            <strong>{{$properties->total()}}</strong> {{FirstReplace($category)}} found</a>
    </div>

    <div class="search-pagination-holder">
        <div id="pagination">
            @include('fragments.search-pagination', ['paginator' => $properties])
        </div>
    </div>
</div>