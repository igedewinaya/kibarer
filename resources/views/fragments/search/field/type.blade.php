<div class="fieldset">
    <div class="label-custom lh-34">
        Type
    </div>
    <div class="field-custom">
        <div class="option-group field" style="width: 100%;">

            <div class="display-flex">
                <?php $allChecked = ''; ?>
                @if ($type == '')
                    <?php $allChecked = 'checked="checked"'; ?>
                @endif
                <div style="width:50%;margin-bottom: 10px;">
                    <label class="option" style="cursor:pointer">
                        <input type="checkbox" name="type[]" class="ajaxSearch type"
                               value="less-than-500k" {{$allChecked}} {{checkCheckBox($type,'less-than-500k')}}>
                        <span class="checkbox"></span> <
                        $500,000
                    </label>
                </div>
                <div style="width:50%;margin-bottom: 10px;">
                    <label class="option" style="cursor:pointer">
                        <input type="checkbox" name="type[]" class="ajaxSearch type"
                               value="more-than-500k" {{$allChecked}} {{checkCheckBox($type,'more-than-500k')}}>
                        <span class="checkbox"></span> >
                        $500,000
                    </label>
                </div>


                @if ($category != 'land')
                    <div style="width:50%;margin-bottom: 10px;">
                        <label class="option" style="cursor:pointer">
                            <input type="checkbox" name="type[]" class="ajaxSearch type"
                                   value="home_and_retirement" {{$allChecked}} {{checkCheckBox($type,'home-and-retirement')}}>
                            <span class="checkbox"></span> Home & Retirement
                        </label>
                    </div>
                @endif

                <div style="width:50%;margin-bottom: 10px;">
                    <label class="option" style="cursor:pointer">
                        <input type="checkbox" name="type[]" class="ajaxSearch type"
                               value="beachfront_property" {{$allChecked}} {{checkCheckBox($type,'beachfront')}}>
                        <span class="checkbox"></span> Beachfront
                    </label>
                </div>
            </div>
        </div>
    </div>
</div><!-- END FIELDSET -->