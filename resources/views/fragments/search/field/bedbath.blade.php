<div class="fieldset">
    <div class="label-custom lh-40">
        Room
    </div>
    <div class="field-custom">
        <div class="field-bedroom">
            <label class="field select">
                <select id="bedroom" name="bedroom" class="selectAjax">
                    <option value="0">-- Bedroom --</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                </select>
                <i class="arrow"></i>
            </label>
        </div>
        <div class="field-bathroom">
            <label class="field select" >
                <select id="bathroom" name="bathroom" class="selectAjax">
                    <option value="0">-- Bathroom --</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                </select>
                <i class="arrow"></i>
            </label>
        </div>
    </div>
</div>