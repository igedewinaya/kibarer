<div class="fieldset">
    <div class="label-price-range lh-36">
        <!--
        <div class="spacer-b15">
            <label for="amounts">Price range:</label>
        </div>
        -->
        Land Size
    </div><!-- END LABEL -->
    <div class="field-price-range">
        <input type="text" id="land-size" name="land_size" class="slider-input"
               style="padding-bottom: 15px;width: 100%;" readonly="readonly">
        <div class="slider-wrapper">
            <div id="slider-land"></div>
        </div>
    </div><!-- END FIELD -->
</div><!-- END FIELDSET -->
