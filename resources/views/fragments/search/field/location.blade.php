<div class="fieldset">
    <div class="label-custom lh-40">
        Location
    </div><!-- END LABEL -->
    <div class="field-price-range">
        <label for="" class="field prepend-icon">
            <input type="text" name="" id="location" class="gui-input pl-35 suggest-location-show"
                   placeholder="Location" onclick="suggestLocation()">
            <label for="firstname" class="field-icon"><i
                        class="fa fa-map-marker"></i></label>
        </label>
    </div><!-- END FIELD -->

    <div class="suggest-location hide">
    <div class="triangle-up"></div>
        <ul>
            <li> <img src="assets/img/ubud.jpg"> <a href=""> <p> Amed <br> Lets go somewhere </p> </a> </li>
            <li> <img src="assets/img/ubud.jpg"> <a href=""> <p> Amed <br> Lets go somewhere </p> </a> </li>
            <li> <img src="assets/img/ubud.jpg"> <a href=""> <p> Amed <br> Lets go somewhere </p> </a> </li>
            <li> <img src="assets/img/ubud.jpg"> <a href=""> <p> Amed <br> Lets go somewhere </p> </a> </li>
            <li> <img src="assets/img/ubud.jpg"> <a href=""> <p> Amed <br> Lets go somewhere </p> </a> </li>
            <li> <img src="assets/img/ubud.jpg"> <a href=""> <p> Amed <br> Lets go somewhere </p> </a> </li>
            <li> <img src="assets/img/ubud.jpg"> <a href=""> <p> Amed <br> Lets go somewhere </p> </a> </li>
            <li> <img src="assets/img/ubud.jpg"> <a href=""> <p> Amed <br> Lets go somewhere </p> </a> </li>
            <li> <img src="assets/img/ubud.jpg"> <a href=""> <p> Amed <br> Lets go somewhere </p> </a> </li>
            <li> <img src="assets/img/ubud.jpg"> <a href=""> <p> Amed <br> Lets go somewhere </p> </a> </li>
            <li> <img src="assets/img/ubud.jpg"> <a href=""> <p> Amed <br> Lets go somewhere </p> </a> </li>
            <li> <img src="assets/img/ubud.jpg"> <a href=""> <p> Amed <br> Lets go somewhere </p> </a> </li>
            <li> <img src="assets/img/ubud.jpg"> <a href=""> <p> Amed <br> Lets go somewhere </p> </a> </li>
            <li> <img src="assets/img/ubud.jpg"> <a href=""> <p> Amed <br> Lets go somewhere </p> </a> </li>
            <li> <img src="assets/img/ubud.jpg"> <a href=""> <p> Amed <br> Lets go somewhere </p> </a> </li>
            <li> <img src="assets/img/ubud.jpg"> <a href=""> <p> Amed <br> Lets go somewhere </p> </a> </li>
            <li> <img src="assets/img/ubud.jpg"> <a href=""> <p> Amed <br> Lets go somewhere </p> </a> </li>
            <li> <img src="assets/img/ubud.jpg"> <a href=""> <p> Amed <br> Lets go somewhere </p> </a> </li>
    </div>
</div><!-- END FIELDSET -->

<script type="text/javascript">
    function suggestLocation(){
        // $('.suggest-location-show').fadeIn('slow');

        $(document).on('click', '.suggest-location-shown', function(i){
        i.preventDefault();

        if($(this).hasClass('suggest-location-show')){
            $(this).fadeOut('slow');
                
                
            } else {
               $(this).fadeIn('slow'); 
            }
        }); 

    }
</script>