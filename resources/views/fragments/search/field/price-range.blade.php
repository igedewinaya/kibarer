<div class="fieldset">
    <div class="label-price-range lh-36">
        <!--
        <div class="spacer-b15">
            <label for="amounts">Price range:</label>
        </div>
        -->
        Price Range
    </div><!-- END LABEL -->
    <div class="field-price-range">
        <input type="text" id="amounts" name="amounts" class="slider-input"
               style="padding-bottom: 15px;width: 100%;" readonly="readonly">
        <div class="slider-wrapper">
            <div id="slider-range"></div>
        </div>
    </div><!-- END FIELD -->
</div><!-- END FIELDSET -->