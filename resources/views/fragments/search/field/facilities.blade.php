<div class="fieldset" style="padding-bottom: 50px;">
    <div class="label-custom">
        Facilities
    </div>

    <div class="field-custom">
        @foreach(\Config::get('facility.sale') as $key => $val)
            <div class="field-amenities-item">
                <label class="option block" style="cursor: pointer;">
                    <input type="checkbox" name="facility[]" value="{{$val}}" class="ajaxSearch">
                    <span class="checkbox"></span> {{ucwords($key)}}
                </label>
            </div>
        @endforeach

    </div>
</div>