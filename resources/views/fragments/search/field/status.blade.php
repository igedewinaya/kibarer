<div class="fieldset">
    <div class="label-custom lh-34">
        Status
    </div>
    <div class="field-custom">
        <div class="option-group field" style="width: 100%;">
            <div class="display-flex">
                <div style="width:50%;margin-bottom: 10px;">
                    <label class="option" style="cursor:pointer">
                        <input type="checkbox" name="leasefree[]" id="leasefree" class="ajaxSearch"
                               value="lease hold"
                               checked="checked">
                        <span class="checkbox"></span> Lease Hold
                    </label>
                </div>
                <div style="width:50%;margin-bottom: 10px;">
                    <label class="option" style="cursor:pointer">
                        <input type="checkbox" name="leasefree[]" id="leasefree" class="ajaxSearch"
                               value="free hold"
                               checked="checked">
                        <span class="checkbox"></span> Free Hold
                    </label>
                </div>
            </div>
        </div>
    </div>
</div><!-- END FIELDSET -->