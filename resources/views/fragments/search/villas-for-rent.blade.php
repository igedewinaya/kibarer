<div class="fieldset">
    <div class="label-custom lh-34">
        Status
    </div>
    <div class="field-custom">
        <div class="option-group field" style="width: 100%;">
            <?php $allChecked = ''; ?>
            @if ($type == '')
                <?php $allChecked = 'checked="checked"'; ?>
            @endif
            <div class="display-flex">
                <div style="width:50%;margin-bottom: 10px;">
                    <label class="option" style="cursor:pointer">
                        <input type="checkbox" name="shortlong[]" class="ajaxSearch" {{$allChecked}} {{checkCheckBox($type,'short-term')}}
                               value="short-term">
                        <span class="checkbox"></span> Short Term
                    </label>
                </div>
                <div style="width:50%;margin-bottom: 10px;">
                    <label class="option" style="cursor:pointer">
                        <input type="checkbox" name="shortlong[]" class="ajaxSearch" {{$allChecked}} {{checkCheckBox($type,'long-term')}}
                               value="long-term">
                        <span class="checkbox"></span> Long Term
                    </label>
                </div>
            </div>
        </div>
    </div>
</div>
@include('fragments.search.field.bedbath')
@include('fragments.search.field.facilities')
@include('fragments.search.field.price-range')
@include('fragments.search.field.location')
@include('fragments.search.field.more-filter')