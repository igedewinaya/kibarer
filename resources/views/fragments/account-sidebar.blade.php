
    
        <section class="w33">
            <div class="account-menu-left flexbox flexbox-wrap justify-between">
                <aside class="account-sidebar">
                    <div class="box">
                        <div class="icon">
                            <div class="avatar">

                                @if(!$customer->image_profile)
                                <img src="assets/img/customer.jpg" alt="" class="avatar-icon">
                                @else
                                <img src="{{ $customer->image_profile }}" alt="" class="avatar-icon">
                                @endif

                            </div>    
                            <div class="account-desc">
                                <h3 class="name">
                                        {{ $customer->firstname .' '. $customer->lastname }}
                                </h3>
                                <p class="join">
                                    Member Since {{ $customer->created_at->format('d M Y') }}
                                </p>
                            </div>
                            
                        </div>
                        <ul class="list-link">
                            <li><a href="{{ route('account', trans('url.account')) }}"><i class="fa fa-user"></i>{{ trans('word.overview') }}</a></li>
                            <li><a href="{{ route('account.wishlist', ['account' => trans('url.account'), 'wishlist' => trans('url.wishlist')]) }}"><i class="fa fa-heart"></i>{{ trans('word.favorite') }}</a></li>
                            <li><a href="{{ route('account.setting', ['account' => trans('url.account'), 'setting' => trans('url.setting')]) }}"><i class="fa fa-gear"></i> {{ trans('word.setting') }}</a></li>
                            <li><a href="{{ route('logout', trans('url.logout')) }}"><i class="fa fa-power-off"></i> {{ trans('word.logout') }}</a></li>
                        </ul>
                    </div>
                </aside>
            </div>
        </section>