<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<!-- NAV & HEADER -->
<header>
    <div class="row flexbox justify-between">
        <p class="logo"><a href="{{ route('home') }}"><img src="assets/img/logo.png" alt="Kibarer Property"></a>Kibarer Property</p>
        <div class="fullscreen-nav flexbox flex-wrap justify-between">
            <section class="bottom-nav">
                <nav>
                    <ul>
                        <li class="current">
                            <a href="#" class="caret" >{{ trans('word.villa') }}</a>
                            <div class="sub-menu"> 
                                <ul class="nav-sub-menu">

                                    <li><a href="{{ route('search', ['search' => trans('url.search'), 'category' => 'villas-for-sale', 'type' => 'less-than-500k']) }}">{{ trans('word.less_than_500k') }}</a></li>
                                    <li><a href="{{ route('search', ['search' => trans('url.search'), 'category' => 'villas-for-sale', 'type' => 'more-than-500k']) }}">{{ trans('word.more_than_500k') }}</a></li>
                                    <li><a href="{{ route('search', ['search' => trans('url.search'), 'category' => 'villas-for-sale', 'type' => 'beachfront-property']) }}">{{ trans('word.beachfront_property') }}</a></li>
                                    <li><a href="{{ route('search', ['search' => trans('url.search'), 'category' => 'villas-for-sale', 'type' => 'home-retirement']) }}">{{ trans('word.home_and_retirement') }}</a></li>
                                    <li><a href="#" class="caret-down" >{{ trans('word.villa_rental') }}</a>
                                        <div class="sub-menu-sub">
                                            <ul class="nav-sub-menu-sub"> 
                                                <li><a href="{{ route('search', ['search' => trans('url.search'), 'category' => 'villas-for-rent', 'type' => 'short-term']) }}">{{ trans('word.short_term') }}</a></li>
                                                <li><a href="{{ route('search', ['search' => trans('url.search'), 'category' => 'villas-for-rent', 'type' => 'long-term']) }}">{{ trans('word.yearly_rental') }}</a></li>
                                            </ul>
                                        </div> 
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <a href="#" class="caret" >{{ trans('word.land') }}</a>
                            <div class="sub-menu"> 
                                <ul class="nav-sub-menu">

                                    <li><a href="{{ route('search', ['search' => trans('url.search'), 'category' => 'land', 'type' => 'less-than-500k']) }}">{{ trans('word.less_than_500k') }}</a></li>
                                    <li><a href="{{ route('search', ['search' => trans('url.search'), 'category' => 'land', 'type' => 'more-than-500k']) }}">{{ trans('word.more_than_500k') }}</a></li>
                                    <li><a href="{{ route('search', ['search' => trans('url.search'), 'category' => 'land', 'type' => 'beachfront-property']) }}">{{ trans('word.beachfront_property') }}</a></li>
                                </ul>
                            </div>
                        </li>
                        <li><a href="{{ route('lawyer_notary', trans('url.lawyer_notary')) }}">{{ trans('word.lawyer_notary') }}</a></li>
                        <li><a href="{{ route('testimonials', trans('url.testimonials')) }}">{{ trans('word.testimonial') }}</a></li>
                        <li><a href="{{ route('contact', trans('url.contact')) }}">{{ trans('word.contact') }}</a></li>
                        <li><a href="{{ route('search', ['search' => trans('url.search')]) }}"><i class="material-icons">{{ trans('word.search') }}</i></a></li>
                    </ul>
                </nav>
                <section class="top-nav">
                    <ul>
                        <li><a href="{{ route('sell_property', trans('url.sell_property')) }}" class="sell"> <i class="fa fa-shopping-cart"></i> {{ trans('word.sell_property') }}</a></li>
                        <li><a href="{{ route('blog', trans('url.blog')) }}"><i class="fa fa-rss"></i> {{ trans('word.blog') }}</a></li>
                        <li><a href="{{ route('account', trans('url.account')) }}"><i class="fa fa-user"></i> {{ trans('word.account') }}</a></li>
                    </ul>
                </section>
            </section>

            <section class="lang-cur">
                
                <div class="header-lang">
                    <button class="link">{{ trans('word.language') }}
                        <i class="material-icons" style="border: 1px solid #DDD;border-radius: 50%;padding: 5px 4px;font-size: 11px;top:0;right:0;position: absolute;">flag</i>
                        <i class="material-icons" style="top: 22px;right:2px;font-size:16px;position: absolute;">keyboard_arrow_down</i>
                        <i class="active-link">{{ Lang::getLocale() }}</i>
                    </button>
                    <div class="link-content">
                        <li><a href="{{ url('/') }}">English</a></li>
                        <li><a href="{{ url('fr') }}">French</a></li>
                        <li><a href="{{ url('id') }}">Indonesian</a></li>
                        <li><a href="{{ url('ru') }}">Russian</a></li>
                    </div>
                </div>
                
                <div class="header-cur">
                    <button class="link">{{ trans('word.currency') }}
                        <i class="material-icons" style="border: 1px solid #DDD;border-radius: 50%;padding: 5px 4px;font-size: 11px;top:0;right:0;position: absolute;">flag</i>
                        <i class="material-icons" style="top: 22px;right:2px;font-size:16px;position: absolute;">keyboard_arrow_down</i>
                        <i class="active-link">{{ Session::get('currency') }}</i>
                    </button>
                    <div class="link-content">
                        <li><a href="{{ route('set_currency', 'usd') }}">USD</a></li>
                        <?php $altCurrencies = Config::get('currencies.alt_currencies'); ?>
                        @foreach($altCurrencies as $curr)
                        <li><a href="{{ route('set_currency', $curr) }}" style="text-transform: uppercase">{{ $curr }}</a></li>
                        @endforeach

                    </div>
                </div>
                                
                <!--
                <div class="header-lang">
                    <div class="lang-link">
                        <a href="">Language
                            <i class="material-icons" style="border: 1px solid #DDD;border-radius: 50%;padding: 5px 4px;font-size: 11px;float:right">flag</i>
                            <i class="material-icons" style="top: 22px;right:2px;font-size:16px;position: absolute;">keyboard_arrow_down</i>
                        </a>
                        <ul>
                            <li><a href="">English</a></li>
                            <li><a href="">French</a></li>
                            <li><a href="">Indonesian</a></li>
                            <li><a href="">Russian</a></li>
                        </ul>
                    </div>
                </div>
    
                <div class="header-currency">
                    <div class="currency-link">
                        <a href="">Currency
                            <i class="material-icons" style="border: 1px solid #DDD;border-radius: 50%;padding: 5px 4px;font-size: 11px;float:right">flag</i>
                            <i class="material-icons" style="top: 22px;right:2px;font-size:16px;position: absolute;">keyboard_arrow_down</i>
                        </a>
                        <ul>
                            <li><a href="">English</a></li>
                            <li><a href="">French</a></li>
                            <li><a href="">Indonesian</a></li>
                            <li><a href="">Russian</a></li>
                        </ul>
                    </div>
                </div> 
                -->
                
                
                               
            </section>
        </div>
        
        
        <section id="burger-nav">
            <a id="burger" href>
                <span></span>
                <span></span>
            </a>
        </section>
                        
    </div>
</header>