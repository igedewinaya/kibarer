
<!-- JQUERY LIB -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

<!-- MAIN JS -->
<!-- <script src="http://maps.googleapis.com/maps/api/js?libraries=places"></script> -->
<script src="assets/js/form/jquery.validate.min.js"></script>
<script src="assets/js/form/additional-methods.min.js"></script>
<script src="assets/js/scripts.js"></script>
<script src="assets/js/form/select2.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() {
    Kibarer.init();
});

$(document).on('click', '#newsletter_submit', function(event) {
    event.preventDefault();

    $('form-newsletter').submit();
});
</script>

@yield('scripts')