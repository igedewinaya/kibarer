<head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title>@yield('title')</title>

    <base href="{{ url('/') }}/" target="_top">

    <meta name="description" content="@yield('description')">

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>

    <link rel="apple-touch-icon" href="apple-touch-icon.png">

    <link href='https://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

    <!-- <link rel="stylesheet" href="assets/css/form.css"> -->

    <link rel="stylesheet" href="assets/css/main.css">

    <link rel="stylesheet" href="assets/css/responsive.css">

    <link rel="stylesheet" href="assets/css/animate.css">


    @yield('styles')
        <!--
        <link rel="stylesheet" href="assets/css/responsive.css">
    -->

        <!--[if lte IE 8]>
        <link type="text/css" rel="stylesheet" href="assets/css/smart-forms-ie8.css">
        <![endif]-->
        

        <!--[if lt IE 7]>
            <style type="text/css">
                #wrapper { height:100%; }
            </style>
            <![endif]-->
            
            
</head>