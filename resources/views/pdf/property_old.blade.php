<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>

    <style>
    *{
        font-family: 'Helvetica', sans-serif;
    }

    table{
        width: 100%;
    }
    </style>
</head>
<body>
    <table>

        <!-- Header -->
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <img src="assets/img/logo.png" alt="Kibarer Property">
                        </td>

                        <td>
                            <h4>Contact:</h4>
                            <h3>Kibarer Property</h3>
                            <p>
                                Jalan Petitenget n.9 Seminyak,<br>
                                Badung, Bali - Indonesia<br>
                                +62 361 474 12 12 , +62 361 805 0000<br>
                                alban@kibarer.com
                            </p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <!-- Title -->
        <tr>
            <td>
                <h1>{{ $property->lang($request->locale)->title }}</h1>
            </td>
        </tr>

        <!-- Body -->
        @if($request->pictures)
        <tr>
            <td>
                <table>

                    <tr>
                        <td width="33.33%">
                            @if($property->thumb()->count() > 0)
                            <img src="uploads/images/property/{{ $property->thumb[0]->value }}" alt="">
                            @else
                            <img src="no-image.png" alt="">
                            @endif
                        </td>
                    </tr>

                </table>
            </td>
        </tr>
        @endif

        <tr>
            <td>
                <table>
                    <tr>
                        <td class="details">
                            <table>

                                <tr>
                                    <td>
                                        <h4>Details</h4>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table>

                                            <tr><td>Location</td><td>{{ $property->city }}</td></tr>
                                            <tr><td>Code</td><td>{{ $property->code }}</td></tr>
                                            <tr><td>Bedroom</td><td>{{ $property->bedroom }}</td></tr>
                                            <tr><td>Bathroom</td><td>{{ $property->bathroom }}</td></tr>
                                            <tr><td>Land Size</td><td>{{ $property->land_size }}</td></tr>
                                            <tr><td>Building Size</td><td>{{ $property->building_size }}</td></tr>
                                            <tr><td>Price</td><td>{{ $request->currency .' '. convertCurrency($property->price, 'IDR', $request->currency) }}</td></tr>

                                        </table>
                                    </td>
                                </tr>

                                @if($request->distances)
                                <tr>
                                    <td>
                                        <h4>Distances</h4>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table>
                                            @foreach($property->distances as $distance)
                                            <tr><td>{{ $distance->name }}</td><td>{{ $distance->value }}</td></tr>
                                            @endforeach
                                        </table>
                                    </td>
                                </tr>
                                @endif

                                @if($request->facilities)
                                <tr>
                                    <td>
                                        <h4>Facilites</h4>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table>
                                            @foreach($property->facilities as $facility)
                                            <tr><td>{{ $facility->name }}</td><td>{{ $facility->value }}</td></tr>
                                            @endforeach
                                        </table>
                                    </td>
                                </tr>
                                @endif

                                @if($request->documents)
                                <tr>
                                    <td>
                                        <h4>Documents</h4>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table>
                                            @foreach($property->documents as $document)
                                            <tr><td>{{ $document->name }}</td><td>{{ $document->value }}</td></tr>
                                            @endforeach
                                        </table>
                                    </td>
                                </tr>
                                @endif

                                @if($request->agent)
                                <tr>
                                    <td>
                                        <h4>Agent</h4>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table>

                                            <tr><td>name</td><td>{{ $property->user->firstname .' '. $property->user->lastname }}</td></tr>
                                            <tr><td>phone</td><td>{{ $property->user->phone }}</td></tr>
                                            <tr><td>email</td><td>{{ $property->user->email }}</td></tr>
                                            
                                        </table>
                                    </td>
                                </tr>
                                @endif

                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>
