<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<style>
	    html, body, table {
	    	margin: 0px;
	    	padding: 0px;
	    }
		h1 {
			font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
			font-size: 24px;
			font-style: normal;
			font-variant: normal;
			font-weight: 500;
			line-height: 26.4px;
		}
		h3 {
			font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
			font-size: 14px;
			font-style: normal;
			font-variant: normal;
			font-weight: 500;
			line-height: 15.4px;
		}
		p, table tr td {
			font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
			font-size: 14px;
			font-style: normal;
			font-variant: normal;
			font-weight: 400;
			line-height: 20px;
		}
		blockquote {
			font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
			font-size: 21px;
			font-style: normal;
			font-variant: normal;
			font-weight: 400;
			line-height: 30px;
		}
		pre {
			font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
			font-size: 13px;
			font-style: normal;
			font-variant: normal;
			font-weight: 400;
			line-height: 18.5714px;
		}
		#description tr td {
            font-size: 13px;
		}
		.content {
			 list-style: none outside none;
		}
		.content li:before {
			content: ":";
			margin: 0 10px;
		}
	</style>
</head>	

<body>
  <div class="wrapper" style="width:720px; margin: 0 auto;"> 
  	<table style="width:100%">
  		<tr>
  			<td style="width:50%">
  				<img src="assets/img/kibarer_pdf.png" alt="Pdf Logo">
  			</td>
  			<td style="width:50%; padding-left: 80px;">
  				<h3>Contact :</h3>
  				<h2>Kibarer Property</h2>
  				<p>Jalan Petitenget n.9 Seminyak, <br>
  					Badung, Bali - Indonesia <br>
  					+62 361 474 12 12 , +62 361 805 0000 <br>
  					alban@kibarer.com</p>
  				</td>
  			</tr>
  			<tr>
  				<td colspan="2" style="width:100%; text-align: center; padding: 0 70px;">
  					<h1 style="font-size: 25px; line-height: 30px; margin: 10px;">{{ $property->lang()->title }}</h1>
  				</td>
  			</tr>
  			<tr>
  				<td style="width:50%">
  					<div class="image-box" style="margin-right: 20px;"> 
  						<img src="assets/img/testimonial2.jpg" alt="Image Left" width="345px">  
  					</div>
  				</td>
  				<td style="width:50%">
  					<div class="image-box">
  						<img src="assets/img/testimonial2.jpg" alt="Image Left" width="345px"> 
  					</div>
  				</td>
  			</tr>
  		</table>
  		<table style="width:100%" >
  			<tr>
  				<td style="width:30%">
  					<div class="image-box" style="margin-top: 5px; margin-bottom: 10px;">
  						<img src="assets/img/testimonial2.jpg" alt="Image Left" width="250px">  
  					</div>
  					<div class="image-box" style="margin-bottom: 10px;">
  						<img src="assets/img/testimonial2.jpg" alt="Image Left" width="250px">  
  					</div>
  					<div class="image-box" style="margin-bottom: 10px;">
  						<img src="assets/img/testimonial2.jpg" alt="Image Left" width="250px">  
  					</div>
  				</td>
  				<td style="width:70%; padding-left: 30px;">
  					<table style="width:100%;" id="description">
  						<tr style="margin: 0px; padding: 0px;">
  							<td colspan="2" style="width: 100%; text-align: center;">
  							 <h2 style="margin: 10px 0 0 0; font-size: 23px;">DESCRIPTIONS:</h2>
  							</td>
  						</tr>
  						<tr style="margin: 0px; padding: 0px;">
  							<td style="width:60%">
  								<h4 style="padding-left: 25px; margin-top: 5px; margin-bottom: 5px; padding-top: 5px; padding-bottom: 5px;">Villa Details</h4>
  								<ul>
  									<li>Location</li>
  									<li>Villa Code</li>
  									<li>Bedroom</li>
  									<li>Bathroom</li>
  									<li>Land Size</li>
  									<li>Building Size</li>
  									<li>Sell in Furnish</li>
  								</ul>
  							</td>
  							<td style="width:40%">
  								<h4>&nbsp;</h4>
  								<ul class="content">
  									<li>Bukit</li>
  									<li>VL1035</li>
  									<li>2</li>
  									<li>2</li>
  									<li>3 are</li>
  									<li>75 sqm</li>
  									<li>Fully Furnished</li>
  								</ul>
  							</td>
  						</tr>
  						<tr style="margin: 0px; padding: 0px;">
  							<td style="width:60%">
  								<h4 style="padding-left: 25px; margin-top: 5px; margin-bottom: 5px; padding-top: 5px; padding-bottom: 5px;">Distances</h4>
  								<ul>
  									<li>Beach</li>
  									<li>Airport</li>
  									<li>Market</li>
  								</ul>
  							</td>
  							<td style="width:40%">
  								<h4>&nbsp;</h4>
  								<ul>
  									<li>8 Minutes</li>
  									<li>25 Minutes</li>
  									<li>5 minutes</li>
  								</ul>
  							</td>
  						</tr>
  						<tr style="margin: 0px; padding: 0px;">
  							<td style="width:60%">
  								<h4 style="padding-left: 25px; margin-top: 5px; margin-bottom: 5px; padding-top: 5px; padding-bottom: 5px;">Additional Facilities</h4>
  								<ul>
  									<li>Swimming Pool</li>
  									<li>Water Resource</li>
  									<li>Electricity</li>
  								</ul>
  							</td>
  							<td style="width:40%">
  								<h4>&nbsp;</h4>
  								<ul class="content">
  									<li>6 x 3 m</li>
  									<li>Well</li>
  									<li>5.500 W</li>
  								</ul>
  							</td>
  						</tr>
  					</table>
  				</td>
  			</tr>
  		</table>
  		<table style="width: 100%">  
  			<tr>
  				<td style="width: 100%" colspan="2">
  					<div class="blue-box" style="text-align:center; background: #f1f1f1">
  						<h1 style="color:#db6700;">USD 129,213.48 LeaseHold 21 years</h1>
  					</div>
  				</td>
  			</tr>
    </table>
  </div>

</body>

</html>	