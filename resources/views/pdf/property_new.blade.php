<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<style>
	  html, body, table {
	    	margin: 0px;
	    	padding: 0px;
	  }
    body {
      font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
    }
    /*
    @font-face {
      font-family: 'Oswald' ;
      src: url('assets/fonts/Oswald-Regular.ttf');
    } */
		h1 {
			font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
			font-size: 22px;
			font-style: normal;
			font-variant: normal;
			font-weight: 500;
			line-height: 26.4px;
		}
		h3 {
			font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
			font-size: 14px;
			font-style: normal;
			font-variant: normal;
			font-weight: 500;
			line-height: 15.4px;
		}
		p, table tr td {
			font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
			font-size: 13px;
			font-style: normal;
			font-variant: normal;
			font-weight: 400;
			line-height: 20px;
		}
		blockquote {
			font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
			font-size: 21px;
			font-style: normal;
			font-variant: normal;
			font-weight: 400;
			line-height: 30px;
		}
		pre {
			font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
			font-size: 13px;
			font-style: normal;
			font-variant: normal;
			font-weight: 400;
			line-height: 18.5714px;
		}
		#description tr td {
            font-size: 13px;
		}
		.content {
			 list-style: none outside none;
		}
		.content li:before {
			content: ":";
			margin: 0 10px;
		}
	</style>
</head>	

<body>
  <div class="wrapper" style="width:720px; margin: 10px auto 0 auto;"> 
  	<table style="width:100%" cellspacing="0" cellpadding="0" border="0">
  		<tr>
  			<td style="width:50%; border-bottom: 2px solid #999999; padding-bottom: 7px; padding-bottom: 7px;">
  				<img src="assets/img/logo.png" alt="Pdf Logo" width="250px">
  			</td>
  			<td style="width:50%; padding-left: 170px; border-bottom: 2px solid #999999;">
  				<h2 style="margin: 0px; padding: 0px; font-size: 17px;">Kibarer Property</h2>
  				<p style="margin: 0px; padding: 0px 0 20px 0; font-size: 10px; color: #777777; line-height: 15px">Jalan Petitenget No.9 Seminyak, <br>
  					Badung, Bali - Indonesia <br>
  					+62 361 474 12 12 , +62 361 805 0000 <br>
  					alban@kibarer.com</p>
  				</td>
  			</tr>
  			<tr>
          <td colspan="2" style="width:100%; text-align: left; padding: 0;">
  					<h1 style="/* font-size: 25px; line-height: 30px; margin: 10px; */ /*padding-top: 25px; */ margin: 5px 0 0 0; padding: 0px; font-size: 20px; text-transform: uppercase; /*padding-bottom: 10px; */ color: #ee5b2c; font-weight: normal;">{{ $property->lang()->title }}</h1>
            <p style="font-size: 12px; letter-spacing: 1px; margin: 0px 0 5px 0; padding: 0px; /*padding-bottom: 15px;*/">{{ $property->code }}</p>
          </td>
  			</tr>
  			<?php //dd($property); ?>
  			<tr>
  				<td style="width:60%">
  					<div class="image-box" style="margin-right: 20px;"> 
  						<img src="assets/img/testimonial2.jpg" alt="Image Left" width="385px">  
  					</div>
  				</td>
  				<td style="width:40%" style="vertical-align:top;">
                  <div class="div text-box" style="width: 100%;">
                    <table style="width:100%;">
                        <tr>
                            <td style="width:30%; background:#ee5b2c;">
                                <div class="price-logo-wrapper" style="width:100%;">
                                    <img src="assets/img/dollar.jpg" alt="" style="margin-left: 25px;">
                                </div>
                            </td>
                            <td style="width:70%; background: #f7f8f9;">
                                <div class="price-wrapper" style="text-align: center;">
                                    <h5>{{ $property->price }}</h2>
                                </div>
                            </td>
                        </tr>
                        <tr>
                        <td style="width:30%;">
                                <div class="price-logo-wrapper" style="width:100%;">
                                    <img src="assets/img/location.jpg" alt="" style="margin-left: 25px;">
                                </div>
                            </td>
                            <td style="width:70%;">
                                <div class="price-wrapper" style="text-align: center;">
                                    <h5>{{ $property->province }}</h5>
                                    <p>{{ $property->city }}</p>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:30%;">
                                <div class="price-logo-wrapper" style="width:100%;">
                                    <img src="assets/img/bedroom.jpg" alt="" style="margin-left: 25px;">
                                </div>
                            </td>
                            <td style="width:70%;">
                                <div class="price-wrapper" style="text-align: center;">
                                    <h5>{{ $property->bedroom }}</h5>
                                    <p>{{ $property->bathroom }}</p>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:30%;">
                                <div class="price-logo-wrapper" style="width:100%;">
                                    <img src="assets/img/square.jpg" alt="" style="margin-left: 25px;">
                                </div>
                            </td>
                            <td style="width:70%; ">
                                <div class="price-wrapper" style="text-align: center;">
                                    <h5>{{ $property->land_size }}</h5>
                                    <p>{{ $property->building_size }}</p>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:30%; ">
                                <div class="price-logo-wrapper" style="width:100%;">
                                    <img src="assets/img/lease.jpg" alt="" style="margin-left: 25px;">
                                </div>
                            </td>
                            <td style="width:70%;">
                                <div class="price-wrapper" style="text-align: center;">
                                    <h5>{{ $property->status }}</h5>
                                </div>
                            </td>
                        </tr>
                    </table>
                  </div>
  				</td>
  			</tr>
  		</table>
  		<table style="width:100%" >
  			<tr>
  				<td style="width:30%">
  					<div class="image-box" style="margin-top: 5px; margin-bottom: 10px;">
  						<img src="assets/img/testimonial2.jpg" alt="Image Left" width="250px">  
  					</div>
  					<div class="image-box" style="margin-bottom: 10px;">
  						<img src="assets/img/testimonial2.jpg" alt="Image Left" width="250px">  
  					</div>
  					<div class="image-box" style="margin-bottom: 10px;">
  						<img src="assets/img/testimonial2.jpg" alt="Image Left" width="250px">  
  					</div>
  				</td>
  				<td style="width:70%; padding-left: 30px;">
  					<table style="width:100%;" id="description">
  						<tr style="margin: 0px; padding: 0px;">
  							<td colspan="2" style="width: 100%; text-align: center;">
  							 <h2 style="margin: 10px 0 0 0; font-size: 18px;">DESCRIPTIONS:</h2>
  							</td>
  						</tr>
  						<tr style="margin: 0px; padding: 0px;">
  							<td style="width:60%">
  								<h4 style="padding-left: 25px; margin-top: 5px; margin-bottom: 5px; padding-top: 5px; padding-bottom: 5px;">Villa Details</h4>
  								<ul>
  									<li>Location</li>
  									<li>Villa Code</li>
  									<li>Bedroom</li>
  									<li>Bathroom</li>
  									<li>Land Size</li>
  									<li>Building Size</li>
  									<li>Sell in Furnish</li>
  								</ul>
  							</td>
  							<td style="width:40%">
  								<h4>&nbsp;</h4>
  								<ul class="content">
  									<li>{{ $property->city }}</li>
  									<li>{{ $property->code }}</li>
  									<li>{{ $property->bedroom }}</li>
  									<li>{{ $property->bathroom }}</li>
  									<li>{{ $property->land_size }}</li>
  									<li>{{ $property->building_size }}</li>
  									<li>{{ $property->sell_in_furnish }}</li>
  								</ul>
  							</td>
  						</tr>
  						<tr style="margin: 0px; padding: 0px;">
  							<td style="width:60%">
  								<h4 style="padding-left: 25px; margin-top: 5px; margin-bottom: 5px; padding-top: 5px; padding-bottom: 5px;">Distances</h4>
  								<ul>
				                    @foreach($property->distances as $distance)
				                    <li>{{ $distance->name }}</li>
				                    @endforeach
  								</ul>
  							</td>
  							<td style="width:40%">
  								<h4>&nbsp;</h4>
  								<ul class="content">
  									@foreach($property->distances as $distance)
				                    <li>{{ $distance->value }}</li>
				                    @endforeach
  								</ul>
  							</td>
  						</tr>
  						<tr style="margin: 0px; padding: 0px;">
  							<td style="width:60%">
  								<h4 style="padding-left: 25px; margin-top: 5px; margin-bottom: 5px; padding-top: 5px; padding-bottom: 5px;">Facilities</h4>
  								<ul>                                    
  									

  									
  								</ul>
  							</td>
  							<td style="width:40%">
  								<h4>&nbsp;</h4>
  								<ul class="content">
                                    &nbsp;
  								</ul>
  							</td>
  						</tr>
  					</table>
  				</td>
  			</tr>
  		</table>
  		<table style="width: 100%">  
  			<tr>
  				<td style="width: 100%" colspan="2">
  					<div class="blue-box" style="text-align:center; background: #f1f1f1">
  						<h1 style="color:#db6700;">USD 129,213.48 ({{ $property->type }})</h1>
  					</div>
  				</td>
  			</tr>
    </table>
  </div>

</body>

</html>	