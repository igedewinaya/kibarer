<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700,600' rel='stylesheet' type='text/css'> -->
	<title>Kibarer Property</title>
	<style type="text/css">
		@font-face {
			font-family: helvetica ;
			src: url('{{ DOMPDF_FONT_DIR }}HelveticaNeue.ttf');
		}

		@font-face {
			font-family: oswald ;
			src: url('{{ DOMPDF_FONT_DIR }}Oswald-Regular.ttf');
		}
		html { margin: 30px}
		body {
			font-family:helvetica, arial;
			font-size:14px;
		}
		p {
			line-height:20px;
			font-family:helvetica, arial;
			font-size:14px;
		}
		h1{
			font-family:oswald;
			font-size:25px;
			color:#333;
			text-transform:uppercase;
			margin-bottom:0px;
		}
		strong {
			font-weight:bold;
		}
		h3 {
			font-family:18px;
			font-weight:bold;
		}
		.wrapper {
			width:100%;
			height:auto;
			margin:0 auto;
			/*padding:50px;*/
		}
		.clear {
			clear:both;
		}

		.logo {
			width:346px;
			height:99px;
			float:left;
		}

		.contact {
			width:320px;
			height:auto;
			float:right;
		}

		.main-pic {
			width:100%;
			/*height:380px;*/
			margin:20px 0;
			background:#333;
			position:relative;
			overflow:hidden;
		}
		.price {
			width:204px;
			/*height:107px;*/
			background:url('{{  public_path() }}/assets/images/price.png') no-repeat;
			bottom:0;
			right:0;
			position:absolute;
			padding:80px 40px 20px 20px;

		}

		.price h3 {
			font-family:oswald,arial;
			float:right;
			color:#fff;
			font-size:25px;
			margin:0;
		}

		.price h2 {
			font-family:oswald,arial;
			float:right;
			color:#fff;
			font-size:50px;
			margin:0;
		}

		.thumb {
			float:left;
			/*margin-right:20px;*/
			width:33%;
			height: 180px;
			/*height:180px;*/
			/*overflow:hidden;*/
			background:#333;
		}

		.thumb img {
			width: 100%;
		}

/*		.thumb:last-child {
			margin-right:0;
		}*/

		.content {
			margin-top:50px;
		}

		.detail {
			margin-top:50px;
			padding-top:30px;
			border-top:1px solid #ccc;
			width:100%;
		}

		table {
			width: 100%;
			text-align: left;
		}

		table.details tr td {
			padding:10px 20px 10px 0;
			font-size:14px;
			border-bottom:1px solid #ccc;
		}

		table.facilities tr td {
			padding:10px 20px 10px 0;
			font-size:14px;
		}

		table.details tr td:first-child {
			width:25%;
		}

		table.details tr td:last-child {
			width:550px;			
			text-align: left;
		}

		.tit-details {
			width:100%;
			padding:10px;
			color:#fff;
			background:#ee5b2c;
			margin:50px 0;
			text-transform:uppercase;
		}

		.blok1 {
			float:left;
			width:300px;
		}

		.blok2 {
			float:right;
			width:300px;
		}

	</style>
</head>

<body>
	<div class="wrapper">
		<div class="logo">
			<img src="{{  public_path() }}/assets/images/logo.jpg">
		</div>
		<div class="contact">
			
			<h3>KIBARER PROPERTY</h3>
			<p>Jalan Petitenget n.9 Seminyak, Badung, 
				Bali - Indonesia
				+62 361 474 12 12 , +62 361 805 0000
				<a href="#">alban@kibarer.com</a></p>
			</div>
			<div class="clear"></div>
			
			@if($request->pictures)
			<div class="main-pic">

                @if($property->thumb()->count() > 0)
                <img src="uploads/images/property/{{ $property->thumb[0]->value }}" alt="">
                @else
                <img src="assets/img/no-image.png" alt="">
                @endif

				<div class="price">
					<h3>PRICE</h3>
					<h2>{{ $request->currency .' '. convertCurrency($property->price, $property->currency, $request->currency) }}</h2>
				</div>
			</div><!--main-pic-->

			@endif
<!-- 
			<div class="wrapper-thumb">
				<div class="thumb">
					<img src="{{  public_path() }}/assets/images/villa1.jpg">
				</div>
				<div class="thumb">
					<img src="{{  public_path() }}/assets/images/villa1.jpg">
				</div>
				<div class="thumb">
					<img src="{{  public_path() }}/assets/images/villa1.jpg">
				</div>
			</div>
 -->
			<div class="clear"></div>


			<div class="detail">
				<h1>{{ $property->lang($request->locale)->title }}</h1>

				<p>
				{!! $property->lang($request->locale)->content !!}
				</p>


				<div class="tit-details">Details</div>
				<table class="details">
					<tr>
						<td>Location</td>
						<td>:</td>
						<td>{{ $property->city }}</td>
					</tr>
					<tr>
						<td>Code</td>
						<td>:</td>
						<td>{{ $property->code }}</td>
					</tr>
					<tr>
						<td>Bedroom</td>
						<td>:</td>
						<td>{{ $property->bedroom }}</td>
					</tr>
					<tr>
						<td>Bathroom</td>
						<td>:</td>
						<td>{{ $property->bathroom }}</td>
					</tr>
					<tr>
						<td>Land Size</td>
						<td>:</td>
						<td>{{ $property->land_size }}</td>
					</tr>
					<tr>
						<td>Building Size</td>
						<td>:</td>
						<td>{{ $property->building_size }}</td>
					</tr>

					@if($request->price)
					<tr>
						<td>Price</td>
						<td>:</td>
						<td>{{ $request->currency .' '. convertCurrency($property->price, $property->currency, $request->currency) }}</td>
					</tr>
					@endif

				</table>
				
				@if($request->distances)
				<div class="tit-details">Distances</div>

				<table class="details">

                    @foreach($property->distances as $distance)
                    <tr><td>{{ $distance->name }}</td><td>: {{ $distance->value }}</td></tr>
                    @endforeach

				</table>
				@endif

				@if($request->facilities)
				<div class="tit-details">Facilites</div>
				<div class="blok1">
					<table class="facilities">

					<?php $arr_facilities = Config::get('facility.sale') ?>

					@foreach($arr_facilities as $key => $facility)

                    <tr>
						<td>
						@if(in_array($key, $exist_facility))
							<img src="{{  public_path() }}/assets/images/check.png" width="20px" height="20px">
						@else
							<img src="{{  public_path() }}/assets/images/none.png" width="20px" height="20px">
						@endif
						</td>

						<td>{{ $key }}</td>
					</tr>

					@endforeach

					</table>
				</div>
				<div class="clear"></div>
				@endif
				
				@if($request->documents)
				<div class="tit-details">Documents</div>
				<table class="details">
                    @foreach($property->documents as $document)
                    <tr><td>{{ $document->name }}</td><td>{{ $document->value }}</td></tr>
                    @endforeach
				</table>
				@endif

				@if($request->agent)
				<div class="tit-details">Agent</div>
				<table class="details">

                    <tr><td>Name</td><td>: {{ $property->user->firstname .' '. $property->user->lastname }}</td></tr>
                    <tr><td>Phone</td><td>: {{ $property->user->phone }}</td></tr>
                    <tr><td>Email</td><td>: {{ $property->user->email }}</td></tr>

				</table>
				@endif

			</div>

		</div>


	</body>


	</html>