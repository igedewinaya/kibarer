@extends('admin.master')
@section('page', 'properties')

@section('fab')

<!-- <m-fab salmon class="modal-open" data-target="#property-add"><i class="material-icons">add</i></m-fab> -->
<a href="{{ route('admin.properties', ['term' => null, 'action' => 'create', 'category' => $request['category']]) }}"><m-fab salmon><i class="material-icons">add</i></m-fab></a>

@stop

@section('content')

<?php $user_role = Auth::user()->get()->role_id ?>

<!-- <a class="export-csv"><i class="material-icons">open_in_new</i> <label style="position: relative; top: -5px">Export CSV</label></a> -->

<m-card class="filter-toolbar">
    <div class="header">
        <div class="filter-box m-input-group fwidth flexbox flexbox-wrap justify-between">
            <m-input select class="m-input-wrapper filter-item w20-8">
                <select class="select-control" name="branch" id="">
                    <option value="">all</option>

                    <?php $branches = \App\Branch::orderBy('name', 'asc')->get() ?>
                    @foreach($branches as $branch)
                    <option value="{{ $branch->id }}">{{ $branch->name }}</option>
                    @endforeach

                </select>
                <label for="">branch</label>
            </m-input>
            <m-input select class="m-input-wrapper filter-item w20-8">
                <select class="select-control" name="type" id="">
                    <option value="">all</option>
                    <option value="free hold">free hold</option>
                    <option value="lease hold">lease hold</option>
                </select>
                <label for="">type</label>
            </m-input>
            <m-input class="filter-item w20-8">
                <input class="input-control" type="text" name="location" placeholder="Search..." style="border-bottom: 1px solid #000">
                <label for="">location</label>
            </m-input>  
            <m-input class="filter-item w20-8">
                <input class="input-control" type="text" name="title" placeholder="Search..." style="border-bottom: 1px solid #000">
                <label for="">title</label>
            </m-input>               
            <m-input class="filter-item w20-8">
                <input class="input-control" type="text" name="code" placeholder="Search..." style="border-bottom: 1px solid #000">
                <label for="">code</label>
            </m-input>

        </div>
    </div>
</m-card>

<m-template list class="property-wrapper">

    <table data-selected="" data-token="{{ csrf_token() }}" id="property-table">
        <thead>
            <tr>
                <th width="15px"><input type="checkbox" class="check-all visible"></th>
                <th>Image</th>
                <th>Title</th>
                <th>Code</th>
                <th>Type</th>
                <th>Status</th>
                @if($user_role == 1)
                <th>Branch</th>
                @elseif($user_role == 2)
                <th>Agent</th>
                @else
                <th>Empty</th>
                @endif
                <th>Price</th>
                <th>View</th>
                <th>Created</th>
                <th>Action</th>
            </tr>
        </thead>
    </table>

</m-template>

@endsection

@section('modal')

<m-modal-wrapper id="csv-export-modal">

    <div class="modal-window" style="max-width: 640px" data-id="">
        <h3>CSV Export Configuration</h3>

        {!! Form::open(['url' => route('export.property.csv', ['type' => Input::get('category'), 'status' => Input::get('status')]), 'id' => 'export-csv']) !!}

        <input id="form-csv-id" type="hidden" name="id" value="">

        <m-input-group fwidth flexwrap justify-between>
            <m-input select w50-6>
                <select name="locale" id="input_locale">
                    <option value="en" selected>english</option>
                    <option value="fr">french</option>
                    <option value="id">indonesian</option>
                    <option value="ru">russian</option>
                </select>
                <label for="title">language</label>
            </m-input>
            <m-input select w50-6>
                <select name="take" id="take">
                    <option value="selected" selected>selected</option>
                    <option value="all">all</option>
                </select>
                <label for="title">Rows</label>
            </m-input>
        </m-input-group>

        <m-input-group fwidth flexwrap justify-between>
            <m-checkbox data-label="user" w50-6 style="margin-bottom: 12px;">
                <input type="checkbox" id="user" name="user">
                <lever></lever>
            </m-checkbox>
            <m-checkbox data-label="category" w50-6 style="margin-bottom: 12px;">
                <input type="checkbox" id="category" name="category">
                <lever></lever>
            </m-checkbox>
            <m-checkbox data-label="facilities" w50-6 style="margin-bottom: 12px;">
                <input type="checkbox" id="facilities" name="facilities">
                <lever></lever>
            </m-checkbox>
            <m-checkbox data-label="documents" w50-6 style="margin-bottom: 12px;">
                <input type="checkbox" id="documents" name="documents">
                <lever></lever>
            </m-checkbox>
            <m-checkbox data-label="distances" w50-6 style="margin-bottom: 12px;">
                <input type="checkbox" id="distances" name="distances">
                <lever></lever>
            </m-checkbox>
        </m-input-group>

        <m-buttons flexbox justify-end style="margin-top: 16px;">
            <m-button plain modal-close>cancel</m-button>
            <m-button export-csv plain>export</m-button>
        </m-buttons>

        {!! Form::close() !!}

    </div>

</m-modal-wrapper>

<m-modal-wrapper id="pdf-export-modal">

    <div class="modal-window" style="max-width: 640px" data-id="">
        <h3>PDF Export Configuration</h3>

        {!! Form::open(['url' => route('export.property.pdf', ['type' => Input::get('category'), 'status' => Input::get('status')]), 'id' => 'export-pdf']) !!}

        <input id="form-pdf-id" type="hidden" name="id" value="">
        <m-input-group fwidth flexwrap justify-between>
            <m-input select w50-6>
                <select name="locale" id="input_locale">
                    <option value="en" selected>english</option>
                    <option value="fr">french</option>
                    <option value="id">indonesian</option>
                    <option value="ru">russian</option>
                </select>
                <label for="title">language</label>
            </m-input>

            <m-input select w50-6>
                <select name="currency" id="input_currency">
                    <option value="usd" selected>usd</option>
                    <option value="idr">idr</option>
                    <option value="eur">eur</option>
                    <option value="aud">aud</option>
                </select>
                <label for="title">currency</label>
            </m-input>
        </m-input-group>

        <m-input-group fwidth flexwrap justify-between>
            <m-checkbox data-label="description" w50-6 style="margin-bottom: 12px;">
                <input type="checkbox" id="with_description" name="description" checked>
                <lever></lever>
            </m-checkbox>
            <m-checkbox data-label="price" w50-6 style="margin-bottom: 12px;">
                <input type="checkbox" id="with_price" name="price" checked>
                <lever></lever>
            </m-checkbox>
            <m-checkbox data-label="pictures" w50-6 style="margin-bottom: 12px;">
                <input type="checkbox" id="with_pictures" name="pictures" checked>
                <lever></lever>
            </m-checkbox>
            <m-checkbox data-label="facilities" w50-6 style="margin-bottom: 12px;">
                <input type="checkbox" id="with_facilities" name="facilities" checked>
                <lever></lever>
            </m-checkbox>
            <m-checkbox data-label="distances" w50-6 style="margin-bottom: 12px;">
                <input type="checkbox" id="with_distances" name="distances" checked>
                <lever></lever>
            </m-checkbox>
            <m-checkbox data-label="documents" w50-6 style="margin-bottom: 12px;">
                <input type="checkbox" id="with_documents" name="documents" checked>
                <lever></lever>
            </m-checkbox>
            <m-checkbox data-label="agent" w50-6 style="margin-bottom: 12px;">
                <input type="checkbox" id="with_agent" name="agent" checked>
                <lever></lever>
            </m-checkbox>
        </m-input-group>

        <m-buttons flexbox justify-end style="margin-top: 16px;">
            <m-button plain modal-close>cancel</m-button>
            <m-button export-pdf plain>export</m-button>
        </m-buttons>

        {!! Form::close() !!}

    </div>

</m-modal-wrapper>
@endsection

@section('scripts')

<script>
    // Matter.admin.properties();

    $(document).ready(function() {

        $(document).on('click', '[delete]', function(event) {
            event.preventDefault();

            var id = $(this).parent().attr('data-id');
            var url = "{{ route('api.property.delete', $id = null) }}/" + id;
            // var method = 'delete';
            var token = "{{ csrf_token() }}";

            Monolog.confirm('delete item', 'are you sure to delete this item? this cannot be undone', function() {
                NProgress.start();
                $.post(url, {_token: token}, function(data, textStatus, xhr) {

                    switch(data.status) {

                        case 200:

                            Monolog.notify(data.monolog.title, data.monolog.message);

                            removeItem(data);

                            break;

                        default:

                            Monolog.notify(data.monolog.title, data.monolog.message);

                            consoleLog(data);
                    }
                });
            });

        });

        // DATATABLE
        var table_url = "{!! $api_url !!}";
        var table_columns = [
            {
                "orderable": false,
                "data": "id",
                "render": function (data, type, row) {
                    return '<input name="id[]" value="' + data + '" type="checkbox" class="check-item visible">';
                }
            },
            // {"data": "id"},
            {
                "orderable": false,
                "targets": 1,
                "data": "thumb",
                "render": function (data, type, row) {

                    if (data.length != 0) {

                        return '<img width="100" src="'+ data[0].value +'">';
                    } else {

                        return '<img width="100" src="'+ baseUrl +'/assets/img/no-image.png">';
                    }

                }

            },
            {
                "data": "property_locales",
                "render": function (data, type, row) {

                    if (data.length != 0) {

                        return data[0].title;
                    } else {

                        return '-';
                    }

                }

            },
            {"data": "code"},
            {"data": "type"},
            {
                "data": "status",
                "render": function (data, type, row) {
                    var output = '';

                    switch(data) {
                        case '0':
                            output = 'UNAVAILABLE';
                            break;
                        case '1':
                            output = 'AVAILABLE';
                            break;
                        case '-1':
                            output = 'HIDDEN';
                            break;
                        case '-2':
                            output = 'MODERATION';
                            break;
                    }
                    return output;
                }
            },
            {
                "data": "user.branch",
                "render": function (data, type, row) {
                    return  data ? data.name : '-';
                }
            },
            {
                "data": "price",
                "render": function (data, type, row) {

                    return (""+ data).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
                }
            },
            {"data": "view"},
            {
                "data": "created_at",
                "render": function (data, type, row) {
                    var date = new Date(data);

                    var newDate = data.substr(0, 10);

                    return newDate;
                }
            },
            {
                "orderable": false,
                "data": 'id',
                "render": function (data, type, row) {

                    return ''
                        + '<m-table-list-more>'
                        + '<i class="material-icons">more_horiz</i>'
                        + '<m-list-menu data-id="'+ data +'">'
                        + '<a href="'+ baseUrl +'/admin/properties?category={{ Input::get("category") }}&action=edit&id='+ data +'"><m-list-menu-item edit data-source="property/get" data-function="populatePropertyEdit">EDIT</m-list-menu-item></a>'

                        @if(Input::get("status") != 'request')
                        + '<a href="'+ baseUrl +'/admin/properties?action=duplicate&id='+ data +'"><m-list-menu-item edit data-source="property/get" data-function="populatePropertyEdit">DUPLICATE</m-list-menu-item></a>'
                        @endif

                        + '<a href="'+ baseUrl +'/admin/properties?category={{ Input::get("category") }}&action=edit-translation&id='+ data +'"><m-list-menu-item data-function="populatePropertyTranslate">TRANSLATION</m-list-menu-item></a>'
                        + '<m-list-menu-item class="export-pdf-button">EXPORT PDF</m-list-menu-item>'
                        + '<m-list-menu-item delete data-url="property/destroy">DELETE</m-list-menu-item>'
                        + '</m-list-menu>'
                        + '</m-table-list-more>';
                }
            }

        ];
        var table_element = $('#property-table');
        var table_order = [[ 0, "desc" ]];
        var delete_url = "{{ route('api.property.remove') }}";
        var status_url = "{{ route('api.property.set_status') }}";

        var action_url = {'status': status_url, 'delete': delete_url};

        Listing.render(table_url, table_element, table_columns, table_order);

        var available, sold, hidden;

        @if(Input::get('status') == 'available')
            available = false;
            sold = true;
            hidden = true;
        @endif

        @if(Input::get('status') == 'sold')
            available = true;
            sold = false;
            hidden = true;
        @endif

        @if(Input::get('status') == 'hidden')
            available = true;
            sold = true;
            hidden = false;
        @endif

        @if(Input::get('status') == 'request')
            available = true;
            sold = true;
            hidden = true;
        @endif

        Listing.action(action_url, available, sold, hidden, true);

    });

    $( document ).on( 'click', '.export-pdf-button', function(event) {
        event.preventDefault();

        var id = $(this).closest('m-list-menu').attr('data-id');

        $('#form-pdf-id').val(id);

        modalOpen('#pdf-export-modal');
    });

    $( document ).on( 'click', '[export-pdf]', function() {

        console.log('export clicked!');

        NProgress.start();

        var frm = $('#export-pdf'),
        url = frm.attr('action'),
        data = frm.serialize();

        $.post(url, data, function(data, textStatus, xhr) {

            console.log(data);

            $.each(data, function(index, val) {

                window.open("{{ url('export/pdf') }}/" + val, '_blank');
            });

            NProgress.done();

        });
        
    });

    $(document).on('click', '.export-csv', function(event) {
        event.preventDefault();

        modalOpen('#csv-export-modal');
    });

    $(document).on('click', '[export-csv]', function(event) {
        event.preventDefault();

        console.log('export clicked!');

        NProgress.start();

        var frm = $('#export-csv'),
        url = frm.attr('action'),
        data = frm.serialize();

        $.post(url, data, function(data, textStatus, xhr) {

            console.log(data);

            window.location.href = "{{ url('export/csv') }}/" + data;

            NProgress.done();

        });

    });

</script>

@endsection
