@extends('admin.master')
@section('page', 'properties')

@section('content')
<h3>Edit Testimony</h3>

{!! Form::open(['url' => route('api.testimony.update', $testimony->id), 'id' => 'formContent']) !!}

{!! method_field('PUT') !!}

    <m-caroussel>

        <m-caroussel-header class="flexbox justify-end">
            <m-caroussel-switch-wrapper class="flexbox">
                <?php $numberOfSlides = 2 ?>
                <m-caroussel-switch class="active">general</m-caroussel-switch>
                <m-caroussel-switch id="tabfr">gallery</m-caroussel-switch>
            </m-caroussel-switch-wrapper>
        </m-caroussel-header>

        <m-caroussel-body>
            <m-caroussel-slider class="flexbox align-start" style="width: <?= $numberOfSlides ?>00%;">

                <m-caroussel-slide class="flexbox flexbox-wrap" id="caroussel-general" style="width: calc(100% / <?= $numberOfSlides ?>)">

                    <div class="m-input-group fwidth flexbox justify-between">
                        <div class="m-input-wrapper w50-6">
                            <input value="{{ $testimony->customer_name }}" type="text" name="customer_name" required>
                            <label for="title">customer name</label>
                        </div>

                        <div class="m-input-wrapper w50-6">
                            <select name="status">
                                <option value="1" {{ $testimony->status == 1 ? 'selected' : '' }}>publish</option>
                                <option value="0" {{ $testimony->status == 0 ? 'selected' : '' }}>draft</option>
                            </select>
                            <label for="title">status</label>
                        </div>
                    </div>

                    <div class="m-input-group fwidth flexbox justify-between">
                        <div class="m-input-wrapper fwidth">
                            <input value="{{ $testimony->title }}" type="text" name="title" required>
                            <label for="title">title</label>
                        </div>
                    </div>
                    <!-- 
                    <div class="m-input-group fwidth flexbox justify-between">
                        <div class="m-input-wrapper w50-6">
                            <input type="text" name="meta_keyword" required>
                            <label for="title">keyword</label>
                        </div>
                        <div class="m-input-wrapper w50-6">
                            <input type="text" name="meta_description" required>
                            <label for="title">description</label>
                        </div>
                    </div>
                     -->
                    <div class="m-input-group textarea fwidth flexbox flexbox-wrap">
                        <h3 class="input-group-title">content</h3>
                        <div class="input-wrapper fwidth">
                            <textarea rows="10" id="editor" name="content" rows="10" style="padding-top: 0">{!! $testimony->content !!}</textarea>
                        </div>
                    </div>                

                </m-caroussel-slide>

                <m-caroussel-slide class="flexbox flexbox-wrap" id="caroussel-gallery" style="width: calc(100% / <?= $numberOfSlides ?>)">

                    <m-input picture>
                        <div class="dz-default dz-message drop-hint"><span>drop files here</span></div>
                    </m-input>

                    <div id="gallery-wrapper" flexwrap style="margin-bottom: 200px"></div>

                </m-caroussel-slide>

            </m-caroussel-slider>
        </m-caroussel-body>

    </m-caroussel>

    <m-buttons flexbox justify-end>
        <m-button plain onclick="window.history.back()">cancel</m-button>
        <m-button save-form plain>save</m-button>
    </m-buttons>

{!! Form::close() !!}

@endsection

@section('scripts')


<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script type="text/javascript" src="{{ asset('assets/js/dropzone.js') }}"></script>
<script>

    $(document).ready(function() {

        var upload_url = "{!! route('api.attachment.upload.image', ['name' => 'testimony', 'watermark' => true]) !!}";

        var myDropzone = new Dropzone('m-input[picture]', {
            url: upload_url,
            previewsContainer: '#gallery-wrapper',
            addRemoveLinks: true,
            init: function () {

                this.on("success", function (file, data) {

                    var inputHid = Dropzone.createElement('<input type="hidden" name="images[]" value="' + data.id + '">');
                    file.previewElement.appendChild(inputHid);
                    reSort();
                });

            }

        });

        function reSort() {
            $("#gallery-wrapper").sortable({
                items: '.dz-preview',
                cursor: 'move',
                opacity: 0.5,
                containment: '#gallery-wrapper',
                distance: 20,
                tolerance: 'pointer'
            });
        }

        <?php
        $galleries = array();
        foreach ($testimony->attachments as $key => $value) {
            $galleries[] = ['id' => $value->id, 'name' => $value->name, 'size' => filesize('uploads/images/testimony/' . $value->name)];
        }
        ?>

        var galleries = {!! json_encode($galleries) !!};

        for (i = 0; i < galleries.length; i++) {
            myDropzone.emit("addedfile", galleries[i]);
            myDropzone.emit("thumbnail", galleries[i], 'uploads/images/testimony/thumb/' + galleries[i]['name']);
            myDropzone.emit("complete", galleries[i]);
            myDropzone.emit("success", galleries[i], galleries[i]);
            myDropzone.files.push(galleries[i]);
        }

        $('#editor').redactor({

            buttons: ['html', '|', 'formatting', '|', 'bold', 'italic', 'deleted', '|', 'unorderedlist', 'orderedlist', 'outdent', 'indent', '|',
                    
                    'fontcolor', 'backcolor', '|', 'alignment', '|', 'horizontalrule'], 

            minHeight: 300
        });
        
        $(document).on('click', '[save-form]', function(event) {
            event.preventDefault();
            
            console.log('save clicked!');

            var url = "{{ route('api.testimony.update', $testimony->id) }}";
            var fd = new FormData($('form')[0]);

            NProgress.start();

            Ajax.post(url, fd, saved);
            

            NProgress.done();
        });
    });

    function saved() {

        $('input').val('');

        $('textarea').val('');

        location.reload();
    }

</script>
@endsection
