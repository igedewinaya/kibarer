@extends('admin.master')
@section('page', 'dashboard')
@section('content')

<m-template dashboard flexwrap justify-between>

    <m-dashboard-item id="overview" fwidth>
        <h3>Overview</h3>

        <m-card flexwrap>
            <div class="overview-item">
                <p class="overview-item-title">Today's PageViews</p>
                <p class="overview-item-value" id="today"></p>
            </div>

            <div class="overview-item">
                <p class="overview-item-title">Last 30 Days PageViews</p>
                <p class="overview-item-value" id="last30"></p>
            </div>

            <div class="overview-item">
                <p class="overview-item-title">Average Bounce Rate</p>
                <p class="overview-item-value" id="bounce"></p>
            </div>

            <div class="overview-item">
                <p class="overview-item-title">New Visitor (30 Days)</p>
                <p class="overview-item-value" id="newvisitor"></p>
            </div>

        </m-card>
    </m-dashboard-item>

    <m-dashboard-item id="statistics" w66-6>
        <h3>Last 30 Days Page Views</h3>

        <div id="analyticsChart" style="height: 300px;">
            <div class="cssload-container"><div class="cssload-whirlpool"></div></div>
        </div>
        {{--<hr/>--}}
        {{--<button data-type="day">Day</button> |--}}
        {{--<button data-type="week">Week</button> |--}}
        {{--<button data-type="month">Month</button>--}}
    </m-dashboard-item>

    <m-dashboard-item id="statistics" w33-6>
        <h3>Top Country Visitors</h3>

        <div id="countryChart" style="height: 300px;"><div class="cssload-container"><div class="cssload-whirlpool"></div></div></div>
    </m-dashboard-item>

    <m-dashboard-item id="ga-quick" w33-8>
        <h3>Property</h3>

        <m-card flexwrap>
            <ul class="quick-ul">
                <li class="flexbox">
                    <p class="quick-title">Total Listing : </p>
                    <p class="quick-description" id="totallist"> 500</p>
                </li>
                <li class="flexbox">
                    <p class="quick-title">Today New Listing : </p>
                    <p class="quick-description" id="newlist"> 23</p>
                </li>
                <li>&nbsp;</li>
                <li>&nbsp;</li>
                <li>&nbsp;</li>
                <li>&nbsp;</li>
                <li>&nbsp;</li>
                <li>&nbsp;</li>
                <li>&nbsp;</li>
                <li>&nbsp;</li>
            </ul>
        </m-card>
    </m-dashboard-item>

    <m-dashboard-item id="calendar" w33-8>
        <h3>User</h3>

        <m-card flexwrap>
            <ul class="quick-ul">
                <li class="flexbox">
                    <p class="quick-title">Total Customers : </p>
                    <p class="quick-description" id="customer"> 350</p>
                </li>
                <li class="flexbox">
                    <p class="quick-title">Today New Customers : </p>
                    <p class="quick-description" id="newcustomer"> 4</p>
                </li>
                <li>&nbsp;</li>
                <li>&nbsp;</li>
                <li>&nbsp;</li>
                <li>&nbsp;</li>
                <li>&nbsp;</li>
                <li>&nbsp;</li>
                <li>&nbsp;</li>
                <li>&nbsp;</li>
            </ul>
        </m-card>
    </m-dashboard-item>

    <m-dashboard-item id="calendar" w33-8>
        <h3>Support Ticket</h3>

        <m-card flexwrap>
            <div class="ticket-wrapper fwidth">
                    {!! Form::open(['url' => route('admin.send_ticket'), 'id' => 'ticket-form']) !!}
                    <div class="m-input-wrapper">
                        <input type="text" name="subject" id="ticket-input-subject" placeholder="SUBJECT" required>
                    </div>
                    <div class="m-input-wrapper">
                        <textarea name="message" id="ticket-message" rows="3" placeholder="MESSAGE"></textarea>
                    </div>
                    <div class="button-holder align-right">
                        <button type="submit" class="md-button md-button-salmon shadow-hover" id="send-ticket">send</button>
                    </div>
                    {!! Form::close() !!}
            </div>
        </m-card>
    </m-dashboard-item>
</m-template>

@stop

@section('scripts')
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
    <script type="text/javascript">
        Matter.admin.dashboard();
    </script>

@stop
