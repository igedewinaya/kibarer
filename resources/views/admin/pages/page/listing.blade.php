@extends('admin.master')
@section('page', 'properties')

@section('fab')

<!-- <m-fab salmon class="modal-open" data-target="#page-add"><i class="material-icons">add</i></m-fab> -->
<a href="{{ route('admin.pages', ['term' => null, 'action' => 'create']) }}"><m-fab salmon data-target="#page-add"><i class="material-icons">add</i></m-fab></a>

@stop

@section('content')

<a class="export-csv"><i class="material-icons">open_in_new</i> <label style="position: relative; top: -5px">Export CSV</label></a>

<m-template list class="page-wrapper">

    <table>
        <thead>
            <th width="15px"><input type="checkbox" class="check-all visible"></th>
            <td>title</td>
            <td>route</td>
            <td>status</td>
            <td>Created</td>
            <td>Action</td>
        </thead>

    </table>


</m-template>

@endsection

@section('modal')
<m-modal-wrapper id="csv-export-modal">

    <div class="modal-window" style="max-width: 640px" data-id="">
        <h3>CSV Export Configuration</h3>

        {!! Form::open(['url' => route('export.page.csv'), 'id' => 'export-csv']) !!}

        <m-input-group fwidth flexwrap justify-between>
            <m-input select w50-6>
                <select name="take" id="take">
                    <option value="10" selected>10</option>
                    <option value="100">100</option>
                    <option value="1000">1000</option>
                    <option value="all">all</option>
                </select>
                <label for="title">Rows</label>
            </m-input>
        </m-input-group>

        <m-buttons flexbox justify-end style="margin-top: 16px;">
            <m-button plain modal-close>cancel</m-button>
            <m-button export-csv plain>export</m-button>
        </m-buttons>

        {!! Form::close() !!}
    </div>

</m-modal-wrapper>
@endsection

@section('scripts')

<script>
    // Matter.admin.pages();

    $(document).ready(function() {

        $(document).on('click', '[delete]', function(event) {
            event.preventDefault();

            var id = $(this).parent().attr('data-id');            
            var url = "{{ route('api.page.destroy', $id = null) }}/" + id;
            var method = 'delete';
            var token = "{{ csrf_token() }}";

            Monolog.confirm('delete item', 'are you sure to delete this item? this cannot be undone', function() {
                NProgress.start();
                $.post(url, {_method: method, _token: token}, function(data, textStatus, xhr) {
                    
                    switch(data.status) {

                        case 200:

                            Monolog.notify(data.monolog.title, data.monolog.message);

                            removeItem(data);

                            break;

                        default:

                            Monolog.notify(data.monolog.title, data.monolog.message);

                            consoleLog(data);
                    }
                });
            });

        });

        $('table').DataTable({
            "processing": true,
            "serverSide": true,            
            "ajax": {
                "url": "{!! $api_url !!}",
                "type": "GET"
            },
            "deferRender": true,
            "columns": [
                {
                    "orderable": false,
                    "data": "id",
                    "render": function (data, type, row) {
                        return '<input value="' + data + '" type="checkbox" class="check-item visible">';
                    }
                },
                {"data": "page_locales.0.title"},
                {"data": "route"},
                {
                    "data": "status",
                    "render": function (data, type, row) {
                        return data == 1 ? 'publish' : 'draft';
                    }
                },
                {
                    "data": "created_at",
                    "render": function (data, type, row) {
                        var date = new Date(data);
                        var newDate = date.toISOString().split('T')[0];
                        return newDate;
                    }
                },
                {
                    "orderable": false,
                    "data": 'id',
                    "render": function (data, type, row) {

                        return ''
                        + '<m-table-list-more>'
                            + '<i class="material-icons">more_horiz</i>'
                            + '<m-list-menu data-id="'+ data +'">'
                                + '<a href="'+ baseUrl +'/admin/pages?action=edit&id='+ data +'"><m-list-menu-item edit data-source="property/get" data-function="populatePropertyEdit">EDIT</m-list-menu-item></a>'
                                + '<m-list-menu-item delete data-url="property/destroy">DELETE</m-list-menu-item>'
                            + '</m-list-menu>'
                        + '</m-table-list-more>';
                    }
                }
            ],
            "createdRow": function ( row, data, index ) {

                $('td:last-child', row).attr('button', '');

                $(row).attr('id', 'row-item-' + data.id);
            },
            "order": [[ 0, "desc" ]]
        });

    });

    function removeItem(data) {

        var id = data.id;

        $('#row-item-' + id).remove();

        NProgress.done();
    }

    $(document).on('click', '.export-csv', function(event) {
        event.preventDefault();
        /* Act on the event */

        modalOpen('#csv-export-modal');
    });

    $(document).on('click', '[export-csv]', function(event) {
        event.preventDefault();

        console.log('export clicked!');

        NProgress.start();

        var frm = $('#export-csv'),
        url = frm.attr('action');
        data = frm.serialize();

        $.post(url, data, function(data, textStatus, xhr) {

            console.log(data);

            window.location.href = "{{ url('export/csv') }}/" + data;

            NProgress.done();

        });

    });

</script>

@endsection