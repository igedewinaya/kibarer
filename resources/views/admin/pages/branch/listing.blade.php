@extends('admin.master')
@section('page', 'properties')

@section('fab')

<!-- <m-fab salmon class="modal-open" data-target="#branch-add"><i class="material-icons">add</i></m-fab> -->
<a href="{{ route('admin.branches', ['term' => null, 'action' => 'create']) }}"><m-fab salmon data-target="#branch-add"><i class="material-icons">add</i></m-fab></a>

@stop

@section('content')

<!-- <a class="export-csv"><i class="material-icons">open_in_new</i> <label style="position: relative; top: -5px">Export CSV</label></a> -->
<div style="margin-top: 30px"></div>
<m-template list class="branch-wrapper">

    <table data-selected="" data-token="{{ csrf_token() }}">
        <thead>
            <th width="15px"><input type="checkbox" class="check-all visible"></th>
            <td>name</td>
            <td>city</td>
            <td>province</td>
            <td>country</td>
            <td>manager</td>
            <td>Created</td>
            <td>Action</td>
        </thead>

    </table>


</m-template>

@endsection

@section('modal')
<m-modal-wrapper id="csv-export-modal">

    <div class="modal-window" style="max-width: 640px" data-id="">
        <h3>CSV Export Configuration</h3>

        {!! Form::open(['url' => route('export.branch.csv'), 'id' => 'export-csv']) !!}

        <input id="form-csv-id" type="hidden" name="id" value="">

        <m-input-group fwidth flexwrap justify-between>
            <m-input select w50-6>
                <select name="take" id="take">
                    <option value="selected" selected>selected</option>
                    <option value="all">all</option>
                </select>
                <label for="title">Rows</label>
            </m-input>
        </m-input-group>

        <m-buttons flexbox justify-end style="margin-top: 16px;">
            <m-button plain modal-close>cancel</m-button>
            <m-button export-csv plain>export</m-button>
        </m-buttons>

        {!! Form::close() !!}
    </div>

</m-modal-wrapper>
@endsection

@section('scripts')

<script>
    // Matter.admin.branches();

    $(document).ready(function() {

        $(document).on('click', '[delete]', function(event) {
            event.preventDefault();

            var id = $(this).parent().attr('data-id');            
            var url = "{{ route('api.branch.destroy', $id = null) }}/" + id;
            var method = 'delete';
            var token = "{{ csrf_token() }}";

            Monolog.confirm('delete item', 'are you sure to delete this item? this cannot be undone', function() {
                NProgress.start();
                $.post(url, {_method: method, _token: token}, function(data, textStatus, xhr) {
                    
                    switch(data.status) {

                        case 200:

                            Monolog.notify(data.monolog.title, data.monolog.message);

                            removeItem(data);

                            break;

                        default:

                            Monolog.notify(data.monolog.title, data.monolog.message);

                            consoleLog(data);
                    }
                });
            });

        });

        var table_url = "{!! $api_url !!}";
        var table_columns = [
                {
                    "orderable": false,
                    "data": "id",
                    "render": function (data, type, row) {
                        return '<input value="' + data + '" type="checkbox" class="check-item visible">';
                    }
                },
                {"data": "name"},
                {"data": "city"},
                {"data": "province"},
                {"data": "country"},
                {"data": "manager"},
                {
                    "data": "created_at",
                    "render": function (data, type, row) {
                        var date = new Date(data);
                        var newDate = date.toISOString().split('T')[0];
                        return newDate;
                    }
                },
                {
                    "orderable": false,
                    "data": 'id',
                    "render": function (data, type, row) {

                        return ''
                        + '<m-table-list-more>'
                            + '<i class="material-icons">more_horiz</i>'
                            + '<m-list-menu data-id="'+ data +'">'
                                + '<a href="'+ baseUrl +'/admin/branches?action=edit&id='+ data +'"><m-list-menu-item edit data-source="property/get" data-function="populatePropertyEdit">EDIT</m-list-menu-item></a>'
                                + '<m-list-menu-item delete data-url="property/destroy">DELETE</m-list-menu-item>'
                            + '</m-list-menu>'
                        + '</m-table-list-more>';
                    }
                }
            ];
        var table_element = $('table');
        var table_order = [[ 0, "desc" ]];
        var delete_url = "{{ route('api.branch.remove') }}";

        var action_url = {'delete': delete_url};

        Listing.render(table_url, table_element, table_columns, table_order);

        Listing.action(action_url, false, false, false);

    });

    function removeItem(data) {

        var id = data.id;

        $('#row-item-' + id).remove();

        NProgress.done();
    }

    $(document).on('click', '.export-csv', function(event) {
        event.preventDefault();
        /* Act on the event */

        modalOpen('#csv-export-modal');
    });

    $(document).on('click', '[export-csv]', function(event) {
        event.preventDefault();

        console.log('export clicked!');

        NProgress.start();

        var frm = $('#export-csv'),
        url = frm.attr('action');
        data = frm.serialize();

        $.post(url, data, function(data, textStatus, xhr) {

            console.log(data);

            window.location.href = "{{ url('export/csv') }}/" + data;

            NProgress.done();

        });

    });

</script>

@endsection