<script type="text/javascript" src="{{ asset('bower/jquery/dist/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower/tinymce/tinymce.jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower/lodash/lodash.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower/nprogress/nprogress.js') }}"></script>
<!-- <script type="text/javascript" src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script> -->
<script type="text/javascript" src="{{ asset('assets/js/admin/jquery.dataTables.js') }}"></script>
<!-- <script type="text/javascript" src="{{ asset('bower/ckeditor/ckeditor.js') }}"></script> -->
<script type="text/javascript" src="{{ asset('assets/js/admin/scripts.js') }}"></script>

<script type="text/javascript">

function datatableColumnSelect(table, columns) {

    var el = '<div style="float:right; margin-top: 10px;" class="w33" flexwrap justify-between><select class="w33-6">';

    $.each(columns, function(index, val) {
         
         el += '<option>' + val + '</option>';
    });

    el += '</select>';

    el += '<div class="w66-6">'
            + '<input class="search-column" type="text" placeholder="Search" style="background: transparent; border-bottom: 1px solid #000;">'
        + '</div>';

    el += '</div>';

    $(el).insertAfter('.dataTables_length');
 
    $('.search-column').on('keyup', function () {

        table.columns(0)
            .search(this.value)
            .draw();

    });

}

$(document).on('click', '#action-go', function(event) {
    event.preventDefault();

    var val = $(this).closest('div').find('select').val();

    if (val == 'delete') {

        Monolog.confirm('delete item', 'are you sure to delete selected item? this cannot be undone', function() {
            NProgress.start();

            NProgress.done();
        });
    }

    if (val == 'pdf') {

        modalOpen('#pdf-export-modal');
    }

    if (val == 'csv') {

        modalOpen('#csv-export-modal');
    }

});

</script>

