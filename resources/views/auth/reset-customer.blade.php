@extends('index')

@section('content')


<div id="content">
    <div class="account">
        
        <div class="forgot-password">
            <h1>{{ trans('word.reset_password') }}</h1>
            <div class="kesato-form">
                    {!! Form::open(['url' => url('password/reset'), 'id' => 'register-form']) !!}

                    <input value="{{ $token }}" type="hidden" name="token">

                    <div class="form-body">
                        <div class="section">
                            <label for="useremail" class="field prepend-icon">
                                <input type="email" name="email" id="useremail" class="gui-input" placeholder="Email address">
                            </label>
                        </div>
                        <div class="section">
                            <label for="useremail" class="field prepend-icon">
                                <input type="password" name="password" id="useremail" class="gui-input" placeholder="Password">
                            </label>
                        </div>
                        <div class="section">
                            <label for="useremail" class="field prepend-icon">
                                <input type="password" name="password_confirmation" id="useremail" class="gui-input" placeholder="Password Confirmation">
                            </label>
                        </div>
                        <!-- <p class="note" style="font-style:italic;">Note : The recovery password will be send to your email </p> -->
                    </div>
                    <div class="form-footer">
                        <button type="submit" class="button btn-primary">{{ trans('word.reset') }}</button>
                        <!--
                        <button type="reset" class="button"> Cancel </button>
                        -->
                    </div>
                {!! Form::close() !!}
            </div><!-- END KESATO FORM -->          
        </div><!-- END LOGIN BOX -->
        
    </div><!-- END ACCOUNT -->
</div><!-- END MAIN -->

@endsection

@section('scripts')
<script type="text/javascript">
$(document).ready(function() {

    $(document).on('submit', '#register-form', function(event) {
        event.preventDefault();
        /* Act on the event */

        var btn = $('button[type=submit]');

        btn.html("{{ trans('word.please_wait') }}");

        var frm = $(this),
        url = frm.attr('action'),
        data = frm.serialize();

        $.post(url, data, function(data, textStatus, xhr) {
            /*optional stuff to do after success */

            if (data.status == 200) {

             btn.html("{{ trans('word.reset') }}");

             window.location.href = "{{ route('login', ['login' => trans('url.login') ]) }}";
         }

     }).error(function(data) { 

        var errors = data.responseJSON;

        btn.html("{{ trans('word.reset') }}");

        if (errors) {
            var msg = "{{ trans('notification.error_reset_password') }}";
            $('.form-body').append('<p class="note" style="font-style:italic;color:red;">' + errors.password + '</p>');

        }


    });
 });
});
</script>
@endsection
