<!DOCTYPE HTML>
<html>
    <head>
        <title>{{ ucfirst(env('APP_NAME', 'Matter')) }} | Matter</title>
        <meta charset="utf-8" />
        <base href="{{ asset('/') }}/" target="_top">
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="<?= baseUrl(); ?>/assets/css/admin.css">
        <link rel="stylesheet" href="{{ asset('bower/normalize-css/normalize.css') }}">
        <link rel="stylesheet" href="{{ asset('bower/nprogress/nprogress.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/admin.css') }}">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href='https://fonts.googleapis.com/css?family=Product+Sans' rel='stylesheet' type='text/css'>

        <style>
            input{
                background-color: #F9F9F9;
            }
            body{
                background: #f9f9f9;
            }

            #login-window{
                width: 100vw;
                height: 100vh;
            }

            .wallpaper{
                background-color: #CCCCCC;
                width: calc(100% - 480px);
                background-size: cover;
                background-position: center;
                text-align: center;
            }

            .wallpaper img {
                position: relative;
                margin-top: 30%;
            }

            .login-container{
                width: 480px;
            }

            #login-form{
                margin: 0 auto;
                max-width: 240px;
                margin-top: calc(50vh - 140px);
            }

            h1{
                font-size: 32px;
                font-weight: 100;
                margin-bottom: 32px;
                text-transform: uppercase;
            }

            .m-input-wrapper{
                width: 100%;
            }

            input:-webkit-autofill {
                -webkit-box-shadow: 0 0 0 1000px #f9f9f9 inset;
            }
        </style>
    </head>

    <body>
        <div class="flexbox" id="login-window">

            <div class="wallpaper">
                <img src="assets/img/logo.png" alt="">
            </div>
            <div class="login-container">
                {!! Form::open(array('id' => 'login-form', 'role' => 'form', 'autocomplete' => 'off')) !!}
                <input type="hidden" name="type" value="user">
                <input type="hidden" name="token" value="{{ $token }}">

                <h1 style="font-size: 24px">password reset</h1>
                <div id="login-form-msg"></div>

                <div class="m-input-wrapper w33-8">
                    <input type="text" name="email" id="login-email" value="{{ old('email') }}" required autocapitalize="off"/>
                    <label for="inspector">email</label>
                </div>

                <div class="m-input-wrapper w33-8">
                    <input type="password" name="password" id="login-password" value="{{ old('password') }}" required autocapitalize="off"/>
                    <label for="inspector">password</label>
                </div>

                <div class="m-input-wrapper w33-8">
                    <input type="password" name="password_confirmation" id="login-password_confirmation" value="{{ old('password_confirmation') }}" required autocapitalize="off"/>
                    <label for="inspector">password confirmation</label>
                </div>

                <div class="button-holder flexbox justify-between">
                    <a href="{{ url('admin') }}" class="md-button md-button-icon" id="back-button"><i class="material-icons">arrow_back</i>login</a>
                    <a href class="md-button md-button-right" id="login-button">reset</a>
                </div>
                {!! Form::close() !!}
            </div>
        </div>

        <script type="text/javascript" src="{{ asset('bower/jquery/dist/jquery.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('bower/nprogress/nprogress.js') }}"></script>
        <script type="text/javascript">

            $(document).on('click', '#login-button', function(e) {

                e.preventDefault();

                login();
            });

            $(document).on('keyup', 'input', function(e) {

                if(e.which == 13 && $('#login-email').val() != '' && $('#login-password').val() != '') login();
            });

            function login() {

                NProgress.start();

                fd = new FormData($('#login-form')[0]);

                $.ajax({

                    url: "{{ url('password/reset') }}",

                    type: 'POST',

                    data: fd,

                    dataType: 'json',

                    contentType: false,

                    processData: false,

                    success: function(data){

                        if(data.status == 200){

							$('#login-form-msg').html('Your password has been reset successfully. Please go to login page.').css({
								'margin-bottom': '23px',
								'color': '#00D231'
							});

							$('.m-input-wrapper').remove();

							$('#login-button').remove();

                            NProgress.done();

                        } else{

                            // console.log(data);
                            if (data.email) $('input[name=email]').css('border-bottom', '1px solid #FF0C0C');
                        }
                    },
                    error: function(data){

                    	var errors = data.responseJSON; 

                    	console.log(errors);
                        // console.log(xhr);
                        // console.log(status);
                        // console.log(message);

                        if (errors.email) $('input[name=email]').css('border-bottom', '1px solid #FF0C0C');

                        if (errors.password) $('input[name=password]').css('border-bottom', '1px solid #FF0C0C');
                        
                        NProgress.done();
                    }
                });
            }

        </script>
    </body>
</html>

