@extends('index')

@section('content')

<div id="content">
    <div class="account">
        
        <div class="forgot-password">
            <h1>Forgot password</h1>
            <div class="kesato-form">
                <form method="post" action="" id="register-form">

                    {!! Form::open(['url' => route('post_forgot_password'), 'id' => 'register-form']) !!}
                    <div class="form-body">
                        <div class="section">
                            <label for="useremail" class="field prepend-icon">
                                <input value="{{ old('email') }}" type="email" name="email" id="useremail" class="gui-input" placeholder="Email address">
                            </label>
                        </div>
                        <p class="note" style="font-style:italic;">Note : The recovery password will be send to your email </p>
                    </div>
                    <div class="form-footer">
                        <button type="submit" class="button btn-primary">{{ trans('word.send_reset_link') }}</button>
                        <!--
                        <button type="reset" class="button"> Cancel </button>
                        -->
                    </div>
                    {!! Form::close() !!}
            </div><!-- END KESATO FORM -->          
        </div><!-- END LOGIN BOX -->
        
    </div><!-- END ACCOUNT -->
</div><!-- END MAIN -->


@endsection

@section('scripts')
<script type="text/javascript">
$(document).ready(function() {
    
    $(document).on('submit', '#register-form', function(event) {
        event.preventDefault();
        /* Act on the event */

        var btn = $('button[type=submit]');

        btn.html("{{ trans('word.please_wait') }}");

        var frm = $(this),
        url = frm.attr('action'),
        data = frm.serialize();

        $.post(url, data, function(data, textStatus, xhr) {
            /*optional stuff to do after success */
            if (data.status == 200) {

                $('input[type=email]').val('');

                var notif = "{{ trans('notification.email_posted') }}";

                $('.note').html(notif).css('color', 'green');

            } else {

                console.log(data);

                var notif = data.monolog.message;

                $('.note').html(notif).css('color', 'red');

            }

            btn.html("{{ trans('word.send_reset_link') }}");

        })
        .error(function(data) {
            /* Act on the event */
            var message = $.parseJSON(data.responseText);
            console.log(message);

            var notif = message.email;
            $('.note').html(notif).css('color', 'red');

            btn.html("{{ trans('word.send_reset_link') }}");
        });

    });
});
</script>
@endsection
