
        var markersToRemove = [];
        var myLatLng = { lat: -8.4420734, lng: 114.9356164 };
        var pin = 'assets/img/marker.png';

        var map = new google.maps.Map(document.getElementById('map'), {
            mapTypeControl: false,
            center: myLatLng,
            scrollwheel: true,
            zoom: 10,
            icon: pin,
            maxZoom: 15,
            minZoom: 8,
            draggable: true,
            zoomControl: false,
            disableDoubleClickZoom: true,
            streetViewControl: false
        });

        var input = document.getElementById('pac-input'),
            searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        map.addListener('bounds_changed', function() {
            searchBox.setBounds(map.getBounds());
        });

        searchBox.addListener('places_changed', function() {

            var places = searchBox.getPlaces();
            if (places.length == 0) {
                return;
            }

            var bounds = new google.maps.LatLngBounds();
            places.forEach(function(place) {
                if (place.geometry.viewport) {
                    // Only geocodes have viewport.
                    bounds.union(place.geometry.viewport);
                } else {
                    bounds.extend(place.geometry.location);
                }


            });
            map.fitBounds(bounds);
        });

        google.maps.event.addListener(map, "click", function (e) {
            removeMarkers();
            var latLng = e.latLng,
                strlatlng = latLng.toString(),
                spllatlng = strlatlng.split(','),
                lats = spllatlng[0].replace("(", ""), 
                longs = spllatlng[1].replace(")", "");

            $("#map_latitude").val(lats);
            $("#map_longitude").val(longs);
            placeMarker(latLng, map);

        });

        function placeMarker(location, map) {
            var marker = new google.maps.Marker({
                position: location, 
                icon: pin,
                map: map
            });

            markersToRemove.push(marker);
        }

        function removeMarkers() {
            for(var i = 0; i < markersToRemove.length; i++) {
                markersToRemove[i].setMap(null);
            }
        }