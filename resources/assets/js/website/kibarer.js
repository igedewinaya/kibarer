var Kibarer = {
	CURRENTPAGE : '',
	init : function() {
		Kibarer.CURRENTPAGE = $('#CURRENTPAGE').val();
		Kibarer.initPage();
		Kibarer.initCommon();
		Kibarer.initForm();
//		Kibarer.initProperty();
	},


	initPage : function(page){
		var page = page==undefined ? Kibarer.CURRENTPAGE : page;
		switch(page) {
			case 'home' : Kibarer.initHome(); break;
			case 'property' : Kibarer.initProperty(); break;
			case 'propertylisting' : Kibarer.initPropertyListing(); break;
			case 'propertydetail' : Kibarer.initPropertyDetail(); break;
			case 'testimonialdetail' : Kibarer.initTestimonialDetail(); break;
		}
	},
	
	
	initCommon: function(){
		
		//STICKY HEADER
		
		$(window).scroll(function() {
		    if ($(this).scrollTop() > 10){  
		        $('header').addClass("sticky");
		    }
		    else{
		        $('header').removeClass("sticky");
		    }
		});

		//BURGER RESPONSIVE NAV
		$(document).on('click', '#burger', function(i){
		
		    i.preventDefault();
		
		    if($(this).hasClass('active')){
		        $(this).removeClass('active');
		        $('.fullscreen-nav').removeClass('active');
		        $('body').css({
		            'height' : 'auto',
		            'overflow' : 'auto',
		            'position': 'relative'
		        });
		    }else {
		        $(this).addClass('active');
		        $('.fullscreen-nav').addClass('active');
		        $('body').css({
		            'height' : '100vh',
		            'overflow' : 'hidden',
		            'position': 'fixed'
		        });
		    }
		});		
		
		/* FOOTER ALWAYS BOTTOM
		var kh = $('#kibarer-footer').height() + 50;
		$('#wrapper').css('padding-bottom',kh);
		*/
	},
	
	initForm :function(){
		$(".register-city").select2();
		  
		//ALL FORM VALIDATION AND SETTING
		$.validator.methods.smartCaptcha = function(value, element, param) {
				return value == param;
		};
		$( "#validation-form" ).validate({
				errorClass: "state-error",
				validClass: "state-success",
				errorElement: "em",
		
				rules: {
					reg_firstname: { 
						required: true 
					},
					reg_lastname: {
						required: true
					},					
					reg_email: {
						required: true,
						email: true
					},
					reg_address:  {
						required: true,
						minlength: 30
					},
					reg_phone: {
						require_from_group: [1, ".phone-group"]
					},											
					reg_password:{
						required: true,
						minlength: 6,
						maxlength: 16						
					},
					reg_repeatPassword:{
						required: true,
						minlength: 6,
						maxlength: 16,						
						equalTo: '#password'
					},
					log_email: {
						required: true,
						email: true
					},
					log_password: {
						required: true
					}																										
				},
					
				messages:{
					reg_firstname: {
						required: 'Enter first name'
					},
					reg_lastname: {
						required: 'Enter last name'
					},					
					reg_email: {
						required: 'Enter email address',
						email: 'Enter a VALID email address'
					},							
					reg_address:  {
						required: 'Fill your address'
					},
					reg_phone: {
						require_from_group: 'Fill at least your phone number'
					},																			
					reg_password:{
						required: 'Please enter a password'
					},
					reg_repeatPassword:{
						required: 'Please repeat the above password',
						equalTo: 'Password mismatch detected'
					},
					log_email: {
						required: 'Your email address'
					},
					log_password: {
						required: 'Please enter your password'
					},
					
				},

				/* @validation highlighting + error placement  
				---------------------------------------------------- */	
				highlight: function(element, errorClass, validClass) {
						$(element).closest('.field').addClass(errorClass).removeClass(validClass);
				},
				unhighlight: function(element, errorClass, validClass) {
						$(element).closest('.field').removeClass(errorClass).addClass(validClass);
				},
				errorPlacement: function(error, element) {
				   if (element.is(":radio") || element.is(":checkbox")) {
							element.closest('.option-group').after(error);
				   } else {
							error.insertAfter(element.parent());
				   }
				}	
		});
	},
	
	
	initHome : function(){
		
		//VIDEO
		var video = document.getElementById("video");
		document.getElementById('video').play();
		var iOS = /iPhone|iPod/.test(navigator.userAgent) && !window.MSStream,
		    ipad = /iPad/.test(navigator.userAgent) && !window.MSStream,
		    bodyH, bodyOv;
		document.addEventListener("orientationchange", updateOrientation);
		if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
		    $(video).remove();
		}
		
		function updateOrientation() {
		    if(window.innerWidth > window.innerHeight ){
		        $("#warning").addClass('active');
		        $('body').css({
		            'height' : '100vh',
		            'overflow' : 'hidden',
		            'position': 'fixed'
		        });
		    }else {
		        $("#warning").removeClass('active');
		        $('body').css({
		            'height' : bodyH,
		            'overflow' : bodyOv,
		            'position': 'relative'
		        });
		    }
		}
		
		// Hover effect for touch devices
		$('body').bind('touchstart', function() {});
		
		// ios media querie
		if( iOS ) {
		    $("body").addClass('ios-device'); 
		}
		if( ipad ) {
		    $("body").addClass('ipad'); 
		}
		
		// button pause and play video
		$(".pause-btn").click(function(i){
		
		    i.preventDefault();
		
		    if($(this).hasClass('play')){
		        video.pause();
		    } else {
		        video.play();
		    }
		
		    $(this).toggleClass("play");
		});
		
		
		$(document).ready(function() {
		    hideVideo();
		});
		
		$(window).resize(function() {
		    hideVideo(); 
		    
		    $('body').css({
		        'height': 'auto',
		        'overflow': 'auto',
		        'position': 'relative'
		    });
		    
		    $('#burger').removeClass('active');
		    $('.fullscreen-nav').removeClass('active');
		});
		
		function hideVideo() {
		    var windowWidth = $("#video-container").outerWidth(),
		        windowHeight = $("#video-container").outerHeight(),
		        videoWidth = $('#video').outerWidth(),
		        videoHeight = $('#video').outerHeight(),
		        videoRatio = videoWidth / videoHeight,
		        windowRatio = windowWidth / (windowHeight);
		
		    if($(window).outerWidth() > 900) {
		
		        $("#video-container").css({'display': 'block'});
		        if(windowRatio < videoRatio) {
		            //$('video').css({'height': '100%', 'width': 'auto'});
		
		        } else {
		            //$('video').css({'height': 'auto', 'width': '100%'});
		        }
		    } else {
		        $("#video-container").css({'display': 'none'});
		    }
		}
		
		// jumbotron villa land section animation
		$(document).on('click', '.category-btn', function(i){
		    i.preventDefault();
		    var target = $(this).attr('data-target');
		    $('.wrapper').removeClass('active');
		    if($(this).hasClass('active')){
		        $(this).removeClass('active');    
		    }else {
		        $('.category-btn').removeClass('active');
		        $(target).addClass('active');
		        $(this).addClass('active');
		        $('#jumbotron > .wrapper').addClass('active');
		    }
		});
	},
	
	
 	initContact : function() {

        var myLatLng = new google.maps.LatLng(-8.6714246,115.1607031);
        var pin = 'assets/img/marker.png';
        
        var marker=new google.maps.Marker({
            position:myLatLng,
            animation: google.maps.Animation.DROP,
            icon : pin,
            title : 'Kibarer Property'
        });
        
        var contentString = '<div id="content-form" style="10px 0px 30px 0px;">'+
            '<img src="assets/img/canggu.jpg" style="width:100px; height:100px; float:left; margin: 0px 10px 0px 0px;">'+
            '<h1 id="title" style="font-size: 1em;color: #ee5b2c;font-weight: 600; text-transform: uppercase; margin-bottom:10px;">Kibarer Property</h1>'+
            '<div id="bodyContent">'+
            '<p> Jalan Petitenget No.9, Badung, Bali 80361 <br> Ph. (+62361) 4741212 </p>' +
            '</div>'+
            '</div>';
        
        var infowindow = new google.maps.InfoWindow({
            content: contentString,
            maxWidth: 300
        });
        
        var map = new google.maps.Map(document.getElementById('map-contact'), {
            mapTypeControl: false,
            center: myLatLng,
            scrollwheel: false,
            zoom: 10,
            icon: pin,
            zoom:15,
            maxZoom: 15,
            minZoom: 8,
            draggable: true,
            zoomControl: false,
            disableDoubleClickZoom: false,
            streetViewControl: false
        });
        
        marker.setMap(map);
        infowindow.open(map, marker);
        marker.addListener('click', function() {
            infowindow.open(map, marker);

        });


    },
	
	initPropertyDetail : function(){

		 $(window).scroll(function() {

		 	if($(this).width() > 840){
				//console.log($(this).scrollTop());
			 	//var wh = $(this).height();
			 	//var st = $(this).scrollTop();
			 	
			    if ($(this).scrollTop() > 111){ 
		 	    	$('.right-side').addClass("sticky");
			    	
			    	//var b = $('#kibarer-footer').height() + 70;
			    	//var bl = b+'px';

			    	//$('.right-side').css('bottom',bl);
		 	        //$('.right-side').css('top','auto');
		 	    }
			    
		 	    else{
			    	//$('.right-side').css('bottom','auto');
			    	//$('.right-side').css('top','120px');
			    	$('.right-side').removeClass("sticky");
			    }	
		  	}

		 });

		
		/* MAP LOCATION ON SIDE */
		initMap();
		function initMap() {
	        var myLatLng = { lat: -8.6714246, lng: 115.1607031 };
	        var pin = 'assets/img/marker.png';
	        var marker = new google.maps.Marker({
	            position:myLatLng,
	            animation: google.maps.Animation.DROP,
	            icon : pin,
	            title : 'Kibarer Property'
	        });
	        var contentString = '<div id="content">'+
	            '<div id="siteNotice">'+
	            '</div>'+
	            '<h1 id="title" style="font-size: 1.5em;color: #ee5b2c;font-weight: 600; font-color">Kibarer Property</h1>'+
	            '<div id="bodyContent">'+
	            '<p><b>Uluru</b>, also referred to as <b>Ayers Rock</b>, is a large ' +
	            'sandstone rock formation in the southern part of the '+
	            'Northern Territory, central Australia. It lies 335&#160;km (208&#160;mi) '+
	            'south west of the nearest large town, Alice Springs; 450&#160;km '+
	            'Heritage Site.</p>'+
	            '</div>'+
	            '</div>';
	            
	        var infowindow = new google.maps.InfoWindow({
	            content: contentString,
	            maxWidth: 300
	        });
	        // Create a map object and specify the DOM element for display.
	        var map = new google.maps.Map(document.getElementById('propertydetailMapLocation'), {
	            mapTypeControl: false,
	            center: myLatLng,
	            scrollwheel: false,
	            zoom: 13,
	            maxZoom: 15,
	            minZoom: 8,
	            draggable: false,
	            zoomControl: false,
	            scrollwheel: false,
	            disableDoubleClickZoom: true,
	            streetViewControl: false
	        });
	        marker.setMap(map);
	        marker.addListener('click', function() {
	            infowindow.open(map, marker);
	            
	        });
	    }
	    /* END MAP LOCATION */
	    
	    /* PROPERTY SLIDESHOW */
		var makeBSS = function (el, options) {
		    var $slideshows = document.querySelectorAll(el), // a collection of all of the slideshow
		        $slideshow = {},
		        Slideshow = {
		            init: function (el, options) {
		                this.counter = 0; // to keep track of current slide
		                this.el = el; // current slideshow container    
		                this.$items = el.querySelectorAll('figure'); // a collection of all of the slides, caching for performance
		                this.numItems = this.$items.length; // total number of slides
		                options = options || {}; // if options object not passed in, then set to empty object 
		                options.auto = options.auto || false; // if options.auto object not passed in, then set to false
		                this.opts = {
		                    auto: (typeof options.auto === "undefined") ? false : options.auto,
		                    speed: (typeof options.auto.speed === "undefined") ? 1500 : options.auto.speed,
		                    pauseOnHover: (typeof options.auto.pauseOnHover === "undefined") ? false : options.auto.pauseOnHover,
		                    fullScreen: (typeof options.fullScreen === "undefined") ? false : options.fullScreen,
		                    swipe: (typeof options.swipe === "undefined") ? false : options.swipe
		                };
		                
		                this.$items[0].classList.add('bss-show'); // add show class to first figure 
		                this.injectControls(el);
		                this.addEventListeners(el);
		                if (this.opts.auto) {
		                    this.autoCycle(this.el, this.opts.speed, this.opts.pauseOnHover);
		                }
		                if (this.opts.fullScreen) {
		                    this.addFullScreen(this.el);
		                }
		                if (this.opts.swipe) {
		                    this.addSwipe(this.el);
		                }
		            },
		            showCurrent: function (i) {
		                // increment or decrement this.counter depending on whether i === 1 or i === -1
		                if (i > 0) {
		                    this.counter = (this.counter + 1 === this.numItems) ? 0 : this.counter + 1;
		                } else {
		                    this.counter = (this.counter - 1 < 0) ? this.numItems - 1 : this.counter - 1;
		                }
		
		                // remove .show from whichever element currently has it 
		                // http://stackoverflow.com/a/16053538/2006057
		                [].forEach.call(this.$items, function (el) {
		                    el.classList.remove('bss-show');
		                });
		  
		                // add .show to the one item that's supposed to have it
		                this.$items[this.counter].classList.add('bss-show');
		            },
		            injectControls: function (el) {
		            // build and inject prev/next controls
		                // first create all the new elements
		                var spanPrev = document.createElement("span"),
		                    spanNext = document.createElement("span"),
		                    docFrag = document.createDocumentFragment();
		        
		                // add classes
		                spanPrev.classList.add('bss-prev');
		                spanNext.classList.add('bss-next');
		        
		                // add contents
		                spanPrev.innerHTML = '<i class="fa fa-angle-left"></i>';
		                spanNext.innerHTML = '<i class="fa fa-angle-right"></i>';
		                
		                // append elements to fragment, then append fragment to DOM
		                docFrag.appendChild(spanPrev);
		                docFrag.appendChild(spanNext);
		                el.appendChild(docFrag);
		            },
		            addEventListeners: function (el) {
		                var that = this;
		                el.querySelector('.bss-next').addEventListener('click', function () {
		                    that.showCurrent(1); // increment & show
		                }, false);
		            
		                el.querySelector('.bss-prev').addEventListener('click', function () {
		                    that.showCurrent(-1); // decrement & show
		                }, false);
		                
		                el.onkeydown = function (e) {
		                    e = e || window.event;
		                    if (e.keyCode === 37) {
		                        that.showCurrent(-1); // decrement & show
		                    } else if (e.keyCode === 39) {
		                        that.showCurrent(1); // increment & show
		                    }
		                };
		            },
		            autoCycle: function (el, speed, pauseOnHover) {
		                var that = this,
		                    interval = window.setInterval(function () {
		                        that.showCurrent(1); // increment & show
		                    }, speed);
		                
		                if (pauseOnHover) {
		                    el.addEventListener('mouseover', function () {
		                        interval = clearInterval(interval);
		                    }, false);
		                    el.addEventListener('mouseout', function () {
		                        interval = window.setInterval(function () {
		                            that.showCurrent(1); // increment & show
		                        }, speed);
		                    }, false);
		                } // end pauseonhover
		                
		            },
		            addFullScreen: function(el){
		                var that = this,
		                fsControl = document.createElement("span");
		                
		                fsControl.classList.add('bss-fullscreen');
		                el.appendChild(fsControl);
		                el.querySelector('.bss-fullscreen').addEventListener('click', function () {
		                    that.toggleFullScreen(el);
		                }, false);
		            },
		            addSwipe: function(el){
		                var that = this,
		                    ht = new Hammer(el);
		                ht.on('swiperight', function(e) {
		                    that.showCurrent(-1); // decrement & show
		                });
		                ht.on('swipeleft', function(e) {
		                    that.showCurrent(1); // increment & show
		                });
		            },
		            toggleFullScreen: function(el){
		                // https://developer.mozilla.org/en-US/docs/Web/Guide/API/DOM/Using_full_screen_mode
		                if (!document.fullscreenElement &&    // alternative standard method
		                    !document.mozFullScreenElement && !document.webkitFullscreenElement &&   
		                    !document.msFullscreenElement ) {  // current working methods
		                    if (document.documentElement.requestFullscreen) {
		                      el.requestFullscreen();
		                    } else if (document.documentElement.msRequestFullscreen) {
		                      el.msRequestFullscreen();
		                    } else if (document.documentElement.mozRequestFullScreen) {
		                      el.mozRequestFullScreen();
		                    } else if (document.documentElement.webkitRequestFullscreen) {
		                      el.webkitRequestFullscreen(el.ALLOW_KEYBOARD_INPUT);
		                    }
		                } else {
		                    if (document.exitFullscreen) {
		                      document.exitFullscreen();
		                    } else if (document.msExitFullscreen) {
		                      document.msExitFullscreen();
		                    } else if (document.mozCancelFullScreen) {
		                      document.mozCancelFullScreen();
		                    } else if (document.webkitExitFullscreen) {
		                      document.webkitExitFullscreen();
		                    }
		                }
		            } // end toggleFullScreen
		            
		        }; // end Slideshow object 
		        
		    // make instances of Slideshow as needed
		    [].forEach.call($slideshows, function (el) {
		        $slideshow = Object.create(Slideshow);
		        $slideshow.init(el, options);
		    });

		   

		};
		var opts = {
		    auto : {
		        speed : 5000, 
		        pauseOnHover : true
		    },
		    fullScreen : false, 
		    swipe : true
		};
		makeBSS('.property_detail_slide', opts);
	    /* END PROPERTY SLIDE */
	},

	initTestimonialDetail : function(){

		/* PROPERTY SLIDESHOW */
		var makeBSS = function (el, options) {
		var $slideshows = document.querySelectorAll(el), // a collection of all of the slideshow
		$slideshow = {},
		Slideshow = {
		init: function (el, options) {
		this.counter = 0; // to keep track of current slide
		this.el = el; // current slideshow container    
		this.$items = el.querySelectorAll('figure'); // a collection of all of the slides, caching for performance
		this.numItems = this.$items.length; // total number of slides
		options = options || {}; // if options object not passed in, then set to empty object 
		options.auto = options.auto || false; // if options.auto object not passed in, then set to false
		this.opts = {
		auto: (typeof options.auto === "undefined") ? false : options.auto,
		speed: (typeof options.auto.speed === "undefined") ? 1500 : options.auto.speed,
		pauseOnHover: (typeof options.auto.pauseOnHover === "undefined") ? false : options.auto.pauseOnHover,
		fullScreen: (typeof options.fullScreen === "undefined") ? false : options.fullScreen,
		swipe: (typeof options.swipe === "undefined") ? false : options.swipe
		};

		this.$items[0].classList.add('bss-show'); // add show class to first figure 
		this.injectControls(el);
		this.addEventListeners(el);
		if (this.opts.auto) {
		this.autoCycle(this.el, this.opts.speed, this.opts.pauseOnHover);
		}
		if (this.opts.fullScreen) {
		this.addFullScreen(this.el);
		}
		if (this.opts.swipe) {
		this.addSwipe(this.el);
		}
		},
		showCurrent: function (i) {
		// increment or decrement this.counter depending on whether i === 1 or i === -1
		if (i > 0) {
		this.counter = (this.counter + 1 === this.numItems) ? 0 : this.counter + 1;
		} else {
		this.counter = (this.counter - 1 < 0) ? this.numItems - 1 : this.counter - 1;
		}

		// remove .show from whichever element currently has it 
		// http://stackoverflow.com/a/16053538/2006057
		[].forEach.call(this.$items, function (el) {
		el.classList.remove('bss-show');
		});

		// add .show to the one item that's supposed to have it
		this.$items[this.counter].classList.add('bss-show');
		},
		injectControls: function (el) {
		// build and inject prev/next controls
		// first create all the new elements
		var spanPrev = document.createElement("span"),
		spanNext = document.createElement("span"),
		docFrag = document.createDocumentFragment();

		// add classes
		spanPrev.classList.add('bss-prev');
		spanNext.classList.add('bss-next');

		// add contents
		spanPrev.innerHTML = '<i class="fa fa-angle-left"></i>';
		spanNext.innerHTML = '<i class="fa fa-angle-right"></i>';

		// append elements to fragment, then append fragment to DOM
		docFrag.appendChild(spanPrev);
		docFrag.appendChild(spanNext);
		el.appendChild(docFrag);
		},
		addEventListeners: function (el) {
		var that = this;
		el.querySelector('.bss-next').addEventListener('click', function () {
		that.showCurrent(1); // increment & show
		}, false);

		el.querySelector('.bss-prev').addEventListener('click', function () {
		that.showCurrent(-1); // decrement & show
		}, false);

		el.onkeydown = function (e) {
		e = e || window.event;
		if (e.keyCode === 37) {
		that.showCurrent(-1); // decrement & show
		} else if (e.keyCode === 39) {
		that.showCurrent(1); // increment & show
		}
		};
		},
		autoCycle: function (el, speed, pauseOnHover) {
		var that = this,
		interval = window.setInterval(function () {
		that.showCurrent(1); // increment & show
		}, speed);

		if (pauseOnHover) {
		el.addEventListener('mouseover', function () {
		interval = clearInterval(interval);
		}, false);
		el.addEventListener('mouseout', function () {
		interval = window.setInterval(function () {
		that.showCurrent(1); // increment & show
		}, speed);
		}, false);
		} // end pauseonhover

		},
		addFullScreen: function(el){
		var that = this,
		fsControl = document.createElement("span");

		fsControl.classList.add('bss-fullscreen');
		el.appendChild(fsControl);
		el.querySelector('.bss-fullscreen').addEventListener('click', function () {
		that.toggleFullScreen(el);
		}, false);
		},
		addSwipe: function(el){
		var that = this,
		ht = new Hammer(el);
		ht.on('swiperight', function(e) {
		that.showCurrent(-1); // decrement & show
		});
		ht.on('swipeleft', function(e) {
		that.showCurrent(1); // increment & show
		});
		},
		toggleFullScreen: function(el){
		// https://developer.mozilla.org/en-US/docs/Web/Guide/API/DOM/Using_full_screen_mode
		if (!document.fullscreenElement &&    // alternative standard method
		!document.mozFullScreenElement && !document.webkitFullscreenElement &&   
		!document.msFullscreenElement ) {  // current working methods
		if (document.documentElement.requestFullscreen) {
		el.requestFullscreen();
		} else if (document.documentElement.msRequestFullscreen) {
		el.msRequestFullscreen();
		} else if (document.documentElement.mozRequestFullScreen) {
		el.mozRequestFullScreen();
		} else if (document.documentElement.webkitRequestFullscreen) {
		el.webkitRequestFullscreen(el.ALLOW_KEYBOARD_INPUT);
		}
		} else {
		if (document.exitFullscreen) {
		document.exitFullscreen();
		} else if (document.msExitFullscreen) {
		document.msExitFullscreen();
		} else if (document.mozCancelFullScreen) {
		document.mozCancelFullScreen();
		} else if (document.webkitExitFullscreen) {
		document.webkitExitFullscreen();
		}
		}
		} // end toggleFullScreen

		}; // end Slideshow object 

		// make instances of Slideshow as needed
		[].forEach.call($slideshows, function (el) {
		$slideshow = Object.create(Slideshow);
		$slideshow.init(el, options);
		});
		};
		var opts = {
		auto : {
		speed : 5000, 
		pauseOnHover : true
		},
		fullScreen : false, 
		swipe : true
		};
		makeBSS('.testimonial_detail_slide', opts);
		/* END PROPERTY SLIDE */
	},	
			
}/** END **/

//more_filter button
function more_filter(){
	$('#second-filter').fadeIn('slow');	
	$("#box").hide();
	$("#box").addClass('hidden');
	$(".empty-property").addClass('hidden');
	$(".more-filter-holder").hide();
}

function close_more_filter(){
	$('#second-filter').fadeOut('slow');
	$("#box").show();
	$("#box").removeClass('hidden');
	$(".empty-property").removeClass('hidden');
	$(".more-filter-holder").show();
}

//box_enquiry on detail page
function box_enquiry_show() {
	//$('#box-enquiry-form').slideDown('slow');	
}

function box_enquiry_hide() {
	//$('#box-enquiry-form').slideUp('slow');
}

//detail_search
function open_detail_search(){
	$('.box').addClass('hide');
	$('.box-detail').removeClass('hide'); 
}


$('.box-link-detail').on('click',function(){
	idbox = $(this).attr('rel');
	$('#boxdetail'+idbox).removeClass('hide').slideUp();
});

function close_detail_search(){
	$('.box-detail').addClass('hide');
	$('.box').removeClass('hide');
}

//modal_enquiry
function open_modal_enquiry(){
	$('.modal').addClass('active');
	$('.modal').fadeIn('slow');
	$('.modal-dialog').removeClass('hide');
}

function close_modal_enquiry(){
	$('.modal').removeClass('active');
	$('.modal-dialog').addClass('hide');
}

//open converter on property detail
function open_converter(){

	$(document).on('click', '.converter', function(i){
	    i.preventDefault();

	    if($(this).hasClass('converter-active')){
	        $(this).removeClass('converter-active');
	        
	        
	    } else {
	        $(this).addClass('converter-active'); 

	        
	    }
	}); 
	
}

function breadcrumbs_mobile() {
	var w= $(window).width();
	var header_height = $('header').outerHeight();
	if(w < 640){
		console.log(header_height);
		$('#wrapper').css({top : header_height+'px'});
	}
}

function accordion() {
  // Level 1 (Nivel 1)

  var w= $(window).width();

  if(w < 640){

  $('.sub-menu').hide();
  $('.bottom-nav nav > ul > li > a').on('click',function(e){
  	//e.preventDefault();
    if($(this).next().is(':visible')){
      $(this).next().slideUp();
    }
    if($(this).next().is(':hidden')){
      $('.bottom-nav nav > ul > li > a').next().slideUp();
      $(this).next().slideDown();
    }
  });
  
  // Level 2 
  $('.sub-menu-sub').hide();
  $('.nav-sub-menu > li > a').on('click',function(e){
  	//e.preventDefault();
    if($(this).next().is(':visible')){
      $(this).next().slideUp();
    }
    if($(this).next().is(':hidden')){
      $('.nav-sub-menu > li > a').next().slideUp();
      $(this).next().slideDown();
    }
  });

 }
}

function popup() {
	
	$("#box-enquiry > a").click(function(){
		
		if($('.popup').hasClass('is-visible')){
        
         $('.popup').removeClass('is-visible');

         } 
        else {
	
	    $('.popup').addClass('is-visible');
    
        }

    });
}

function close_popup() {
	$("#box-enquiry-form .cancel").click(function(){  
       $('.popup').removeClass('is-visible');
    });
}

$(window).resize(function() {
	breadcrumbs_mobile();
    //accordion();
    $(".property-list .box").each(function(e){ $(this).children('.box-detailz').css("height",$(this).children('.box-item').css("height"));})
    $(".home-page section.locations-search-icon > div.single ").each(function(e){ $(this).css("height",$(this).css("width"));})

});

$(window).load(function() {
	breadcrumbs_mobile();
	$(".property-list .box").each(function(e){ $(this).children('.box-detailz').css("height",$(this).children('.box-item').css("height"));})
    //accordion();
    $(".home-page section.locations-search-icon > div.single ").each(function(e){ $(this).css("height",$(this).css("width"));})
    
});

$(document).ready(function() {
	breadcrumbs_mobile();
	accordion();
	$(".property-list .box").each(function(e){ $(this).children('.box-detailz').css("height",$(this).children('.box-item').css("height"));})
	$(".home-page section.locations-search-icon > div.single ").each(function(e){ $(this).css("height",$(this).css("width"));})
    popup();
    close_popup();
});






