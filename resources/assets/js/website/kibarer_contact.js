var Kibarer = {
	CURRENTPAGE : '',
	init : function() {
		Kibarer.CURRENTPAGE = $('#CURRENTPAGE').val();
		Kibarer.initPage();
		Kibarer.initCommon();
		Kibarer.initForm();
        //Kibarer.initMap();
        Kibarer.initContact();
	},


	initPage : function(page){
		var page = page==undefined ? Kibarer.CURRENTPAGE : page;
		switch(page) {
			case 'home' : Kibarer.initHome(); break;
		}
	},
	
	
	initCommon: function(){
		
		//STICKY HEADER
		$(window).scroll(function() {
		    if ($(this).scrollTop() > 10){  
		        $('header').addClass("sticky");
		    }
		    else{
		        $('header').removeClass("sticky");
		    }
		});
		
		//BURGER RESPONSIVE NAV
		$(document).on('click', '#burger', function(i){
		
		    i.preventDefault();
		
		    if($(this).hasClass('active')){
		        $(this).removeClass('active');
		        $('.fullscreen-nav').removeClass('active');
		        $('body').css({
		            'height' : 'auto',
		            'overflow' : 'auto',
		            'position': 'relative'
		        });
		    }else {
		        $(this).addClass('active');
		        $('.fullscreen-nav').addClass('active');
		        $('body').css({
		            'height' : '100vh',
		            'overflow' : 'hidden',
		            'position': 'fixed'
		        });
		    }
		});		
		
		/* FOOTER ALWAYS BOTTOM
		var kh = $('#kibarer-footer').height() + 50;
		$('#wrapper').css('padding-bottom',kh);
		*/
		
	},
	
	
	initForm :function(){
		$(".register-city").select2();
		  
		//ALL FORM VALIDATION AND SETTING
		$.validator.methods.smartCaptcha = function(value, element, param) {
				return value == param;
		};
		$( "#validation-form" ).validate({
				errorClass: "state-error",
				validClass: "state-success",
				errorElement: "em",
		
				rules: {
					reg_firstname: { 
						required: true 
					},
					reg_lastname: {
						required: true
					},					
					reg_email: {
						required: true,
						email: true
					},
					reg_address:  {
						required: true,
						minlength: 30
					},
					reg_phone: {
						require_from_group: [1, ".phone-group"]
					},											
					reg_password:{
						required: true,
						minlength: 6,
						maxlength: 16						
					},
					reg_repeatPassword:{
						required: true,
						minlength: 6,
						maxlength: 16,						
						equalTo: '#password'
					},
					log_email: {
						required: true,
						email: true
					},
					log_password: {
						required: true
					},
                    sell_firstname: { 
                        required: true 
                    },
                    sell_lastname: {
                        required: true
                    },					
                    sell_email: {
                        required: true,
                        email: true
                    },
                    sell_desc:  {
                        required: true,
                        minlength: 30
                    },
                    sell_phone: {
                        require_from_group: [1, ".phone-group"]
                    },					
				},
					
				messages:{
					reg_firstname: {
						required: 'Enter first name'
					},
					reg_lastname: {
						required: 'Enter last name'
					},					
					reg_email: {
						required: 'Enter email address',
						email: 'Enter a VALID email address'
					},							
					reg_address:  {
						required: 'Fill your address'
					},
					reg_phone: {
						require_from_group: 'Fill at least your phone number'
					},																			
					reg_password:{
						required: 'Please enter a password'
					},
					reg_repeatPassword:{
						required: 'Please repeat the above password',
						equalTo: 'Password mismatch detected'
					},
					log_email: {
						required: 'Your email address'
					},
					log_password: {
						required: 'Please enter your password'
					},
                    sell_firstname: { 
                        required: 'Enter first name'
                    },
                    sell_lastname: {
                        required: 'Enter last name'
                    },					
                    sell_email: {
                        required: 'Enter email address',
                        email: 'Enter a VALID email address'
                    },
                    sell_desc:  {
                        required: 'Fill your description'
                    },
                    sell_phone: {
                        require_from_group: 'Fill at least your phone number'
                    },
				},

				/* @validation highlighting + error placement  
				---------------------------------------------------- */	
				highlight: function(element, errorClass, validClass) {
						$(element).closest('.field').addClass(errorClass).removeClass(validClass);
				},
				unhighlight: function(element, errorClass, validClass) {
						$(element).closest('.field').removeClass(errorClass).addClass(validClass);
				},
				errorPlacement: function(error, element) {
				   if (element.is(":radio") || element.is(":checkbox")) {
							element.closest('.option-group').after(error);
				   } else {
							error.insertAfter(element.parent());
				   }
				}	
		});
	},
	
	
	initHome : function(){
		
		//VIDEO
		var video = document.getElementById("video");
		document.getElementById('video').play();
		var iOS = /iPhone|iPod/.test(navigator.userAgent) && !window.MSStream,
		    ipad = /iPad/.test(navigator.userAgent) && !window.MSStream,
		    bodyH, bodyOv;
		document.addEventListener("orientationchange", updateOrientation);
		if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
		    $(video).remove();
		}
		
		function updateOrientation() {
		    if(window.innerWidth > window.innerHeight ){
		        $("#warning").addClass('active');
		        $('body').css({
		            'height' : '100vh',
		            'overflow' : 'hidden',
		            'position': 'fixed'
		        });
		    }else {
		        $("#warning").removeClass('active');
		        $('body').css({
		            'height' : bodyH,
		            'overflow' : bodyOv,
		            'position': 'relative'
		        });
		    }
		}
		
		// Hover effect for touch devices
		$('body').bind('touchstart', function() {});
		
		// ios media querie
		if( iOS ) {
		    $("body").addClass('ios-device'); 
		}
		if( ipad ) {
		    $("body").addClass('ipad'); 
		}
		
		// button pause and play video
		$(".pause-btn").click(function(i){
		
		    i.preventDefault();
		
		    if($(this).hasClass('play')){
		        video.pause();
		    } else {
		        video.play();
		    }
		
		    $(this).toggleClass("play");
		});
		
		
		$(document).ready(function() {
		    hideVideo();
		});
		
		$(window).resize(function() {
		    hideVideo(); 
		    
		    $('body').css({
		        'height': 'auto',
		        'overflow': 'auto',
		        'position': 'relative'
		    });
		    
		    $('#burger').removeClass('active');
		    $('.fullscreen-nav').removeClass('active');
		});
		
		function hideVideo() {
		    var windowWidth = $("#video-container").outerWidth(),
		        windowHeight = $("#video-container").outerHeight(),
		        videoWidth = $('#video').outerWidth(),
		        videoHeight = $('#video').outerHeight(),
		        videoRatio = videoWidth / videoHeight,
		        windowRatio = windowWidth / (windowHeight);
		
		    if($(window).outerWidth() > 900) {
		
		        $("#video-container").css({'display': 'block'});
		        if(windowRatio < videoRatio) {
		            //$('video').css({'height': '100%', 'width': 'auto'});
		
		        } else {
		            //$('video').css({'height': 'auto', 'width': '100%'});
		        }
		    } else {
		        $("#video-container").css({'display': 'none'});
		    }
		}
		
		// jumbotron villa land section animation
		$(document).on('click', '.category-btn', function(i){
		    i.preventDefault();
		    var target = $(this).attr('data-target');
		    $('.wrapper').removeClass('active');
		    if($(this).hasClass('active')){
		        $(this).removeClass('active');    
		    }else {
		        $('.category-btn').removeClass('active');
		        $(target).addClass('active');
		        $(this).addClass('active');
		        $('#jumbotron > .wrapper').addClass('active');
		    }
		});
	},
    
    initMap : function() {

        var markersToRemove = [];
        var myLatLng = { lat: -8.4420734, lng: 114.9356164 };
        var pin = 'assets/img/marker.png';

        var map = new google.maps.Map(document.getElementById('map'), {
            mapTypeControl: false,
            center: myLatLng,
            scrollwheel: true,
            zoom: 10,
            icon: pin,
            maxZoom: 15,
            minZoom: 8,
            draggable: true,
            zoomControl: false,
            disableDoubleClickZoom: true,
            streetViewControl: false
        });

        var input = document.getElementById('pac-input'),
            searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        map.addListener('bounds_changed', function() {
            searchBox.setBounds(map.getBounds());
        });

        searchBox.addListener('places_changed', function() {

            var places = searchBox.getPlaces();
            if (places.length == 0) {
                return;
            }

            var bounds = new google.maps.LatLngBounds();
            places.forEach(function(place) {
                if (place.geometry.viewport) {
                    // Only geocodes have viewport.
                    bounds.union(place.geometry.viewport);
                } else {
                    bounds.extend(place.geometry.location);
                }


            });
            map.fitBounds(bounds);
        });

        google.maps.event.addListener(map, "click", function (e) {
            removeMarkers();
            var latLng = e.latLng,
                strlatlng = latLng.toString(),
                spllatlng = strlatlng.split(','),
                lats = spllatlng[0].replace("(", ""), 
                longs = spllatlng[1].replace(")", "");

            $("#map_latitude").val(lats);
            $("#map_longitude").val(longs);
            placeMarker(latLng, map);

        });

        function placeMarker(location, map) {
            var marker = new google.maps.Marker({
                position: location, 
                icon: pin,
                map: map
            });

            markersToRemove.push(marker);
        }

        function removeMarkers() {
            for(var i = 0; i < markersToRemove.length; i++) {
                markersToRemove[i].setMap(null);
            }
        }
    },
	
    initContact : function() {

        var myLatLng = new google.maps.LatLng(-8.6714246,115.1607031);
        var pin = 'assets/img/marker.png';
        
        var marker=new google.maps.Marker({
            position:myLatLng,
            animation: google.maps.Animation.DROP,
            icon : pin,
            title : 'Kibarer Property'
        });
        
        var contentString = '<div id="content-form" style="10px 0px 30px 0px;">'+
            '<img src="assets/img/canggu.jpg" style="width:100px; height:100px; float:left; margin: 0px 10px 0px 0px;">'+
            '<h1 id="title" style="font-size: 1em;color: #ee5b2c;font-weight: 600; text-transform: uppercase; margin-bottom:10px;">Kibarer Property</h1>'+
            '<div id="bodyContent">'+
            '<p> Jalan Petitenget No.9, Badung, Bali 80361 <br> Ph. (+62361) 4741212 </p>' +
            '</div>'+
            '</div>';
        
        var infowindow = new google.maps.InfoWindow({
            content: contentString,
            maxWidth: 300
        });
        
        var map = new google.maps.Map(document.getElementById('map-contact'), {
            mapTypeControl: false,
            center: myLatLng,
            scrollwheel: false,
            zoom: 10,
            icon: pin,
            zoom:15,
            maxZoom: 15,
            minZoom: 8,
            draggable: true,
            zoomControl: false,
            disableDoubleClickZoom: false,
            streetViewControl: false
        });
        
        marker.setMap(map);
        infowindow.open(map, marker);
        marker.addListener('click', function() {
            infowindow.open(map, marker);

        });


    },
			
}/** END **/