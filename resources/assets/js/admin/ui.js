var UI = {

    previewImage: function(input) {

        var countFiles = $(input)[0].files.length;

        var imgPath = $(input)[0].value;
        var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
        var image_holder = $("#gallery-wrapper");
        // image_holder.empty();
        var id = $(input).attr('data-id');

        if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {

            if (typeof (FileReader) != "undefined") {

                for (var i = 0; i < countFiles; i++) {

                    var reader = new FileReader();

                    reader.onload = function (e) {

                        var star = 'star_border';

                        var el = '<m-gallery-item style="background-image: url(\''+ e.target.result +'\')" data-id="23">'
                            + '<m-gallery-item-menu>'
                                + '<m-button class="make-thumbnail" data-function="makeThumbnail">'
                                    + '<i class="material-icons">'+ star +'</i>'
                                + '</m-button>'
                                + '<m-button delete data-id="0" class="delete-gallery-item" data-input="'+ id +'">'
                                    + '<i class="material-icons">close</i>'
                                + '</m-button>'
                            + '</m-gallery-item-menu>'
                        + '</m-gallery-item>';

                        image_holder.append(el);

                    }

                    reader.readAsDataURL($(input)[0].files[i]);
                }

            } else {

                alert("This browser does not support FileReader.");
            }

        } else {

            alert("Please select only images");
        }

        return true;

    }
}
