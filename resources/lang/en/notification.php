<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
   'email_posted' => 'Please check your email. The link has been sent to your email.',

   'contact_description' => 'Kibarer Property is THE ONLY realtor in Bali that also provides Lawyers and Notary under the same roof.',

   'check_email' => 'Thank You! Please check your email to activate your account.',

   'error_reset_password' => 'Oops, something wrong!',

   'confirm_success' => 'Thank You! Your email has been confirmed successfully.',
];
