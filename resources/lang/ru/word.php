<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
   
    // top nav
    'sell_property' => 'sell my property',
    'blog' => 'blog',
    'account' => 'account',
    'language' => 'language',
    'currency' => 'currency',

    // navbar
    'villa' => 'villas',
    'land' => 'lands',
    'lawyer_notary' => 'lawyer & notary',
    'testimonial' => 'testimonial',
    'contact' => 'contact',

    // search
    'search_tagline' => 'in',

    

    //search icon
    'villa_sell' => 'villa sell',
    'villa_rental' => 'villa rental',
    'short_term' => 'short term',    
    'yearly_rental' => 'yearly rental',    
    'beachfront_property' => 'Beachfront Properties',    
    'less_than_500k' => 'Investment under $500.000', 
    'more_than_500k' => 'Investment over $500.000',
    'home_and_retirement' => 'Home &amp; Retirement',
    
    // contact
    'email' => 'email',
    'phone' => 'phone',
    'sending' => 'sending...',

    // customer dashboard
    'overview' => 'overview',
    'wishlist' => 'wishlists',
    'setting' => 'settings',
    'save' => 'save',


    'search' => 'search',

    'related_items' => 'related items',
    'similar_items' => 'similar items',
    'product_description' => 'product description',
    'my_orders' => 'my orders',
    'my_personal_info' => 'my personal info',
    'faqs' => 'FAQs',
    'privacy_policy' => 'privacy policy',
    'condition_of_sales' => 'condition of sales',
    'terms_and_conditions' => 'terms and conditions',
    'home' => 'home',
    'products' => 'products',
    'projects' => 'projects',
    'materials' => 'materials',
    'showroom' => 'showroom',
    'contact' => 'contact',
    'my_account' => 'my account',
    'my_orders' => 'my orders',
    'my_personal_info' => 'my personal info',
    'privacy_policy' => 'privacy policy',
    'condition_of_sales' => 'condition of sales',
    'terms_and_conditions' => 'terms and conditions',
    'bestseller' => 'bestseller',
    'projects' => 'projects',
    'materials' => 'materials',
    'showroom' => 'showroom',
    'contact' => 'contact',
    'contact_us' => 'contact_us',
    'about' => 'about',
    'about_us' => 'about us',
    'warranty' => 'warranty',
    'login' => 'login',
    'logout' => 'logout',
    'register' => 'register',
    'create_an_account' => 'create an account',
    'my_orders' => 'my orders',
    'my_personal_info' => 'my personal info',
    'faqs' => 'faqs',
    'privacy_policy' => 'privacy policy',
    'condition_of_sales' => 'condition of sales',
    'terms_and_conditions' => 'terms and conditions',
    'please_select_a_country' => 'please select a country',
    'submit' => 'submit',
    'view' => 'view',
    'name' => 'name',
    'logout' => 'logout',
    'login' => 'login',
    'create_an_account' => 'create an account',
    'about' => 'about',
    'products' => 'products',
    'projects' => 'projects',
    'materials' => 'materials',
    'showroom' => 'showroom',
    'contact' => 'contact',
    'contact_us' => 'contact us',
    'name' => 'name',
    'address' => 'address',
    'city' => 'city',
    'state_province' => 'state province',
    'postal' => 'postal',
    'please_select_a_project' => 'please select a project',
    'you_are_a' => 'you are a',
    'submit' => 'submit',
    'please_select_a_country' => 'please select a country',
    'this_information_is_required' => 'this information is required',
    'continue_shopping' => 'continue shopping',
    'checkout' => 'checkout',
    'username' => 'username',
    'password' => 'password',
    'dont_have_account' => 'don\'t have account',
    'create_now' => 'create now',
    'catalogues' => 'catalogues',
    'read_more' => 'read more',
    'load_more' => 'load more',
    'confirmation' => 'confirmation',
    'forgot_password' => 'Forgot your password',
    'send_reset_link' => 'Send reset link',
    'please_wait' => 'Please wait...',
    'reset_password' => 'Reset password',
    'error_input_estimate' => 'Incorrect input format',
    'available' => 'Available',
    'sold' => 'Sold Out',
];
