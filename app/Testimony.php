<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Testimony extends Model
{
    //
    use SluggableTrait;
    
    protected $table = 'testimonials';

    protected $sluggable = [
        'build_from' => 'title',
        'save_to' => 'slug',
    ];

    public function attachments()
    {
        return $this->morphMany('App\Attachment', 'attachable')->orderBy('order', 'asc');
    }
    
    public function user() {

        return $this->belongsTo('App\User');
    }
    
    // public function property() {

    //     return $this->belongsTo('App\Property');
    // }

    public static function boot()
    {
        parent::boot();

        static::creating(function($testimony)
        {

        });

        static::updating(function($testimony)
        {

        });

        static::deleting(function($testimony)
        {

        });

        static::created(function($testimony)
        {

        });

        static::updated(function($testimony)
        {

        });

        static::deleted(function($testimony)
        {

        });

    }
}
