<?php 

namespace App\Services;


class GetBaseCRM{


	// API token from Alban Kibarer account
	protected $access_token = '61dd3b849d89f9614ae07b7b2e835e6a02efd905fdbb5a666f5df3d17d1c15a5';


	public function customer_add($data)
	{
		if(is_array($data) && count($data) > 0)
		{
			try
			{
				$client = new \BaseCRM\Client(['accessToken' => $this->access_token]);
				//$contact = $client->contacts->all(['email' => 'abdulrajk_b@yahoo.ca']);
				//return json_encode($contact);

				$custom_field = new \stdClass();
				if(array_key_exists('lang',$data)) $custom_field->Language = [$this->get_language($data['lang'])];
				else $custom_field->Language = [$this->get_language()];
				$custom_field->Month = date('F');
				$custom_field->Year = date('Y');
				$custom_field->Source = 'Website';

				if(array_key_exists('ip',$data)) $new_contact['address']['country'] = $this->get_country($data['ip']);
				else $new_contact['address']['country'] = $new_contact['address']['country'] = $this->get_country();
				 
				$new_contact['first_name'] = ucwords($data['firstname']);
				if(array_key_exists('lastname',$data)) $new_contact['last_name'] = ucwords($data['lastname']);
				$new_contact['email'] = $data['email'];
				if(array_key_exists('phone',$data)) $new_contact['phone'] = $data['phone'];
				if(array_key_exists('mobile',$data)) $new_contact['mobile'] = $data['mobile'];
				if(array_key_exists('tags',$data)) $new_contact['tags'] = $data['tags'];
				$new_contact['custom_fields'] = $custom_field;
				$new_contact['description'] = 'Kesato Agency - Kibarer website development testing';

				$contact = $client->contacts->all(['email' => $new_contact['email']]);
				
				if(count($contact) == 0) 
				{
					// Add new contact
					$nc = $client->contacts->create($new_contact);
					if(count($nc) > 0)
					{
						$contact_name = array_key_exists('lastname',$data) ? $new_contact['first_name'].' '.$new_contact['last_name'] : $new_contact['first_name'];
						$response['status'] = '1';
						$response['message'] = $contact_name.' Contact has been added to CRM.';	
					}
					else
					{
						$response['status'] = '-1';
						$response['message'] = 'Unable to add this contact to CRM. Please try again!';
					}
				}
				else
				{
					$response['status'] = '0';
					$response['message'] = 'This contact already exist on CRM.';
				}
			}
			catch (\BaseCRM\Errors\ConfigurationError $e)
			{
				$response['message'] = 'Invalid client configuration option';
			}
			catch (\BaseCRM\Errors\ResourceError $e)
			{
				$message  = 'Http status = ' . $e->getHttpStatusCode() . "\n";
				$message .= 'Request ID = ' . $e->getRequestId() . "\n";
				foreach ($e->errors as $error)
			  	{
				    $message .= 'field = ' . $error['field'] . "\n";
				    $message .= 'code = ' . $error['code'] . "\n";
				    $message .= 'message = ' . $error['message'] . "\n";
				    $message .= 'details = ' . $error['details'] . "\n";
			  	}
			}
			catch (\BaseCRM\Errors\RequestError $e)
			{
				$response['message'] = 'Invalid query parameters, authentication error etc.';
			}
			catch (\BaseCRM\Errors\Connectionerror $e)
			{
			  $message  = 'Errno = ' . $e->getErrno() . "\n";
			  $message .= 'Error message = ' . $e->getErrorMessage() . "\n";
			  $response['message'] = $message;
			}
			catch (Exception $e)
			{
				$response['message'] = 'Other kind of exception';
			}
			
		}

		return json_encode($response);
	}


	public function deal_add($data)
	{
		if(is_array($data) && count($data) > 0)
		{
			$res = json_decode($this->customer_add($data), true);
			try
			{
				if($res['status'] != '-1')
				{
					$client = new \BaseCRM\Client(['accessToken' => $this->access_token]);
					$contact = $client->contacts->all(['email' => $data['email']]);

					$deal_value = preg_replace("/,/i", "", $data['value']);
					$new_deal['name'] = $contact[0]['data']['first_name'].' '.$contact[0]['data']['last_name'];
					$new_deal['value'] = intval($deal_value);
					$new_deal['currency'] = strtoupper($data['currency']);
					$new_deal['source_id'] = 2439454; //Website
					$new_deal['contact_id'] = $contact[0]['data']['id'];
					$nd = $client->deals->create($new_deal);

					if(count($nd) > 0)
					{
						$response['status'] = '1';
						$response['message'] = 'New Deal from '.$new_deal['name'].' has been added to CRM.';	
					}
					else
					{
						$response['status'] = '-1';
						$response['message'] = 'Unable create a new deal for '.$new_deal['name'].' on CRM. Please try again!';	
					}
				}
			}
			catch (\BaseCRM\Errors\ConfigurationError $e)
			{
				$response['message'] = 'Invalid client configuration option';
			}
			catch (\BaseCRM\Errors\ResourceError $e)
			{
				$message  = 'Http status = ' . $e->getHttpStatusCode() . "\n";
				$message .= 'Request ID = ' . $e->getRequestId() . "\n";
				foreach ($e->errors as $error)
			  	{
				    $message .= 'field = ' . $error['field'] . "\n";
				    $message .= 'code = ' . $error['code'] . "\n";
				    $message .= 'message = ' . $error['message'] . "\n";
				    $message .= 'details = ' . $error['details'] . "\n";
			  	}
			}
			catch (\BaseCRM\Errors\RequestError $e)
			{
				$response['message'] = 'Invalid query parameters, authentication error etc.';
			}
			catch (\BaseCRM\Errors\Connectionerror $e)
			{
			  $message  = 'Errno = ' . $e->getErrno() . "\n";
			  $message .= 'Error message = ' . $e->getErrorMessage() . "\n";
			  $response['message'] = $message;
			}
			catch (Exception $e)
			{
				$response['message'] = 'Other kind of exception';
			}
		}
	}


	private function get_country($ip = '')
	{
		$geo = json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip={$ip}"));
        if($geo->geoplugin_countryName == NULL) return 'Indonesia';
        return $geo->geoplugin_countryName;
	}


	private function get_language($lang_code = '')
	{
		$language = array(
						'en' => 1,
						'fr' => 2,
						'id' => 3,
						'ru' => 4,
						'es' => 5,
						'it' => 6
					);
		if(array_key_exists($lang_code,$language)) return $language[$lang_code];
		return 1; 
	}



	public function customer_add_old($data=array())
	{
		$token = $this->access_token;
		$response['status'] = '0';

		try
		{
			$new_contact['owner'] = 'Ubud Agency';
			$new_contact['title'] = 'Manager';
			$new_contact['first_name'] = $data['data_first_name'];
			$new_contact['last_name'] = $data['data_last_name'];
			$new_contact['email'] = $data['data_email'];
			$new_contact['phone'] = '0361-732873';
			$new_contact['tags'] = ['Investment More 500K','seller','Villa','Yearly'];
			$new_contact['custom_fields'] = ['Language' => ['Indonesia']];
			$new_contact['description'] = 'Kesato Agency - Kibarer website development testing';

			$new_deal['name'] = $data['data_first_name'].' '.$data['data_last_name'];
			$new_deal['value'] = intval($data['data_value']);
			$new_deal['currency'] = $data['data_currency'];
			//$new_deal['source_id'] = 2439454; // Deals source : Kibarer Website
			//$new_deal['source_id'] = 2795876; // Deals source : Kibarer English Website
			//$new_deal['source_id'] = 2643993; // Deals source : Kibarer French Website


			require 'vendor/autoload.php';
			
			$client = new \BaseCRM\Client(['accessToken' => $token]);
			$contact = $client->contacts->all(['email' => $data['data_email']]);
			//$contact = $client->contacts->get(117339668);
			//$response['message'] = json_encode($contact);
			
			if(count($contact) == 0) 
			{
				// Add new contact
				$nc = $client->contacts->create($new_contact);

				if(count($nc) > 0)
				{
					// If new contact has been created, create new deal for this contact also
					$new_deal['contact_id'] = $nc['id'];
					$nd = $client->deals->create($new_deal);

					if(count($nd) > 0)
					{
						$response['status'] = '1';
						$response['message'] = $data['data_first_name'].' '.$data['data_last_name'].' Contact and Deal has been added to CRM.';	
					}
					else
					{
						$response['status'] = '0';
						$response['message'] = $data['data_first_name'].' '.$data['data_last_name'].' Contact has been added to CRM., but system unable to create a new Deal. Please try again!';	
					}
				}
				else
				{
					$response['status'] = '0';
					$response['message'] = 'Unable to add this contact to CRM. Please try again!';
				}
			}
			else
			{
				// If contact already exist, just create new deal for this contact
				$new_deal['contact_id'] = $contact[0]['data']['id'];
				$nd = $client->deals->create($new_deal);

				if(count($nd) > 0)
				{
					$response['status'] = '1';
					$response['message'] = 'New Deal from '.$data['data_first_name'].' '.$data['data_last_name'].' has been added to CRM.';	
				}
				else
				{
					$response['status'] = '0';
					$response['message'] = 'Unable create a new deal for '.$data['data_first_name'].' '.$data['data_last_name'].' on CRM. Please try again!';	
				}
			}
			
		}
		catch (\BaseCRM\Errors\ConfigurationError $e)
		{
			$response['message'] = 'Invalid client configuration option';
		}
		catch (\BaseCRM\Errors\ResourceError $e)
		{
			$message  = 'Http status = ' . $e->getHttpStatusCode() . "\n";
			$message .= 'Request ID = ' . $e->getRequestId() . "\n";
			foreach ($e->errors as $error)
		  	{
			    $message .= 'field = ' . $error['field'] . "\n";
			    $message .= 'code = ' . $error['code'] . "\n";
			    $message .= 'message = ' . $error['message'] . "\n";
			    $message .= 'details = ' . $error['details'] . "\n";
		  	}
		}
		catch (\BaseCRM\Errors\RequestError $e)
		{
			$response['message'] = 'Invalid query parameters, authentication error etc.';
		}
		catch (\BaseCRM\Errors\Connectionerror $e)
		{
		  $message  = 'Errno = ' . $e->getErrno() . "\n";
		  $message .= 'Error message = ' . $e->getErrorMessage() . "\n";
		  $response['message'] = $message;
		}
		catch (Exception $e)
		{
			$response['message'] = 'Other kind of exception';
		}

		echo json_encode($response);

	}

}