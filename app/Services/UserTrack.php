<?php 

namespace App\Services;


class UserTrack{


	function __construct()
	{
		if(! \Session::has('track'))
        {
        	$track = array(
        		'1' => '',
        		'2' => '',
        		'3' => '',
        		'4' => '',
        		'5' => '',
        		'6' => '',
        		'7' => ''
        	);
            \Session::set('track',$track);
            \Session::save();
        }
	}


	public function save($page = '', $category = '', $type = '', $area = '', $att = array())
	{
		$t1 = $t2 = $t3 = $t4 = $t5 = $t6 = $t7 = $t8 = '';
		$t1 = (preg_match("/villa/i", $category)) ? 'Villa' : 'Land';
		$t2 = ($page == 'search') ? 'buyer' : 'Seller';
		if($type == 'less-than-500k') $t3 = 'Investment Less 500K';
		else if($type == 'more-than-500k') $t3 = 'Investment More 500K';
		else if($type == 'beachfront-property') $t3 = 'Beachfront';
		else $t3 = 'retirement';
		if(preg_match("/rent/i", $category))
		{
			$t2 = 'rental';
		}
		if($t2 == 'rental')
		{
			$t3 = $t6 = $t7 = '';
			$t8 = ($type == 'short-term') ? 'Listing monthly' : 'Listing yearly';
		}
		else if($t2 == 'buyer' OR $t2 == 'Seller')
		{
			$t5 = $t8 = '';
			if(is_array($att) && count($att) > 0)
			{
				if(array_key_exists('lease_hold',$att)) $t6 = $att['lease_hold'];
				if(array_key_exists('free_hold',$att)) $t7 = $att['free_hold'];
			}
		}
		//if($t2 == 'rental' && is_array($att) && count($att) > 0 && array_key_exists('term',$att)) $t4 = $att['term'];
		//if(is_array($att) && count($att) > 0 && array_key_exists('source',$att)) $t4 = $att['source'];

		$track = array(
        		'1' => $t1,
        		'2' => $t2,
        		'3' => $t3,
        		'4' => $t4,
        		'5' => $t5,
        		'6' => $t6,
        		'7' => $t7,
        		'8' => $t8
        	);
		\Session::set('track',$track);
        \Session::save();
	}


	public function get()
	{
		$tl = array();
		$track = \Session::get('track');
		foreach($track as $t)
		{
			if($t != '') $tl[] = $t;
		}
		return $tl;
	}


	public function update($l = '1', $val = '')
	{
		$track = \Session::get('track');
		$track[$l] = $val;
		\Session::set('track',$track);
        \Session::save();
	}


	public function track($param = '', $val = '')
	{
		$track = \Session::get('track');
		
		if($param == 'source')
		{
			$track['5'] = $val;
			\Session::set('track',$track);
        	\Session::save();
		}
	}


	public function untrack($param = '')
	{
		$track = \Session::get('track');
		
		if($param == 'source')
		{
			$track['4'] = '';
			\Session::set('track',$track);
        	\Session::save();
		}
	}


}