<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //

    public function posts()
    {
        return $this->morphedByMany('App\Post', 'catable');
    }

    public function properties()
    {
        return $this->morphedByMany('App\Property', 'catable');
    }

    public function parent()
    {
        return $this->belongTo('App\Category', 'parent_id');
    }

    public function childs()
    {
        return $this->hasMany('App\Category', 'parent_id');
    }

    public function scopeWithChild($query, $category)
    {
        foreach ($category as $key => $value) {

            // filter category
            $query->orWhere('categories.id', $value->id);

            // check child category
            if ($value->childs) {

                $this->scopeWithChild($query, $value->childs);
            }
        }

        return $query;

    }

}
