<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class BingTranslateController extends Controller
{
    public function __construct()
    {

        $this->secret = env('BING_KEY');

    }

    public function trans($to, Request $request)
    {
        $allLang = [$to];
        if ($to == 'all'){
            $allLang = ['fr','ru','id'];
        }

        foreach ($allLang as $lang){
            $url = 'https://api.datamarket.azure.com/Bing/MicrosoftTranslator/v1/Translate?Text=%27'.urlencode($request->title['en'].'-|-|-'.$request->content['en']).'%27&To=%27'.$lang.'%27&From=%27en%27';


            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Basic  '  .base64_encode($this->secret . ":" . $this->secret) ));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            $rsp = curl_exec($ch);

            $explode = explode('m:type="Edm.String">',$rsp);
            $explode = explode("</d:Text>",$explode[1]);


            $explode = explode('-|-|-',$explode[0]);

            $title = $explode[0];
            $content = $explode[1];

            $data[] = [
                'title' => $title,
                'content' => $content,
                'translated' => $lang,
            ];
        }

        return $data;




    }
}
