<?php

namespace App\Http\Controllers;

use App\PostLocale;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Post;

use DB;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $category = $request->category;
        $status = $request->status;

        // datatable parameter
        $draw = $request->draw;
        $start = $request->start;
        $length = $request->length;
        $search = $request->search['value'];

        // sorting
        $column = 'id';
        $sort = $request->order[0]['dir'] ? $request->order[0]['dir'] : 'desc'; //asc

        // new object
        $posts = new Post;

        $posts = $posts->select('posts.*')
            ->join('post_locales', 'post_locales.post_id', '=', 'posts.id')
            ->where('post_locales.locale', 'en');

        $posts = $posts->with(['postLocales' => function ($q) {

            $q->where('locale', 'en');
        }])->with('categories');

        // searching
        if ($search) {

            $posts = $posts->where(function ($q) use ($search) {

                $q->where('post_locales.title', 'like', $search . '%');
            });
        }

        $posts = $posts->groupBy('posts.id');

        // total records
        $count = $posts->count();

        // pagination
        $posts = $posts->take($length)->skip($start);

        // order
        if ($request->order[0]['column']) {

            $column = $request->columns[$request->order[0]['column']]['data'];

            if ($column == 'post_locales.0.title') {

                $posts = $posts->orderBy('post_locales.title', $sort);
            } else if ($column == 'categories.0.name') {

                $posts = $posts->join('catables', 'catables.catable_id', '=', 'posts.id')
                    ->join('categories', 'categories.id', '=', 'catables.category_id')
                    ->where('catables.catable_type', 'App\Post')
                    ->orderBy('categories.name', $sort);
                    
            } else {

                $posts = $posts->orderBy('posts.' . $column, $sort);
            }

        } else {

            $posts = $posts->orderBy('posts.' . $column, $sort);
        }

        // get data
        $posts = $posts->get();

        // datatable response
        $respose = [
                "draw" => $draw,
                "recordsTotal" => $count,
                "recordsFiltered" => $count,
                "data" => $posts

            ];

        return $respose;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $validator = \Validator::make($request->all(), [
            'title' => 'required'
        ]);


        if ($validator->fails()) {
            return response()->json(array('status' => 500, 'monolog' => array('title' => 'errors', 'message' => implode($validator->errors()->all(), '<br>') )));
        }

        DB::beginTransaction();

        $post = new Post;

        $post->user_id = \Auth::user()->get()->id;
        $post->status = $request->status;

        $post->save();

        // locale
        foreach ($request->title as $key => $value) {
            if (!empty($value)){
                $locale = new PostLocale();

                $locale->post_id = $post->id;
                $locale->title = $request->title[$key];
                $locale->content = $request->content[$key];
                $locale->meta_keyword = $request->meta_keyword[$key];
                $locale->meta_description = $request->meta_description[$key];
                $locale->locale = $key;

                $locale->save();
            }
        }

        // category
        if ($request->category_id) {

            $post->categories()->sync([$request->category_id]);
        }

        DB::commit();

        return response()->json(array('status' => 200, 'monolog' => array('title' => 'success', 'message' => 'object has been saved')));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validator = \Validator::make($request->all(), [
            'title' => 'required'
        ]);


        if ($validator->fails()) {
            return response()->json(array('status' => 500, 'monolog' => array('title' => 'errors', 'message' => implode($validator->errors()->all(), '<br>') )));
        }

        DB::beginTransaction();

        $post = Post::find($id);

        $post->user_id = \Auth::user()->get()->id;
        $post->status = $request->status;

        $post->save();

        // delete first
        $post->postLocales()->delete();

        // locale
        foreach ($request->title as $key => $value) {

            if (!empty($value)) {
                $locale = new \App\postLocale;

                $locale->post_id = $post->id;
                $locale->title = $request->title[$key];
                $locale->content = $request->content[$key];
                $locale->meta_keyword = $request->meta_keyword[$key];
                $locale->meta_description = $request->meta_description[$key];
                $locale->locale = $key;

                $locale->save();

            }

        }

        // category
        if ($request->category_id) {

            $post->categories()->sync([$request->category_id]);
        }

        // $postTerm->save();

        DB::commit();

        return response()->json(array('status' => 200, 'monolog' => array('title' => 'success', 'message' => 'object has been updated')));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $post = Post::find($id);

        $post->delete();

        return response()->json(array('status' => 200, 'monolog' => array('title' => 'delete success', 'message' => 'object has been deleted'), 'id' => $id));
    }

    public function listing(Request $request, $term = null)
    {
        $limit = 3;

        $posts = new Post;

        if ($term != null) {

            $segments = explode('/', $term);

            $slug = end($segments);

            $posts = $posts->join('slug', $slug);

        }

        if ($request->category) {

            $posts = $posts->whereHas('categories', function($q) use ($request) {
                $q->where('slug', $request->category);
            });
        }

        $posts = $posts->where('status', 1);

        $posts = $posts->orderBy('posts.created_at', 'desc');

        $posts = $posts->paginate($limit);

        return view('pages.blog-listing', compact('posts'));
    }

    public function detail(Request $request, $post, $term = null)
    {

        if ($term == null) return $this->listing($request);

        $segments = explode('/', $term);

        $slug = end($segments);

        $post = new Post;

        $post = $post->whereHas('postLocales', function ($q) use ($slug) {
            $q->where('slug', $slug);
        });

        if ($post->count() == 0) return abort('404');


        $post = $post->where('status', 1);

        $post = $post->first();

        // return $post;

        return view('pages.blog-view', compact('post'));
    }

    public function export(Request $request)
    {
        $ids = explode(',', $request->id);

        $admin = \Auth::user()->get();

        $file_name = $admin->id . '-blog';

        $take = ($request->take) ? $request->take : 'all';

        $posts = new Post;

        if ($request->take == 'selected') {

            $posts = $posts->whereIn('posts.id', $ids);

            // $posts = $posts->take($take);
        }

        $posts = $posts->orderBy('id', 'desc');

        $posts = $posts->get();

        foreach ($posts as $key => $value) {            

            $value->title = $value->lang('en')->title;

            $value->description = $value->lang('en')->content;
        }

        $data = json_decode(json_encode($posts), true);

        \Excel::create($file_name, function($excel) use ($data) {

            $excel->sheet('Sheet1', function($sheet) use ($data) {

                $sheet->setOrientation('landscape');

                $sheet->fromArray($data);

            });

        })
        // ->export('csv');
        ->store('csv', 'export/csv');

        return $file_name . '.csv';
    }

    public function remove(Request $request)
    {
        //

        foreach ($request->id as $id) {
            
            $post = Post::find($id);

            $post->delete();
        }

        return response()->json(array('status' => 200, 'monolog' => array('title' => 'delete success', 'message' => 'object has been deleted'), 'id' => $request->id));
    }

}
