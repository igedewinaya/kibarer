<?php

namespace App\Http\Controllers;

use App\PropertyLocale;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Property;

use App\Services\GetBaseCRM;
use App\Services\UserTrack;

use Mail;
use DB;

class PropertyController extends Controller
{


    protected $user_track;


    function __construct()
    {
        $this->user_track = new UserTrack();
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $category = $request->category;
        $status = $request->status;

        // datatable parameter
        $draw = $request->draw;
        $start = $request->start;
        $length = $request->length;
        $search = $request->search['value'];


        // sorting
        $column = 'updated_at';
        $sort = $request->order[0]['dir'] ? $request->order[0]['dir'] : 'desc'; //asc

        // new object
        $properties = new Property;

        $properties = $properties->select('properties.*')
            ->leftJoin(\DB::raw('(SELECT * FROM property_locales WHERE locale = \'en\') as property_locales'), 'property_locales.property_id', '=', 'properties.id')
            // ->where('property_locales.locale', 'en')
            ->leftJoin('users', 'users.id', '=', 'properties.user_id')
            ->leftJoin('branches', 'branches.id', '=', 'users.branch_id');

        // with user
        $properties = $properties->with('user.branch');

        // with locale
        $properties = $properties->with(['propertyLocales' => function ($q) {
            $q->where('locale', 'en');
        }]);

        // with image
        $properties = $properties->with(['attachments' => function ($q) {
            $q->where('type', 'img');
        }]);

        $properties = $properties->with('thumb');

        if ($request->branch != '') {

            $properties = $properties->where('branches.id', $request->branch);
        }

        if ($request->type != '') {

            $properties = $properties->where('properties.type', $request->type);
        }

        if ($request->location != '') {

            $properties = $properties->where('properties.city', 'like', '%' . $request->location . '%');
        }

        if ($request->code != '') {

            $properties = $properties->where('properties.code', 'like', '%' . $request->code . '%');
        }

        if ($request->title != '') {

            $properties = $properties->where('property_locales.title', 'like', $request->title . '%');
        }

        // searching
        // if ($search) {

        //     $properties = $properties->where('property_locales.title', 'like', $search . '%');
        // }

        // $properties = $properties->orderBy('properties.' . $column, $sort);

        // villa or land, ..
        $category = \App\Category::where('slug', $category)->where('type', 'property')->first();

        if ($category) {

            $properties = $properties->whereHas('categories', function ($q) use ($category) {

                $q->where('categories.id', $category->id);

                if ($category->childs) {

                    $q->withChild($category->childs);
                }

            });
        }

        // availbale, un ,...
        if ($status) {

            $properties = $properties->where('properties.status', statusToInteger($status));
        }

        // access
        $properties = $properties->access();

        // total records
        $count = $properties->count();

        // pagination
        $properties = $properties->take($length)->skip($start);

        // order
        if ($request->order[0]['column']) {

            $column = $request->columns[$request->order[0]['column']]['data'];

            if ($column == 'user.branch') {

                $properties = $properties->orderBy('branches.name', $sort);

            } else if ($column == 'property_locales') {

                $properties = $properties->orderBy('property_locales.title', $sort);
            } else {

                $properties = $properties->orderBy('properties.' . $column, $sort);
            }

        } else {

            $properties = $properties->orderBy('properties.' . $column, $sort);
        }

        // get data
        $properties = $properties->get();

        // datatable response
        $respose = [
            "draw" => $draw,
            "recordsTotal" => $count,
            "recordsFiltered" => $count,
            "data" => $properties

        ];

        return $respose;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = \Validator::make($request->all(), [
            'title' => 'required',
            'city' => 'required',
            'price' => 'required:numeric'
        ]);

        if ($validator->fails()) {
            return response()->json(array('status' => 500, 'monolog' => array('title' => 'errors', 'message' => implode($validator->errors()->all(), '<br>'))));
        }

        if ($request->edit != 0) return $this->update($request, $request->edit);

        DB::beginTransaction();

        $user_id = \Auth::user()->get()->id;

        $property = new Property;

        $property->user_id = $user_id;

        // $property->customer_id = $request->customer_id;
        // $property->category_id = $request->category_id;

        $property->currency = $request->currency;
        $property->price = $request->price;
        $property->price_label = $request->price_label;
        $property->type = $request->type;

        $property->building_size = $request->building_size;
        $property->land_size = $request->land_size;
        $property->bedroom = $request->bedroom;
        $property->bathroom = $request->bathroom;
        $property->bed = $request->bed;
        $property->sell_in_furnish = $request->sell_in_furnish;

        $property->code = $request->code;
        $property->status = $request->status;
        $property->year = $request->year;

        $property->map_latitude = $request->map_latitude;
        $property->map_longitude = $request->map_longitude;

        $property->view_north = $request->view_north;
        $property->view_east = $request->view_east;
        $property->view_west = $request->view_west;
        $property->view_south = $request->view_south;

        $property->is_price_request = $request->is_price_request;
        $property->is_exclusive = $request->is_exclusive;

        $property->price_usd = convert_curr($request->price, $request->currency, 'usd');

        $property->owner_name = $request->owner_name;
        $property->owner_email = $request->owner_email;
        $property->owner_phone = $request->owner_phone;

        $property->agent_commission = $request->agent_commission;
        $property->agent_contact = $request->agent_contact;
        $property->agent_meet_date = $request->agent_meet_date;
        $property->agent_inspector = $request->agent_inspector;

        $property->sell_reason = $request->sell_reason;
        $property->sell_note = $request->sell_note;
        $property->other_agent = $request->other_agent;

        $property->orientation = $request->orientation;
        $property->sell_in_furnish = $request->sell_in_furnish;
        $property->lease_period = $request->lease_period;
        $property->lease_year = $request->lease_year;

        $property->city = $request->city;
        // find province, country
        // $city = \App\City::where('city_name', $request->city)->first();

        // $property->city = $request->city;
        // $property->province = $city->province->province_name;
        // $property->country = $city->province->country->name;

        // $property->save();


        // Model::unguard();

        // category
        $property->categories()->attach($request->category);

        // tag
        if ($request->tag) {

            foreach ($request->tag as $key => $value) {

                $property->tags()->attach($value);
            }

        }

//         locale
        $propertyLocale = new \App\PropertyLocale;

        $propertyLocale->locale = 'en';
        $propertyLocale->title = $request->title;
        $propertyLocale->content = $request->content;
        $propertyLocale->property_id = $property->id;

        $propertyLocale->save();


        // distances
        if ($request->distance_name) {
            foreach ($request->distance_name as $key => $value) {

                $propertyMeta = new \App\PropertyMeta;

                $propertyMeta->name = $value;
                $propertyMeta->value = $request->distance_value[$key];
                $propertyMeta->type = 'distance';
                $propertyMeta->property_id = $property->id;

                $propertyMeta->save();
            }
        }

        // documents
        if ($request->document_name) {
            foreach ($request->document_name as $key => $value) {

                $propertyMeta = new \App\PropertyMeta;

                $propertyMeta->name = $value;
                $propertyMeta->value = 1;
                $propertyMeta->type = 'document';
                $propertyMeta->property_id = $property->id;

                $propertyMeta->save();
            }
        }

        // facilities
        if ($request->facility_name) {
            foreach ($request->facility_name as $key => $value) {

                $propertyMeta = new \App\PropertyMeta;

                $propertyMeta->name = $value;
                $propertyMeta->value = 1;
                $propertyMeta->type = 'facility';
                $propertyMeta->property_id = $property->id;

                $propertyMeta->save();
            }
        }

        // files
        // if ($request->hasFile('files')) {

        //     foreach ($request->file('files') as $key => $value) {

        //         if ($value) {

        //             $destinationPath = 'uploads/images/property';

        //             $extension = $value->getClientOriginalExtension();
        //             $fileName = date('YmdHis') . '_' . $key . '_kibarer_property' . '.' . $extension;

        //             $value->move($destinationPath, $fileName);

        //             $attachment = new \App\Attachment;

        //             $attachment->attachable_id = $property->id;
        //             $attachment->attachable_type = 'App\Property';
        //             $attachment->name = 'property';
        //             $attachment->file = $fileName;
        //             $attachment->type = 'img';

        //             $attachment->save();

        //         }

        //     }

        // }

        // images
        if ($request->images) {

            foreach ($request->images as $key => $value) {

                $attachment = \App\Attachment::find($value);

                if ($key == 0) {

                    $propertyMeta = new \App\PropertyMeta;

                    $propertyMeta->property_id = $property->id;
                    $propertyMeta->name = 'thumbnail';
                    $propertyMeta->value = $attachment->thumb;
                    $propertyMeta->type = 'thumbnail';

                    $propertyMeta->save();

                }

                $attachment->attachable_id = $property->id;

                $attachment->order = $key;

                $attachment->save();

            }

        }

        // Model::reguard();

        DB::commit();


        return response()->json(array('status' => 200, 'monolog' => array('title' => 'success', 'message' => 'object has been saved'), 'data' => ['category' => $property->categories[0]->slug, 'id' => $property->id]));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        DB::beginTransaction();

        $user_id = \Auth::user()->get()->id;

        $property = Property::find($id);

        $property->user_id = $user_id;

        // $property->customer_id = $request->customer_id;
        // $property->category_id = $request->category_id;

        $property->currency = $request->currency;
        $property->price = $request->price;
        $property->price_label = $request->price_label;
        $property->type = $request->type;

        $property->building_size = $request->building_size;
        $property->land_size = $request->land_size;

        $property->code = $request->code;
        $property->status = $request->status;
        $property->year = $request->year;

        $property->map_latitude = $request->map_latitude;
        $property->map_longitude = $request->map_longitude;

        $property->view_north = $request->view_north;
        $property->view_east = $request->view_east;
        $property->view_west = $request->view_west;
        $property->view_south = $request->view_south;

        $property->is_price_request = $request->is_price_request;
        $property->is_exclusive = $request->is_exclusive;

        $property->owner_name = $request->owner_name;
        $property->owner_email = $request->owner_email;
        $property->owner_phone = $request->owner_phone;

        $property->price_usd = convert_curr($request->price, $request->currency, 'usd');

        $property->agent_commission = $request->agent_commission;
        $property->agent_contact = $request->agent_contact;
        $property->agent_meet_date = $request->agent_meet_date;
        $property->agent_inspector = $request->agent_inspector;

        $property->sell_reason = $request->sell_reason;
        $property->sell_note = $request->sell_note;
        $property->other_agent = $request->other_agent;

        $property->orientation = $request->orientation;
        $property->sell_in_furnish = $request->sell_in_furnish;
        $property->lease_period = $request->lease_period;
        $property->lease_year = $request->lease_year;

        $property->city = $request->city;

        // find province, country
        // $city = \App\City::where('city_name', $request->city)->first();

        // $property->city = $request->city;
        // $property->province = $city->province->province_name;
        // $property->country = $city->province->country->name;

        $property->save();

        $property->categories()->sync([$request->category]);

        // tag
        if ($request->tag) {

            $property->tags()->sync($request->tag);
        }

        // locale
        $propertyLocale = $property->propertyLocales()
            ->where('locale', 'en')
            ->first();

        if (count($propertyLocale) == 0) $propertyLocale = new \App\PropertyLocale;

        $propertyLocale->property_id = $property->id;
        $propertyLocale->title = $request->title;
        $propertyLocale->content = $request->content;
        $propertyLocale->locale = 'en';

        $propertyLocale->save();

        // distances
        if ($request->distance_name) {

            $property->propertyMetas()->where('type', 'distance')->delete();

            foreach ($request->distance_name as $key => $value) {

                $propertyMeta = new \App\PropertyMeta;

                $propertyMeta->name = $value;
                $propertyMeta->value = $request->distance_value[$key];
                $propertyMeta->type = 'distance';
                $propertyMeta->property_id = $property->id;

                $propertyMeta->save();
            }
        }

        // documents
        if ($request->document_name) {

            $property->propertyMetas()->where('type', 'document')->delete();

            foreach ($request->document_name as $key => $value) {

                $propertyMeta = new \App\PropertyMeta;

                $propertyMeta->name = $value;
                $propertyMeta->value = 1;
                $propertyMeta->type = 'document';
                $propertyMeta->property_id = $property->id;

                $propertyMeta->save();
            }
        }

        // facilities
        if ($request->facility_name) {

            $property->propertyMetas()->where('type', 'facility')->delete();

            foreach ($request->facility_name as $key => $value) {

                $propertyMeta = new \App\PropertyMeta;

                $propertyMeta->name = $value;
                $propertyMeta->value = 1;
                $propertyMeta->type = 'facility';
                $propertyMeta->property_id = $property->id;

                $propertyMeta->save();
            }

        }

        // files

        // if ($request->hasFile('files')) {

        //     foreach ($request->file('files') as $key => $value) {

        //         if ($value) {

        //             $destinationPath = 'uploads/images/property';

        //             $extension = $value->getClientOriginalExtension();
        //             $fileName = date('YmdHis') . '_' . $key . '_kibarer_property' . '.' . $extension;

        //             $value->move($destinationPath, $fileName);

        //             $attachment = new \App\Attachment;

        //             $attachment->attachable_id = $property->id;
        //             $attachment->attachable_type = 'App\Property';
        //             $attachment->name = 'property';
        //             $attachment->file = $fileName;
        //             $attachment->type = 'img';

        //             $attachment->save();

        //         }

        //     }

        // }


        // images
        if ($request->images) {

            foreach ($property->galleries as $key => $value) {

                if (!in_array($value->id, $request->images)) \App\Attachment::find($value->id)->delete();
            }


            foreach ($request->images as $key => $value) {

                $attachment = $property->galleries()->find($value);

                if (count($attachment) == 0) {

                    $attachment = \App\Attachment::find($value);

                    $attachment->attachable_id = $property->id;

                    $attachment->order = $key;

                    $attachment->save();

                } else {

                    $attachment->order = $key;

                    $attachment->save();
                }

                if ($key == 0) {

                    $property->propertyMetas()->where('type', 'thumbnail')->delete();

                    $propertyMeta = $property->propertyMetas()->where('type', 'thumbnail')->first();

                    if (count($propertyMeta) == 0) $propertyMeta = new \App\PropertyMeta;

                    $propertyMeta->property_id = $property->id;
                    $propertyMeta->name = 'thumbnail';
                    $propertyMeta->value = $attachment->thumb;
                    $propertyMeta->type = 'thumbnail';

                    $propertyMeta->save();

                }

            }

        } else {

            $property->propertyMetas()->where('type', 'thumbnail')->delete();

            $property->galleries()->delete();
        }


        DB::commit();

        return response()->json(array('status' => 200, 'monolog' => array('title' => 'success', 'message' => 'object has been updated')));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $property = Property::find($id);

        $property->delete();

        return response()->json(array('status' => 200, 'monolog' => array('title' => 'delete success', 'message' => 'object has been deleted'), 'id' => $id));
    }

    public function delete($id)
    {
        //
        $property = Property::find($id);

        $property->delete();

        return response()->json(array('status' => 200, 'monolog' => array('title' => 'delete success', 'message' => 'object has been deleted'), 'id' => $id));
    }

    public function remove(Request $request)
    {
        //

        foreach ($request->id as $id) {

            $property = Property::find($id);

            $property->delete();
        }

        return response()->json(array('status' => 200, 'monolog' => array('title' => 'delete success', 'message' => 'object has been deleted'), 'id' => $request->id));
    }

    public function search(Request $request, $search, $category = null, $type = null, $area = null)
    {

        //online dev
        //echo $search.' -- '.$category.' -- '.$type.' -- '.$area;
        // url : http://kibarer.kesato.com/search/villas-for-sale/less-than-500k/denpasar
        // output : search -- villas-for-sale -- less-than-500k -- denpasar

        if($request->ajax())
        {
            $att = array();
            $hold = $request->get('leasefree');
            if(count($hold) > 1)
            {
                $att['free_hold'] = 'Free hold';
                $att['lease_hold'] = 'Lease hold';
            }
            else if(count($hold) > 1)
            {
                if($hold[0] == 'free hold') $att['free_hold'] = 'Free hold';
                else $att['lease_hold'] = 'Lease hold';
            }
        }
        else
        {
            $att = array('lease_hold' => 'Lease Hold', 'free_hold' => 'Free Hold');
        }
        
        $this->user_track->save($search ,$category, $type, $area, $att);


        if ($category == null) return $this->listing();

        $limit = 24;
        if ($request->limit) {
            $limit = $request->limit;
        }

        if ($area) {
            $url = 'http://maps.google.com/maps/api/geocode/json?address=' . $area . '&sensor=false';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            $rsp = curl_exec($ch);
            $jsonResult = json_decode($rsp);
            $nwLat = $jsonResult->results[0]->geometry->viewport->northeast->lat;
            $nwLng = $jsonResult->results[0]->geometry->viewport->northeast->lng;
            $seLat = $jsonResult->results[0]->geometry->viewport->southwest->lat;
            $seLng = $jsonResult->results[0]->geometry->viewport->southwest->lng;
        }


        $properties = new Property;

        $properties = $properties->select(DB::raw('
                properties.*,property_locales.*,
                Group_concat(distinct property_metas.value) as thumbs,
                Group_concat(distinct catchild.slug) as catchilds
            '));
//
        // join catables + categories + where category slug
        $properties = $properties->join('catables', function ($q) {
            $q->on('catables.catable_id', '=', 'properties.id');
            $q->where('catables.catable_type', '=', 'App\Property');
        })
            ->join('categories', 'categories.id', '=', 'catables.category_id');

        if ($category) {
            $properties = $properties->where('categories.slug', $category);
        }

//
//        if ($area) {
//            $properties = $properties->where('properties.city', 'LIKE', '%' . $area . '%');
//        }


//        join thumbnail
        $properties = $properties->leftjoin('property_metas', function ($q) {
            $q->on('property_metas.property_id', '=', 'properties.id');
            $q->where('property_metas.type', '=', 'thumbnail');
        });

        //join category child
        $properties = $properties->leftjoin('categories as catchild', function ($q) {
            $q->on('catchild.parent_id', '=', 'categories.id');
        });

        // join locales
        $properties = $properties->join('property_locales', 'properties.id', '=', 'property_locales.property_id');

//         join tagables + tags + where tags slug

        $properties = $properties->join('tagables', function ($q) {
            $q->on('tagables.tagable_id', '=', 'properties.id');
            $q->where('tagables.tagable_type', '=', 'App\Property');
        })
            ->join('tags', 'tags.id', '=', 'tagables.tag_id');


        if ($type) {
            if ($category != 'villas-for-rent') {
                if ($request->type) {

                    if (is_array($request->type)) {

                        $properties = $properties->whereIn('tags.slug', $request->type);

                    } else {

                        $properties = $properties->where('tags.slug', $request->type);
                    }

                }else{
                    $properties = $properties->where('tags.slug', $type);
                }
            } else {
                if ($request->shortlong) {
                    $list = [];
                    foreach ($request->shortlong as $lg) {
                        if ($lg == 'short-term') {
                            array_push($list, 'daily');
                            array_push($list, 'weekly');
                            array_push($list, 'monthly');
                        } else {
                            array_push($list, 'annually');
                        }
                    }
                    $properties = $properties->whereIn('properties.price_label', $list);
                } else {
                    $list = [];
                    if ($type == 'short-term') {
                        array_push($list, 'daily');
                        array_push($list, 'weekly');
                        array_push($list, 'monthly');
                    } else {
                        array_push($list, 'annually');
                    }
                    $properties = $properties->whereIn('properties.price_label', $list);
                }
            }

        }

        // check child category
//            if ($term->childs) $properties = $properties->categoryChild($term->childs);

        if ($request->nwLat && $request->nwLng && $request->seLat && $request->seLng) {
            $nwLat = $request->nwLat;
            $nwLng = $request->nwLng;
            $seLat = $request->seLat;
            $seLng = $request->seLng;
        }
        if ($area OR ($request->nwLat && $request->nwLng && $request->seLat && $request->seLng)) {
            $properties = $properties->where(DB::raw("map_latitude BETWEEN least($seLat,$nwLat) AND greatest($seLat,$nwLat)"), true);
            $properties = $properties->whereBetween('map_longitude', [$seLng, $nwLng]);
        }


        if ($request->bedroom && $request->bedroom != 0) {
            $properties = $properties->where('properties.bedroom', $request->bedroom);
        }

        if ($request->bathroom && $request->bathroom != 0) {
            $properties = $properties->where('properties.bathroom', $request->bathroom);
        }

        if ($request->amounts) {
            $amounts = explode(' - ', $request->amounts);

            $priceMin = str_replace(',','',explode(' ',$amounts[0]));
            $priceMax = str_replace(',','',explode(' ',$amounts[1]));

            $priceMin = convert_curr($priceMin[1],\Session::get('currency'),'usd');
            $priceMax = convert_curr($priceMax[1],\Session::get('currency'),'usd');

            $properties = $properties->whereBetween('properties.price_usd', [$priceMin, $priceMax]);
        }

        if ($request->land_size) {
            $land_size = explode(' - ', $request->land_size);
            $landMin = str_replace(',', '', explode(' ', $land_size[0]));
            $landMax = str_replace(',', '', explode(' ', $land_size[1]));
            $properties = $properties->whereBetween('properties.land_size', [$landMin[0], $landMax[0]]);
        }


        if ($request->leasefree) {
            if (is_array($request->leasefree)) {
                $properties = $properties->whereIn('properties.type', $request->leasefree);

            } else {
                $properties = $properties->where('properties.type', $request->leasefree);
            }

        }

        $properties = $properties->groupBy('properties.id')
            ->where('properties.status', '>=', 0)
            ->paginate($limit);


        $maxPrice = replaceComma(convertCurrency(Property::max('price'), 'usd', \Session::get('currency')));


        if ($request->ajax()) {
//            dd($category);
            return view('pages.ajax-property', compact('properties', 'maxPrice', 'category', 'area', 'type', 'latitude'));
        }

        return view('pages.search-property', compact('properties', 'maxPrice', 'category', 'area', 'type', 'longitude'));
    }


    public function listing()
    {
        return view('pages.property-listing');
    }
    

    public function detail(Request $request, $page, $term = null)
    {

        // return \Session::get('currency');
        // if ($term == null) return $this->search($request, $page, $term);
        if ($term == null) return abort('404');

        $segment = explode('/', $term);

        $slug = end($segment);

        $property = new Property;

        $property = $property->whereHas('propertyLocales', function ($q) use ($slug) {
            $q->where('slug', $slug);
        });

        // if ($property->count() == 0) return abort('404');
        if ($property->count() == 0) return $this->search($request, null, $term);

        $property = $property->first();

        // print PDF
        if ($request->print == 'pdf') {            

            $pdf = \PDF::loadView('pdf.property_new', compact('property'));

            return $pdf->stream();
        }

        return view('pages.property-view', compact('property', 'segment'));

    }

    public function sellProperty(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'owner_firstname' => 'required',
            'owner_lastname' => 'required',
            'owner_email' => 'required',
            'owner_phone' => 'required',
            'location' => 'required',
            'g-recaptcha-response' => 'required|recaptcha'
        ]);

        if ($validator->fails()) {
            return response()->json(array('status' => 500, 'monolog' => array('title' => 'errors', 'message' => $validator->errors())));
        }

        DB::beginTransaction();

        $property = new Property;

        $property->map_latitude = $request->map_latitude;
        $property->map_longitude = $request->map_longitude;
        // $property->owner_name = $request->owner_name;
        $property->owner_name = $request->owner_firstname . ' ' . $request->owner_lastname;
        $property->owner_email = $request->owner_email;
        $property->owner_phone = $request->owner_phone;
        $property->sell_note = $request->sell_note;

        $property->price = $request->price;
        $property->currency = $request->currency;

        $property->type = $request->type;

        if ($request->category == 1) {
            $property->building_size = $request->building_size_villa_sell;
            $property->land_size = $request->land_size_villa_sell;

            $property->bedroom = $request->bedroom_villa_sell;
            $property->bathroom = $request->bathroom_villa_sell;

        }

        if ($request->category == 2) {
            $property->building_size = $request->building_size_villa_rent;
            $property->land_size = $request->land_size_villa_rent;

            $property->bedroom = $request->bedroom_villa_rent;
            $property->bathroom = $request->bathroom_villa_rent;
            
        }

        if ($request->category == 3) {

            $property->land_size = $request->land_size_land;
            
        }

        $property->city = $request->location;
        // find province, country
        // $city = \App\City::where('city_name', $request->city)->first();

        // $property->city = $request->city;
        // $property->province = $city->province->province_name;
        // $property->country = $city->province->country->name;


        //moderation
        $property->status = -2;

        $property->save();

        // category
        $property->categories()->attach($request->category);

        // files
        foreach ($request->file('images') as $key => $value) {

            if ($value) {

                $attachment = $this->uploadImage($property->id, $value, 'property', true);


                if ($key == 0) {

                    // $value->move('uploads', 'test.jpg');exit;

                    $property->propertyMetas()->where('type', 'thumbnail')->delete();

                    $propertyMeta = $property->propertyMetas()->where('type', 'thumbnail')->first();

                    if (count($propertyMeta) == 0) $propertyMeta = new \App\PropertyMeta;

                    $propertyMeta->property_id = $property->id;
                    $propertyMeta->name = 'thumbnail';
                    $propertyMeta->value = $attachment->thumb;
                    $propertyMeta->type = 'thumbnail';

                    $propertyMeta->save();

                }
            }

        }

        // facilities
        if ($request->facility_name) {
            foreach ($request->facility_name as $key => $value) {

                $propertyMeta = new \App\PropertyMeta;

                $propertyMeta->name = $value;
                $propertyMeta->value = 1;
                $propertyMeta->type = 'facility';
                $propertyMeta->property_id = $property->id;

                $propertyMeta->save();
            }
        }

        DB::commit();

        $this->email($property);

        
        // Add this contact to CRM
        $type = ($request->get('category') == '3') ? 'Land' : 'Villa';
        $this->user_track->update('1',$type);
        $this->user_track->update('2','Seller');
        $sell_request = $request->all();
        $data['firstname'] = $sell_request['owner_firstname'];
        $data['lastname'] = $sell_request['owner_lastname'];
        $data['email'] = $sell_request['owner_email'];
        $data['phone'] = $sell_request['owner_phone'];
        $data['ip'] = $request->server('SERVER_ADDR');
        $data['lang'] = substr($request->server('HTTP_ACCEPT_LANGUAGE'), 0, 2);
        $data['tags'] = $this->user_track->get();
        $crm = new GetBaseCRM;
        $crm->customer_add($data);

        return response()->json(array('status' => 200, 'monolog' => array('title' => 'success', 'message' => 'Thanks. Your property has been sent successfully.')));
    }

    public function email($property)
    {
        $email = 'gusman@kesato.com';

        Mail::send('emails.property-listing', ['property' => $property], function ($message) use ($email) {

            $message->from('boris@kesato.com', 'Kibarer');

            $message->to($email, $email)->subject('Listing Request Kibarer');
        });

        return true;
    }

    // CSV
    public function export(Request $request)
    {
        $ids = explode(',', $request->id);

        $admin = \Auth::user()->get();

        $file_name = $admin->id . '-property';

        $take = ($request->take) ? $request->take : 'all';

        $properties = new Property;

        if ($request->type) {

            $properties = $properties->select('properties.*')
                ->join('catables', 'catables.catable_id', '=', 'properties.id')
                ->join('categories', 'categories.id', '=', 'catables.category_id')
                ->where('categories.slug', $request->type);
        }

        if ($request->status) {

            $properties = $properties->where('properties.status', statusToInteger($request->status));
        }

        // access
        $properties = $properties->access();

        if ($request->take == 'selected') {

            // selected only
            $properties = $properties->whereIn('properties.id', $ids);
        }

        // $properties = $properties->take($take);

        $properties = $properties->orderBy('id', 'desc');

        $properties = $properties->get();

        foreach ($properties as $key => $value) {

            $value->title = $value->lang($request->locale)->title;

            $value->description = $value->lang($request->locale)->content;

            if ($request->user) {

                $value->users = $value->user->username;
            }

            if ($request->category) {

                $value->category = $value->categories[0]->name;
            }

            if ($request->facilities) {

                $arr_facility = $value->facilities->lists('name')->toArray();

                foreach (\Config::get('facility')['rental'] as $facility) {

                    $value->$facility = in_array($facility, $arr_facility) ? 'yes' : '';
                }

            }

            if ($request->documents) {

                foreach (\Config::get('document') as $k => $v) {

                    $value->$v = in_array($v, $value->documents->lists('name')->toArray()) ? 'yes' : '';
                }
            }

            if ($request->distances) {

                $arr_distance = array();
                foreach ($value->distances as $k => $v) {

                    $arr_distance[$v->name] = $v->value;
                }

                $value->distance = json_encode($arr_distance);

            }

        }

        $data = json_decode(json_encode($properties), true);

        \Excel::create($file_name, function ($excel) use ($data) {

            $excel->sheet('Sheet1', function ($sheet) use ($data) {

                $sheet->setOrientation('landscape');

                $sheet->fromArray($data);

            });

        })
            // ->export('csv');
            ->store('csv', 'export/csv');

        return $file_name . '.csv';
    }

    public function exportPDF(Request $request)
    {
        $ids = explode(',', $request->id);

        $admin = \Auth::user()->get();

        $file_name = $admin->id . '-property';

        $properties = new Property;

        $properties = $properties->with('facilities');

        $properties = $properties->with(['propertyLocales' => function ($q) {
            $q->where('locale', 'en');
        }]);

        $properties = $properties->whereIn('id', $ids);

        $properties = $properties->get();

        $pdf_name = array();

        foreach ($properties as $key => $property) {

            $exist_facility = array();
            foreach ($property->facilities as $key => $value) {
                $exist_facility[] = strtolower($value->name);
            }

            $pdf = \PDF::loadView('pdf.property_back', compact('property', 'request', 'exist_facility'));

            $pdf->save('export/pdf/' . $file_name . '-' . $property->id . '.pdf');

            $pdf_name[] = $file_name . '-' . $property->id . '.pdf';
        }

        return $pdf_name;

    }

    public function setStatus(Request $request)
    {

        $property = new Property;

        $property->whereIn('id', $request->id)
            ->update(['status' => $request->status]);

        return response()->json(array('status' => 200, 'monolog' => array('title' => 'delete success', 'message' => 'object has been updated'), 'id' => $request->id));
    }


    public function uploadImage($propertyId, $file, $name, $watermark)
    {
        $dir = 'uploads/images/' . $name;

        \File::makeDirectory($dir, 0755, true, true);

        $attachable_type = 'App\Property';

        // setting file's mysterious name
        $filename = date('Y-m-d') . '-' . $name . '-' . uniqid() . '.jpg';

        $file->move($dir, $filename);

        $img = \Image::make($dir . '/' . $filename);

        // resize 1000
        $img->resize(1000, null, function ($constraint) {
            $constraint->aspectRatio();
        });

        $img->save(public_path($dir . '/' . $filename));

        // watermark
        if ($watermark) {

            $img = \Image::make($dir . '/' . $filename);
            $img->insert('assets/img/logo.png', 'bottom-right', 10, 10);
            $img->save(public_path($dir . '/' . $filename));
        }

        // resize 400
        $img->resize(400, null, function ($constraint) {
            $constraint->aspectRatio();
        });

        $img->save(public_path($dir . '/thumb/' . $filename));

        $attachment = new \App\Attachment;

        $attachment->attachable_type = $attachable_type;
        $attachment->attachable_id = $propertyId;
        $attachment->name = $filename;
        $attachment->file = '/' . $dir . '/' . $filename;
        $attachment->thumb = '/' . $dir . '/thumb/' . $filename;
        $attachment->type = 'img';

        $attachment->save();

        return $attachment;

    }

}
