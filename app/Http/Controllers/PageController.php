<?php

namespace App\Http\Controllers;

use App\City;
use App\Estimate;
use App\Setting;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Page;

use DB;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $category = $request->category;
        $status = $request->status;

        // datatable parameter
        $draw = $request->draw;
        $start = $request->start;
        $length = $request->length;
        $search = $request->search['value'];

        // sorting
        $column = 'id';
        $sort = $request->order[0]['dir'] ? $request->order[0]['dir'] : 'desc'; //asc

        // new object
        $pages = new Page;

        $pages = $pages->select('pages.*')
            ->join('page_locales', 'page_locales.page_id', '=', 'pages.id')
            ->where('page_locales.locale', 'en');

        $pages = $pages->with(['pageLocales' => function ($q) {

            $q->where('locale', 'en');
        }]);

        // searching
        if ($search) {

            $pages = $pages->where(function ($q) use ($search) {

                $q->where('page_locales.title', 'like', $search . '%');
            });
        }

        // total records
        $count = $pages->count();

        // pagination
        $pages = $pages->take($length)->skip($start);

        // order
        if ($request->order[0]['column']) {

            $column = $request->columns[$request->order[0]['column']]['data'];

            if ($column == 'page_locales.0.title') {

                $pages = $pages->orderBy('page_locales.title', $sort);
            } else {

                $pages = $pages->orderBy('pages.' . $column, $sort);
            }

        } else {

            $pages = $pages->orderBy('pages.' . $column, $sort);
        }

        // get data
        $pages = $pages->get();

        // datatable response
        $respose = [
            "draw" => $draw,
            "recordsTotal" => $count,
            "recordsFiltered" => $count,
            "data" => $pages

        ];

        return $respose;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $validator = \Validator::make($request->all(), [
            'title' => 'required'
        ]);


        if ($validator->fails()) {
            return response()->json(array('status' => 500, 'monolog' => array('title' => 'errors', 'message' => implode($validator->errors()->all(), '<br>'))));
        }

        DB::beginTransaction();

        $page = new Page;

        $page->user_id = \Auth::user()->get()->id;
        $page->status = $request->status;

        $page->save();

        // locale
        foreach ($request->title as $key => $value) {
            if (!empty($value)){
                $locale = new \App\PageLocale;

                $locale->page_id = $page->id;
                $locale->title = $request->title[$key];
                $locale->content = $request->content[$key];
                $locale->meta_keyword = $request->meta_keyword[$key];
                $locale->meta_description = $request->meta_description[$key];
                $locale->locale = $key;

                $locale->save();
            }
        }

        DB::commit();

        return response()->json(array('status' => 200, 'monolog' => array('title' => 'success', 'message' => 'object has been saved')));

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validator = \Validator::make($request->all(), [
            'title' => 'required'
        ]);


        if ($validator->fails()) {
            return response()->json(array('status' => 500, 'monolog' => array('title' => 'errors', 'message' => implode($validator->errors()->all(), '<br>'))));
        }

        DB::beginTransaction();

        $page = Page::find($id);

        $page->user_id = \Auth::user()->get()->id;
        $page->status = $request->status;

        $page->save();


        // delete first
        $page->pageLocales()->delete();

        // locale
        foreach ($request->title as $key => $value) {
            if (!empty($value)){
                $locale = new \App\PageLocale;

                $locale->page_id = $page->id;
                $locale->title = $request->title[$key];
                $locale->content = $request->content[$key];
                $locale->meta_keyword = $request->meta_keyword[$key];
                $locale->meta_description = $request->meta_description[$key];
                $locale->locale = $key;

                $locale->save();
            }
        }

        DB::commit();

        return response()->json(array('status' => 200, 'monolog' => array('title' => 'success', 'message' => 'object has been updated')));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $page = Page::find($id);

        if (in_array($page->id, [1, 2, 3])) {
            return response()->json(array('status' => 500, 'monolog' => array('title' => 'errors', 'message' => 'you are not allowed to delete this item')));
        }

        $page->delete();

        return response()->json(array('status' => 200, 'monolog' => array('title' => 'delete success', 'message' => 'object has been deleted'), 'id' => $id));
    }

    public function home()
    {
        $page = Page::find(1);
//
//        $page->meta_description = '';
//        $page->meta_keyword = '';

        $testimonials = \App\Testimony::where('status', 1)->orderBy('created_at', 'desc')->take(6)->get();

        $posts = \App\Post::where('status', 1)->orderBy('created_at', 'desc')->take(3)->get();

        return view('pages.home', compact('page', 'posts', 'testimonials'));
    }

    public function about($page = null)
    {
        $page = Page::find(2);

        $page->meta_description = $page->lang()->meta_description;
        $page->meta_keyword = $page->lang()->meta_keyword;

        return view('pages.about', compact('page'));
    }

    public function contact()
    {
        $page = Page::find(3);


        return view('pages.contact', compact('page'));
    }

    public function resource()
    {

        return view('pages.resources');
    }

    public function testimony($page, $term = null)
    {
        if ($term != null) return $this->testimonyDetail($term);

        $testimonials = new \App\Testimony;

        $testimonials = $testimonials->where('status', 1);

        $testimonials = $testimonials->orderBy('created_at', 'desc');

        $testimonials = $testimonials->paginate(6);

        return view('pages.testimony', compact('testimonials'));

    }

    public function testimonyDetail($term)
    {
        $testimony = \App\Testimony::where('slug', $term)->first();

        if (count($testimony) == 0) return abort('404');

        return view('pages.testimony-detail', compact('testimony'));
    }

    public function sellProperty()
    {
        return view('pages.sell-property');
    }

    public function lawyerNotary()
    {
        return view('pages.lawyer-notary');
    }

    public function sitemap()
    {

        return view('pages.sitemap');
    }

    public function export(Request $request)
    {
        $admin = \Auth::user()->get();

        $file_name = $admin->id . 'page';

        $take = ($request->take) ? $request->take : 'all';

        $pages = new Page;

        if ($request->take != 'all') {

            $pages = $pages->take($take);
        }

        $pages = $pages->orderBy('id', 'desc');

        $pages = $pages->get();

        $data = json_decode(json_encode($pages), true);

        \Excel::create($file_name, function ($excel) use ($data) {

            $excel->sheet('Sheet1', function ($sheet) use ($data) {

                $sheet->setOrientation('landscape');

                $sheet->fromArray($data);

            });

        })
            // ->export('csv');
            ->store('csv', 'export/csv');

        return $file_name . '.csv';
    }

    public function estimate(Request $request)
    {
        if ($request->ajax() && $request->method() == 'POST'){

            $validator = \Validator::make($request->all(), [
                'land' => 'required|integer',
                'building' => 'required|integer',
                'bedroom' => 'required|integer',
                'bathroom' => 'required|integer',
            ]);

            if ($validator->fails()) {
                return \Response::json(array(
                    'success' => false,
                    'errors' => $validator->getMessageBag()->toArray()
                ), 400);
            }


            $estimate = Estimate::where('cities_id',$request->cities_id)->first();
            $calculate = ($request->land*$estimate->land)+($request->building*$estimate->building)+($request->bedroom*$estimate->bedroom)+($request->bathroom*$estimate->bathroom);
            return \Response::json([
                'success' => true,
                'result' => $calculate,
            ],200);
        }

        $cities = City::lists('city_name_full','id');
        return view('pages.estimate',compact('cities'));
    }

    public function soldVilla()
    {
        $limit = 12;

        $properties = new \App\Property;

        $properties = $properties->where('status', 0);

        $properties = $properties->paginate($limit);

        return view('pages.sold-villas', compact('properties'));
    }

}
