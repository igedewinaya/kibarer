<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Contact;
use App\Services\GetBaseCRM;
use App\Services\UserTrack;

use Mail;

class ContactController extends Controller
{

    protected $user_track;


    function __construct()
    {
        $this->user_track = new UserTrack();
    }

    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $category = $request->category;
        $status = $request->status;

        // datatable parameter
        $draw = $request->draw;
        $start = $request->start;
        $length = $request->length;
        $search = $request->search['value'];

        // sorting
        $column = 'id';
        $sort = $request->order[0]['dir'] ? $request->order[0]['dir'] : 'desc'; //asc

        // new object
        $contacts = new Contact;

        // searching
        if ($search) {

            $contacts = $contacts->where(function ($q) use ($search) {
                    $q->where('contacts.email', 'like', $search . '%')
                        ->orWhere('contacts.message', 'like', $search . '%');
                });
        }

        // total records
        $count = $contacts->count();

        // pagination
        $contacts = $contacts->take($length)->skip($start);

        // order
        if ($request->order[0]['column']) {

            $column = $request->columns[$request->order[0]['column']]['data'];

            $contacts = $contacts->orderBy('contacts.' . $column, $sort);

        } else {

            $contacts = $contacts->orderBy('contacts.' . $column, $sort);
        }

        // get data
        $contacts = $contacts->get();

        // datatable response
        $respose = [
                "draw" => $draw,
                "recordsTotal" => $count,
                "recordsFiltered" => $count,
                "data" => $contacts

            ];

        return $respose;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'email' => 'email|required',
            'firstname' => 'required',
            'lastname' => 'required',
            'g-recaptcha-response' => 'required|recaptcha'
        ]);

        if ($validator->fails()) {
            return response()->json(array('status' => 500, 'monolog' => array('title' => 'errors', 'message' => $validator->errors() )));
        }

        /*
        $contact = new Contact;

        $contact->firstname = $request->firstname;
        $contact->lastname = $request->lastname;
        $contact->email = $request->email;
        $contact->message = $request->message;

        $contact->save();

        $this->email($contact);
        */

        // Add this contact to CRM
        $data = $request->all();
        $data['ip'] = $request->server('SERVER_ADDR');
        $data['lang'] = substr($request->server('HTTP_ACCEPT_LANGUAGE'), 0, 2);
        $data['tags'] = $this->user_track->get();
        $crm = new GetBaseCRM;
        $crm->customer_add($data);

        return response()->json(array('status' => 200, 'monolog' => array('title' => 'success', 'message' => 'success')));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $contact = Contact::find($id);

        $contact->delete();

        return response()->json(array('status' => 200, 'monolog' => array('title' => 'delete success', 'message' => 'object has been deleted'), 'id' => $id));
    }

    public function email($contact)
    {
        $email = \Config::get('mail.contact');

        Mail::send('emails.contact', ['contact' => $contact], function($message) use ($email) {

            $message->from('boris@kesato.com', 'Kibarer');

            $message->to($email, $email)->subject('Contact Kibarer');
        });

        return true;
    }

    public function export(Request $request)
    {
        $ids = explode(',', $request->id);

        $admin = \Auth::user()->get();

        $file_name = $admin->id . '-message';

        $take = ($request->take) ? $request->take : 'all';

        $contacts = new Contact;

        if ($request->take == 'selected') {

            $contacts = $contacts->whereIn('contacts.id', $ids);

            // $contacts = $contacts->take($take);
        }

        $contacts = $contacts->orderBy('id', 'desc');

        $contacts = $contacts->get();

        $data = json_decode(json_encode($contacts), true);

        \Excel::create($file_name, function($excel) use ($data) {

            $excel->sheet('Sheet1', function($sheet) use ($data) {

                $sheet->setOrientation('landscape');

                $sheet->fromArray($data);

            });

        })
        // ->export('csv');
        ->store('csv', 'export/csv');

        return $file_name . '.csv';
    }

    public function remove(Request $request)
    {
        //

        foreach ($request->id as $id) {
            
            $contact = Contact::find($id);

            $contact->delete();
        }

        return response()->json(array('status' => 200, 'monolog' => array('title' => 'delete success', 'message' => 'object has been deleted'), 'id' => $request->id));
    }

}
