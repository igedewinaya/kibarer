<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Attachment;
use DB;

class AttachmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $attachment = Attachment::find($id);

        DB::beginTransaction();

        // delete thumbnail
        if (\Input::get('name') == 'property') {

            $delete_thumb = \App\PropertyMeta::where('value', $attachment->file)->delete();
        }

        if ($attachment->file) {

            if (\Input::get('name'))
                \File::delete('uploads/images/' . \Input::get('name') . '/' . $attachment->file);

            else
                \File::delete('uploads/files/' . $attachment->file);
        }

        $attachment->delete();

        DB::commit();

        return response()->json(array('status' => 200, 'monolog' => array('title' => 'delete success', 'message' => 'object has been deleted'), 'id' => $id));
    }

    public function getImage()
    {
        $attachments = new Attachment;

        $attachments = $attachments->where('type', 'img')
            ->where('name', '!=', 'property')
            ->orderBy('created_at', 'desc')
            ->get();

        $responses = array();
        foreach ($attachments as $key => $value) {

            $row = [
                'thumb' => asset('uploads/images/' . $value->name . '/' . $value->file),
                'image' => asset('uploads/images/' . $value->name . '/'. $value->file),
                'title' => 'image title',
                'folder' => $value->name
            ];

            $responses[] = $row;
        }

        return $responses;
    }

    public function uploadImage(Request $request)
    {
        $dir = 'uploads/images/' . $request->name;

        \File::makeDirectory($dir, 0755, true, true);

        $attachable_type = 'App\Post';

        if ($request->name == 'post') $attachable_type = 'App\Post';

        if ($request->name == 'page') $attachable_type = 'App\Page';

        if ($request->name == 'property') $attachable_type = 'App\Property';

        if ($request->name == 'testimony') $attachable_type = 'App\Testimony';

        $file = $request->file('file');
         
        $_FILES['file']['type'] = strtolower($_FILES['file']['type']);
         
        if ($_FILES['file']['type'] == 'image/png' 
        || $_FILES['file']['type'] == 'image/jpg' 
        || $_FILES['file']['type'] == 'image/gif' 
        || $_FILES['file']['type'] == 'image/jpeg'
        || $_FILES['file']['type'] == 'image/pjpeg')
        {   
            // setting file's mysterious name
            $filename = date('Y-m-d') . '-' . $request->name . '-' . uniqid() . '.jpg';

            $file->move($dir, $filename);

            $img = \Image::make($dir . '/' . $filename);

            // resize 1000
            $img->resize(1000, null, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->save(public_path($dir . '/' . $filename));

            // watermark
            if ($request->watermark) {

                $img = \Image::make($dir . '/' . $filename);
                $img->insert('assets/img/logo.png', 'bottom-right', 10, 10);
                $img->save(public_path($dir . '/' . $filename));
            }

            // resize 400
            $img->resize(400, null, function ($constraint) {
                $constraint->aspectRatio();
            });

            $thumb_dir = $dir . '/thumb';

            \File::makeDirectory($thumb_dir, 0755, true, true);

            $img->save(public_path($thumb_dir . '/' . $filename));


            $attachment = new Attachment;

            $attachment->attachable_type = $attachable_type;
            $attachment->name = $filename;
            $attachment->file = '/' . $dir . '/' . $filename;
            $attachment->thumb = '/' . $dir . '/thumb/' . $filename;
            $attachment->type = 'img';

            $attachment->save();

            // displaying file    
            $array = array(
                'filelink' => asset($dir . '/' .$filename),
                'filename' => $filename,
                'id' => $attachment->id
            );
            
            // echo stripslashes(json_encode($array));

            return $array;
            
        }
    }

    public function uploadFile(Request $request)
    {

        // copy($_FILES['file']['tmp_name'], '/files/'.$_FILES['file']['name']);

        $file = $request->file('file');

        $attachable_type = 'App\Post';

        if ($request->name == 'post') $attachable_type = 'App\Post';

        if ($request->name == 'page') $attachable_type = 'App\Page';

        if ($request->name == 'property') $attachable_type = 'App\Property';

        $file->move('uploads/files/' . $request->name, $_FILES['file']['name']);
                            
        $array = array(
            'filelink' => asset('uploads/files/'. $request->name . '/' . $_FILES['file']['name']),
            'filename' => $_FILES['file']['name']
        );        

        $attachment = new Attachment;

        $attachment->attachable_type = $attachable_type;
        $attachment->name = $request->name;
        $attachment->file = $_FILES['file']['name'];
        $attachment->type = 'pdf';

        $attachment->save();

        echo stripslashes(json_encode($array));

    }


}
