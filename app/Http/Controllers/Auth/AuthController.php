<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Customer;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

use Illuminate\Http\Request;
use Auth;
use Mail;
use DB;

use App\Services\GetBaseCRM;
use App\Services\UserTrack;

use Socialite;

class AuthController extends Controller
{

    protected $user_track;

    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
        $this->user_track = new UserTrack();
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    public function getLogin()
    {
        if (Auth::user()->check()) return redirect('admin/dashboard');

        return view('auth.login');
    }


    public function postLogin(Request $request)
    {
        $field = filter_var($request->input('email'), FILTER_VALIDATE_EMAIL) ? 'email' : 'username';

        $request->merge([$field => $request->input('email'), 'active' => 1]);

        $auth = $this->authenticate($request, $request->remember, $field);

        if ($auth == 'user') return response()->json(array('log' => 1, 'redirect' => '/admin/dashboard'));

        if ($auth == 'customer') return redirect('account');

        return redirect()->back();
    }


    public function authenticate(Request $request, $remember, $field)
    {
        if (Auth::user()->attempt($request->only($field, 'password', 'active'), $remember)) {

            return 'user';
        }

        if (Auth::customer()->attempt($request->only($field, 'password', 'active'), $remember)) {

            return 'customer';
        }
    }


    public function getLogout()
    {
        if (Auth::user()->check()) {
            Auth::user()->logout();
            return redirect('auth/login');
        }

        if (Auth::customer()->check()) {
            Auth::customer()->logout();
            return redirect()->route('login', trans('url.login'));
        }

        return redirect()->back();

    }


    // Customer registration
    public function postRegister(Request $request)
    {

        $validator = \Validator::make($request->all(), [
            'email' => 'required|unique:customers',
            'password' => 'required|confirmed',
            'firstname' => 'required',
            'location' => 'required',
            'g-recaptcha-response' => 'required|recaptcha'
        ]);

        if ($validator->fails()) {
            return response()->json(array('status' => 500, 'monolog' => array('title' => 'errors', 'message' => $validator->errors() )));
        }

        DB::beginTransaction();

        $confirmation_code = str_random(30);

        $customer = new Customer;

        $customer->username = $customer->getUsername($request->firstname);
        $customer->email = $request->email;
        $customer->password = \Hash::make($request->password);
        $customer->firstname = $request->firstname;
        $customer->lastname = $request->lastname;
        $customer->address = $request->address;

        $customer->city = $request->location;

        // find province, country
        // $city = \App\City::where('city_name', $request->city)->first();

        // $customer->city = $request->city;
        // $customer->province = $city->province->province_name;
        // $customer->country = $city->province->country->name;

        $customer->confirmation_code = $confirmation_code;

        $customer->save();

        // send confirm email
        $this->sendConfirmationEmail($customer->firstname, $customer->email, $confirmation_code);

        // $request->session()->flash('alert-success', 'Thankyou for registration. Please check your email address to activate your account.');

        // Add this contact to CRM
        $data = $request->all();
        $data['ip'] = $request->server('SERVER_ADDR');
        $data['lang'] = substr($request->server('HTTP_ACCEPT_LANGUAGE'), 0, 2);
        $data['tags'] = $this->user_track->get();
        $crm = new GetBaseCRM;
        $crm->customer_add($data);

        DB::commit();

        return response()->json(array('status' => 200, 'monolog' => array('title' => 'success', 'message' => 'Thanks for registration. Please check your email to activate your account.')));
    }

    public function sendConfirmationEmail($firstname, $email, $confirmation_code)
    {

        Mail::send('emails.confirm', ['firstname' => $firstname, 'confirmation_code' => $confirmation_code], function($message) use ($email, $firstname) {

            $message->from('no-reply@villabalisale.com', 'Kibarer');

            $message->to($email, $firstname)->subject('Verify your email address');
        });

        return true;
    }

    public function getCustomerLogin()
    {        
        if (\Auth::customer()->check()) return redirect()->route('account', trans('url.account'));

        return view('pages.login');
    }

    public function getCustomerRegister()
    {        
        //
        if (\Auth::customer()->check()) return redirect()->route('account', trans('url.account'));

        return view('pages.register');
    }


    public function confirm($confirm, $confirmation_code)
    {
        if( ! $confirmation_code)
        {
            throw \App::abort(404);
        }

        $customer = \App\Customer::where('confirmation_code', $confirmation_code)->first();

        if ( ! $customer)
        {
            throw \App::abort(404);
        }

        $customer->active = 1;
        $customer->confirmed = 1;
        $customer->confirmation_code = null;
        $customer->save();

        return redirect()->route('login', trans('url.login'))->with('confirm-success', trans('notification.confirm_success'));
    }


    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return Response
     */
    public function handleProviderCallback()
    {
        
        $user = Socialite::driver('facebook')->user();

        $pass = \Hash::make('facebook');
        $results = DB::select("SELECT * FROM customers WHERE username = :username AND email=:email", ['username' => $user['id'], 'email' => $user['email'] ]);
        if(count($results) < 1)
        {
            $customer = new Customer;
            $customer->username = $user['id'];
            $customer->email = $user['email'];
            $customer->password = $pass;
            $customer->firstname = $user['first_name'];
            $customer->lastname = $user['last_name'];
            $customer->image_profile = $user->avatar_original;
            $customer->active = 1;
            $customer->save();

            // Add this contact to CRM
            $data['firstname'] = $customer->firstname;
            $data['lastname'] = $customer->lastname;
            $data['email'] = $customer->email;
            $data['ip'] = '';
            $data['lang'] = '';
            $data['tags'] = $this->user_track->get();

            // Track facebook login
            $this->user_track->track('source','facebook');

            $crm = new GetBaseCRM;
            $crm->customer_add($data);

            // Untrack facebook login after new contact added to CRM
            $this->user_track->untrack('source');
            
        }        

        if (Auth::customer()->attempt(['username' => $user['id'], 'password' => 'facebook', 'email' => $user['email'], 'active' => '1'])) {
            return redirect()->intended('account');
        }
        
    }


}
