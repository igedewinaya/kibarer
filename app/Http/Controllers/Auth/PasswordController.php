<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;

use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Display the form to request a password reset link.
     *
     * @return \Illuminate\Http\Response
     */
    public function getEmail()
    {
        if (\Input::get('user') == 'admin') return view('auth.password');
        
        return view('auth.password-customer');
    }

    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postEmail(Request $request)
    {

        $this->validate($request, ['email' => 'required|email']);

        if ($request->type == 'user') {

            $response = Password::user()->sendResetLink($request->only('email'), function (Message $message) {
                $message->subject($this->getEmailSubject());
            });

        } else {

            $response = Password::customer()->sendResetLink($request->only('email'), function (Message $message) {
                $message->subject($this->getEmailSubject());
            });

        }

        switch ($response) {
            case Password::RESET_LINK_SENT:
            // return redirect()->back()->with('status', trans($response));
            return response()->json(array('status' => 200, 'monolog' => array('title' => 'success', 'message' => 'passsword reset link has been sent.')));

            case Password::INVALID_USER:
            // return redirect()->back()->withErrors(['email' => trans($response)]);
            return response()->json(array('status' => 500, 'monolog' => array('title' => 'error', 'message' => trans($response))));
        }
    }

    /**
     * Get the e-mail subject line to be used for the reset link email.
     *
     * @return string
     */
    protected function getEmailSubject()
    {
        return property_exists($this, 'subject') ? $this->subject : 'Your Password Reset Link';
    }

    /**
     * Display the password reset view for the given token.
     *
     * @param  string  $token
     * @return \Illuminate\Http\Response
     */
    public function getReset($type, $token = null)
    {
        if (is_null($token)) {
            throw new NotFoundHttpException;
        }

        if ($type == 'user') return view('auth.reset')->with('token', $token);

        return view('auth.reset-customer')->with('token', $token);
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postReset(Request $request)
    {
        $this->validate($request, [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed|min:6',
            ]);

        $credentials = $request->only(
            'email', 'password', 'password_confirmation', 'token'
            );

        if ($request->type == 'user') {

            $response = Password::user()->reset($credentials, function ($user, $password) {
                $this->resetPassword($user, $password);
            });

        } else {

            $response = Password::customer()->reset($credentials, function ($user, $password) {
                $this->resetPassword($user, $password);
            });
        }

        switch ($response) {
            case Password::PASSWORD_RESET:
            return response()->json(array('status' => 200, 'monolog' => array('title' => 'success', 'message' => 'passsword has been reseted.')));
            // return redirect($this->redirectPath())->with('status', trans($response));

            default:
            return response()->json(array('status' => 500, 'monolog' => array('title' => 'errors', 'message' => '' )));
            // return redirect()->back()
            // ->withInput($request->only('email'))
            // ->withErrors(['email' => trans($response)]);
        }
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Contracts\Auth\CanResetPassword  $user
     * @param  string  $password
     * @return void
     */
    protected function resetPassword($user, $password)
    {
        $user->password = bcrypt($password);

        $user->save();

        // Auth::user()->login($user);
    }

}
