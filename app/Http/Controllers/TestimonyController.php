<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Testimony;

class TestimonyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $category = $request->category;
        $status = $request->status;

        // datatable parameter
        $draw = $request->draw;
        $start = $request->start;
        $length = $request->length;
        $search = $request->search['value'];

        // sorting
        $column = 'id';
        $sort = $request->order[0]['dir'] ? $request->order[0]['dir'] : 'desc'; //asc

        // new object
        $testimonials = new Testimony;

        $testimonials = $testimonials->select('testimonials.*')
            ->join('users', 'users.id', '=', 'testimonials.user_id');

        // with user
        $testimonials = $testimonials->with('user');

        $testimonials = $testimonials->with('attachments');

        // searching
        if ($search) {

            $testimonials = $testimonials->where(function ($q) use ($search) {
                    $q->where('testimonials.title', 'like', $search . '%')
                        ->orWhere('testimonials.content', 'like', $search . '%');
                });
        }

        // total records
        $count = $testimonials->count();

        // pagination
        $testimonials = $testimonials->take($length)->skip($start);

        // order
        if ($request->order[0]['column']) {

            $column = $request->columns[$request->order[0]['column']]['data'];

            if ($column == 'user.username') {

                $testimonials = $testimonials->orderBy('users.username', $sort);
            } else {

                $testimonials = $testimonials->orderBy('testimonials.' . $column, $sort);
            }            

        } else {

            $testimonials = $testimonials->orderBy('testimonials.' . $column, $sort);
        }

        // get data
        $testimonials = $testimonials->get();

        // datatable response
        $respose = [
                "draw" => $draw,
                "recordsTotal" => $count,
                "recordsFiltered" => $count,
                "data" => $testimonials

            ];

        return $respose;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $validator = \Validator::make($request->all(), [
            // 'property_id' => 'required',
            'customer_name' => 'required',
            'title' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(array('status' => 500, 'monolog' => array('title' => 'errors', 'message' => implode($validator->errors()->all(), '<br>') )));
        }


        $testimony = new Testimony;
        $testimony->user_id = \Auth::user()->get()->id;
        // $testimony->property_id = $request->property_id;
        $testimony->customer_name = $request->customer_name;
        $testimony->title = $request->title;
        $testimony->content = $request->content;
        $testimony->status = ($request->status) ? $request->status : 0;

        $testimony->save();

        foreach ($request->images as $key => $id) {

            $testimony->attachments()->save(\App\Attachment::find($id));
        }

        return response()->json(array('status' => 200, 'monolog' => array('title' => 'success', 'message' => 'object has been saved')));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $validator = \Validator::make($request->all(), [
            // 'property_id' => 'required',
            'customer_name' => 'required',
            'title' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(array('status' => 500, 'monolog' => array('title' => 'errors', 'message' => implode($validator->errors()->all(), '<br>') )));
        }

        $testimony = Testimony::find($id);
        $testimony->user_id = \Auth::user()->get()->id;
        // $testimony->property_id = $request->property_id;
        $testimony->customer_name = $request->customer_name;
        $testimony->title = $request->title;
        $testimony->content = $request->content;
        $testimony->status = ($request->status) ? $request->status : 0;

        $testimony->save();


        // images
        if ($request->images) {

            foreach ($testimony->attachments as $key => $value) {

                if (!in_array($value->id, $request->images)) \App\Attachment::find($value->id)->delete();
            }

            foreach ($request->images as $key => $value) {

                $attachment = $testimony->attachments()->find($value);

                if (count($attachment) == 0) {

                    $attachment = \App\Attachment::find($value);

                    $attachment->attachable_id = $testimony->id;

                    $attachment->order = $key;

                    $attachment->save();

                } else {

                    $attachment->order = $key;

                    $attachment->save();
                }

            }

        } else {

            $testimony->attachments()->delete();
        }

        return response()->json(array('status' => 200, 'monolog' => array('title' => 'success', 'message' => 'object has been updated')));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $testimony = Testimony::find($id);

        $testimony->delete();

        return response()->json(array('status' => 200, 'monolog' => array('title' => 'delete success', 'message' => 'object has been deleted'), 'id' => $id));
    }

    public function export(Request $request)
    {
        $ids = explode(',', $request->id);

        $admin = \Auth::user()->get();

        $file_name = $admin->id . 'testimony';

        $take = ($request->take) ? $request->take : 'all';

        $testimonials = new Testimony;

        if ($request->take == 'selected') {

            $testimonials = $testimonials->whereIn('testimonials.id', $ids);

            // $testimonials = $testimonials->take($take);
        }

        $testimonials = $testimonials->orderBy('id', 'desc');

        $testimonials = $testimonials->get();

        $data = json_decode(json_encode($testimonials), true);

        \Excel::create($file_name, function($excel) use ($data) {

            $excel->sheet('Sheet1', function($sheet) use ($data) {

                $sheet->setOrientation('landscape');

                $sheet->fromArray($data);

            });

        })
        // ->export('csv');
        ->store('csv', 'export/csv');

        return $file_name . '.csv';
    }

    public function remove(Request $request)
    {
        //

        foreach ($request->id as $id) {
            
            $testimony = Testimony::find($id);

            $testimony->delete();
        }

        return response()->json(array('status' => 200, 'monolog' => array('title' => 'delete success', 'message' => 'object has been deleted'), 'id' => $request->id));
    }

}
