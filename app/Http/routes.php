<?php


Session::set('currency', 'usd');
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
 *  Set up locale and locale_prefix if other language is selected
 */
if (in_array(Request::segment(1), Config::get('app.alt_langs'))) {

    App::setLocale(Request::segment(1));

    Config::set('app.locale_prefix', Request::segment(1));
}


/*
 * Set up route patterns - patterns will have to be the same as in translated route for current language
 */
foreach(Lang::get('url') as $k => $v) {

    Route::pattern($k, $v);
}

Route::get('make_dir', function () {

    $dir = date('Y/m/d');

    File::makeDirectory($dir, 0755, true);

    return 'success';
});

Route::get('test_pdf', function () { 

    #return view('emails.enquiry');

    $pdf = PDF::loadView('pdf.property_new');

    return $pdf->stream();

});

// API
Route::group(['prefix' => 'api'], function() {

    Route::resource('branch', 'BranchController');

    Route::resource('city', 'CityController');

    Route::resource('message', 'ContactController');

    Route::resource('country', 'CountryController');

    Route::resource('customer', 'CustomerController');

    Route::resource('enquiry', 'EnquiryController');

    Route::resource('locale', 'LocaleController');

    Route::resource('log_customer', 'LogCustomerController');

    Route::resource('log_user', 'LogUserController');

    Route::resource('notification', 'NotificationController');

    Route::resource('page', 'PageController');

    Route::resource('page_locale', 'PageLocaleController');

    Route::resource('page_meta', 'PageMetaController');

    Route::resource('page_term', 'PageTermController');

    Route::resource('post', 'PostController');

    Route::resource('post_locale', 'PostLocaleController');

    Route::resource('post_meta', 'PostMetaController');

    Route::resource('post_term', 'PostTermController');

    Route::resource('properties', 'PropertyController');

    Route::resource('property_locale', 'PropertyLocaleController');

    Route::resource('property_meta', 'PropertyMetaController');

    Route::resource('property_term', 'PropertyTermController');

    Route::resource('province', 'ProvinceController');

    Route::resource('role', 'RoleController');

    Route::resource('category', 'CategoryController');

    Route::resource('testimony', 'TestimonyController');

    Route::resource('newsletter', 'NewsletterController');

    Route::resource('user', 'UserController');

    Route::resource('wishlist', 'WishlistController');

    Route::resource('attachment', 'AttachmentController');

    Route::resource('setting', 'SettingController');

    Route::get('attachment/get/image', ['as' => 'api.attachment.get.image', 'uses' => 'AttachmentController@getImage']);

    Route::post('upload/image', ['as' => 'api.attachment.upload.image', 'uses' => 'AttachmentController@uploadImage']);

    Route::post('upload/file', ['as' => 'api.attachment.upload.file', 'uses' => 'AttachmentController@uploadFile']);

    Route::post('sellproperty', ['as' => 'sellproperty.store', 'uses' => 'PropertyController@sellProperty']);

    Route::post('property/delete/{id}', ['as' => 'api.property.delete', 'uses' => 'PropertyController@delete']);

    Route::post('property_meta/thumb', ['as' => 'api.thumb.store', 'uses' => 'PropertyMetaController@thumb']);

    Route::post('translate/{to}',['as' => 'api.translate', 'uses' => 'BingTranslateController@trans']);

    Route::post('property/remove', ['as' => 'api.property.remove', 'uses' => 'PropertyController@remove']);

    Route::post('property/set_status', ['as' => 'api.property.set_status', 'uses' => 'PropertyController@setStatus']);

    Route::post('enquiry/remove', ['as' => 'api.enquiry.remove', 'uses' => 'EnquiryController@remove']);

    Route::post('customer/remove', ['as' => 'api.customer.remove', 'uses' => 'CustomerController@remove']);

    Route::post('testimony/remove', ['as' => 'api.testimony.remove', 'uses' => 'TestimonyController@remove']);

    Route::post('contact/remove', ['as' => 'api.contact.remove', 'uses' => 'ContactController@remove']);

    Route::post('newsletter/remove', ['as' => 'api.newsletter.remove', 'uses' => 'NewsletterController@remove']);

    Route::post('post/remove', ['as' => 'api.post.remove', 'uses' => 'PostController@remove']);

    Route::post('user/remove', ['as' => 'api.user.remove', 'uses' => 'UserController@remove']);

    Route::post('branch/remove', ['as' => 'api.branch.remove', 'uses' => 'BranchController@remove']);


});


// Back-End
Route::group(['middleware' => 'auth'], function () {

    Route::group(['prefix' => 'admin'], function () {

        // dashbord
        Route::get('/', function() {

            return redirect('/admin/dashboard');
        });

        Route::get('dashboard',['as' => 'admin.dashboard', 'uses' => 'AdminController@dashboard']);

        // property
        Route::get('properties/{term?}',['as' => 'admin.properties', 'uses' => 'AdminController@properties'])->where('term', '(.*)');

        // enquiry
        Route::get('enquiries/{term?}',['as' => 'admin.enquiries', 'uses' => 'AdminController@enquiries']);


        // customer
        Route::get('customers/{term?}',['as' => 'admin.customers', 'uses' => 'AdminController@customers']);

        // Page
        Route::get('pages/{term?}',['as' => 'admin.pages', 'uses' => 'AdminController@pages']);

        // Post
        Route::get('blog/{term?}',['as' => 'admin.posts', 'uses' => 'AdminController@posts']);

        // my-account
        Route::get('my-account',['as' => 'admin.my_account', 'uses' => 'AdminController@my_account']);

        // accounts
        Route::get('accounts',['as' => 'admin.accounts', 'uses' => 'AdminController@accounts']);

        // branches
        Route::get('branches',['as' => 'admin.branches', 'uses' => 'AdminController@branches']);

        // setting
        Route::get('settings',['as' => 'admin.setting', 'uses' => 'AdminController@settings']);

        // about
        Route::get('about',['as' => 'admin.about', 'uses' => 'AdminController@about']);


        // PDF Export
        Route::group(['prefix' => 'pdf'], function () {

            Route::get('property/{id}', ['as' => 'admin.pdf.property', 'uses' => 'PdfController@property']);
        });

        Route::group(['prefix' => 'export'], function () {

            Route::group(['prefix' => 'property'], function () {

                Route::post('csv', ['as' => 'export.property.csv', 'uses' => 'PropertyController@export']);

                Route::post('pdf', ['as' => 'export.property.pdf', 'uses' => 'PropertyController@exportPDF']);

                Route::get('pdf', function () {

                    $property = \App\Property::find(1);

                    return view('pdf.property', compact('property'));
                });
            });

            Route::group(['prefix' => 'enquiry'], function () {

                Route::post('csv', ['as' => 'export.enquiry.csv', 'uses' => 'EnquiryController@export']);
            });

            Route::group(['prefix' => 'customer'], function () {

                Route::post('csv', ['as' => 'export.customer.csv', 'uses' => 'CustomerController@export']);
            });

            Route::group(['prefix' => 'testimony'], function () {

                Route::post('csv', ['as' => 'export.testimony.csv', 'uses' => 'TestimonyController@export']);
            });

            Route::group(['prefix' => 'contact'], function () {

                Route::post('csv', ['as' => 'export.contact.csv', 'uses' => 'ContactController@export']);
            });

            Route::group(['prefix' => 'post'], function () {

                Route::post('csv', ['as' => 'export.post.csv', 'uses' => 'PostController@export']);
            });

            Route::group(['prefix' => 'page'], function () {

                Route::post('csv', ['as' => 'export.page.csv', 'uses' => 'PageController@export']);
            });

            Route::group(['prefix' => 'user'], function () {

                Route::post('csv', ['as' => 'export.user.csv', 'uses' => 'UserController@export']);
            });

            Route::group(['prefix' => 'branch'], function () {

                Route::post('csv', ['as' => 'export.branch.csv', 'uses' => 'BranchController@export']);
            });

            Route::group(['prefix' => 'newsletter'], function () {

                Route::post('csv', ['as' => 'export.newsletter.csv', 'uses' => 'NewsletterController@export']);
            });

        });

        // Support Ticket
        Route::post('send_ticket', ['as' => 'admin.send_ticket' , 'uses' => function (Illuminate\Http\Request $request) {

            $user = \Auth::user()->get();

            Mail::raw($request->message, function ($m) use ($user, $request) {

                $m->from($user->email, $user->firstname);

                $m->to(\Config::get('mail.support'), 'Support')->subject('KIBARER Ticket: '. $request->subject);

            });

            return response()->json(array('status' => 200, 'monolog' => array('title' => 'success', 'message' => 'Your message has been sent successfully.')));
            // return redirect()->back();

        }]);

    });

});


// User Auth
Route::controllers([

    'auth' => 'Auth\AuthController',

    'password' => 'Auth\PasswordController',
]);


// Front-End
Route::group(['prefix' => Config::get('app.locale_prefix')], function() {

    Route::get('currency/{currrency}', ['as' => 'set_currency', 'uses' => function ($currency) {

        Session::set('currency', $currency);

        // return Session::get('currency');

        return redirect()->back();
    }]);

    Route::get('test_email', function () {

        Mail::send('emails.property-listing', [], function ($m) {
            $m->from('hello@app.com', 'Your Application');

            $m->to('gusmanwidodo@gmail.com', 'gusman')->subject('Your Reminder!');
        });

        return 'sucess';
    });

    // forgot password
    Route::get('password/email', ['as' => 'forgot_password', 'uses' => 'Auth\PasswordController@getEmail']);
    Route::post('password/email', ['as' => 'post_forgot_password', 'uses' => 'Auth\PasswordController@postEmail']);

    Route::get('register/success', ['as' => 'register_success', 'uses' => function () {

        return view('pages.register-success');
    }]);
    
    // customer
    Route::get('{login}', ['as' => 'login', 'uses' => 'Auth\AuthController@getCustomerLogin']);
    Route::post('{login}',['as' => 'login.attempt', 'uses' => 'Auth\AuthController@postLogin']);

    // LOGIN SOCIAL
    Route::get('login/fb', 'Auth\AuthController@redirectToProvider');
    Route::get('login/fb/callback', 'Auth\AuthController@handleProviderCallback');

    Route::get('{logout}', ['as' => 'logout', 'uses' => 'Auth\AuthController@getLogout']);

    Route::get('{register}', ['as' => 'register', 'uses' => 'Auth\AuthController@getCustomerRegister']);
    Route::post('{register}',['as' => 'register.store', 'uses' => 'Auth\AuthController@postRegister']);

    Route::get('email/{confirm}/{confirmation_code}', [
        'as' => 'confirm',
        'uses' => 'Auth\AuthController@confirm'
    ]);

    Route::group(['middleware' => 'auth.customer'], function () {

        Route::get('/{account}/', ['as' => 'account', 'uses' => 'CustomerController@account']);

        Route::get('/{account}/{wishlist}', ['as' => 'account.wishlist', 'uses' => 'CustomerController@wishlist'])
            ->where('wishlist', trans('url.wishlist'));

        Route::get('/{account}/{setting}', ['as' => 'account.setting', 'uses' => 'CustomerController@setting'])
            ->where('wishlist', trans('url.setting'));

    });


    // home
    Route::get('/',['as' => 'home', 'uses' => 'PageController@home']);

    // about
    Route::get('{about}',['as' => 'about', 'uses' => 'PageController@about']);

    // resource
    Route::get('{resource}',['as' => 'resource', 'uses' => 'PageController@resource']);

    // sold villas
    Route::get('{sold_villa}',['as' => 'sold_villa', 'uses' => 'PageController@soldVilla']);

    // sitemap
    Route::get('{sitemap}',['as' => 'sitemap', 'uses' => 'PageController@sitemap']);

    // estimate land
    Route::match(['GET', 'POST'], 'estimate', ['as' => 'estimate', 'uses' => 'PageController@estimate']);

    // contact
    Route::any('{contact}',['as' => 'contact', 'uses' => 'PageController@contact']);

    // sell_property
    Route::get('{sell_property}',['as' => 'sell_property', 'uses' => 'PageController@sellProperty']);

    // lawyer_notary
    Route::any('{lawyer_notary}',['as' => 'lawyer_notary', 'uses' => 'PageController@lawyerNotary']);

    // testimony
    Route::any('{testimonials}/{term?}',['as' => 'testimonials', 'uses' => 'PageController@testimony'])->where('term', '(.*)');

    // search
    Route::any('{search}/{category?}/{type?}/{area?}',['as' => 'search', 'uses' => 'PropertyController@search']);

    // property
    Route::get('{property}/{term?}',['as' => 'property', 'uses' => 'PropertyController@detail'])->where('term', '(.*)');

    // post
    Route::get('{blog}/{term?}',['as' => 'blog', 'uses' => 'PostController@detail'])->where('term', '(.*)');

    // page
    Route::get('{page?}',['as' => 'page', 'uses' => 'PageController@index']);

});

Route::group(['middleware' => 'auth'], function () {

    Route::group(['prefix' => 'system/ajax'], function () {

        Route::group(['prefix' => 'notifications'], function () {

            Route::any('insert', 'AnalyticsController@insert');

            Route::any('getall', 'AnalyticsController@index');

            Route::any('getunread', 'AnalyticsController@getUnread');
        });

        Route::group(['prefix' => 'blog'], function () {

            Route::any('index', 'BlogController@index');

            Route::any('create', 'BlogController@create');

            Route::get('retrieve/{id}', ['as' => 'id', 'uses' => 'BlogController@retrieve']);

            Route::any('delete/{id}', ['as' => 'id', 'uses' => 'BlogController@destroy']);
        });

        Route::group(['prefix' => 'analytics'], function () {

            Route::any('getall', 'AnalyticsController@getData');
        });

        Route::group(['prefix' => 'customer'], function () {

            Route::any('login', 'CustomerController@login');

            Route::any('register', 'CustomerController@register');

            Route::any('get/{id}', 'CustomerController@show');

            Route::any('store', 'CustomerController@store');

            Route::any('destroy/{id}', 'CustomerController@destroy');

        });

        Route::group(['prefix' => 'testimony'], function () {

            Route::any('get/{id}', 'CustomerController@showTestimony');

            Route::any('store', 'CustomerController@storeTestimony');

            Route::any('destroy/{id}', 'CustomerController@destroyTestimony');

        });

        Route::group(['prefix' => 'message'], function () {

            Route::any('destroy/{id}', 'ContactController@destroy');

        });

        Route::group(['prefix' => 'property'], function () {

            Route::any('translate/get/{id}', 'PropertiesController@getTranslate');

            Route::any('translate/store', 'PropertiesController@storeTranslate');

            Route::any('get/{id}', 'PropertiesController@show');

            Route::any('store', 'PropertiesController@store');

            Route::any('destroy/{id}', 'PropertiesController@destroy');

            Route::any('image/destroy/{id}', 'PropertiesController@destroyImage');

            Route::any('data/{category}/{status}/', 'PropertiesController@index');

        });

        Route::group(['prefix' => 'category'], function () {

            Route::any('translate/get/{id}', 'CategoryController@getTranslate');

            Route::any('translate/store', 'CategoryController@storeTranslate');

            Route::any('get/{id}', 'CategoryController@show');

            Route::any('store', 'CategoryController@store');

            Route::any('destroy/{id}', 'CategoryController@destroy');

        });

        Route::group(['prefix' => 'inquiry'], function () {

            Route::any('get/{id}', 'InquiryController@show');

            Route::any('store', 'InquiryController@store');

            Route::any('destroy/{id}', 'InquiryController@destroy');

        });

        Route::group(['prefix' => 'account'], function () {

            Route::any('prepare', 'UserController@invite');

            Route::any('store', 'UserController@store');

            Route::any('update', 'UserController@update');

            Route::any('profile/store', 'UserController@storeProfile');

            Route::any('get/{id}', 'UserController@show');

            Route::any('destroy/{id}', 'UserController@destroy');

        });

        Route::group(['prefix' => 'branch'], function () {

            Route::any('store', 'BranchController@store');

            Route::any('get/{id}', 'BranchController@show');

            Route::any('destroy/{id}', 'BranchController@destroy');

        });

        Route::group(['prefix' => 'settings'], function () {

            Route::group(['prefix' => 'general'], function () {

                Route::any('get', 'SystemController@getGeneral');

                Route::any('set', 'SystemController@setGeneral');
            });

            Route::group(['prefix' => 'social'], function () {

                Route::any('get', 'SystemController@getSocial');

                Route::any('set', 'SystemController@setSocial');
            });

            Route::group(['prefix' => 'currency'], function () {

                Route::any('get', 'SystemController@getExchange');

                Route::any('update', 'SystemController@updateExchange');

                Route::any('set', 'SystemController@setExchange');

                Route::any('auto/{state}', ['as' => 'state', 'uses' => 'SystemController@setExchangeAuto']);
            });

            Route::any('reindexdata', 'SystemController@reindexData');

            Route::any('clearcache', 'SystemController@clearCache');
        });
    });
});

