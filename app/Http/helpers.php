<?php

function baseUrl()
{

    $fallback = Config::get('app.fallback_locale');

    $lang = App::getLocale();

    $locale = ($lang == $fallback ? '' : '/' . $lang);

    return url() . $locale;
}

function humanize($value)
{

    if ($value > 1000000000) {

        $head = $value / 1000000000;

        $head = number_format((float)$head, 1, '.', '');

        return $head . 'b';

    } elseif ($value > 1000000) {

        $head = $value / 1000000;

        $head = number_format((float)$head, 1, '.', '');

        return $head . 'm';

    } elseif ($value > 1000) {

        $head = $value / 1000;

        $head = number_format((float)$head, 1, '.', '');

        return $head . 'k';

    } else {

        return $value;
    }
}

function getSocialAccounts()
{

    return json_decode(File::get(storage_path('json/social_accounts.json')));
}

function currency($cur)
{

    $currencies = json_decode(File::get(storage_path('json/conversion.json')), true);


    return $currencies[$cur];
}

function renderCategory($categories, $count = 0)
{
    $space = '';

    $t = $count;

    for ($i = 0; $i < $t; $i++) {
        $space .= '&nbsp;&nbsp;&nbsp;&nbsp;';
    }

    foreach ($categories as $category) {

        if ($category->parent) {
            if ($category->id == $category->parent->id) break;
        }

        echo '<m-option value="' . $category->id . '">' . $space . $category->name() . '</m-option>';

        if ($category->childs) {

            renderCategory($category->childs, ++$count);
        }
    }
}

function category_slug($term)
{

    $slug = term_slug($term);

    return array_reverse($slug);
}

function term_slug($term, $slug = array())
{

    $slug[] = $term->name;

    // TO DO
    if ($term->parent) {

        $slug = term_slug($term->parent, $slug);

    }

    return $slug;

}

function rootCategory($category)
{
    if ($category->parent) rootCategory($category->parent);
    else return $category;
}

function propertyStatus($status)
{

    switch ($status) {
        case -2:
            return 'MODERATION';
        case -1:
            return 'HIDDEN';
        case 0:
            return 'UNAVAILABLE';
        case 1:
            return 'AVAILABLE';

        default:
            return 'UNAVAILABLE';
    }
}

function statusToInteger($status)
{

    switch ($status) {
        case 'request':
            return -2;
        case 'hidden':
            return -1;
        case 'unavailable':
            return 0;
        case 'available':
            return 1;

        default:
            return 0;
    }
}

function checkWishlist($id, $property_id)
{

    $count = \App\WishList::where('customer_id', $id)
        ->where('property_id', $property_id)
        ->count();

    return $count > 0 ? true : false;
}

function convertCurrency($amount, $cur, $target)
{

    $cur = strtolower($cur);

    $target = strtolower($target);

    if ($cur == $target) return number_format($amount);

    $usd = $amount / currency($cur);

    $result = $usd * currency($target);

    return number_format($result);
}


function convert_curr($amount, $cur, $target)
{

    $cur = strtolower($cur);

    $target = strtolower($target);

    if ($cur == $target) return $amount;

    $usd = $amount / currency($cur);

    $result = $usd * currency($target);

    return $result;
}


function dateRange($first, $last, $step = '+1 day', $format = 'd-m-Y')
{

    $dates = array();
    $current = strtotime($first);
    $last = strtotime($last);

    while ($current <= $last) {

        $dates[] = date($format, $current);
        $current = strtotime($step, $current);
    }

    return $dates;
}

function property_term($property)
{

    $slug = $property->slug;

    $categories = implode('/', $property->categories->lists('slug')->toArray());

    // $tags = implode('/', $property->tags->lists('slug')->toArray());

    return implode('/', [$categories, $slug]);

}

function FirstReplace($string)
{
    return ucwords(str_replace('-', ' ', $string));
}

function replaceComma($string)
{
    return str_replace(',', '', $string);
}

function checkEmptyTwoString($string1, $string2, $initString, $newString)
{
    if (empty($string1) && empty($string2)) {
        return $newString;
    } elseif (empty($string1)) {
        return $initString . ' ' . $string2;
    } elseif (empty($string2)) {
        return $initString . ' ' . $string1;
    } else {
        return $initString . ' ' . $string1 . ' ' . $string2;
    }
}

function checkCheckBox($item, $nameField)
{

        if ($item == $nameField) {
            return 'checked="checked"';
        }


    return "";

}

function getInput($name)
{
    if (\Input::get($name)){
        return \Input::get($name);
    }else{
        return [];
    }
}