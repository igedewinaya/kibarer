<?php

return [
    // 'rental' => [
    //     'wireless internet' => 'wifi',
    //     'pool' => 'pool',
    //     'kitchen' => 'kitchen',
    //     'air conditioning' => 'ac_unit',
    //     'breakfast' => 'free_breakfast',
    //     'cable tv' => 'live_tv',
    //     // 'essentials' => 'android',
    //     // 'family/kid friendly' => 'child_friendly',
    //     // 'fire extinguisher' => 'whatshot',
    //     // 'first aid kit' => 'healing',
    //     // 'free parking on premises' => 'local_parking',
    //     // 'hair dryer' => 'toys',
    //     // 'heating' => 'whatshot',
    //     // 'hot tub' => 'hot_tub',
    //     // 'internet' => 'public',
    //     // 'laptop friendly workspace' => 'laptop_chromebook',
    //     // 'pets allowed' => 'pets',
    //     // 'shampoo' => 'android',
    //     // 'smoking allowed' => 'smoking_rooms',
    //     'suitable for events' => 'cake',
    //     // 'tv' => 'tv',
    //     // 'washer' => 'local_laundry_service',
    //     // 'wheelchair accessible' => 'accessible'
    //     'garage' => 'garage'
    // ],

    'sale' => [
        'wireless internet' => 'wifi',
        'pool' => 'pool',
        'kitchen' => 'kitchen',
        'air conditioning' => 'ac_unit',

        'breakfast' => 'free_breakfast',

        'cable tv' => 'live_tv',
        // 'heating' => 'whatshot',
        // 'hot tub' => 'hot_tub',
        // 'internet' => 'public',
        'suitable for events' => 'cake',
        // 'tv' => 'tv',
        // 'washer' => 'local_laundry_service',
        // 'wheelchair accessible' => 'accessible'
        // 
        'garage' => 'garage',

        // aditional
        '24h security' => 'security',
        'DVD' => 'music_video',
        'washer or laundry' => 'local_laundry_service',
        'gym' => 'accessibility',
        'yoga' => 'airline_seat_flat_angled',
    ]
];
