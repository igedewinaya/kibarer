<?php

return [

    "super" => [

        "dashboard" => [

            "dashboard" => "dashboard"
        ],

        "villas" => [

            "properties?category=villas-for-sale&status=available" => "available",
            "properties?category=villas-for-sale&status=sold" => "sold",
            "properties?category=villas-for-sale&status=hidden" => "hidden",
            "properties?category=villas-for-sale&status=request" => "request"

        ],

        "villa rental" => [

            "properties?category=villas-for-rent&status=available" => "available",
            "properties?category=villas-for-rent&status=sold" => "sold",
            "properties?category=villas-for-rent&status=hidden" => "hidden",
            "properties?category=villas-for-rent&status=request" => "request"

        ],

        "lands" => [

            "properties?category=land&status=available" => "available",
            "properties?category=land&status=sold" => "sold",
            "properties?category=land&status=hidden" => "hidden",
            "properties?category=land&status=request" => "request"

        ],

        "enquiries" => [

            "enquiries" => "manage enquiries"

        ],

        "customers" => [

            "customers" => "manage customers",
            "customers/testimonials" => "testimonials",
            "customers/messages" => "messages",
            "customers/newsletters" => "newsletters"

        ],

        "blog" => [

            "blog" => "blog",
            "blog/categories" => "categories",
            // "blog/comments" => "comments",
            // "blog/settings" => "settings"
        ],

        // "pages" => [

        //     "pages" => "manage pages"
        // ],

        "settings" => [

            "my-account" => "my account",
            "accounts" => "admin accounts",
            "branches" => "branches",
            "settings" => "settings",
            "about" => "about"
        ]
    ],

    "agent" => [

        "dashboard" => [

            "dashboard" => "dashboard"
        ],

        "villas" => [

            "properties?category=villa&status=available" => "available",
            "properties?category=villa&status=sold" => "sold",
            "properties?category=villa&status=hidden" => "hidden",
            "properties?category=villa&status=request" => "request"

        ],

        "villa rental" => [

            "properties?category=villa-rental&status=available" => "available",
            "properties?category=villa-rental&status=sold" => "sold",
            "properties?category=villa-rental&status=hidden" => "hidden",
            "properties?category=villa-rental&status=request" => "request"

        ],

        "lands" => [

            "properties?category=land&status=available" => "available",
            "properties?category=land&status=sold" => "sold",
            "properties?category=land&status=hidden" => "hidden",
            "properties?category=land&status=request" => "request"

        ],

        "enquiries" => [

            "enquiries" => "manage enquiries"

        ],

        // "customers" => [

        //     "customers" => "manage customers",
        //     "customers/testimonials" => "testimonials",
        //     "customers/messages" => "messages"

        // ],

        // "blog" => [

        //     "blog" => "blog",
        //     "blog/categories" => "categories",
        //     "blog/comments" => "comments",
        //     "blog/settings" => "settings"
        // ],

        // "pages" => [

        //     "pages" => "manage pages"
        // ],

        "settings" => [

            "my-account" => "my account",
            // "accounts" => "admin accounts",
            // "branches" => "branches",
            // "settings" => "settings",
            "about" => "about"
        ]
    ],

    "manager" => [

        "dashboard" => [

            "dashboard" => "dashboard"
        ],

        "villas" => [

            "properties?category=villa&status=available" => "available",
            "properties?category=villa&status=sold" => "sold",
            "properties?category=villa&status=hidden" => "hidden",
            "properties?category=villa&status=request" => "request"

        ],

        "villa rental" => [

            "properties?category=villa-rental&status=available" => "available",
            "properties?category=villa-rental&status=sold" => "sold",
            "properties?category=villa-rental&status=hidden" => "hidden",
            "properties?category=villa-rental&status=request" => "request"

        ],

        "lands" => [

            "properties?category=land&status=available" => "available",
            "properties?category=land&status=sold" => "sold",
            "properties?category=land&status=hidden" => "hidden",
            "properties?category=land&status=request" => "request"

        ],

        "enquiries" => [

            "enquiries" => "manage enquiries"

        ],

        // "customers" => [

        //     "customers" => "manage customers",
        //     "customers/testimonials" => "testimonials",
        //     "customers/messages" => "messages"

        // ],

        // "blog" => [

        //     "blog" => "blog",
        //     "blog/categories" => "categories",
        //     "blog/comments" => "comments",
        //     "blog/settings" => "settings"
        // ],

        // "pages" => [

        //     "pages" => "manage pages"
        // ],

        "settings" => [

            "my-account" => "my account",
            "accounts" => "admin accounts",
            // "branches" => "branches",
            // "settings" => "settings",
            "about" => "about"
        ]
    ],

    "superagent" => [

        "dashboard" => [

            "dashboard" => "dashboard"
        ],

        "villas" => [

            "properties?category=villa&status=available" => "available",
            "properties?category=villa&status=sold" => "sold",
            "properties?category=villa&status=hidden" => "hidden",
            "properties?category=villa&status=request" => "request"

        ],

        "villa rental" => [

            "properties?category=villa-rental&status=available" => "available",
            "properties?category=villa-rental&status=sold" => "sold",
            "properties?category=villa-rental&status=hidden" => "hidden",
            "properties?category=villa-rental&status=request" => "request"

        ],

        "lands" => [

            "properties?category=land&status=available" => "available",
            "properties?category=land&status=sold" => "sold",
            "properties?category=land&status=hidden" => "hidden",
            "properties?category=land&status=request" => "request"

        ],

        "enquiries" => [

            "enquiries" => "manage enquiries"

        ],

        // "customers" => [

        //     "customers" => "manage customers",
        //     "customers/testimonials" => "testimonials",
        //     "customers/messages" => "messages"

        // ],

        // "blog" => [

        //     "blog" => "blog",
        //     "blog/categories" => "categories",
        //     "blog/comments" => "comments",
        //     "blog/settings" => "settings"
        // ],

        // "pages" => [

        //     "pages" => "manage pages"
        // ],

        "settings" => [

            "my-account" => "my account",
            // "accounts" => "admin accounts",
            // "branches" => "branches",
            // "settings" => "settings",
            "about" => "about"
        ]
    ]
];
